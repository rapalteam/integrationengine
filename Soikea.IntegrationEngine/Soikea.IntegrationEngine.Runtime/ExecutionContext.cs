﻿using System;
using System.Diagnostics.Contracts;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Soikea.IntegrationEngine.Core.Implementation;
using Soikea.IntegrationEngine.Core.Implementation.Events;
using Soikea.IntegrationEngine.Core.Interfaces;
using Soikea.IntegrationEngine.Core.Logging;

namespace Soikea.IntegrationEngine.Runtime
{
    public class ExecutionContext
    {
        private readonly SubscriptionContext _subscriptionContext;      
        
        public ExecutionContext(SubscriptionContext subscriptionContext)
        {
            _subscriptionContext = subscriptionContext;            
        }

        public Task RunJob(IJobAbortionWatchDog jobAbortionWatchDog, Guid runId, IIntegrationJob job)
        {
            Contract.Requires(job != null);
            Contract.Requires(!string.IsNullOrEmpty(job.TenantId));
            Contract.Requires(job.ResultType == JobResultType.Void);

            return Task.Run(() =>
            {
                Log.Info($"Starting execution of background job {job.Name}, with job-id {runId}");
                
                job.JobId = runId;

                var startofJobEvent = new StartOfJobEvent(job.TenantId, runId, job.Name);
                _subscriptionContext.PublishOutputEvent(startofJobEvent);

                var inputCounter = new JobResultCounter();
                var outputCounter = new JobResultCounter();

                using (var inputCounterSubscription = _subscriptionContext.InputStream
                    .Where(x => x.JobId == runId)
                    .Subscribe(inputCounter.Receive))
                using (var outputCounterSubscription = _subscriptionContext.OutputStream
                    .Where(x => x.JobId == runId)
                    .Subscribe(outputCounter.Receive))
                {
                    job.Execute(jobAbortionWatchDog, _subscriptionContext.PublishInputEvent, _subscriptionContext.PublishOutputEvent);           
                }

                var endOfJobReport = new EndOfJobEvent(job.TenantId, runId, job.Name, JobResultStatus.Ok, inputCounter.ItemsPassedThrough, outputCounter.ItemsPassedThrough);
                _subscriptionContext.PublishOutputEvent(endOfJobReport);         
            });
        }

        public IJobResult RunJobSynchronous(IJobAbortionWatchDog jobAbortionWatchDog, Guid runId, IIntegrationJob job)
        {
            Contract.Requires(job != null);
            Contract.Requires(!string.IsNullOrEmpty(job.TenantId));
            Contract.Requires(job.ResultType == JobResultType.Synchronous);
            
            job.JobId = runId;

            var result = job.CreateJobResult(runId);
                        
            // Subscribe to in & out streams to be able to synchronize end-state to job result
            var inputCounter = new JobResultCounter();
            var outputCounter = new JobResultCounter();

            using (var inputCounterSubscription = _subscriptionContext.InputStream
                .Where(x => x.JobId == runId)
                .Subscribe(inputCounter.Receive))
            using (var outputCounterSubscription = _subscriptionContext.OutputStream
                .Where(x => x.JobId == runId)
                .Subscribe(outputCounter.Receive))
            using (var outputSubscription = _subscriptionContext.OutputStream
                .Where(x => x.JobId == runId)
                .Subscribe(@event => ((JobResultBase) result).Receive(@event)))
                //TODO: Asking for trouble with having to cast to base implementation
            {
                Log.Info($"Starting execution of synchronous job {job.Name}, with job-id {runId}");

                var startofJobEvent = new StartOfJobEvent(job.TenantId, runId, job.Name);
                _subscriptionContext.PublishOutputEvent(startofJobEvent);

                result.Status = job.Execute(jobAbortionWatchDog, _subscriptionContext.PublishInputEvent, _subscriptionContext.PublishOutputEvent);

                if (result.Status == JobResultStatus.Started)
                    // Job still running in the background, so all results must be waited on 
                {
                    //TODO: We need an await-pattern for the synchronous job, and how to deal with multiple subscribers/targets ?
                    var allOk = _subscriptionContext.VerifyEndOfJob(inputCounter, outputCounter);

                    result.Status = allOk ? JobResultStatus.Ok : JobResultStatus.Error;
                }

                var endOfJobReport = new EndOfJobEvent(job.TenantId, runId, job.Name, result.Status, inputCounter.ItemsPassedThrough,
                    outputCounter.ItemsPassedThrough);
                _subscriptionContext.PublishOutputEvent(endOfJobReport);

                Log.Info($"Finished execution of synchronous job {job.Name} with status {result.Status}");
            }

            return result;
        }


        public void RunJobAsynchronousCallback(IJobAbortionWatchDog jobAbortionWatchDog, Guid runId, IIntegrationJob job)
        {
            Contract.Requires(job != null);
            Contract.Requires(!string.IsNullOrEmpty(job.TenantId));
            Contract.Requires(job.ResultType == JobResultType.Callback);

            throw new NotImplementedException();
        }
    }
}
