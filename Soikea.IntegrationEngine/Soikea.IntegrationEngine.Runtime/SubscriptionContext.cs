﻿using System;
using System.Diagnostics.Contracts;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Runtime.Serialization;
using Soikea.IntegrationEngine.Core.Implementation;
using Soikea.IntegrationEngine.Core.Implementation.Events;
using Soikea.IntegrationEngine.Core.Interfaces;
using Soikea.IntegrationEngine.Core.Logging;

namespace Soikea.IntegrationEngine.Runtime
{
    public class SubscriptionContext : IDisposable
    {
        
        public readonly Subject<IEvent> InputStream;

        public readonly Subject<IEvent> OutputStream;
        
        public SubscriptionContext()
        {
            InputStream = new Subject<IEvent>();

            OutputStream = new Subject<IEvent>();
        }
        
#region InputStream members

        public virtual void SubscribeToInput(IMessageAdapter subscriber, SubscriptionCategory category)
        {
            Contract.Requires(subscriber != null);
            Contract.Requires(!string.IsNullOrEmpty(subscriber.TenantId));
            Contract.Requires(category != null);

            InputStream.Where(x => x.TenantId == subscriber.TenantId && Equals(x.Category, category))
                //TODO: Threading-strategy parameterized or something .SubscribeOn(NewThreadScheduler.Default)
                .Subscribe(subscriber.Receive);
            
            Log.Info($"Result subscription added: {subscriber.Name} for input-events on {category.Name} with tenant {subscriber.TenantId}");
        }

        public virtual void SubscribeToInput(IMessageAdapter subscriber, string categoryName)
        {
            Contract.Requires(subscriber != null);
            Contract.Requires(!string.IsNullOrEmpty(subscriber.TenantId));
            Contract.Requires(categoryName != null);

            var category = new SubscriptionCategory(categoryName);

            SubscribeToInput(subscriber, category);
        }

        public virtual void PublishInputEvent(string tenantId, Guid jobId, ISerializable eventPayload, SubscriptionCategory category)
        {
            Contract.Requires(jobId != Guid.Empty);
            Contract.Requires(eventPayload != null);
            Contract.Requires(category != null);

            PublishInputEvent(new AnonymousEvent(tenantId, jobId, category, eventPayload));
        }

        public virtual void PublishInputEvent(IEvent @event)
        {
            Contract.Requires(@event != null);

            InputStream.OnNext(@event);
            Log.Trace($"An input-event published from job {@event.JobId} to category {@event.Category}");
        }

#endregion


#region OutputStream members

        public virtual void SubscribeToOutput(IMessageAdapter subscriber, SubscriptionCategory category)
        {
            Contract.Requires(subscriber != null);
            Contract.Requires(!string.IsNullOrEmpty(subscriber.TenantId));
            Contract.Requires(category != null);

            OutputStream.Where(x => x.TenantId == subscriber.TenantId && Equals(x.Category, category))
                .Subscribe(subscriber.Receive);

            Log.Info($"Result subscription added: {subscriber.Name} for output-events on {category.Name} with tenant {subscriber.TenantId}");
        }

        public virtual void SubscribeToOutput(IMessageAdapter subscriber, string categoryName)
        {
            Contract.Requires(subscriber != null);
            Contract.Requires(!string.IsNullOrEmpty(subscriber.TenantId));
            Contract.Requires(categoryName != null);

            var category = new SubscriptionCategory(categoryName);

            SubscribeToOutput(subscriber, category);
        }

        public virtual void PublishOutputEvent(string tenantId, Guid jobId, ISerializable result, SubscriptionCategory category)
        {
            Contract.Requires(jobId != Guid.Empty);
            Contract.Requires(result != null);
            Contract.Requires(category != null);

            PublishOutputEvent(new AnonymousEvent(tenantId, jobId, category, result));
        }

        public virtual void PublishOutputEvent(IEvent @event)
        {
            Contract.Requires(@event != null);

            OutputStream.OnNext(@event);

            Log.Trace($"An output-event published from job {@event.JobId} to category {@event.Category}");
        }

#endregion

        public virtual bool VerifyEndOfJob(JobResultCounter inputCounter, JobResultCounter outputCounter)
        {
            Contract.Requires(inputCounter != null, "inputCounter != null");
            Contract.Requires(outputCounter != null, "outputCounter != null");
            //TODO: Parameterized timeout

            return inputCounter.ItemsPassedThrough == outputCounter.ItemsPassedThrough;
        }

        public virtual void Dispose()
        {
            InputStream.Dispose();
            OutputStream.Dispose();
            Log.Trace("SubscriptionContext streams disposed");
        }
    }
}
