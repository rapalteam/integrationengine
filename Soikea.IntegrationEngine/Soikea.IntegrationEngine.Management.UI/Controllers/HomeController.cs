﻿using System;
using System.Web.Mvc;
using Soikea.IntegrationEngine.Management.UI.Models;

namespace Soikea.IntegrationEngine.Management.UI.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Integration Engine Management";

            return View();
        }

        [Authorize]
        public ActionResult Instances()
        {
            if (Request.Url == null || !Request.Url.AbsoluteUri.EndsWith("/"))
            {
                return Redirect($"{Url.Action("Instances")}/#/");
    }
            return View();
        }

        [Authorize]
        public ActionResult Logs()
        {
            if (Request.Url == null || !Request.Url.AbsoluteUri.EndsWith("/"))
            {
                return Redirect($"{Url.Action("Logs")}/#/");
            }
            return View();
        }

        [Authorize]
        public ActionResult Hangfire()
        {
            if (!Url.IsLocalUrl(Request.RawUrl))
            {
                return View("Index");
            }
            
            return View(new HangfireDashboardInitialization { HangfireUrl = Request.Url.GetLeftPart(UriPartial.Authority) + Url.Content("~/hangfire") });
        }
        [Authorize]
        public ActionResult Events()
        {
            if (Request.Url == null || !Request.Url.AbsoluteUri.EndsWith("/"))
            {
                return Redirect($"{Url.Action("Events")}/#/");
            }
            return View();
        }

        public ActionResult Error()
        {
            return View("Error");
        }
    }

}
