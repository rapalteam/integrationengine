﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Soikea.IntegrationEngine.Management.UI.Models.Log;

namespace Soikea.IntegrationEngine.Management.UI.Controllers
{
    /// <summary>
    /// A REST -api to search for Integration engines log entries.
    /// </summary>
    [RoutePrefix("api/log")]
    public class LogController : ApiController
    {
        private readonly LogRepository _logRepository;

        private static int _pageSize = 25;
        public LogController(LogRepository logRepository)
        {
            _logRepository = logRepository;
        }

        /// <summary>
        /// Get log entries by given correlation-id.
        /// </summary>
        /// <param name="request">Searchterms to search with</param>
        /// <returns>Matching Log entries as LogApiModel</returns>
        public SearchLogsResponse Get([FromUri] SearchLogsRequest request)
        {
            var response = new SearchLogsResponse();
            switch (request.Paged)
            {
                case true:
                {
                    int pageCount;
                    response.Logs = SearchPaged(request, out pageCount)
                            .Select(LogApiModel.Create)
                            .ToList();
                    response.PageCount = pageCount;
                    break;
                }
                case false:
                {
                    response.Logs = Search(request)
                            .Select(LogApiModel.Create)
                            .ToList();
                    break;
                }
            }

            if (response.Logs.Any())
            {
                return response;
            }

            response.ErrorMessage = "Please check the search parameters";
            response.PageCount = 0;
            return response;
        }

        private IEnumerable<Log> Search(SearchLogsRequest request)
        {
            switch (request.Method)
            {
                case LogSearchMethod.CorrelationId:
                    {
                        return _logRepository.GetByJobId(request.Term);
                    }
                case LogSearchMethod.Message:
                    {

                        return _logRepository.GetByLogMessage(request.Term, request.Start.DateTime.ToUniversalTime(), request.End.DateTime.ToUniversalTime());

                    }
                case LogSearchMethod.Module:
                    {
                        return _logRepository.GetByModule(request.Term, request.Start.DateTime.ToUniversalTime(), request.End.DateTime.ToUniversalTime());
                    }
                case LogSearchMethod.Severity:
                    {
                        var severity = (Constants.LogSeverity)Enum.Parse(typeof(Constants.LogSeverity), request.Term);
                        return _logRepository.GetBySeverity(severity, request.Start.DateTime.ToUniversalTime(), request.End.DateTime.ToUniversalTime());
                    }
            }
            return new List<Log>();
        }

        private IEnumerable<Log> SearchPaged(SearchLogsRequest request, out int pageCount)
        {
            switch (request.Method)
            {
                    case LogSearchMethod.CorrelationId:
                {
                    return _logRepository.GetByJobId(request.Term, request.Page, _pageSize, out pageCount);
                }
                case LogSearchMethod.Message:
                {
                    
                    return _logRepository.GetByLogMessage(request.Term, request.Start.DateTime.ToUniversalTime(), request.End.DateTime.ToUniversalTime(), request.Page, _pageSize, out pageCount);

                }
                case LogSearchMethod.Module:
                {
                    return _logRepository.GetByModule(request.Term, request.Start.DateTime.ToUniversalTime(), request.End.DateTime.ToUniversalTime(), request.Page, _pageSize, out pageCount);
                }
                case LogSearchMethod.Severity:
                {
                    var severity = (Constants.LogSeverity) Enum.Parse(typeof (Constants.LogSeverity), request.Term);
                    return _logRepository.GetBySeverity(severity, request.Start.DateTime.ToUniversalTime(), request.End.DateTime.ToUniversalTime(), request.Page, _pageSize, out pageCount);
                }
            }
            pageCount = 0;
            return new List<Log>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Route("kpi")]
        public LogKPIResponse Get()
        {
            var result = new LogKPIResponse();
            var start = new DateTimeOffset(DateTime.UtcNow).AddDays(-14);
            var end = new DateTimeOffset(DateTime.UtcNow);
            int pageCount;
            var errors = _logRepository.GetBySeverity(Constants.LogSeverity.Error, start.DateTime, end.DateTime, 0,100, out pageCount);

            result.ErrorCount = errors.Count();

            return result;
        }

        /// <summary>
        /// Search for log-entries by matching Log.Severity.
        /// TODO: add paging 
        /// </summary>
        /// <param name="severity">Log.Severity to match</param>
        /// <returns>Matching Log entries as LogApiModel</returns>
        [Route("BySeverity/{severity}")]
        public IEnumerable<LogApiModel> GetBySeverity(Constants.LogSeverity severity)
        {
            int pageCount;
            var entities = _logRepository.GetBySeverity(severity, DateTime.UtcNow.AddDays(-1), DateTime.UtcNow, 0, _pageSize, out pageCount);

            return entities.Select(LogApiModel.Create)
                .ToList();
        }

        /// <summary>
        /// Get an extended view of a log entry by given LogId.
        /// </summary>
        /// <param name="id">Log.LogId of Logentry</param>
        /// <returns>An extended view of the log-entry</returns>
        [Route("ByLog/{id}")]
        public IEnumerable<ExtendedLogApiModel> GetByLogId(int id)
        {
            if (id < 1)
            {
                return new List<ExtendedLogApiModel>();
            }

            var entities = _logRepository.GetByLogId(id);

            return entities.Select(ExtendedLogApiModel.Create)
                .ToList();
        }

        /// <summary>
        /// Search for log-entries by matching Log category.
        /// </summary>
        /// <param name="id">Category.CategoryId of log-category</param>
        /// <param name="from">Filter by Log.TimeStamp from</param>
        /// <param name="to">Filter by Log.TimeStamp to</param>
        /// <param name="page">Page number</param>
        /// <returns>Matching Log entries as LogApiModel</returns>
        [Route("ByCategory/{id}")]
        public IEnumerable<LogApiModel> GetByCategory(int id, DateTimeOffset from, DateTimeOffset to, int page)
        {
            int pageCount;
            return _logRepository.GetByCategoryId(id, from.DateTime, to.DateTime, page, _pageSize, out pageCount)
                .Select(LogApiModel.Create)
                .ToList();
        }

        [Route("Categories")]
        public IEnumerable<CategoryApiModel> GetAllCategories()
        {
            return _logRepository.GetAllCategories()
                .Select(CategoryApiModel.Create)
                .ToList();
        }
    }
}
