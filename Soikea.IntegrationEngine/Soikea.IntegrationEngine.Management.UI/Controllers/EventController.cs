﻿using System;
using System.Collections.Generic;
using System.Linq;
using Soikea.IntegrationEngine.EventStore;
using System.Web.Http;
using Soikea.IntegrationEngine.Core.Interfaces;
using Soikea.IntegrationEngine.Management.UI.Models.Event;

namespace Soikea.IntegrationEngine.Management.UI.Controllers
{
    [RoutePrefix("api/event")]
    public class EventController : ApiController
    {
        private readonly IEventStoreRepository _repository;

        public EventController(IEventStoreRepository repository)
        {
            _repository = repository;
        }

        public IntegrationEventSearchResult Get([FromUri] IntegrationEventSearchRequest request)
        {
            var result = new IntegrationEventSearchResult();

            if (request.Start.Equals(default(DateTimeOffset))) request.Start = DateTimeOffset.UtcNow.AddDays(2);
            if (request.End.Equals(default(DateTimeOffset))) request.End = DateTimeOffset.UtcNow;

            switch (request.Method)
            {
                case EventSearchMethod.Direction:
                {
                    result.Events = _repository.GetByDirection(request.Start, request.End, (EventStream) Enum.Parse(typeof (EventSearchMethod), request.Term))
                            .Select(IntegrationEventApiModel.Create)
                            .ToList();
                    break;
                }
                case EventSearchMethod.JobId:
                {
                    result.Events =
                        _repository.GetByJobId(request.Term)
                        .Select(IntegrationEventApiModel.Create)
                        .ToList();
                    break;
                }
                case EventSearchMethod.Tenant:
                {
                    result.Events = _repository.GetByTenant(request.Start, request.End, request.Term)
                            .Select(IntegrationEventApiModel.Create)
                            .ToList();
                    break;
                }
                case EventSearchMethod.SubscriptionCategory:
                {
                    result.Events =
                        _repository.GetBySubscriptionCategory(request.Start, request.End, request.Term)
                            .Select(IntegrationEventApiModel.Create)
                            .ToList();
                    break;
                }
                default:
                {
                    result.Events = new List<IntegrationEventApiModel>();
                    result.ErrorMessage = "Not supported search method";
                    break;
                }
            }
            return result;
        }

        [Route("id")]
        public ExtendedIntegrationEventApiModel Get([FromUri] int id)
        {
            return ExtendedIntegrationEventApiModel.Create(_repository.GetByEventId(id));
        }
    }
}
