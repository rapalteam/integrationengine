﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Soikea.IntegrationEngine.Administration.Model;
using Soikea.IntegrationEngine.Administration.Repository;
using Soikea.IntegrationEngine.Core.Interfaces;
using Soikea.IntegrationEngine.Management.UI.Models.Instance;

namespace Soikea.IntegrationEngine.Management.UI.Controllers
{
    /// <summary>
    /// Executed Integration job instances.
    /// </summary>
    [RoutePrefix("api/instance")]
    public class InstanceController : ApiController
    {
        public IInstanceRepository _readRepository;


        public InstanceController(IInstanceRepository read)
        {
            _readRepository = read;
        }

        /// <summary>
        /// Find all ran instances between timestamps start and end.
        /// </summary>
        /// <param name="request">Filtering terms</param>
        /// <returns>A list of InstanceApiModel objects</returns>        
        public InstanceSearchResult Get([FromUri] GetInstancesRequest request)
        {
            var result = new InstanceSearchResult();

            var start = request.Start ?? new DateTimeOffset(DateTime.UtcNow).AddDays(-2);
            var end = request.End ?? new DateTimeOffset(DateTime.UtcNow);

            var instances = request.Status.HasValue && request.Status.Value != JobResultStatus.None
                ? _readRepository.Get(start, end, request.Status.Value) 
                : _readRepository.Get(start, end);

            result.Instances = instances
                .Select(InstanceApiModel.Create)
                .ToList();

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Route("kpi")]
        public InstanceKPIResult Get()
        {
            var result = new InstanceKPIResult();

            var start = new DateTimeOffset(DateTime.UtcNow).AddDays(-14);
            var end = new DateTimeOffset(DateTime.UtcNow);

            var instances = _readRepository.Get(start, end);

            var instanceList = instances as List<Instance> ?? instances.ToList();

            result.RanInstances = instanceList.Count();
            result.FailedInstances = instanceList.Count(w => w.Result == JobResultStatus.Error);
            result.RunningJobs = instanceList.Count(w => w.Result == JobResultStatus.Started);

            return result;
        }

        [Route("latest")]
        public InstanceSearchResult GetLatest()
        {
            var result = new InstanceSearchResult();

            var start = new DateTimeOffset(DateTime.UtcNow).AddDays(-14);
            var end =  new DateTimeOffset(DateTime.UtcNow);

            var instances =  _readRepository.Get(start, end);

            result.Instances = instances
                .Select(InstanceApiModel.Create).Take(10)
                .ToList();

            return result;
        }
    }
}
