using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Soikea.IntegrationEngine.Management.UI.Models.Log
{

    [Table("Log")]
    public partial class Log
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Log()
        {
            CategoryLogs = new HashSet<CategoryLog>();
        }

        public long LogID { get; set; }

        public int? EventID { get; set; }

        public int Priority { get; set; }

        [Required]
        [StringLength(32)]
        public string Severity { get; set; }

        [Required]
        [StringLength(256)]
        public string Title { get; set; }

        public DateTime Timestamp { get; set; }

        [Required]
        [StringLength(32)]
        public string MachineName { get; set; }

        public string Message { get; set; }

        [StringLength(128)]
        public string JobId { get; set; }

        [StringLength(256)]
        public string Module { get; set; }
        
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CategoryLog> CategoryLogs { get; set; }
    }
}
