﻿namespace Soikea.IntegrationEngine.Management.UI.Models.Log
{
    public class CategoryApiModel
    {
        public string Category { get; set; }
        public int CategoryId { get; set; }

        public static CategoryApiModel Create(Category cat)
        {
            return new CategoryApiModel
            {
                Category = cat.CategoryName,
                CategoryId = cat.CategoryID
            };
        }
    }
}