﻿using System;

namespace Soikea.IntegrationEngine.Management.UI.Models.Log
{
    public class SearchLogsRequest
    {
        public string Term { get; set; }
        public LogSearchMethod Method { get; set; }
        public DateTimeOffset Start { get; set; }
        public DateTimeOffset End { get; set; }
        public int Page { get; set; }
        public bool  Paged { get; set; }
    }
}