using System.ComponentModel.DataAnnotations.Schema;

namespace Soikea.IntegrationEngine.Management.UI.Models.Log
{    
    [Table("CategoryLog")]
    public partial class CategoryLog
    {
        public long CategoryLogID { get; set; }

        public int CategoryID { get; set; }

        public long LogID { get; set; }

        public virtual Category Category { get; set; }

        public virtual Log Log { get; set; }
    }
}
