﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Soikea.IntegrationEngine.Management.UI.Models.Log
{
    public interface ILogRepository
    {
        IEnumerable<Log> GetByJobId(string correlationId);
        IEnumerable<Log> GetBySeverity(Constants.LogSeverity severity, DateTime start, DateTime end);
        IEnumerable<Log> GetByLogId(long logId);
        IEnumerable<Log> GetByCategoryId(int categoryId, DateTime start, DateTime end);
        IEnumerable<Category> GetAllCategories();
        IEnumerable<Log> GetByModule(string module, DateTime start, DateTime end);
        IEnumerable<Log> GetByLogMessage(string message, DateTime start, DateTime end); 
    }

    public interface IPagedLogRepository
    {
        IEnumerable<Log> GetByJobId(string correlationId, int page, int pageSize, out int pageCount);
        IEnumerable<Log> GetBySeverity(Constants.LogSeverity severity, DateTime start, DateTime end, int page, int pageSize, out int pageCount);
        IEnumerable<Log> GetByLogId(long logId);
        IEnumerable<Log> GetByCategoryId(int categoryId, DateTime start, DateTime end, int page, int pageSize, out int pageCount);
        IEnumerable<Category> GetAllCategories();
        IEnumerable<Log> GetByModule(string module, DateTime start, DateTime end, int page, int pageSize, out int pageCount);
        IEnumerable<Log> GetByLogMessage(string message, DateTime start, DateTime end, int page, int pageSize, out int pageCount);
    }

    public class LogRepository : ILogRepository, IPagedLogRepository
    {
        private IQueryable<Log> FilterByTimestamp(IQueryable<Log> resultSet, DateTime start, DateTime end)
        {
            return resultSet.Where(w => w.Timestamp >= start && w.Timestamp <= end);
        }

        private IQueryable<Log> IncludeCategories(IQueryable<Log> resultSet)
        {
            return resultSet.Include("CategoryLogs.Category");
        } 

        public IEnumerable<Log> GetByJobId(string correlationId, int page, int pageSize, out int pageCount)
        {
            using (var context = new LogContext())
            {
                var query = IncludeCategories(
                        context.Logs
                        .Where(w => w.JobId.Equals(correlationId))
                    );
                pageCount = CalculatePages(query.Count(), pageSize);

                return query.OrderByDescending(x => x.Timestamp)
                    .Skip(pageSize * page).Take(pageSize)
                    .ToList();

            }
        }

        public IEnumerable<Log> GetBySeverity(Constants.LogSeverity severity, DateTime start, DateTime end, int page, int pageSize, out int pageCount)
        {
            using (var context = new LogContext())
            {
                var query = FilterByTimestamp(
                        IncludeCategories(context.Logs.Where(w => w.Severity.Equals(severity.ToString()))),
                        start, end);
                pageCount = CalculatePages(query.Count(), pageSize);

                return query.OrderByDescending(x => x.Timestamp)
                    .Skip(pageSize * page).Take(pageSize)
                    .ToList();
            }
        }


        public IEnumerable<Log> GetByJobId(string correlationId)
        {
            using (var context = new LogContext())
            {
                return IncludeCategories(
                    context.Logs
                        .Where(w => w.JobId.Equals(correlationId))
                    ).OrderByDescending(x => x.Timestamp)
                    .ToList();
            }
        }

        public IEnumerable<Log> GetBySeverity(Constants.LogSeverity severity, DateTime start, DateTime end)
        {
            using (var context = new LogContext())
            {
                return FilterByTimestamp(
                    IncludeCategories(context.Logs.Where(w => w.Severity.Equals(severity.ToString()))),
                    start, end).OrderByDescending(x => x.Timestamp)
                    .ToList();

            }
        }

        public IEnumerable<Log> GetByLogId(long logId)
        {
            using (var context = new LogContext())
            {
                return IncludeCategories(
                        context.Logs
                        .Where(w => w.LogID == logId))
                    .OrderByDescending(x => x.Timestamp)
                    .ToList();
            }
        }

        public IEnumerable<Log> GetByCategoryId(int categoryId, DateTime start, DateTime end)
        {
            using (var context = new LogContext())
            {
                return FilterByTimestamp(IncludeCategories(
                          from l in context.Logs
                          join cl in context.CategoryLogs on l.LogID equals cl.LogID
                          where cl.CategoryID == categoryId
                          select l),
                          start,
                          end)
                      .OrderByDescending(x => x.Timestamp)
                      .ToList();
            }
        }

        public IEnumerable<Log> GetByCategoryId(int categoryId, DateTime start, DateTime end, int page, int pageSize, out int pageCount)
        {
            using (var context = new LogContext())
            {
                var query = FilterByTimestamp(IncludeCategories(
                        from l in context.Logs
                        join cl in context.CategoryLogs on l.LogID equals cl.LogID
                        where cl.CategoryID == categoryId
                        select l),
                        start,
                        end);
               pageCount = CalculatePages(query.Count(), pageSize);
               return query.OrderByDescending(x => x.Timestamp)
                    .Skip(pageSize * page).Take(pageSize)
                    .ToList();
            }
        }

        public IEnumerable<Category> GetAllCategories()
        {
            using (var context = new LogContext())
            {
                return context.Categories
                    .ToList();
            }
        }

        public IEnumerable<Log> GetByModule(string module, DateTime start, DateTime end)
        {
            using (var context = new LogContext())
            {
                return FilterByTimestamp(IncludeCategories(
                            context.Logs.Where(w => w.Module.Equals(module))),
                            start,
                            end)
                        .OrderByDescending(x => x.Timestamp)
                        .ToList();
            }
         }

        public IEnumerable<Log> GetByLogMessage(string message, DateTime start, DateTime end)
        {
            using (var context = new LogContext())
            {
                return FilterByTimestamp(IncludeCategories(
                        context.Logs.Where(w => w.Message.Contains(message))),
                        start,
                        end)
                    .OrderByDescending(x => x.Timestamp)
                    .ToList();
            }
        }

        public IEnumerable<Log> GetByModule(string module, DateTime start, DateTime end, int page, int pageSize, out int pageCount)
        {
            using (var context = new LogContext())
            {
                var query = FilterByTimestamp(IncludeCategories(
                        context.Logs.Where(w => w.Module.Equals(module))),
                        start,
                        end);
                pageCount = CalculatePages(query.Count(), pageSize);
                return query.OrderByDescending(x => x.Timestamp)
                    .Skip(pageSize * page).Take(pageSize)
                    .ToList();
            }
        }

        public IEnumerable<Log> GetByLogMessage(string message, DateTime start, DateTime end, int page, int pageSize, out int pageCount)
        {
            using (var context = new LogContext())
            {
                var query = FilterByTimestamp(IncludeCategories(
                        context.Logs.Where(w => w.Message.Contains(message))),
                        start,
                        end);
                pageCount = CalculatePages(query.Count(), pageSize);

                return query.OrderByDescending(x => x.Timestamp)
                    .Skip(pageSize * page).Take(pageSize)
                    .ToList();
            }
        }

        private static int CalculatePages(int count, int pageSize)
        {
                return (count + pageSize - 1) / pageSize;
        }
    }
}