﻿using System.Collections;
using System.Collections.Generic;

namespace Soikea.IntegrationEngine.Management.UI.Models.Log
{
    public class SearchLogsResponse
    {
        public IEnumerable<LogApiModel> Logs { get; set; }
        public string ErrorMessage { get; set; }
        public int PageCount { get; set; }
    }
}