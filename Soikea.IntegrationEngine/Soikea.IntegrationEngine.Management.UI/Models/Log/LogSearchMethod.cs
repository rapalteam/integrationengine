﻿namespace Soikea.IntegrationEngine.Management.UI.Models.Log
{
    public enum LogSearchMethod
    {
        CorrelationId,
        Severity,
        Module,
        Message
    }
}