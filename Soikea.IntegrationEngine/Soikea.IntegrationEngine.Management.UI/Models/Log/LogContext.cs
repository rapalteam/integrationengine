using System.Data.Entity;
using Soikea.IntegrationEngine.Management.UI.Migrations;

namespace Soikea.IntegrationEngine.Management.UI.Models.Log
{
    public partial class LogContext : DbContext
    {
        public LogContext()
            : base("LogContext")
        {
#if TESTLOGDATA
            Database.SetInitializer(new TestLogDataInitializer<LogContext>());
#endif
        }

        public virtual IDbSet<Category> Categories { get; set; }
        public virtual IDbSet<CategoryLog> CategoryLogs { get; set; }
        public virtual IDbSet<Log> Logs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>()
                .HasMany(e => e.CategoryLogs)
                .WithRequired(e => e.Category)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Log>()
                .HasMany(e => e.CategoryLogs)
                .WithRequired(e => e.Log)
                .WillCascadeOnDelete(false);
        }
    }
}
