﻿using System;
using System.Linq;

namespace Soikea.IntegrationEngine.Management.UI.Models.Log
{
    public class LogApiModel
    {
        public long LogId { get; set; }
        public Constants.LogSeverity Severity { get; set; }
        public string Message { get; set; }
        public string CorrelationId { get; set; }
        public string Module { get; set; }
        public DateTimeOffset Timestamp { get; set; }
        public string Category { get; set; }

        public static LogApiModel Create(Log log)
        {
            var category = (log.CategoryLogs?.Count > 1)
                ? log.CategoryLogs.Last().Category.CategoryName + " : " + log.CategoryLogs.First().Category.CategoryName
                : log.CategoryLogs?.FirstOrDefault()?.Category?.CategoryName;

            return new LogApiModel
            {
                CorrelationId = log.JobId,
                LogId = log.LogID,
                Message = log.Message,
                Severity = (Constants.LogSeverity) Enum.Parse(typeof (Constants.LogSeverity), log.Severity),
                Module = log.Module,
                Timestamp = new DateTimeOffset(log.Timestamp, TimeSpan.Zero).ToLocalTime(),
                Category = category
            };
        }        
    }

    public class ExtendedLogApiModel : LogApiModel
    {
        public string Payload { get; set; }

        public static ExtendedLogApiModel Create(Log log)
        {
            return new ExtendedLogApiModel()
            {
                CorrelationId = log.JobId,
                LogId = log.LogID,
                Message = log.Message,
                Severity = (Constants.LogSeverity) Enum.Parse(typeof(Constants.LogSeverity), log.Severity),
                Module = log.Module,
                Payload = string.Empty,
                Timestamp = new DateTimeOffset(log.Timestamp, TimeSpan.Zero).ToLocalTime(),
                Category = log.CategoryLogs?.FirstOrDefault()?.Category?.CategoryName
            };
        }
    }
}