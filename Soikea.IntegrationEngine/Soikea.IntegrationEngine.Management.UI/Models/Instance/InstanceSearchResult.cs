﻿using System.Collections.Generic;

namespace Soikea.IntegrationEngine.Management.UI.Models.Instance
{
    public class InstanceSearchResult
    {
        public List<InstanceApiModel> Instances { get; set; }
        public string ErrorMessage { get; set; } 
    }
}