﻿using System;
using Soikea.IntegrationEngine.Core.Interfaces;

namespace Soikea.IntegrationEngine.Management.UI.Models.Instance
{
    public class InstanceApiModel
    {
        public TimeSpan Duration { get; set; }
        public DateTimeOffset StartTime { get; set; }
        public DateTimeOffset? EndTime { get; set; }
        public string InstanceName { get; set; }
        public JobResultStatus Result { get; set; }
        public string Payload { get; set; }
        public string CorrelationId { get; set; }
        public string TenantId { get; set; }

        public static InstanceApiModel Create(Administration.Model.Instance instance)
        {
            return new InstanceApiModel
            {
                TenantId = instance.TenantId,
                CorrelationId = instance.CorrelationId,
                StartTime = instance.StartTime, 
                EndTime = instance.EndTime,
                Result = instance.Result,
                Duration = instance.Duration,
                Payload = instance.Payload,
                InstanceName = instance.JobName
            };
        }
    }
}