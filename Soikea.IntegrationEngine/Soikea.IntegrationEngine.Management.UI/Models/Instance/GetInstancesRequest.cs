﻿using System;
using Soikea.IntegrationEngine.Core.Interfaces;

namespace Soikea.IntegrationEngine.Management.UI.Models.Instance
{
    /// <summary>
    /// Filter-terms for GetInstaces -operation.
    /// </summary>
    public class GetInstancesRequest
    {        
        /// <summary>
        /// Start-time of the time-window to search for, if not given - a default is used.
        /// </summary>
        public DateTimeOffset? Start { get; set; }

        /// <summary>
        /// End-time of the time-window to search for, if not given - a default is used.
        /// </summary>
        public DateTimeOffset? End { get; set; }

        /// <summary>
        /// Filter using the outcome-status of a job instance.
        /// </summary>
        public JobResultStatus? Status { get; set; }
    }
}