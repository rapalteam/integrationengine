﻿namespace Soikea.IntegrationEngine.Management.UI.Models.Instance
{
    public class InstanceKPIResult
    {
        public int RanInstances { get; set; }
        public int Errors { get; set; }
        public int FailedInstances { get; set; }
        public int RunningJobs { get; set; }
    }
}