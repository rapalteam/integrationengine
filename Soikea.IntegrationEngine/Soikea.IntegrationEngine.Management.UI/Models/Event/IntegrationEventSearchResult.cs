using System.Collections.Generic;

namespace Soikea.IntegrationEngine.Management.UI.Models.Event
{
    public class IntegrationEventSearchResult
    {
        public List<IntegrationEventApiModel>  Events { get; set; }
        public string ErrorMessage { get; set; }
    }
}