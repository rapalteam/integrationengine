using Soikea.IntegrationEngine.EventStore.Model;

namespace Soikea.IntegrationEngine.Management.UI.Models.Event
{
    public class ExtendedIntegrationEventApiModel : IntegrationEventApiModel
    {
        public string Payload { get; set; }

        public static ExtendedIntegrationEventApiModel Create(IntegrationEvent @event)
        {
            return new ExtendedIntegrationEventApiModel()
            {
                JobId = @event.JobId.ToString(),
                PublishedAt = @event.PublishedAt,
                TenantId = @event.Tenant.Identifier,
                Id = @event.Id,
                Direction = @event.Direction,
                TypeName = @event.TypeName,
                SubscriptionCategory = @event.SubscriptionCategory.CategoryKey,
                Payload = @event.Payload
            };
        }
    }
}