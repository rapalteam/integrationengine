﻿using System;
using Soikea.IntegrationEngine.Core.Interfaces;
using Soikea.IntegrationEngine.EventStore.Model;

namespace Soikea.IntegrationEngine.Management.UI.Models.Event
{
    public class IntegrationEventApiModel
    {
        public long Id { get; set; }
        public string TenantId { get; set; }
        public string JobId { get; set; }
        public EventStream Direction { get; set; }
        public DateTimeOffset PublishedAt { get; set; }
        public string TypeName { get; set; }
        public string SubscriptionCategory { get; set; }

        public static IntegrationEventApiModel Create(IntegrationEvent @event)
        {
            
            return new IntegrationEventApiModel
            {
                JobId = @event.JobId.ToString(),
                PublishedAt = @event.PublishedAt,
                TenantId = @event.Tenant.Identifier,
                Id = @event.Id,
                Direction = @event.Direction,
                TypeName = @event.TypeName,
                SubscriptionCategory = @event.SubscriptionCategory.CategoryKey,
            };
        }
    }
}