﻿namespace Soikea.IntegrationEngine.Management.UI.Models.Event
{
    public enum EventSearchMethod
    {
        JobId,
        Tenant,
        Direction,
        SubscriptionCategory
    }
}