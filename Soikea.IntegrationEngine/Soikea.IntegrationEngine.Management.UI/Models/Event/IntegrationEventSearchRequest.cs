﻿using System;

namespace Soikea.IntegrationEngine.Management.UI.Models.Event
{
    public class IntegrationEventSearchRequest
    {
        public DateTimeOffset Start { get; set; }
        public DateTimeOffset End { get; set; }
        public EventSearchMethod Method { get; set; }
        public string Term { get; set; }
    }
}