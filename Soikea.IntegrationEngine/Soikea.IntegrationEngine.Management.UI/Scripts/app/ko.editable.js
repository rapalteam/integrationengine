﻿ko.observable.fn.editable = function () {
    /// Extends observable with store-values i.e. enables update-operation on model.
    var self = this;
    var oldValue = ko.observable(self());

    // Subscribes to changes using computed
    var observable = ko.computed({
        read: function () {
            return self();
        },
        write: function (value) {
            oldValue(self());
            self(value);
        }
    });

    self.revert = function () {
        self(oldValue());
    };

    self.commit = function () {
        oldValue(self());
    };

    self.dirty = ko.computed(function () {
        return self() !== oldValue();
    });

    self.hadInitialValue = ko.computed(function () {
        return typeof oldValue() !== "undefined";
    });

    return self;
};

ko.editableObservable = function (observable, isInitiallyDirty) {
    // Extends observable-object with functionalities to control editable-observables

    var self = observable;

    var initiallyDirty = isInitiallyDirty || false;

    self.isDirty = ko.observable(initiallyDirty);

    self.checkDirty = function () {
        var dirtExists = false;
        for (var prop in self) {
            if (self.hasOwnProperty(prop) && self[prop] && self[prop].dirty) {
                if (self[prop].dirty()) {
                    dirtExists = true;
                    break;
                }
            }
        }

        self.isDirty(dirtExists);

        return dirtExists;
    };

    self.commitChanges = function () {
        for (var prop in self) {
            if (self.hasOwnProperty(prop) && self[prop] && self[prop].commit) {
                self[prop].commit();
            }
        }

        self.isDirty(false);
    };

    self.revertChanges = function () {
        for (var prop in self) {
            if (self.hasOwnProperty(prop) && self[prop]) {
                if (self[prop].revert)
                    self[prop].revert();
                else if (self[prop].revertChanges)
                    self[prop].revertChanges();
            }
        }
        self.isDirty(false);
    };

    return self;
};