﻿function SearchInstancesViewModel(app, configuration) {
    var self = this;
    self.configuration = configuration || {};
    self.loading = ko.observable(false);

    self.endTime = ko.observable(new Date());
    self.startTime = ko.observable(moment().add(-1, "days").toDate());

    self.selectedJobResult = ko.observable(undefined);
    self.showInspectDialog = ko.observable(false);
    self.itemInInspection = ko.observable(undefined);
    self.goToLogEvents = function(listItemId) {
// ReSharper disable once UseOfImplicitGlobalInFunctionScope
        window.location.href = routing.combineUrl("Home/Logs/#/byCorrelationId/" + listItemId);
    };

    self.inspectInstance = function(listItemId) {
        Sammy().setLocation("#/inspect/" + listItemId);
        Sammy().trigger("location-changed");
    }

    self.searchResults = new ListViewModel("Instances", [
           { key: "correlationId", label: "Correlation Id" },
           { key: "tenantId", label: "Tenant Id" },
           { key: "instanceName", label: "Instance name" },
           { key: "startTime", label: "Start time", binding: "datetime" },
           { key: "duration", label: "Duration", binding: "time" },
           { key: "result", label: "Jobresult status", binding: "lookupLabel", bindingOptions: app.constants.jobResultStatuses }
       ],
       [{ action: self.inspectInstance, label: "Inspect" },
        { action: self.goToLogEvents, label: "Logs" }],
       false,
       20);

    self.search = function () {
        self.searchResults.loading(true);

        var params = {
            start: moment(self.startTime()).format(),
            end: moment(self.endTime()).format(),
            status: self.selectedJobResult().key
        };

        app.dataModel.searchInstances(params, function (data) {
            if (data.errorMessage) {
                app.warning(data.errorMessage);
            }

            if (!isEmpty(data.instances)) {
                self.searchResults.populate(data.instances, function(item) {
                    item.id = item.correlationId;
                    return item;
                });
            }

            self.searchResults.loading(false);
        }, function () {
            self.searchResults.loading(false);
        });
    };

    Sammy(function () {

        this.get("#/search", function () {
            app.navigateToSearchInstancesViewModel();
        });

        this.get("#/inspect/:id", function() {
            self.showInspectDialog(false);
            var item = self.searchResults.find(this.params["id"]);

            if (isEmpty(item)) {
                app.warning("Error");
                return;
            }

            var callback = function () {
                self.showInspectDialog(false);
                self.itemInInspection(undefined);
                Sammy().setLocation("#/search");
                Sammy().trigger("location-changed");
            }

            self.itemInInspection(new InspectInstanceViewModel(app, self.configuration, item, callback));
            self.showInspectDialog(true);
        });

        this.get("#/", function () { this.app.runRoute("get", "#/search") });
        this.around(function(callback) {
            if (this.path.indexOf("#") > -1) {
                callback();
            }
        });
    });
}

app.addViewModel({
    name: "SearchInstancesViewModel",
    bindingMemberName: "searchInstances",
    factory: SearchInstancesViewModel
});