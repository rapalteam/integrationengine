﻿function InspectInstanceViewModel(app, configuration, item, callback) {
    var self = this;

    
    self.startTime = ko.observable(moment(item.startTime));
    self.endTime = ko.observable(moment(item.endTime));
    self.duration = ko.observable(item.duration);
    self.instanceName = ko.observable(item.instanceName);

    self.result = ko.computed(function() {
        return app.constants.jobResultStatuses[item.result].label;
    });

    self.payload = JSON.parse(item.payload);
    self.headers = ko.observableArray();
    if (!isEmpty(self.payload)) {
        self.headers(Object.keys(self.payload));
    };

    self.correlationId = ko.observable(item.correlationId);
    self.tenantId = ko.observable(item.tenantId);

    self.close = function() {
        callback();
    }

    return self;
}

