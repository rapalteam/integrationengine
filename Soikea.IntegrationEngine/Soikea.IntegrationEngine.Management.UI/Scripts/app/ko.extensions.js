﻿// Add some extensions for ko objects to help development
ko.observableArray.fn.pushAll = function (valuesToPush) {
    var underlyingArray = this();
    this.valueWillMutate();
    ko.utils.arrayPushAll(underlyingArray, valuesToPush);
    this.valueHasMutated();
    return this;
};

ko.observableArray.fn.any = function () {
    return ko.pureComputed(function () {
        var allItems = this();
        return allItems.length > 0;
    }, this);
};

ko.observableArray.fn.distinct = function (prop) {
    ///<summary>
    /// Group observable array by prop. 
    /// Usage: observable.index.prop() => Would return an array of objects indexed by property, the index key would be the propertys value.
    ///</summary>
    var target = this;
    target.index = {};
    target.index[prop] = ko.observableArray([]);

    ko.computed(function () {
        //rebuild index
        var propIndex = [];

        ko.utils.arrayForEach(target(), function (item) {
            var key = ko.utils.unwrapObservable(item[prop]);
            if (key) {
                propIndex[key] = propIndex[key] || [];
                propIndex[key].push(item);
            }
        });

        target.index[prop].pushAll(propIndex);
    });

    return target;
};

ko.utils.initializeSelectedLookup = function (domain, target, key) {
    if (typeof key === "undefined" || key == null) return;

    domain = ko.utils.unwrapObservable(domain);
    var foundItem = ko.utils.arrayFirst(domain, function (item) {
        return item.key === key || item.label === key;
    });

    if (foundItem !== null) {
        target(foundItem);
    } else {
        target(undefined);
    }
};

ko.utils.initializeSelectedLookupFuzzy = function (domain, target, key) {
    if (typeof key === "undefined" || key == null) return;

    var foundItem = ko.utils.arrayFirst(domain, function (item) {
        return item.key.toLowerCase().search(key.toLowerCase()) !== -1;
    });

    if (foundItem !== null) {
        target(foundItem);
    } else {
        target(undefined);
    }
};

ko.utils.arrayFirstIndexOf = function (array, predicate) {
    for (var i = 0, j = array.length; i < j; i++) {
        if (predicate.call(undefined, array[i])) {
            return i;
        }
    }
    return -1;
};

ko.utils.getSelectedKeyOrEmpty = function (target) {
    if (typeof target() !== "undefined" && target() != null)
        return target().key;
    return "";
};

ko.utils.getSelectedKeys = function (target) {
    return ko.utils.arrayMap(ko.utils.arrayFilter(target(), function (fltr) { return typeof fltr !== "undefined"; }), function (item) { return item.key; });
};

ko.utils.isObservableArray = function (value) {
    return ko.isObservable(value) && "push" in value;
};

ko.utils.isArray = function (o) {
    return (typeof o !== "undefined" && o != null && o.isArray) || Object.prototype.toString.call(o) === "[object Array]";
};

ko.utils.stringEndsWith = function (string, endsWith) {
    string = string || "";
    if (endsWith.length > string.length)
        return false;
    return string.substring(string.length - endsWith.length) === endsWith;
};

ko.utils.stringTrimEnd = function (string, endsWith) {
    string = string || "";
    if (!ko.utils.stringEndsWith(string, endsWith))
        return string;

    return string.substring(0, string.length - endsWith.length);
};

ko.utils.isInitialized = function (item) {
    return typeof item !== "undefined" && item != null;
};

ko.utils.stringJoin = function (separator, array) {
    var ret = "";
    ko.utils.arrayForEach(array, function (item) {
        ret = ret + item + separator;
    });

    ret = ko.utils.stringTrimEnd(ret, separator);

    return ret;
};

// Punches filters and stuff
ko.punches.enableAll();
