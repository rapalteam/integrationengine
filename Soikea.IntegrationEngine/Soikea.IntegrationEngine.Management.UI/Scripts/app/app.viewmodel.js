﻿function AppViewModel(dataModel) {
    // Private state
    var self = this;

    // Private operations
    function cleanUpLocation() {
        window.location.hash = "";

        if (typeof (history.pushState) !== "undefined") {
            history.pushState("", document.title, location.pathname);
        }
    }

    self.constants = new Constants();

    // Data
    self.Views = {
        Loading: {} // Other views are added dynamically by app.addViewModel(...).
    };
    self.dataModel = dataModel;

    self.landedParameters = {};

    self.defaults = new DefaultSelectionsViewModel(self);

    // UI state
    self.activeView = ko.observable(undefined);
    self.view = ko.observable(self.Views.Loading);

    self.loading = ko.computed(function () {
        var currentView = self.Views[self.activeView()];
        return currentView && currentView.loading();
    });

    // UI operations

    self.info = function (msg) {
        toastr.info(msg);
    };

    self.warning = function (msg) {
        toastr.warning(msg);
    };

    self.error = function (msg) {
        toastr.error(msg);
    };

    // Other navigateToX functions are added dynamically by app.addViewModel(...).

    // Other operations
    self.addViewModel = function (options) {
        var viewItem = new options.factory(self, {}),
            navigator;

        // Add view to AppViewModel.Views enum (for example, app.Views.Home).
        self.Views[options.name] = viewItem;

        // Add binding member to AppViewModel (for example, app.home);
        self[options.bindingMemberName] = ko.computed(function () {

            // Commented the following out since authentication was not desired

            return self.activeView() === options.bindingMemberName
                ? self.Views[options.name]
                : undefined;
        });

        if (typeof (options.navigatorFactory) !== "undefined") {
            navigator = options.navigatorFactory(self);
        } else {
            navigator = function () {
                self.activeView(options.bindingMemberName);
            };
        }

        // Add navigation member to AppViewModel (for example, app.NavigateToHome());
        self["navigateTo" + options.name] = navigator;
    };

    self.log = function (event) {
        Sammy().log(event);
    };

    self.initialize = function () {
        Sammy().run();

    }

    self.redirectTo = function (url) {
        window.location = url;
    };
}

var app = new AppViewModel(new AppDataModel());
