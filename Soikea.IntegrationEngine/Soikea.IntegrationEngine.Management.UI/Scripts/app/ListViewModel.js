﻿///<field name="listItemSearchType">enumeration of search-types for ListViewModel</field>
var listItemSearchType = {
    none: 0,
    singleTerm: 1,
    perColumn: 2
};

function SearchColumnsViewModel(cols) {
    var self = this;

    self.cols = [];

    self.nonEmptyFilters = ko.pureComputed(function () {
        return ko.utils.arrayFilter(self.cols, function (col) {
            return col.term().length > 0;
        });
    }).extend({ notify: 'always' });


    // Initialize empty values for search
    ko.utils.arrayForEach(cols, function (col) {
        var item = { key: col.key, term: ko.observable(""), col: col };
        item.term.subscribe(function () { //Force subscriptions to re-evaluate search-terms
            self.nonEmptyFilters.notifySubscribers();
        });
        self.cols.push(item);
    });



    return self;
}

function ListViewModel(title, cols, actions, multipleSelectionEnabled, pageSize, searchType, initialSortByPredicate) {
    /// <summary>Viewmodel with basic list-operations and properties</summary> 
    /// <param name="title">Title for listing</param>
    /// <param name="cols">an array of key-label -objects, key matches binding-name for items and label acts as a name for column</param>
    /// <param name="actions">an array of action-label -objects where action are pointers to functions and label is used as a title for a button in the ui</param>
    /// <param name="multipleSelectionEnabled">whether selection of multiple items in the listing is enabled</param>
    /// <param name="pageSize">page size for listing</param>
    /// <param name="searchType">enumeration value of listItemSearchType to use with this listing</param>
    var self = this;
    self.loading = ko.observable(false);

    self.searchType = searchType || listItemSearchType.none;

    self.title = ko.observable(title);

    self.items = ko.observableArray();

    self.push = function (item) {
        self.items.push(item);
    };

    self.any = ko.computed(function () {
        return self.items().length > 0;
    });

    self.find = function (predicate) {
        if (typeof predicate === "function") {
            return ko.utils.arrayFirst(self.items(), predicate);
        } else {
            return ko.utils.arrayFirst(self.items(), function (item) {
                return item.id === predicate;
            });
        }
    };

    self.remove = function (predicate) {
        var item;
        if (typeof predicate === "function") {
            item = ko.utils.arrayFirst(self.items(), predicate);
        } else {
            item = ko.utils.arrayFirst(self.items(), function (item) {
                return item.id === predicate;
            });
        }

        if (item) {
            self.items.remove(item);
            return true;
        }
        return false;
    };

    self.removeAll = function () {
        self.items.removeAll();
    };

    self.cols = ko.observableArray(cols);
    self.sortBy = typeof initialSortByPredicate === "function" ? initialSortByPredicate : undefined;

    /// an array of action-label -objects, where action is a function and a label is a title for a button
    self.actions = ko.observableArray(actions);

    self.searchText = ko.observable(undefined);
    self.searchPredicate = function (item) {
        if (typeof item.key !== "undefined")
            return item.key.indexOf(self.searchText()) > -1;

        return true; // As default all are listed
    };

    self.extractColumnValue = function (col, item) {
        var extractedValue;
        if (col.key.indexOf(",") > -1) {
            var commaTokens = col.key.split(",");
            extractedValue = "";
            for (var j = 0; j < commaTokens.length; j++) {
                extractedValue = extractedValue + item[commaTokens[j]] + ", ";
            }
            extractedValue = ko.utils.stringTrimEnd(extractedValue, ", ", false);
        } else if (col.key.indexOf(".") > -1) {
            var tokens = col.key.split(".");
            extractedValue = item;
            for (var i = 0; i < tokens.length; i++) {
                extractedValue = extractedValue[tokens[i]];
            }
        }
        else {
            extractedValue = item[col.key];
        }
        
        if (typeof extractedValue === "function") {
            extractedValue = item[col.key]();
        }


        if (typeof col.binding !== "undefined" && typeof ko.filters[col.binding] === "function") {
            if (typeof col.bindingOptions !== "undefined") {

                extractedValue = ko.filters[col.binding](
                    extractedValue,
                    ko.utils.isObservableArray(col.bindingOptions)
                        ? col.bindingOptions()
                        : col.bindingOptions
                );
            } else {
                extractedValue = ko.filters[col.binding](extractedValue);
            }
        }

        return extractedValue;
    };

    self.searchFromCols = new SearchColumnsViewModel(cols); //TODO: Optimize subsciptions for perf
    self.searchPredicateCols = function (item, nonEmptyFilters) {
        if (nonEmptyFilters.length < 1) { // No filters specified
            return true; // As default all are listed
        }

        var matchesAllPredicates = true;

        ko.utils.arrayForEach(nonEmptyFilters, function (filter) {
            var valueToCompare = self.extractColumnValue(filter.col, item);

            if (!valueToCompare) {
                matchesAllPredicates = false;
            } else if (valueToCompare instanceof Array) {
                var matchFound = ko.utils.arrayFirst(valueToCompare, function (item) {
                    return ("" + item).toLowerCase().indexOf(filter.term().toLowerCase()) > -1;
                });
                if (!matchFound) {
                    matchesAllPredicates = false;
                }
            }
            else if (("" + valueToCompare).toLowerCase().indexOf(filter.term().toLowerCase()) < 0) {
                matchesAllPredicates = false;
            }
        });

        return matchesAllPredicates;
    };

    self.sort = function (arr) {
        if (self.sortBy) {
            return arr.sort(self.sortBy);
        }

        return arr;
    }

    self.filteredItemsRefresher = ko.observable();
    self.orderedByColumn = ko.observable(undefined);
    self.orderedByAscending = ko.observable(true);

    self.sortByColumn = function (col) {
        if (self.orderedByColumn() === col) {
            self.orderedByAscending(!self.orderedByAscending());
        } else {
            self.orderedByColumn(col);
            self.orderedByAscending(true);
        }

        self.sortBy = function (left, right) {
            var a = self.orderedByAscending() ? left : right;
            var b = self.orderedByAscending() ? right : left;

            var aValue = self.extractColumnValue(col, a);
            var bValue = self.extractColumnValue(col, b);

            if (!aValue && !bValue) {
                return 0;
            }

            if (!aValue && bValue) {
                return 1;
            }

            if (!bValue && aValue) {
                return -1;
            }

            if (typeof aValue === "string") {
                return aValue.localeCompare(bValue);
            }

            return aValue > bValue;
        };

        self.filteredItemsRefresher.notifySubscribers();
    };

    self.filteredItems = ko.computed(function () {
        self.filteredItemsRefresher(); // Refreshes when sortby changed
        var nonEmptyFilters = self.searchFromCols.nonEmptyFilters();
        if (nonEmptyFilters) {
            var items = ko.utils.arrayFilter(self.items(), function (item) {
                return self.searchPredicateCols(item, nonEmptyFilters);
            });

            return self.sort(items);
        }

        if (!self.searchText() || self.searchText().length < 1) {
            return self.sort(self.items());
        }

        return self.sort(ko.utils.arrayFilter(self.items(), function (item) {
            return self.searchPredicate(item);
        }));
    });

    self.length = ko.computed(function () {
        return self.filteredItems().length;
    });

    self.any = ko.computed(function () {
        return self.length() > 0;
    });


    self.selectedItems = ko.observableArray();

    self.notSelectedItems = ko.computed(function () {
        var items = self.items(); // Subscribes to changes
        var selectedItems = self.selectedItems(); // Subscribes to changes

        var results = [];

        ko.utils.arrayForEach(items, function (item) {
            if (selectedItems.indexOf(item) < 0) { // If not found in selecteditems, obviously is not selected
                results.push(item);
            }
        });

        return results;
    });

    self.selectItem = function (item) {
        if (multipleSelectionEnabled) {
            if (self.selectedItems.indexOf(item) === -1)
                self.selectedItems.push(item);
            return;
        }

        // If previous selection has checkDirty-function, update dirtyness
        if (self.selectedItem() && self.selectedItem()['checkDirty']) {
            self.selectedItem().checkDirty();
        }

        self.selectedItem(item);

        if (typeof item.errors !== "undefined"
            && typeof item.errors.showAllMessages === "function") {
            item.errors.showAllMessages(true);
        }
    };

    self.toggleSelection = function (item) {
        if (multipleSelectionEnabled) {
            if (self.selectedItems.indexOf(item) === -1)
                self.selectedItems.push(item);
            else {
                self.selectedItems.remove(item);
            }
            return;
        }

    };

    self.selectedItem = ko.observable(undefined);

    // Paging

    self.currentPageIndex = ko.observable(0);
    self.pageSize = pageSize || 5;

    self.pagedItems = ko.computed(function () {
        var startIndex = self.pageSize * self.currentPageIndex();

        return self.filteredItems().slice(startIndex, startIndex + self.pageSize);
    }, self);

    self.pageCount = ko.computed(function () {
        return Math.ceil(self.length() / self.pageSize);
    }, self);


    // Current paged items in page-object
    self.page = ko.computed(function () {
        var items = self.pagedItems();

        if (!items || items.length < 1) { // If none are present, return undefined to make with-binding possible
            return undefined;
        }

        var ret = {};
        for (var i = 0; i < items.length; i++) {
            ret[i + 1] = items[i];
            // Attach $parent for coherent binding syntax with foreach
            ret[i + 1].$parent = self;
        }

        return ret;
    });


    self.notSelectedCurrentPageIndex = ko.observable(0);

    self.notSelectedPagedItems = ko.computed(function () {
        var startIndex = self.pageSize * self.notSelectedCurrentPageIndex();
        return self.notSelectedItems().slice(startIndex, startIndex + self.pageSize);
    }, self);

    self.notSelectedPageCount = ko.computed(function () {
        return Math.ceil(ko.utils.unwrapObservable(self.notSelectedItems).length / self.pageSize);
    }, self);



    self.populate = function (newItems, ctor) {
        self.items.removeAll();
        self.currentPageIndex(0);

        if (typeof ctor !== "undefined") {
            self.items.pushAll(
                        ko.utils.arrayMap(newItems, ctor)
                    );
        } else {
            self.items.pushAll(newItems);
        }

    };

    return self;
};