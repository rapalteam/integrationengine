﻿function DefaultSelectionsViewModel(app) {
    var self = this;

    self.setDefault = function (propKey, selection) {
        if (typeof selection === "undefined" || selection == null) {
            localStorage.removeItem(propKey);
            app.info("Default removed");
        } else {
            var key = selection.key;

            if (ko.utils.isArray(selection)) {
                key = "";
                ko.utils.arrayForEach(selection, function (item) {
                    key = key + item.key + ",";
                });

                key = key.replace(/(^,)|(,$)/g, "");
            }

            localStorage.setItem(propKey, key);

            app.info("Default set.");
        }

    };

    self.hasDefault = function (propKey) {
        return typeof localStorage.getItem(propKey) !== "undefined";
    };

    self.getDefault = function (propKey) {
        var value = localStorage.getItem(propKey);

        if (value == null)
            return value;

        if (value.search(",") !== -1) {
            value = value.split(",");
        }

        return value;
    };

}