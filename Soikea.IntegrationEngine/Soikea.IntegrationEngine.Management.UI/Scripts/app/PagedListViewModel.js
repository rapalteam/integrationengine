﻿function PagedListViewModel(title, cols, searchFunction) {
    var self = this;

    self.loading = ko.observable(false);
    self.title = ko.observable(title);
    self.items = ko.observableArray();

    self.currentPageIndex = ko.observable(0);
    self.lastPageSearched = 0;
    self.pageCount = ko.observable(0);

    self.searchFunc = searchFunction;

    self.cols = ko.observableArray(cols);
    
    self.any = ko.computed(function() {
        return self.items().length > 0;
    });

    self.length = ko.computed(function () {
        return self.items().length;
    });

    self.extractColumnValue = function (col, item) {
        var extractedValue;
        if (col.key.indexOf(",")) {
            var commaTokens = col.key.split(",");
            extractedValue = "";
            for (var j = 0; j < commaTokens.length; j++) {
                extractedValue = extractedValue + item[commaTokens[j]] + ", ";
            }
            extractedValue = ko.utils.stringTrimEnd(extractedValue, ", ", false);
        } else if (col.key.indexOf(".") > -1) {
            var tokens = col.key.split(".");
            extractedValue = item;
            for (var i = 0; i < tokens.length; i++) {
                extractedValue = extractedValue[tokens[i]];
            }
        }
        else {
            extractedValue = item[col.key];
        }

        if (typeof extractedValue === "function") {
            extractedValue = item[col.key]();
        }

        return extractedValue;
    };

    self.populate = function(newItems, ctor) {
        self.items.removeAll();
     
        if (typeof ctor !== "undefined") {
            self.items.pushAll(ko.utils.arrayMap(newItems.logs, ctor));
        } else {
            self.items.pushAll(newItems.logs);
        }
        self.pageCount(newItems.pageCount);
    };

    self.reset = function() {
        self.items.removeAll();
        self.pageCount(0);
        self.currentPageIndex(0);
        self.lastPageSearched = 0;
    };

    self.performSearch = function () {
        var searchPage = self.currentPageIndex();
        self.loading(true);
        async.series([
            function(callback) {
                self.searchFunc(searchPage, callback, self.populate);
            },
            function(callback) {
                self.loading(false);
                callback();
            }
        ]);
    };

    self.updatePage = ko.computed(function () {
        var searchPage = self.currentPageIndex();

        if (searchPage !== self.lastPageSearched) {
            self.performSearch();
            self.lastPageSearched = searchPage;
        }
    });

    return self;
}