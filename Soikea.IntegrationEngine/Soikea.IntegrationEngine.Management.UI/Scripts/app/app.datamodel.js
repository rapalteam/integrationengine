﻿function AppDataModel() {
    var self = this;
    // Routes    
    self.siteUrl = "/";
    self.instancesUrl = "api/instance";
    self.logUrl = "api/log";
    self.eventUrl = "api/event";

    // Data
    self.returnUrl = self.siteUrl;

    // Data access operations
    self.setAccessToken = function (accessToken) {
        sessionStorage.setItem("accessToken", accessToken);
    };

    self.getAccessToken = function () {
        return sessionStorage.getItem("accessToken");
    };

    // Other private operations
    function getSecurityHeaders() {
        var headers = {};

        // CSRF Token
        var csrfToken = $("input[name='__RequestVerificationToken']").val();

        if (csrfToken) {
            headers = {
                "X-XSRF-Token": csrfToken
            };
        }

        headers.Authorization = "Bearer " + self.getAccessToken();

        return headers;
    }

    // Base -operations

    self.toErrorsArray = function (data) {
        var errors = new Array(),
            items;

        if (!data || !data.message) {
            return null;
        }

        if (data.modelState) {
            for (var key in data.modelState) {
                if (data.modelState.hasOwnProperty(key)) {
                    items = data.modelState[key];

                    if (items.length) {
                        for (var i = 0; i < items.length; i++) {
                            errors.push(items[i]);
                        }
                    }
                }
            }
        }

        if (errors.length === 0) {
            errors.push(data.message);
        }

        return errors;
    };

    self.handleError = function (jqXhr, callback) {
        var resData;

        try {
            resData = $.parseJSON(jqXhr.responseText);
        }
        catch (e) { // If it isn't an object, then it's just plain error message
            app.error(jqXhr.responseText);
            callback();

            return;
        }

        var errors = self.toErrorsArray(resData);
        if (errors)
            callback(errors);
        else
            callback(resData);
    }

    // Data fetching
    var GET = "GET", POST = "POST", PUT = "PUT", DELETE = "DELETE";

    self.get = function (url, data, success, error) {
        if (routing)
            url = routing.combineUrl(url);

        return $.ajax(url,
            {
                method: GET,
                headers: getSecurityHeaders(),
                data: data
            })
        .done(function (resData) {
            if (!success)
                return;
            success(resData);
        })
        .fail(function (jqXhr) {
            self.handleError(jqXhr, error);
        });
    }

    self.post = function (url, data, success, error) {
        if (routing)
            url = routing.combineUrl(url);

        return $.ajax(url,
            {
                method: POST,
                headers: getSecurityHeaders(),
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(data)
            })
        .done(function (resData) {
            if (!success)
                return;
            success(resData);
        })
        .fail(function (jqXhr) {
            self.handleError(jqXhr, error);
        });
    }

    self.postMultipart = function (url, data, success, error) {
        if (routing)
            url = routing.combineUrl(url);

        return $.ajax(url,
            {
                method: POST,
                headers: getSecurityHeaders(),
                contentType: false,
                "data": data,
                processData: false

            })
        .done(function (resData) {
            if (!success)
                return;
            success(resData);
        })
        .fail(function (jqXhr) {
            self.handleError(jqXhr, error);
        });
    }

    self.put = function (url, data, success, error) {
        if (routing)
            url = routing.combineUrl(url);

        return $.ajax(url,
            {
                method: PUT,
                headers: getSecurityHeaders(),
                contentType: "application/json",
                data: JSON.stringify(data)
            })
        .done(function (resData) {
            if (!success)
                return;
            success(resData);
        })
        .fail(function (jqXhr) {
            self.handleError(jqXhr, error);
        });
    }

    self.deleteItem = function (url, data, success, error) {
        if (routing)
            url = routing.combineUrl(url);

        return $.ajax(url,
            {
                method: DELETE,
                headers: getSecurityHeaders(),
                contentType: "application/json",
                data: JSON.stringify(data)
            })
        .done(function (resData) {
            if (!success)
                return;
            success(resData);
        })
        .fail(function (jqXhr) {
            self.handleError(jqXhr, error);
        });
    }

    // Data access operations

    self.searchInstances = function (params, success, error) {
        return self.get(self.instancesUrl, params, success, error);
    }

    self.searchLogs = function(params, success, error) {
        return self.get(self.logUrl, params, success, error);
    }

    self.searchEvents = function(params, success, error) {
        return self.get(self.eventUrl, params, success, error);
    }

    self.getEventByLogId = function(params, success, error) {
        return self.get(self.eventUrl + "/id", params, success, error);
    }

    self.getInstanceKpi = function(params, success, error) {
        return self.get(self.instancesUrl + "/kpi", params, success, error);
    }

    self.getLogKpi = function(params, success, error) {
        return self.get(self.logUrl + "/kpi", params, success, error);
    }

    self.getLatestInstances = function(params, success, error) {
        return self.get(self.instancesUrl + "/latest", params, success, error);
    }
}
