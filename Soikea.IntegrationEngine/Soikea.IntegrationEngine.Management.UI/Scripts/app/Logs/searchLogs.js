﻿function SearchLogsViewModel(app, configuration) {
    var self = this;

    self.loading = ko.observable(false);
    self.searchTerm = ko.observable(undefined);

    self.searchMethods = ko.observableArray([
        { key: app.constants.logSearchMethod.module, label: "Module" },
        { key: app.constants.logSearchMethod.severity, label: "Severity" },
        { key: app.constants.logSearchMethod.message, label: "Message"},
        { key: app.constants.logSearchMethod.correlationId, label: "CorrelationId" }
    ]);

    self.selectedSeverity = ko.observable(app.constants.logSeverities[0]);

    self.paged = ko.observable(true);

    self.searchTermEnabled = ko.observable(true);

    self.searchMethod = ko.observable(self.searchMethods()[0]);

    self.severitySelected = ko.computed(function () {
        return self.searchMethod().key === app.constants.logSearchMethod.severity;
    });
    
    self.showTime = ko.computed(function () {
        return self.searchMethod().key !== app.constants.logSearchMethod.correlationId;
    });

    self.moduleSelected = ko.computed(function() {
        return self.searchMethod().key === app.constants.logSearchMethod.module;
    });

    self.selectedModule = ko.observable(app.constants.modules[0]);

    self.startTime = ko.observable(moment().add(-1, "days"));
    self.endTime = ko.observable(moment());

    self.searchResults = new ListViewModel("Logs", [
           { key: "timestamp", label: "Timestamp", binding: "datetime" },
           { key: "severity", label: "Severity", binding: "lookupLabel", bindingOptions: app.constants.logSeverities },
           { key: "category", label: "Category" },
           { key: "message", label: "Message" },
           { key: "correlationId", label: "CorrelationId" },
           { key: "module", label: "Module" }
    ], [], false, 20, listItemSearchType.perColumn);

    self.pagedSearchResults = new PagedListViewModel("Logs", [
           { key: "timestamp", label: "Timestamp", binding: "datetime" },
           { key: "severity", label: "Severity", binding: "lookupLabel", bindingOptions: app.constants.logSeverities },
           { key: "category", label: "Category" },
           { key: "message", label: "Message" },
           { key: "correlationId", label: "CorrelationId" },
           { key: "module", label: "Module" }
        ],
        function (currentPageIndex, callback, populate) {
            
            var searchTerm;
            switch (true) {
                case self.severitySelected():
                    searchTerm = self.selectedSeverity().key;
                    break;
                case self.moduleSelected():
                    searchTerm = self.selectedModule().label;
                    break;
                default:
                    searchTerm = self.searchTerm();
                    break;
            }

            var params = {
                term: searchTerm,
                method: self.searchMethod().key,
                start: self.startTime().format(),
                end: self.endTime().format(),
                page: currentPageIndex,
                paged: self.paged()
           };


            app.dataModel.searchLogs(params, function (data) {
                if (data.errorMessage) {
                    app.warning(data.errorMessage);
                }

                if (!isEmpty(data.logs)) {
                    populate(data);
                }
                callback();
            }, function () {
                callback();
            });
        }
    );

    self.performSearch = function () {
        if (self.paged()) {
            self.pagedSearchResults.reset();
            self.pagedSearchResults.performSearch();
        } else {
            self.searchResults.loading(true);
            var searchTerm;
            switch (true) {
                case self.severitySelected():
                    searchTerm = self.selectedSeverity().key;
                    break;
                case self.moduleSelected():
                    searchTerm = self.selectedModule().label;
                    break;
                default:
                    searchTerm = self.searchTerm();
                    break;
            }

            var params = {
                term: searchTerm,
                method: self.searchMethod().key,
                start: self.startTime().format(),
                end: self.endTime().format(),
                paged: self.paged()
            };

            app.dataModel.searchLogs(params, function (data) {
                if (data.errorMessage) {
                    app.warning(data.errorMessage);
                }

                if (!isEmpty(data.logs)) {
                    self.searchResults.populate(data.logs);
                    self.searchResults.loading(false);
                }
                }, function () {
                     self.searchResults.loading(false);
            });
        }
    };

    Sammy(function () {
        this.get("#/search", function () {
            app.navigateToSearchLogsViewModel();
        });

        this.get("#/byCorrelationId/:id", function() {
            app.navigateToSearchLogsViewModel();
            self.searchTerm(this.params["id"]);
            self.searchMethod(self.searchMethods()[3]);
            self.pagedSearchResults.performSearch();
        });

        this.get("#/", function () { this.app.runRoute("get", "#/search") });

        this.around(function(callback) {
            if (this.path.indexOf("#") > -1) {
                callback();
            }
        });
    });
    return self;
}

app.addViewModel({
    name: "SearchLogsViewModel",
    bindingMemberName: "searchLogs",
    factory: SearchLogsViewModel
});