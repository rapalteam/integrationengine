﻿$(function () {
    app.initialize();

    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-center",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "300",
        "timeOut": "0",
        "extendedTimeOut": "0",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "slideDown",
        "hideMethod": "slideUp",
        "containerId": "toast-container"
    }
    /*
    // Activate Knockout
    ko.validation.init({
        grouping: { observable: true },
        insertMessages: false,
        errorElementClass: "has-error",
        decorateElement: true,
        decorateInputElement: true,
        decorateElementOnModified: false
    });
    */
    ko.applyBindings(app);
});
