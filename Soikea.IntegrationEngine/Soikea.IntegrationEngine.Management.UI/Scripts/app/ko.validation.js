﻿// Validation extensions
/*
// Translation overrides of validation-error-messages

ko.validation.rules["required"].message = _("validation_error_required");
ko.validation.rules["min"].message = _("validation_error_min");
ko.validation.rules["max"].message = _("validation_error_max");
ko.validation.rules["minLength"].message = _("validation_error_minLength");
ko.validation.rules["maxLength"].message = _("validation_error_maxLength");
ko.validation.rules["pattern"].message = _("validation_error_pattern");
ko.validation.rules["step"].message = _("validation_error_step");
ko.validation.rules["email"].message = _("validation_error_email");
ko.validation.rules["date"].message = _("validation_error_date");
ko.validation.rules["dateISO"].message = _("validation_error_dateISO");
ko.validation.rules["number"].message = _("validation_error_number");
ko.validation.rules["digit"].message = _("validation_error_digit");
ko.validation.rules["phoneUS"].message = _("validation_error_phoneUS");
ko.validation.rules["equal"].message = _("validation_error_equal");
ko.validation.rules["notEqual"].message = _("validation_error_notEqual");
ko.validation.rules["unique"].message = _("validation_error_unique");

// Custom validation-rules would go here

ko.validation.rules["flagsRequired"] = {
    validator: function (sum, validate) {
        return validate && sum >= 1;
    },
    message: _("validation_error_flags_required")
};

ko.validation.rules["phoneFI"] = {
    validator: function (phoneNumber, validate) {
        if (ko.validation.utils.isEmptyVal(phoneNumber)) { return true; } // makes it optional, use 'required' rule if it should be required
        if (typeof (phoneNumber) !== "string") { return false; }

        return validate && phoneNumber.match(/(\b[0][0-9]{1,2}[- ]?[0-9]{3}[ ]?[0-9]{4}|\+[0-9]{1,3}[ ]?[0-9]{1,2}[ -]?[0-9]{3}[ ]?[0-9]{4})\b/);
    },
    message: _("validation_error_phoneFI")
};

ko.validation.rules["dateFI"] = {
    validator: function (dateVal, validate) {
        if (ko.validation.utils.isEmptyVal(dateVal)) { return true; } // makes it optional, use 'required' rule if it should be required

        return validate && moment(dateVal).isValid();
    },
    message: _("validation_error_date")
};


ko.validation.rules["postalCode"] = {
    validator: function (postal, validate) {
        if (ko.validation.utils.isEmptyVal(postal)) { return true; } // makes it optional, use 'required' rule if it should be required
        if (typeof (postal) !== "string") { return false; }

        return validate && postal.match(/^[0-9]{5}$/);
    },
    message: _("validation_error_postal")
};


ko.validation.rules["isUnique"] = {
    validator: function (newVal, options) {
        if (options.predicate && typeof options.predicate !== "function")
            throw new Error("Invalid option for isUnique validator. The 'predicate' option must be a function.");

        function equalityDelegate() {
            return options.predicate ? options.predicate : function (v1, v2) { return v1 === v2; };
        }

        var array = options.array || options;
        var count = 0;
        ko.utils.arrayMap(ko.utils.unwrapObservable(array), function (existingVal) {
            if (equalityDelegate()(existingVal, newVal)) count++;
        });

        return count < 2;
    },
    message: _("validation_error_arrayvalues_not_unique")
};

ko.validation.rules["atLeastOne"] = {
    validator: function (arr) {
        var valueUnwrapped = ko.utils.unwrapObservable(arr);

        return valueUnwrapped.length >= 1;
    },
    message: _("validation_error_atLeastOne")
};

ko.validation.registerExtenders();
*/