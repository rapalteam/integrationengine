﻿function InspectEventViewModel(app, item, callback) {
    var self = this;

    self.id = ko.observable(item.id);
    self.tenant = ko.observable(item.tenantId);
    self.jobId = ko.observable(item.jobId);
    self.subscriptionCategory = ko.observable(item.subscriptionCategory);
    self.direction = ko.observable(app.constants.eventStream[item.direction-1].label);
    self.publishedAt = ko.observable(moment(item.publishedAt));
    self.typeName = ko.observable(item.typeName);
    self.payload = JSON.parse(item.payload);

    self.headers = ko.observableArray();
    
    self.close = function() {
        callback();
    }

    return self;
}