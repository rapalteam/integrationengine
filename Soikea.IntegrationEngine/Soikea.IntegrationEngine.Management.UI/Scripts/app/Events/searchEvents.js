﻿function SearchEventsViewModel(app, configuration) {
    var self = this;
    self.itemInInspection = ko.observable(undefined);
    self.showInspectDialog = ko.observable(false);
    self.endTime = ko.observable(new Date());
    self.startTime = ko.observable(moment().add(-1, "days").toDate());
    self.loading = ko.observable(false);
    self.selectedEvent = ko.observable(undefined);
    self.tenantId = ko.observable(undefined);
    
    self.selectedDirection = ko.observable(app.constants.eventStream[0]);
    self.searchTerm = ko.observable();

    self.searchMethods = ko.observableArray([
        { key: app.constants.eventSearchMethod.jobId, label: "JobId" },
        { key: app.constants.eventSearchMethod.tenant, label: "Tenant" },
        { key: app.constants.eventSearchMethod.direction, label: "Direction" },
        { key: app.constants.eventSearchMethod.subscriptionCategory, label: "Category" }
    ]);

    self.searchMethod = ko.observable(self.searchMethods()[1]);

    self.showTime = ko.computed(function() {
        return self.searchMethod().key !== app.constants.eventSearchMethod.jobId;
    });

    self.directionSelected = ko.computed(function() {
        return self.searchMethod().key === app.constants.eventSearchMethod.direction;
    });

    self.inspectEvent = function (listItemId) {
        Sammy().setLocation("#/inspect/" + listItemId);
        Sammy().trigger("location-changed");
    }

    self.searchResults = new ListViewModel("Events", [
           { key: "id", label: "Id"},
           { key: "tenantId", label: "TenantId" },
           { key: "jobId", label: "JobId" },
           { key: "subscriptionCategory", label: "Subscription category" },
           { key: "direction", label: "Direction", binding: "lookupLabel", bindingOptions: app.constants.eventStream },
           { key: "publishedAt", label: "PublishedAt", binding: "datetime" },
        ],
       [{ action: self.inspectEvent, label: "Inspect" }],
       false,
       20);

    self.search = function () {
        self.searchResults.loading(true);

        var params = {
            start: moment(self.startTime()).format(),
            end: moment(self.endTime()).format(),
            method: self.searchMethod().key,
            term: self.directionSelected() ? self.selectedDirection().key : self.searchTerm()
        };

        app.dataModel.searchEvents(params, function (data) {
            if (data.errorMessage) {
                app.warning(data.errorMessage);
            }

            if (!isEmpty(data.events)) {
                self.searchResults.populate(data.events);
            }

            self.searchResults.loading(false);
        }, function () {
            self.searchResults.loading(false);
        });
    };

    Sammy(function () {

        this.get("#/search", function () {
            app.navigateToSearchEventsViewModel();
        });

        this.get("#/inspect/:id", function () {
            self.showInspectDialog(false);
            var item = self.searchResults.find(parseInt(this.params["id"]));

            if (isEmpty(item)) {
                app.warning("Error");
                return;
            }

            var callback = function () {
                self.showInspectDialog(false);
                self.itemInInspection(undefined);
                Sammy().setLocation("#/search");
                Sammy().trigger("location-changed");
            }

            var params = {
                id: item.id
            }
            app.dataModel.getEventByLogId(params, function (data) {
                self.itemInInspection(new InspectEventViewModel(app, data, callback));
                self.showInspectDialog(true);
            }, function(data) {
                app.error("Could not find event with id " + item.id);
            });
        });

        this.get("/", function () { this.app.runRoute("get", "#/search") });
    });

    return self;
}

app.addViewModel({
    name: "SearchEventsViewModel",
    bindingMemberName: "searchEvents",
    factory: SearchEventsViewModel
});