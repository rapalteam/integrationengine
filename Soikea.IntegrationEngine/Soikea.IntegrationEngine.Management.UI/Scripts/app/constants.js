﻿function Constants() {
    var self = this;
    // App-scope enumerations
    self.lookupCategories = {

    };

    self.jobResultStatuses = [
        { key: 0, label: "All" },
        { key: 1, label: "NotStarted" },
        { key: 2, label: "Started" },
        { key: 3, label: "Ok" },
        { key: 4, label: "Aborted" },
        { key: 5, label: "Error" }
    ];

    self.logSearchMethod = {
        correlationId: 0,
        severity: 1,
        module: 2,
        message: 3
    };

    self.modules = [
        { key: 0, label: "IntegrationJobs" },
        { key: 1, label: "StageAPI" },
        { key: 2, label: "ManagementUI" }
    ];

    self.logSeverities = [
        { key: 1, label: "Critical" },
        { key: 2, label: "Error" },
        { key: 4, label: "Warning" },
        { key: 8, label: "Information" },
        { key: 16, label: "Verbose" },
        { key: 256, label: "Start" },
        { key: 512, label: "Stop" },
        { key: 1024, label: "Suspend" },
        { key: 2048, label: "Resume" },
        { key: 4096, label: "Transfer" },
    ];

    self.eventStream = [
        { key: 1, label: "Input" },
        { key: 2, label: "Output" }
    ];

    self.eventSearchMethod = {
        jobId: 0,
        tenant: 1,
        direction: 2,
        subscriptionCategory: 3
    }

    self.dateFormat = "D.M.YYYY";
    self.dateTimeFormat = "D.M.YYYY HH:mm:ss";

    self.instanceSearchMethod = {
        date: 0,
        result: 1
    };

    return self;
}

var constants = new Constants();