﻿function DashboardViewModel(app, configuration) {
    var self = this;

    self.totalJobs = ko.observable(0);
    self.jobsWithErrors = ko.observable(0);
    self.totalErrors = ko.observable(0);
    self.runningJobs = ko.observable(0);

    self.updateInstanceKPI = function(data) {
        self.totalJobs(data.ranInstances);
        self.jobsWithErrors(data.failedInstances);
        self.runningJobs(data.runningJobs);
    }
    self.updateLogKpi = function(data) {
        self.totalErrors(data.errorCount);
    }

    self.latestJobs = new ListViewModel("Instances", [
           { key: "correlationId", label: "Correlation Id" },
           { key: "tenantId", label: "Tenant Id" },
           { key: "instanceName", label: "Instance name" },
           { key: "startTime", label: "Start time", binding: "datetime" },
           { key: "duration", label: "Duration", binding: "time" },
           { key: "result", label: "Jobresult status", binding: "lookupLabel", bindingOptions: app.constants.jobResultStatuses }
    ],[],
    false,
    10);

    self.initialize = function () {
        var params = {};
        
        app.dataModel.getInstanceKpi(params, function(data) {
            self.updateInstanceKPI(data);
        }, function(data) {

        });

        app.dataModel.getLogKpi(params, function(data) {
            self.updateLogKpi(data);
        }, function(data) {

        });

        app.dataModel.getLatestInstances(params, function(data) {
            if (data.warning)
                app.warning(data.warning);
            if (!isEmpty(data.instances)) {
                self.latestJobs.populate(data.instances);
            }
        }, function(data) {
            
        });
    }

    self.initialize();

    Sammy(function () {
        this.get("/", function () {
            app.navigateToDashboardViewModel();
        });
    });

    return self;
}

app.addViewModel({
    name: "DashboardViewModel",
    bindingMemberName: "dashboard",
    factory: DashboardViewModel
});