﻿function TableRowItemViewModel(params) {
    ///<summary>Row item view model for ListViewModel-listing</summary>
    ///<param name="params">an object whose key/value pairs are the parameters passed from the component binding or custom element.</param>

    var self = this;

    this.id = params.id;

    this.cols = [];

    ko.utils.arrayForEach(params.cols, function (item) {
        var valueToPush;

        var keys = item.key.split(",");

        ko.utils.arrayForEach(keys, function (currentKey) {
            var iterVal;
            if (currentKey.indexOf(".") > -1) {
                var tokens = currentKey.split(".");
                iterVal = params.data;
                for (var i = 0; i < tokens.length; i++) {
                    iterVal = iterVal[tokens[i]];
                }
            } else {
                iterVal = params.data[currentKey];
            }

            if (typeof iterVal === "boolean") {
                iterVal = iterVal ? "Yes" : "No";
            }

            if (typeof valueToPush !== "undefined") {
                valueToPush += ", " + iterVal;
            } else {
                valueToPush = iterVal;
            }
        });

        self.cols.push(valueToPush);
    });

    this.actions = [];



    if (params.actions) {
        if (typeof params.actions === "function")
            ko.utils.arrayPushAll(this.actions, params.actions());
        else if (typeof params.actions === "object")
            ko.utils.arrayPushAll(this.actions, params.actions);
    }
}

ko.components.register("table-row-item", {
    viewModel: TableRowItemViewModel,
    template: { element: "table-row-list-template" }
});

function FormattedTableRowItemViewModel(params) {
    ///<summary>Row item view model for ListViewModel-listing</summary>
    ///<param name="params">an object whose key/value pairs are the parameters passed from the component binding or custom element.</param>
    var self = this;

    this.id = params.id;

    this.cols = [];

    ko.utils.arrayForEach(params.cols, function (item) {
        var valueToPush;

        var keys = item.key.split(",");

        ko.utils.arrayForEach(keys, function (currentKey) {
            var iterVal;
            if (currentKey.indexOf(".") > -1) {
                var tokens = currentKey.split(".");
                iterVal = params.data;
                for (var i = 0; i < tokens.length; i++) {
                    iterVal = iterVal[tokens[i]];
                }
            } else {
                iterVal = params.data[currentKey];
            }

            if (typeof item.binding !== "undefined" && typeof ko.filters[item.binding] === "function") {
                if (typeof item.bindingOptions !== "undefined") {

                    iterVal = ko.filters[item.binding](
                        iterVal,
                        ko.utils.isObservableArray(item.bindingOptions)
                            ? item.bindingOptions()
                            : item.bindingOptions
                    );
                } else {
                    iterVal = ko.filters[item.binding](iterVal);
                }
            }

            if (typeof iterVal === "undefined" || iterVal == null)
                return;

            if (typeof valueToPush !== "undefined") {
                valueToPush += ", " + iterVal;
            } else {
                valueToPush = iterVal;
            }
        });

        self.cols.push(valueToPush);
    });

    this.actions = [];

    if (params.actions) {
        if (typeof params.actions === "function")
            ko.utils.arrayPushAll(this.actions, params.actions());
        else if (typeof params.actions === "object")
            ko.utils.arrayPushAll(this.actions, params.actions);
    }
}


ko.components.register("table-row-item-formatted", {
    viewModel: FormattedTableRowItemViewModel,
    template: { element: "table-row-list-template" }
});