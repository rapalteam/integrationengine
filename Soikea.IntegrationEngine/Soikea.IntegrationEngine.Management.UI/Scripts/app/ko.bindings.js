﻿// Helper functions 

function setTextContent(element, textContent) {
    var value = ko.utils.unwrapObservable(textContent);
    if ((value === null) || (value === undefined))
        value = "";

    // We need there to be exactly one child: a text node.
    // If there are no children, more than one, or if it's not a text node,
    // we'll clear everything and create a single text node.
    var innerTextNode = ko.virtualElements.firstChild(element);
    if (!innerTextNode || innerTextNode.nodeType != 3 || ko.virtualElements.nextSibling(innerTextNode)) {
        ko.virtualElements.setDomNodeChildren(element, [document.createTextNode(value)]);
    } else {
        innerTextNode.data = value;
    }
}


// Custom bindings 

ko.bindingHandlers.date = {
    init: function () {
        // Prevent binding on the dynamically-injected text node (as developers are unlikely to expect that, and it has security implications).
        // It should also make things faster, as we no longer have to consider whether the text node might be bindable.
        return { 'controlsDescendantBindings': true };
    },
    update: function (element, valueAccessor) {
        // First get the latest data that we're bound to
        var valueUnwrapped = ko.unwrap(valueAccessor());
        var asMoment = moment(valueUnwrapped);
        var dateStr = asMoment.isSame(moment(0))
            ? ""
            : asMoment.format(constants.dateFormat);


        setTextContent(element, dateStr);
    }
};
ko.filters.date = function (value) {
    return value ? moment(value).format(constants.dateFormat) : "";
};

ko.bindingHandlers.datetime = {
    init: function () {
        // Prevent binding on the dynamically-injected text node (as developers are unlikely to expect that, and it has security implications).
        // It should also make things faster, as we no longer have to consider whether the text node might be bindable.
        return { 'controlsDescendantBindings': true };
    },
    update: function (element, valueAccessor) {
        // First get the latest data that we're bound to
        var valueUnwrapped = ko.unwrap(valueAccessor());
        var asMoment = moment(valueUnwrapped);
        var dateStr = asMoment.isSame(moment(0))
            ? ""
            : asMoment.format(constants.dateTimeFormat);


        setTextContent(element, dateStr);
    }
};
ko.filters.datetime = function (value) {
    return value ? moment(value).format(constants.dateTimeFormat) : "";
};

ko.bindingHandlers.day = {
    init: function () {
        // Prevent binding on the dynamically-injected text node (as developers are unlikely to expect that, and it has security implications).
        // It should also make things faster, as we no longer have to consider whether the text node might be bindable.
        return { 'controlsDescendantBindings': true };
    },
    update: function (element, valueAccessor) {
        // First get the latest data that we're bound to
        var valueUnwrapped = ko.unwrap(valueAccessor());

        var dateStr = moment(valueUnwrapped).format('LL');


        setTextContent(element, dateStr);
    }
};

ko.bindingHandlers.time = {
    init: function () {
        // Prevent binding on the dynamically-injected text node (as developers are unlikely to expect that, and it has security implications).
        // It should also make things faster, as we no longer have to consider whether the text node might be bindable.
        return { 'controlsDescendantBindings': true };
    },
    update: function (element, valueAccessor) {
        // First get the latest data that we're bound to
        var valueUnwrapped = ko.unwrap(valueAccessor());

        var timeStr = moment(valueUnwrapped).format('H:mm');

        setTextContent(element, timeStr);
    }
};

ko.bindingHandlers.daymonth = {
    init: function () {
        // Prevent binding on the dynamically-injected text node (as developers are unlikely to expect that, and it has security implications).
        // It should also make things faster, as we no longer have to consider whether the text node might be bindable.
        return { 'controlsDescendantBindings': true };
    },
    update: function (element, valueAccessor) {
        // First get the latest data that we're bound to
        var valueUnwrapped = ko.unwrap(valueAccessor());

        var dateStr = moment(valueUnwrapped).format('D.M');

        setTextContent(element, dateStr);
    }
};


ko.bindingHandlers.minutes = {
    init: function (element, valueAccessor, allBindingsAccessor) {
        var underlyingObservable = valueAccessor();

        var interceptor = ko.computed({
            read: function () {
                // Return value with padded with leading zero
                var stringVal = "00" + underlyingObservable();
                return stringVal.substr(stringVal.length - 2, 2);
            },

            write: function (newValue) {
                var current = underlyingObservable(),
                    valueToWrite = Math.round(parseFloat(newValue.replace("00", "0")));

                if (valueToWrite !== current) {
                    underlyingObservable(valueToWrite);
                } else {
                    if (newValue !== current.toString())
                        underlyingObservable.valueHasMutated();
                }
            }
        });
        // Apply value-binding to the element
        ko.applyBindingsToNode(element, { value: interceptor });
    }
};

ko.bindingHandlers.modal = {
    init: function (element, valueAccessor) {
        $(element).modal({
            show: false,
            keyboard: false,
            backdrop: "static"
        });

        var value = valueAccessor();
        if (ko.isObservable(value)) {
            $(element).on("hide.bs.modal", function () {
                value(false);
            });
        }
    },
    update: function (element, valueAccessor) {
        var value = valueAccessor();
        if (ko.utils.unwrapObservable(value)) {
            $(element).modal("show");
        } else {
            $(element).modal("hide");
            $("body").removeClass("modal-open");
            $(".modal-backdrop").remove();
        }
    }
}

ko.bindingHandlers.loader = {
    init: function (element, valueAccessor) {
        $(element).addClass("loader");
    },
    update: function (element, valueAccessor) {
        var value = valueAccessor();
        if (ko.utils.unwrapObservable(value)) {
            $(element).addClass("loading");
        } else {
            $(element).removeClass("loading");
        }
    }
}
ko.virtualElements.allowedBindings.loader = true;


ko.filters.bool = function (value) {
    var valueUnwrapped = ko.utils.unwrapObservable(value);
    return valueUnwrapped === true ? _("Yes") : _("No");
};


ko.bindingHandlers.bool = {
    init: function () {
        // Prevent binding on the dynamically-injected text node (as developers are unlikely to expect that, and it has security implications).
        // It should also make things faster, as we no longer have to consider whether the text node might be bindable.
        return { 'controlsDescendantBindings': true };
    },
    update: function (element, valueAccessor) {
        // First get the latest data that we're bound to
        var valueUnwrapped = ko.utils.unwrapObservable(valueAccessor);

        setTextContent(element, valueUnwrapped ? _("Yes") : _("No"));
    }
};

var windowURL = window.URL || window.webkitURL;
ko.bindingHandlers.file = {
    init: function (element, valueAccessor) {
        $(element).change(function () {
            var file = this.files[0];
            if (ko.isObservable(valueAccessor())) {
                valueAccessor()(file);
            }
        });
    },
    update: function (element, valueAccessor, allBindingsAccessor) {
        var file = ko.utils.unwrapObservable(valueAccessor());
        var bindings = allBindingsAccessor();

        if (bindings.fileBinaryData && ko.isObservable(bindings.fileBinaryData)) {
            if (!file) {
                bindings.fileBinaryData(null);
            } else {
                var reader = new FileReader();
                reader.onload = function (e) {
                    var result = e.target.result || {};
                    var resultParts = result.split(",");
                    if (resultParts.length === 2) {
                        bindings.fileBinaryData(resultParts[1]);
                    }

                };
                reader.readAsDataURL(file);
            }
        }
    }
};

ko.bindingHandlers.label = {
    init: function () {
        // Prevent binding on the dynamically-injected text node (as developers are unlikely to expect that, and it has security implications).
        // It should also make things faster, as we no longer have to consider whether the text node might be bindable.
        return { 'controlsDescendantBindings': true };
    },
    update: function (element, valueAccessor) {
        // First get the latest data that we're bound to
        var valueUnwrapped = ko.unwrap(valueAccessor());

        var str = isEmpty(valueUnwrapped) ? "" : valueUnwrapped.label;

        setTextContent(element, str);
    }
};

ko.filters.lookupLabel = function (value, domain) {
    if (domain instanceof Array) {
        var match = ko.utils.arrayFirst(domain, function (item) {
            return item.key === value;
        });

        if (match && typeof match.label !== "undefined") {
            return match.label;
        }
    }

    return value;
};

ko.bindingHandlers.radioBool = {
    init: function (element, valueAccessor) {
        var observable = valueAccessor(),
            interceptor = ko.computed({
                read: function () {
                    return typeof observable() !== "undefined" ? observable().toString() : undefined;
                },
                write: function (newValue) {
                    observable(newValue === "true");
                },
                owner: this
            });
        ko.applyBindingsToNode(element, { checked: interceptor });
    }
};

ko.bindingHandlers.datepicker = {
    init: function (element, valueAccessor, allBindingsAccessor) {
        //initialize datepicker with some optional options
        var options = allBindingsAccessor().datepickerOptions || {format: "dd.mm.yyyy"};
        $(element).datepicker(options);
        var initialDate = valueAccessor();
        $(element).datepicker("setDates", initialDate());

        //when a user changes the date, update the view model
        ko.utils.registerEventHandler(element, "changeDate", function (event) {
            var value = valueAccessor();
            if (ko.isObservable(value)) {
                value(event.date);
            }
        });
    },
    update: function (element, valueAccessor) {
        var widget = $(element).data("datepicker");
        //when the view model is updated, update the widget
        if (widget) {
            widget.date = ko.utils.unwrapObservable(valueAccessor());
            widget.setValue();
        }
    }
}; // end KO binding datepicker


ko.bindingHandlers.datetimepicker = {
    init: function (element, valueAccessor, allBindingsAccessor) {
        //initialize datepicker with some optional options
        var options = allBindingsAccessor().datetimepickerOptions || { locale: "fi", defaultDate: ko.utils.unwrapObservable(valueAccessor()) };
        $(element).datetimepicker(options);
        
        //when a user changes the date, update the view model
        ko.utils.registerEventHandler(element, "dp.change", function (event) {
            var value = valueAccessor();
            if (ko.isObservable(value)) {
                value(event.date);
            }
        });
    },
    update: function (element, valueAccessor) {
        var widget = $(element).data("datetimepicker");
        //when the view model is updated, update the widget
        if (widget) {
            widget.date = ko.utils.unwrapObservable(valueAccessor());
            widget.setValue();
        }
    }
}; // end KO binding datepicker

ko.bindingHandlers.recursiveList = {

    init: function() {
        // Prevent binding on the dynamically-injected text node (as developers are unlikely to expect that, and it has security implications).
        // It should also make things faster, as we no longer have to consider whether the text node might be bindable.
        return { 'controlsDescendantBindings': true };
    },
    update: function (element, valueAccessor) {
        // First get the latest data that we're bound to
        var valueUnwrapped = ko.unwrap(valueAccessor());
        //valueAccessor should be a observable with parsed json. (JSON.parse()...)
        function buildList(data, isSub) {
            var html = (isSub) ? "<div>" : "";

            if (isEmpty(data)) {
                html += "null</div>";
                return html;
            }

            html += "<ul>";
            var headers = Object.keys(data);
            for (var header in headers) {
                html += "<li>";
                if (typeof (data[headers[header]]) === "object") {
                    html += "<b>" + headers[header] + "</b>";
                    html += buildList(data[headers[header]], true);
                } else {
                    html += "<b>" + headers[header] + "</b>" + ": " + data[headers[header]];
                }
                html += "</li>";
            }
            html += "</ul>";
            html += (isSub) ? "</div>" : "";
            return html;
        }

        var value = buildList(valueUnwrapped, false);
        element.innerHTML = value;
    }
}