﻿using System.Web.Http;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using Soikea.IntegrationEngine.Administration.Repository;
using Soikea.IntegrationEngine.Core.Interfaces;
using Soikea.IntegrationEngine.EventStore;
using Soikea.IntegrationEngine.HangfireConfiguration.Configuration.Settings;
using Soikea.IntegrationEngine.Management.UI.Controllers;
using Soikea.IntegrationEngine.Management.UI.Models.Log;

namespace Soikea.IntegrationEngine.Management.UI.App_Start
{
    public static class AutofacConfig
    {
        public static void RegisterDependencies(SettingsConfigurationSection settings)
        {
            var builder = new ContainerBuilder();

            // Register dependencies in controllers
            builder.RegisterControllers(typeof(HomeController).Assembly);
            builder.RegisterApiControllers(typeof(AccountController).Assembly);

            // Register dependencies in filter attributes
            builder.RegisterFilterProvider();

            // Register dependencies in custom views
            builder.RegisterSource(new ViewRegistrationSource());

            builder.Register(c => Settings.Create(settings.SettingItems))
                            .As<ISettings>();
            builder.RegisterType<LogRepository>().As<LogRepository>();
            builder.RegisterType<InstanceRepository>().As<IInstanceRepository>();
            builder.Register(c => new EventStoreRepository(c.Resolve<ISettings>())).As<IEventStoreRepository>();
            
            var container = builder.Build();

            // Set MVC DI resolver to use our Autofac container
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }
    }
}