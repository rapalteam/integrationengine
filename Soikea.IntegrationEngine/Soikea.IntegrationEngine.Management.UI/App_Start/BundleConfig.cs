﻿using System.Web;
using System.Web.Optimization;

namespace Soikea.IntegrationEngine.Management.UI
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"
                      ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css",
                      "~/Content/bootstrap-datepicker.min.css",
                      "~/Content/toastr.min.css",
                      "~/Content/bootstrap-datetimepicker.min.css"));

            bundles.Add(new ScriptBundle("~/bundles/lib").Include(
                  "~/Scripts/knockout-{version}.js",
                  "~/Scripts/knockout.punches.min.js",
                  "~/Scripts/knockout.validation.js",
                  "~/Scripts/moment-with-locales.min.js",
                  "~/Scripts/translate.js",
                  "~/Scripts/sammy-0.7.5.min.js",
                  "~/Scripts/toastr.min.js",
                  "~/Scripts/moment-with-locales.min.js", 
                  "~/Scripts/bootstrap-datetimepicker.min.js",
                  "~/Scripts/bootstrap-datepicker.min.js",
                  "~/Scripts/async.js"
                  ));

            bundles.Add(new ScriptBundle("~/bundles/app").Include(
                    "~/Scripts/app/common.js",
                    "~/Scripts/app/constants.js",
                    "~/Scripts/app/ListViewModel.js",
                    "~/Scripts/app/PagedListViewModel.js",
                    "~/Scripts/app/DefaultSelectionsViewModel.js",
                    "~/Scripts/app/ko.extensions.js",
                    "~/Scripts/app/ko.components.js",
                    "~/Scripts/app/ko.bindings.js",
                    "~/Scripts/app/ko.editable.js",
                    "~/Scripts/app/ko.validation.js",
                    "~/Scripts/app/app.datamodel.js",
                    "~/Scripts/app/app.viewmodel.js"
                    ));

            bundles.Add(new ScriptBundle("~/bundles/app/instances").Include(
                 "~/Scripts/app/Instances/searchInstances.js",
                 "~/Scripts/app/Instances/inspectInstance.js",
                 "~/Scripts/app/Instances/_run.js"
             ));

            bundles.Add(new ScriptBundle("~/bundles/app/logs").Include(
                 "~/Scripts/app/Logs/searchLogs.js",
                 "~/Scripts/app/Logs/_run.js"
             ));
            bundles.Add((new ScriptBundle("~/bundles/app/dashboard").Include(
                "~/Scripts/app/Dashboard/dashboard.js",
                "~/Scripts/app/Dashboard/_run.js"
                )));

            bundles.Add(new ScriptBundle("~/bundles/app/events").Include(
                "~/Scripts/app/Events/searchEvents.js",
                "~/Scripts/app/Events/inspectEvent.js",
                "~/Scripts/app/Events/_run.js"
            ));
#if !DEBUG
            BundleTable.EnableOptimizations = true;
#endif
        }
    }
}
