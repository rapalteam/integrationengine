using System.Data.Entity;
using Soikea.IntegrationEngine.Management.UI.Models.Log;

namespace Soikea.IntegrationEngine.Management.UI.Migrations
{

    using System;
    using System.Data.Entity.Migrations;

    public sealed class TestLogDataInitializer<TContext> : IDatabaseInitializer<TContext> where TContext : LogContext
    {
        /// <summary>
        /// Reference time stamp that is used when unit-testing queries.
        /// </summary>
        public static readonly DateTime TestReferenceTimeStamp = new DateTime(2016, 10, 10);

        public void InitializeDatabase(TContext context)
        {
#if TESTLOGDATA               

                context.Logs.AddOrUpdate(new Log
                {
                    JobId = "default-job-id",
                    Message = "test log entry1",
                    Priority = 1,
                    Title = "TestEntry",
                    Severity = Constants.LogSeverity.Critical.ToString(),
                    MachineName = "TestMachine",
                    EventID = 1,
                    LogID = 1,
                    Module = "TestModule",
                    Timestamp = TestReferenceTimeStamp
                });

                context.Logs.AddOrUpdate(new Log
                {
                    JobId = "default-job-id",
                    Message = "test log entry2",
                    Priority = 2,
                    Title = "TestEntry2 before TestReferenceTimeStamp",
                    Severity = Constants.LogSeverity.Error.ToString(),
                    MachineName = "TestMachine2",
                    EventID = 2,
                    LogID = 2,
                    Module = "TestModule2",
                    Timestamp = TestReferenceTimeStamp.AddSeconds(-1)
                });

                context.Logs.AddOrUpdate(new Log
                {
                    JobId = "not-default-job-id",
                    Message = "test log entry3",
                    Priority = 3,
                    Title = "TestEntry3 after TestReferenceTimeStamp",
                    Severity = Constants.LogSeverity.Warning.ToString(),
                    MachineName = "TestMachine3",
                    EventID = 3,
                    LogID = 3,
                    Module = "TestModule3",
                    Timestamp = TestReferenceTimeStamp.AddSeconds(1)
                });

                context.Categories.AddOrUpdate(new Category
                {
                    CategoryID = 1,
                    CategoryName = "category 1"
                });
                context.Categories.AddOrUpdate(new Category
                {
                    CategoryID = 2,
                    CategoryName = "category 2"
                });
                context.Categories.AddOrUpdate(new Category
                {
                    CategoryID = 3,
                    CategoryName = "category 3"
                });

                context.CategoryLogs.AddOrUpdate(
                    cl => cl.LogID, 
                    new CategoryLog
                    {
                        LogID = 1,
                        CategoryID = 1
                    }, 
                    new CategoryLog
                    {
                        LogID = 2,
                        CategoryID = 2
                    },
                    new CategoryLog
                    {
                        LogID = 3,
                        CategoryID = 3
                    });


            context.SaveChanges();
#endif
        }
        
    }
    
    }
