﻿using Hangfire;
using Hangfire.Dashboard;
using Hangfire.Logging;
using Hangfire.SqlServer;
using Microsoft.Owin;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Owin;
using Soikea.IntegrationEngine.Core.Logging;
using Soikea.IntegrationEngine.Core.Logging.Providers;
using Logger = Microsoft.Practices.EnterpriseLibrary.Logging.Logger;

[assembly: OwinStartup(typeof(Soikea.IntegrationEngine.Management.UI.Startup))]

namespace Soikea.IntegrationEngine.Management.UI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            DatabaseFactory.SetDatabaseProviderFactory(new DatabaseProviderFactory());
            Logger.SetLogWriter(new LogWriterFactory().Create());
            Log.SetCurrentLogProvider(new EntLibLogProvider(), "ManagementUI");

            ConfigureAuth(app);

            var dashboardOptions = new DashboardOptions()
            {
                AuthorizationFilters = new []
                {
                    new LocalRequestsOnlyAuthorizationFilter()
                }
            };
            LogProvider.SetCurrentLogProvider(new Hangfire.Logging.LogProviders.EntLibLogProvider());
            JobStorage.Current = new SqlServerStorage("Hangfire");
            app.UseHangfireDashboard("/hangfire", dashboardOptions);
        }
    }
}
