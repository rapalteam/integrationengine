﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Practices.EnterpriseLibrary.Logging.TraceListeners;

namespace Soikea.EntLibExtensions
{
    public sealed class StreamWriterHelper
    {

        /// <summary>
        /// A tally keeping writer used when file size rolling is configured.<para/>
        /// The original stream writer from the base trace listener will be replaced with
        /// this listener.
        /// </summary>
        private TallyKeepingFileStreamWriter managedWriter;

        private DateTime? nextRollDateTime;

        /// <summary>
        /// The trace listener for which rolling is being managed.
        /// </summary>
        private RollingActiviteFlatFileListener owner;

        /// <summary>
        /// A flag indicating whether at least one rolling criteria has been configured.
        /// </summary>
        private bool performsRolling;

        /// <summary>
        /// Initializes a new instance of the <see cref="StreamWriterHelper"/>.
        /// </summary>
        /// <param name="owner">The <see cref="RollingFlatFileTraceListener"/> to use.</param>
        public StreamWriterHelper(RollingActiviteFlatFileListener owner)
        {
            this.owner = owner;            
        }



        /// <summary>
        /// Gets the file name to use for archiving the file.
        /// </summary>
        /// <param name="actualFileName">The actual file name.</param>
        /// <param name="currentDateTime">The current date and time.</param>
        /// <returns>The new file name.</returns>
        public string ComputeArchiveFileName(string actualFileName, DateTime currentDateTime)
        {
            string directory = Path.GetDirectoryName(actualFileName);
            string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(actualFileName);
            string extension = Path.GetExtension(actualFileName);

            StringBuilder fileNameBuilder = new StringBuilder(fileNameWithoutExtension);
            
            // look for max sequence for date
            int newSequence = FindMaxSequenceNumber(directory, fileNameBuilder.ToString(), extension) + 1;
            fileNameBuilder.Append('.');
            fileNameBuilder.Append(newSequence.ToString(CultureInfo.InvariantCulture));
            

            fileNameBuilder.Append(extension);

            return Path.Combine(directory, fileNameBuilder.ToString());
        }

        /// <summary>
        /// Finds the max sequence number for a log file.
        /// </summary>
        /// <param name="directoryName">The directory to scan.</param>
        /// <param name="fileName">The file name.</param>
        /// <param name="extension">The extension to use.</param>
        /// <returns>The next sequence number.</returns>
        public static int FindMaxSequenceNumber(string directoryName, string fileName, string extension)
        {
            string[] existingFiles =
                Directory.GetFiles(directoryName, string.Format(CultureInfo.InvariantCulture, "{0}*{1}", fileName, extension));

            int maxSequence = 0;
            Regex regex = new Regex(string.Format(CultureInfo.InvariantCulture, @"{0}\.(?<sequence>\d+){1}$", fileName, extension));
            for (int i = 0; i < existingFiles.Length; i++)
            {
                Match sequenceMatch = regex.Match(existingFiles[i]);
                if (sequenceMatch.Success)
                {
                    int currentSequence = 0;

                    string sequenceInFile = sequenceMatch.Groups["sequence"].Value;
                    if (!int.TryParse(sequenceInFile, out currentSequence))
                    {
                        continue; // very unlikely
                    }

                    if (currentSequence > maxSequence)
                    {
                        maxSequence = currentSequence;
                    }
                }
            }

            return maxSequence;
        }

        /// <summary>
        /// Perform the roll for the next date.
        /// </summary>
        /// <param name="rollDateTime">The roll date.</param>
        public void PerformRoll(DateTime rollDateTime)
        {
            string actualFileName = ((FileStream)((StreamWriter)this.owner.Writer).BaseStream).Name;

            // calculate archive name
            string archiveFileName = this.ComputeArchiveFileName(actualFileName, rollDateTime);

            // close file
            this.owner.Writer.Close();

            // move file
            this.SafeMove(actualFileName, archiveFileName, rollDateTime);
        
            // update writer - let TWTL open the file as needed to keep consistency
            this.owner.Writer = null;
            this.managedWriter = null;
            this.nextRollDateTime = null;
            this.UpdateRollingInformationIfNecessary();
        }


        /// <summary>
        /// Updates book keeping information necessary for rolling, as required by the specified rolling configuration.
        /// </summary>
        /// <returns>true if update was successful, false if an error occurred.</returns>
        public bool UpdateRollingInformationIfNecessary()
        {
            StreamWriter currentWriter = null;

            // replace writer with the tally keeping version if necessary for size rolling
            if (this.managedWriter == null)
            {
                currentWriter = this.owner.Writer as StreamWriter;

                if (currentWriter == null)
                {
                    // TWTL couldn't acquire the writer - abort
                    return false;
                }

                var actualFileName = ((FileStream)currentWriter.BaseStream).Name;

                currentWriter.Close();

                FileStream fileStream = null;
                try
                {
                    fileStream = File.Open(actualFileName, FileMode.Append, FileAccess.Write, FileShare.Read);
                    this.managedWriter = new TallyKeepingFileStreamWriter(fileStream, GetEncodingWithFallback());
                }
                catch (IOException)
                {
                    // there's a slight chance of error here - abort if this occurs and just let TWTL handle it without attempting to roll
                    return false;
                }

                this.owner.Writer = this.managedWriter;
            }


            return true;
        }

        private static Encoding GetEncodingWithFallback()
        {
            Encoding encoding = (Encoding)new UTF8Encoding(false).Clone();
            encoding.EncoderFallback = EncoderFallback.ReplacementFallback;
            encoding.DecoderFallback = DecoderFallback.ReplacementFallback;
            return encoding;
        }

        private void SafeMove(string actualFileName, string archiveFileName, DateTime currentDateTime)
        {
            try
            {
                if (File.Exists(archiveFileName))
                {
                    File.Delete(archiveFileName);
                }

                // take care of tunneling issues http://support.microsoft.com/kb/172190
                File.SetCreationTime(actualFileName, currentDateTime);
                File.Move(actualFileName, archiveFileName);
            }
            catch (IOException)
            {
                // catch errors and attempt move to a new file with a GUID
                archiveFileName = archiveFileName + Guid.NewGuid().ToString();

                try
                {
                    File.Move(actualFileName, archiveFileName);
                }
                catch (IOException)
                {
                }
            }
        }
    }

}
