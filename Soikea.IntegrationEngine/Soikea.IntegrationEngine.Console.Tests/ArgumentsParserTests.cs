﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soikea.IntegrationEngine.Console.Tasks;

namespace Soikea.IntegrationEngine.Console.Tests
{
    [DeploymentItem("Moq.dll")]
    [TestClass]
    public class ArgumentsParserTests
    {
        private static Commands GetCommands()
        {
            return new Commands
            {
                new Command("help", () => new HelpTask()),
                new Command("dummy", () => null)
            };
        }

        [TestMethod]
        public void CommandGivenDefaultsToHelp()
        {
            var arguments = new Arguments(new string[0], GetCommands());

            Assert.AreEqual("help", arguments.CommandGiven.CommandString);
        }

        [TestMethod]
        public void CommandGivenInitializesAccordingly()
        {
            var arguments = new Arguments(new string[]{ "dummy" }, GetCommands());

            Assert.AreEqual("dummy", arguments.CommandGiven.CommandString);            
        }

        [TestMethod]
        public void FreeFormArgumentsInitializeAccordingly()
        {
            const string testParam = "foobar:fafafaaf7/fafa";
            var arguments = new Arguments(new string[] { "dummy", testParam }, GetCommands());

            Assert.IsTrue(arguments.CommandLineArgs.Count == 1);
            Assert.AreEqual(testParam, arguments.CommandLineArgs.First());
        }

        [TestMethod]
        public void NamedParametersInitializeAccordingly()
        {
            const string param1 = "first one";
            const string param2 = "the_second 2";

            var arguments = new Arguments(new[] {
                    "dummy",
                    "--param1", "first", "one", 
                    "--param2", "the_second", "2"
                },
                GetCommands());

            Assert.AreEqual(param1, arguments.NamedParameters["param1"]);
            Assert.AreEqual(param2, arguments.NamedParameters["param2"]);
        }
    }
}
