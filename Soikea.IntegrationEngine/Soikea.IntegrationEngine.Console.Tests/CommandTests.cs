﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Soikea.IntegrationEngine.Console.Tests
{
    [TestClass()]
    public class CommandTests
    {
        [TestMethod()]
        public void GetHashCodeDoesEqualOnSameCommandStrings()
        {
            var command1 = new Command("test", () => null);
            var command2 = new Command("test", () => null);

            Assert.AreEqual(command1.GetHashCode(), command2.GetHashCode());
        }

        [TestMethod()]
        public void GetHashCodeDoesNotEqualOnDifferentCommandStrings()
        {
            var command1 = new Command("test", () => null);
            var command2 = new Command("test2", () => null);

            Assert.AreNotEqual(command1.GetHashCode(), command2.GetHashCode());
        }

        [TestMethod()]
        public void EqualsReturnsTrueOnCommandStringMatch()
        {
            var command1 = new Command("test", () => null);
            var command2 = new Command("test", () => null);

            Assert.IsTrue(command1.Equals((Command) command2));
        }

        [TestMethod()]
        public void EqualsReturnsFalseOnCommandStringDifference()
        {
            var command1 = new Command("test", () => null);
            var command2 = new Command("test2", () => null);

            Assert.IsFalse(command1.Equals((Command)command2));
        }

        [TestMethod()]
        public void ObjEqualsReturnsTrueOnCommandStringMatch()
        {
            var command1 = new Command("test", () => null);
            var command2 = new Command("test", () => null);

            Assert.IsTrue(command1.Equals((object)command2));
        }

        [TestMethod()]
        public void ObjEqualsReturnsTrueOnReferenceMatch()
        {
            var command1 = new Command("test", () => null);

            Assert.IsTrue(command1.Equals((object)command1));
        }

        [TestMethod()]
        public void ObjEqualsReturnsFalseOnCommandStringDifference()
        {
            var command1 = new Command("test", () => null);
            var command2 = new Command("test2", () => null);

            Assert.IsFalse(command1.Equals((object)command2));
        }

        [TestMethod()]
        public void ObjEqualsReturnsFalseOnNull()
        {
            var command1 = new Command("test", () => null);
            
            Assert.IsFalse(command1.Equals(null));
        }

        [TestMethod()]
        public void ObjEqualsReturnsFalseOnDifferentType()
        {
            var command1 = new Command("test", () => null);
            var unknown = new {Field = "Text"};

            Assert.IsFalse(command1.Equals(unknown));
        }

    }
}
