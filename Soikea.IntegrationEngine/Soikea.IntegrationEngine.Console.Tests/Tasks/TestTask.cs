﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Soikea.IntegrationEngine.Console.Tasks;

namespace Soikea.IntegrationEngine.Console.Tests.Tasks
{
    public class TestTask : TaskBase
    {

        public const string TestHelpString = "Testing help";

        public static readonly Regex TestPattern = new Regex(@"^([a-zA-Z\-]*) (.*):([a-zA-Z \-]*)$");

        public List<string> Parameters { get; private set; }

        public override string GetHelp()
        {
            return TestHelpString;
        }

        public override void Execute(WriteOutput writeOutputDelegate, IArguments arguments)
        {
            Parameters = ParseFreeFormParameters(TestPattern, arguments.CommandLineArgsString);
        }
    }
}
