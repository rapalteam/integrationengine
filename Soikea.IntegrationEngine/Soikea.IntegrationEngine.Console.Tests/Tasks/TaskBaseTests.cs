﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Soikea.IntegrationEngine.Console.Tests.Tasks
{
    [TestClass()]
    public class TaskBaseTests
    {
        
        [TestMethod()]
        public void GetHelpTest()
        {
            var testTask = new TestTask();

            Assert.IsNotNull(testTask.GetHelp());
        }

        [ExpectedException(typeof(ArgumentException))]
        [TestMethod()]
        public void ExecuteFailsOnInvalidArgumentsPattern()
        {
            var testTask = new TestTask();
            var commands = new Commands { new Command("test", () => new TestTask()) };

            var arguments = new Arguments(new[] { "test", "invalid" }, commands);

            string outputResult = "";
            testTask.Execute((string output) => { outputResult += output; }, arguments);

            Assert.Fail("Should have thrown an ArgumentException");
        }

        [TestMethod()]
        public void ExecuteParsesFreeFormParametersCorrectly()
        {
            var testTask = new TestTask();
            var commands = new Commands { new Command("test", () => new TestTask()) };

            var arguments = new Arguments(new[] { "test", "first", "second:third" }, commands);

            string outputResult = "";
            testTask.Execute((string output) => { outputResult += output; }, arguments);

            Assert.AreEqual(3, testTask.Parameters.Count);
            Assert.AreEqual("first", testTask.Parameters[0]);
            Assert.AreEqual("second", testTask.Parameters[1]);
            Assert.AreEqual("third", testTask.Parameters[2]);
        }
    }
}
