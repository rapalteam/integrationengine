﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soikea.IntegrationEngine.Console.Tasks;

namespace Soikea.IntegrationEngine.Console.Tests.Tasks
{
    [TestClass]
    public class HelpTaskTests
    {        
        [TestMethod]
        public void GetHelpReturnsSomething()
        {
            var helpTask = new HelpTask();
            Assert.IsNotNull(helpTask.GetHelp());
        }

        [TestMethod]
        public void ExecuteOutputsAvailableTasksWhenNoCommandGiven()
        {
            var helpTask = new HelpTask();

            var commands = new Commands {new Command("help", () => new HelpTask())};

            var arguments = new Arguments(new string[0], commands);

            string outputResult = "";
            helpTask.Execute((string output)=> { outputResult += output; }, arguments);

            Assert.IsTrue(outputResult.Contains("Available tasks"));
            Assert.IsTrue(outputResult.Contains(arguments.GetAvailableCommands()));
        }

        [TestMethod]
        public void ExecuteOutputsInvalidArgumentWhenCommandNotAvailable()
        {
            var helpTask = new HelpTask();

            var commands = new Commands { new Command("help", () => new HelpTask()) };

            var arguments = new Arguments(new []{ "help", "doesnotexist" }, commands);

            string outputResult = "";
            helpTask.Execute((string output) => { outputResult += output; }, arguments);

            Assert.AreEqual("Invalid argument: doesnotexist", outputResult);
        }

        [TestMethod]
        public void ExecuteOutputsInvalidArgumentsWhenTryingToUseNamedParameters()
        {
            var helpTask = new HelpTask();

            var commands = new Commands { new Command("help", () => new HelpTask()) };

            var arguments = new Arguments(new[] { "help","--task", "doesnotexist" }, commands);

            string outputResult = "";
            helpTask.Execute((string output) => { outputResult += output; }, arguments);

            Assert.AreEqual("Invalid arguments", outputResult);
        }

        [TestMethod]
        public void ExecuteOutputsTestTasksHelp()
        {
            var helpTask = new HelpTask();

            var commands = new Commands
            {
                new Command("help", () => new HelpTask()),
                new Command("test", () => new TestTask())
            };

            var arguments = new Arguments(new[] { "help", "test" }, commands);

            string outputResult = "";
            helpTask.Execute((string output) => { outputResult += output; }, arguments);

            Assert.AreEqual(TestTask.TestHelpString, outputResult);
            
        }


    }
}
