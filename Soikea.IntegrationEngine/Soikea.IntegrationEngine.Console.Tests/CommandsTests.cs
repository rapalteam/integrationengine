﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Soikea.IntegrationEngine.Console.Tasks;

namespace Soikea.IntegrationEngine.Console.Tests
{
    [DeploymentItem("Moq.dll")]
    [TestClass()]
    public class CommandsTests
    {
        [TestMethod()]
        public void AddSucceeds()
        {
            var commands = new Commands();

            var command = new Command("test", () => null);

            commands.Add(command);

            Assert.AreEqual(1, commands.Count());
        }

        [TestMethod()]
        public void IsAvailableDoesWhat()
        {
            var commands = new Commands();

            var command = new Command("test", () => null);

            commands.Add(command);

            Assert.IsTrue(commands.IsAvailable("test"));
        }

        [TestMethod()]
        public void GetDoesWhat()
        {
            var commands = new Commands();            
            commands.Add(new Command("test", () => null));

            var command = commands.Get("test");

            Assert.AreEqual("test", command.CommandString);
        }

        [TestMethod()]
        public void GetHelpDoesWhat()
        {
            var mockTask = new Mock<TaskBase>();
            mockTask.Setup(x => x.GetHelp())
                .Returns("testhelp");

            var commands = new Commands();
            commands.Add(new Command("test", () => mockTask.Object));

            var commandHelp = commands.GetHelp("test");

            Assert.AreEqual("testhelp", commandHelp);
        }

        [TestMethod()]
        public void GetEnumeratorEnumeratesCommandStringsInCorrectOrder()
        {
            var commands = new Commands();
            commands.Add(new Command("test1", () => null));
            commands.Add(new Command("test2", () => null));
            commands.Add(new Command("test3", () => null));
            commands.Add(new Command("test4", () => null));
            commands.Add(new Command("test5", () => null));


            var enumerated = commands.AsEnumerable().ToArray();

            CollectionAssert.AreEqual(
                new[] { "test1", "test2", "test3", "test4", "test5" },
                enumerated
                );
        }
    }
}
