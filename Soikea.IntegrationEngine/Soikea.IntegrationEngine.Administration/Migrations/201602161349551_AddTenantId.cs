namespace Soikea.IntegrationEngine.Administration.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTenantId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Instances", "TenantId", c => c.String(nullable: false, maxLength: 64, defaultValueSql: "1430"));            
        }
        
        public override void Down()
        {
            DropColumn("dbo.Instances", "TenantId");
        }
    }
}
