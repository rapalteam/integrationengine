namespace Soikea.IntegrationEngine.Administration.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddJobParameters : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.JobParameters",
                c => new
                    {
                        TenantId = c.String(nullable: false, maxLength: 128),
                        JobName = c.String(nullable: false, maxLength: 128),
                        Key = c.String(nullable: false, maxLength: 128),
                        DateTimeOffsetValue = c.DateTimeOffset(nullable: false, precision: 7),
                        StringValue = c.String(),
                        IntValue = c.Int(nullable: false),
                        ValueType = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.TenantId, t.JobName, t.Key });
            
        }
        
        public override void Down()
        {
            DropTable("dbo.JobParameters");
        }
    }
}
