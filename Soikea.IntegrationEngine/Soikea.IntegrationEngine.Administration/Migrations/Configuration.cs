using System;
using Soikea.IntegrationEngine.Administration.Model;
using Soikea.IntegrationEngine.Core.Interfaces;

namespace Soikea.IntegrationEngine.Administration.Migrations
{
    using System.Data.Entity.Migrations;
#if INITIALIZETESTDATA
    public sealed class Configuration : DbMigrationsConfiguration<AdministrationContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(AdministrationContext context)
        {
            var duration = new TimeSpan(0, 0, 1, 0);

            context.Instances.AddOrUpdate(new Instance
            {
                TenantId = "Default",
                CorrelationId = "default-correlation-id",
                StartTime = new DateTimeOffset(2016, 2, 2, 13, 37, 00, new TimeSpan(0,0,0,0)),
                EndTime = new DateTimeOffset(2016, 2, 2, 13, 38, 00, new TimeSpan(0, 0, 0, 0)),
                Duration = duration,
                JobName = "default-job-id",
                Id = 1,               
                Result = JobResultStatus.Ok
            });

            context.Instances.AddOrUpdate(new Instance
            {
                TenantId = "Default",
                CorrelationId = "default-correlation-id2",
                StartTime = new DateTimeOffset(2016, 2, 2, 13, 37, 00, new TimeSpan(0, 0, 0, 0)),
                EndTime = new DateTimeOffset(2016, 2, 2, 13, 38, 00, new TimeSpan(0, 0, 0, 0)),
                Duration = duration,
                JobName = "default-job-id",
                Id = 2,
                Result = JobResultStatus.Ok,
                Payload = "payload"
            });

             context.Instances.AddOrUpdate( new Instance
             {
                 TenantId = "Default",
                 CorrelationId = "correlation-id-for-update-test",
                 StartTime = new DateTimeOffset(DateTime.UtcNow),
                 Result = JobResultStatus.Started,
                 Id = 3,
                 JobName = "startedJob",
             });

            context.Instances.AddOrUpdate(new Instance
            {
                TenantId = "Default",
                CorrelationId = "default-correlation-id3",
                StartTime = new DateTimeOffset(2016, 2, 2, 13, 36, 00, new TimeSpan(0, 0, 0, 0)),
                EndTime = new DateTimeOffset(2016, 2, 2, 13, 37, 00, new TimeSpan(0, 0, 0, 0)),
                Duration = duration,
                JobName = "default-job-id3",
                Id = 4,
                Result = JobResultStatus.Error,
                Payload = "payload3"
            });

            context.Instances.AddOrUpdate(new Instance
            {
                TenantId = "Default",
                CorrelationId = "default-correlation-id4",
                StartTime = new DateTimeOffset(2016, 2, 2, 13, 36, 00, new TimeSpan(0, 0, 0, 0)),
                EndTime = new DateTimeOffset(2016, 2, 2, 13, 37, 00, new TimeSpan(0, 0, 0, 0)),
                Duration = duration,
                JobName = "default-job-id3",
                Id = 5,
                Result = JobResultStatus.Aborted,
                Payload = "payload3"
            });
        }
    }
#else
    public sealed class Configuration : DbMigrationsConfiguration<AdministrationContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }
    }
#endif
}
