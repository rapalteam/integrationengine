namespace Soikea.IntegrationEngine.Administration.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Instances",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Duration = c.Time(nullable: false, precision: 7),
                        StartTime = c.DateTimeOffset(nullable: false, precision: 7),
                        EndTime = c.DateTimeOffset(precision: 7),
                        JobName = c.String(nullable: false, maxLength: 255),
                        Result = c.Int(nullable: false),
                        Payload = c.String(),
                        CorrelationId = c.String(nullable: false, maxLength: 36),
                        PayloadType = c.String(maxLength: 255),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Instances");
        }
    }
}
