// <auto-generated />
namespace Soikea.IntegrationEngine.Administration.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AddJobParameters : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddJobParameters));
        
        string IMigrationMetadata.Id
        {
            get { return "201602241235342_AddJobParameters"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
