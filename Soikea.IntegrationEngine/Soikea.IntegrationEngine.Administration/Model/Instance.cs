﻿using System;
using System.ComponentModel.DataAnnotations;
using Soikea.IntegrationEngine.Core.Interfaces;

namespace Soikea.IntegrationEngine.Administration.Model
{
    public class Instance
    {
        public long Id { get; set; }
        public TimeSpan Duration { get; set; }
        [Required]
        public DateTimeOffset StartTime { get; set; }
        public DateTimeOffset? EndTime { get; set; }
        [Required]
        [StringLength(255)]
        public string JobName { get; set; }
        [Required]
        public JobResultStatus Result { get; set; }
        public string Payload { get; set; }
        [Required]
        [StringLength(36)]
        public string CorrelationId { get; set; }
        [StringLength(255)]
        public string PayloadType { get; set; }
        [Required]
        [StringLength(64)]
        public string TenantId { get; set; }
    }
}
