﻿using System.Data.Entity;

namespace Soikea.IntegrationEngine.Administration.Model
{
    /// <summary>
    /// Code-first model to store and retrieve statistics on
    /// integration job execution instances.
    /// </summary>
    public class AdministrationContext : DbContext
    {
        public virtual IDbSet<Instance> Instances { get; set; }
        public virtual IDbSet<JobParameter> JobParameters { get; set; }
        public AdministrationContext() : base("AdministrationContext")
        {
            
        }
    }
}
