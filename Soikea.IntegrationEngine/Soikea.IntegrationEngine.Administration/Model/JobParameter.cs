﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Soikea.IntegrationEngine.Administration.Model
{
    public class JobParameter
    {
        [Key, Column(Order = 0)]
        public string TenantId { get; set; }
        [Key, Column(Order = 1)]
        public string JobName { get; set; }
        [Key, Column(Order = 2)]
        public string Key { get; set; }
        public DateTimeOffset DateTimeOffsetValue { get; set; }
        public string StringValue { get; set; }
        public int IntValue { get; set; }
        public ValueType ValueType { get; set; }
    }

    public enum ValueType
    {
        Int,
        String,
        DateTimeOffset
    }
}
