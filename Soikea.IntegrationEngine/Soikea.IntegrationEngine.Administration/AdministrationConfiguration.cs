﻿using System;
using System.Reactive.Linq;
using Autofac;
using Soikea.IntegrationEngine.Administration.EventSubscribers;
using Soikea.IntegrationEngine.Core;
using Soikea.IntegrationEngine.Core.Interfaces;
using Soikea.IntegrationEngine.Runtime;

namespace Soikea.IntegrationEngine.Administration
{
    public class AdministrationConfiguration
    {
        private readonly SubscriptionContext _context;

        public const string AdministrationTenantId = "Administration";

        public AdministrationConfiguration(SubscriptionContext context)
        {
            _context = context;
        }

        public void Configure(ILifetimeScope scope)
        {
            IEventSubscriber startJob = scope.Resolve<StartOfJobEventSubscriber>();
            IEventSubscriber endJob = scope.Resolve<EndOfJobEventSubscriber>();
            
            _context.OutputStream
                .Where(w => Equals(w.Category, SubscriptionCategories.StartOfJob))
                .Subscribe(startJob.Receive);

            _context.OutputStream
                .Where(w => Equals(w.Category, SubscriptionCategories.EndOfJob))
                .Subscribe(endJob.Receive);
        } 
    }
}
