﻿using Soikea.IntegrationEngine.Administration.Model;
using Soikea.IntegrationEngine.Administration.Repository;
using Soikea.IntegrationEngine.Core.Implementation.Events;
using Soikea.IntegrationEngine.Core.Interfaces;
using Soikea.IntegrationEngine.Core.Logging;


namespace Soikea.IntegrationEngine.Administration.EventSubscribers
{
    public class StartOfJobEventSubscriber : IEventSubscriber
    {
        private readonly IInstanceRepository _repository;

        public StartOfJobEventSubscriber(IInstanceRepository repository)
        {
            _repository = repository;
        }

        public string Name => "PersistStartOfJobSubscriber"; 
        public void Receive(IEvent @event)
        {
            var start = (StartOfJobEvent) @event;
            Log.Trace($"Persisting StartOfJobEvent from Tenant {start.TenantId} with CorrelationId {start.JobId}");
            _repository.Add(new Instance
                {
                    TenantId = start.TenantId,
                    CorrelationId = start.JobId.ToString(),
                    StartTime = start.StartTime,
                    Result = JobResultStatus.Started,
                    JobName = start.JobName,
                });
            Log.Trace($"Created new instance for correlationId: {start.JobId} Tenant: {start.TenantId}");
        }
    }
}
