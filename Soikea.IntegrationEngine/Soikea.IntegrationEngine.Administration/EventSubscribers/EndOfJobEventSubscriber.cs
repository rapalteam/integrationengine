﻿using Newtonsoft.Json;
using Soikea.IntegrationEngine.Administration.Repository;
using Soikea.IntegrationEngine.Core.Implementation.Events;
using Soikea.IntegrationEngine.Core.Interfaces;
using Soikea.IntegrationEngine.Core.Logging;

namespace Soikea.IntegrationEngine.Administration.EventSubscribers
{
    public class EndOfJobEventSubscriber : IEventSubscriber
    {
        private readonly IInstanceRepository _repository;

        public EndOfJobEventSubscriber(IInstanceRepository repository)
        {
            _repository = repository;
        }

        public string Name => "PersistEndOfJobSubscriber";
        public void Receive(IEvent @event)
        {
            var end = (EndOfJobEvent) @event;
            Log.Trace($"Persisting EndOfJobEvent from Tenant {end.TenantId} for CorrelationId of {end.JobId}");
            var instance = _repository.GetByCorrelationId(end.JobId.ToString());
            if (instance == null)
            {
                Log.Warn($"Could not find instance with id {end.JobId}" );
                return;
            }

            var endrequest = new EndInstance
            {
                CorrelationId = end.JobId.ToString(),
                EndTime = end.EndTime,
                ResultStatus = end.ExecutionDetails.Status,
                Payload = JsonConvert.SerializeObject(end.ExecutionDetails),
                PayloadType = end.ExecutionDetails.GetType().AssemblyQualifiedName
            };

            _repository.EndInstanceExecution(endrequest);
            Log.Trace($"Persisted EndOfJobEvent for CorrelationId: {end.JobId} Tenant: {end.TenantId}");
        }
    }
}
