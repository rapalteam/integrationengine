﻿using Autofac;
using Soikea.IntegrationEngine.Administration.EventSubscribers;
using Soikea.IntegrationEngine.Administration.Jobs;
using Soikea.IntegrationEngine.Administration.Repository;
using Soikea.IntegrationEngine.Core.Implementation.Sql;
using Soikea.IntegrationEngine.Core.Interfaces;
using Soikea.IntegrationEngine.Core.Logging;

namespace Soikea.IntegrationEngine.Administration
{
    public class AdministrationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            Log.Trace("Registering types for AdministartionModule");
            builder.RegisterType<InstanceRepository>().As<IInstanceRepository>();
            builder.RegisterType<EndOfJobEventSubscriber>().SingleInstance();
            builder.RegisterType<StartOfJobEventSubscriber>().SingleInstance();
            builder.RegisterType<AdministrationConfiguration>();
            builder.RegisterType<JobParameterRepository>();
            builder.RegisterType<ClearLogsJob>()
                .Named<IIntegrationJob>("ClearLogsJob")
                .OnActivated((m) =>
                {
                    m.Instance.Settings = m.Context.Resolve<ISettings>();
                    m.Instance.StoredProcedureCallProvider = new StoredProcedureCallProvider();
                });
            
            base.Load(builder);
        }
    }
}
