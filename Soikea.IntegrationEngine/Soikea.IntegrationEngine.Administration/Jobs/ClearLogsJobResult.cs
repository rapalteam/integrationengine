﻿using System;
using Soikea.IntegrationEngine.Core.Implementation;

namespace Soikea.IntegrationEngine.Administration.Jobs
{
    public class ClearLogsJobResult : JobResultBase
    {
        public ClearLogsJobResult(Guid jobId) : base(jobId)
        {
        }

        public override string Name => "ClearLogsJobResult";
    }
}
