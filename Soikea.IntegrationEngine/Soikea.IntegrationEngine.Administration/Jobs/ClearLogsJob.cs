﻿using System;
using System.Diagnostics.Contracts;
using Soikea.IntegrationEngine.Core;
using Soikea.IntegrationEngine.Core.Implementation;
using Soikea.IntegrationEngine.Core.Implementation.Sql;
using Soikea.IntegrationEngine.Core.Interfaces;
using Soikea.IntegrationEngine.Core.Logging;

namespace Soikea.IntegrationEngine.Administration.Jobs
{
    public class ClearLogsJob : SynchronousIntegrationJob
    {
        public override string Name => "ClearLogsJob";

        public const string LogDatabaseConnectionStringName = "LoggingConnection";

        public override IJobResult CreateJobResult(Guid jobId)
        {
            return new ClearLogsJobResult(jobId);
        }

        public ISettings Settings { get; set; }

        public IStoredProcedureCallProvider StoredProcedureCallProvider { get; set; }

        public override JobResultStatus Execute(IJobAbortionWatchDog jobAbortionWatchDog, Constants.PublishEvent publishInput, Constants.PublishEvent publishOutput)
        {
            Contract.Requires(Settings != null);
            Contract.Requires(StoredProcedureCallProvider != null);

            var connectionStringName = Settings.ContainsKey(LogDatabaseConnectionStringName) 
                ? Settings.GetValue(LogDatabaseConnectionStringName)
                : "Logging";

            Log.Info($"Starting ClearLogs operation on connection '{connectionStringName}'");
            var timer = new LogTimer((message => Log.Info(message)), true);

            try
            {
                var successful = StoredProcedureCallProvider.ExecuteProcedureCall(connectionStringName, "ClearLogs", jobAbortionWatchDog);

                var wasSuccessful = successful ? "successful" : "failed";
                timer.StopAndLog($"ClearLogs operation completed with {wasSuccessful} result");

                return successful ? JobResultStatus.Ok : JobResultStatus.Error;
            }
            catch (JobAbortedException)
            {
                Log.Info("Received abortion signal => aborting ClearLogs-procedure call");
                return JobResultStatus.Aborted;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "An unexpected error happened calling ClearLogs-procedure");
                return JobResultStatus.Error;
            }            
        }
    }
}
