﻿using System;
using System.Collections.Generic;
using Soikea.IntegrationEngine.Administration.Model;
using Soikea.IntegrationEngine.Core.Interfaces;

namespace Soikea.IntegrationEngine.Administration.Repository
{
    public interface IInstanceRepository
    {
        IEnumerable<Instance> Get(DateTimeOffset start, DateTimeOffset end);
        IEnumerable<Instance> GetByStatus(IEnumerable<JobResultStatus> statuses);
        IEnumerable<Instance> GetRunningInstances();
        void Add(Instance instance);
        void EndInstanceExecution(EndInstance end);
        Instance GetByCorrelationId(string correlationId);
        IEnumerable<Instance> Get(DateTimeOffset start, DateTimeOffset end, JobResultStatus status);
    }
}