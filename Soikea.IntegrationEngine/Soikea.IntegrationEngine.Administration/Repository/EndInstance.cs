using System;
using Soikea.IntegrationEngine.Core.Interfaces;

namespace Soikea.IntegrationEngine.Administration.Repository
{
    public class EndInstance
    {
        public string CorrelationId { get; set; }
        public DateTimeOffset EndTime { get; set; }
        public string Payload { get; set; }
        public string PayloadType { get; set; }
        public JobResultStatus ResultStatus { get; set; }
    }
}