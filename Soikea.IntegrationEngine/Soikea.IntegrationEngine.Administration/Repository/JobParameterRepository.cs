﻿using System;
using System.Collections.Generic;
using System.Linq;
using Soikea.IntegrationEngine.Administration.Model;
using Soikea.IntegrationEngine.Core.Logging;
using ValueType = Soikea.IntegrationEngine.Administration.Model.ValueType;

namespace Soikea.IntegrationEngine.Administration.Repository
{
    public class JobParameterRepository : IJobParameterRepository
    {
        //For automatic factory creation in autofac
        public delegate JobParameterRepository Factory(string tenantId, string jobName);

        private string JobName { get; set; }
        private string TenantId { get; set; }
 
        private static object writeLock = new object();

        public JobParameterRepository(string tenantId, string jobName)
        {
            JobName = jobName;
            TenantId = tenantId;
        }

        private readonly List<string> _supportedTypes = new List<string>
        {
            "System.Int32",
            "System.String",
            "System.DateTimeOffset"
        };

        /// <summary>
        /// Finds the parameter of matching to type T and key. 
        /// </summary>
        /// <typeparam name="T">requested type</typeparam>
        /// <param name="key">key</param>
        /// <returns>Returns default value of T if the entry does not contain the requested type value</returns>
        public T GetParameter<T>(string key)
        {
            var type = typeof (T).ToString();

            if (!IsSupported(type))
            {
                throw new NotSupportedException();
            }

            using (var context = new AdministrationContext())
            {
                var param = context.JobParameters
                    .SingleOrDefault( w => w.Key.Equals(key) && w.JobName.Equals(JobName) && w.TenantId == TenantId );

                if (param == null) return default(T);

                object returnValue = null;

                switch (type)
                {
                    case "System.Int32":
                    {
                        returnValue = param.IntValue;
                        break;
                    }
                    case "System.String":
                    {
                        returnValue = param.StringValue;
                        break;
                    }
                    case "System.DateTimeOffset":
                    {
                        returnValue = param.DateTimeOffsetValue;
                        break;
                    }
                }
                return returnValue == null ? default(T) : (T) returnValue;
            }
        }

        /// <summary>
        /// Adds or if the key is already in db, removes the old entry and then adds the new entry.
        /// </summary>
        /// <typeparam name="T">type of the entry</typeparam>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void AddOrUpdateParameter<T>(string key, object value)
        {
            var type = typeof (T).ToString();
            if (!IsSupported(type))
            {
                throw new NotSupportedException();
            }

            using (var context = new AdministrationContext())
            {

                var jobParameter = new JobParameter
                {
                    Key = key,
                    TenantId = TenantId,
                    JobName = JobName
                };

                switch (type)
                {
                    case "System.Int32":
                    {
                        jobParameter.IntValue = (int) value;
                        jobParameter.ValueType = ValueType.Int;
                        break;
                    }
                    case "System.String":
                    {
                        jobParameter.StringValue = (string) value;
                        jobParameter.ValueType = ValueType.String;
                        break;
                    }
                    case "System.DateTimeOffset":
                    {
                        jobParameter.DateTimeOffsetValue = (DateTimeOffset) value;
                        jobParameter.ValueType = ValueType.DateTimeOffset;
                        break;
                    }
                }
                
                lock (writeLock)
                {
                    var existingParameter = context.JobParameters.FirstOrDefault(
                        w => w.Key.Equals(key) && w.JobName.Equals(JobName) && w.TenantId == TenantId);

                    if (existingParameter != null)
                    {
                        context.JobParameters.Remove(existingParameter);
                    }

                    context.JobParameters.Add(jobParameter);

                    context.SaveChanges();
                }   
            }
        }

        private bool IsSupported(string type)
        {
            return _supportedTypes.Contains(type);
        }
    }

 
}



