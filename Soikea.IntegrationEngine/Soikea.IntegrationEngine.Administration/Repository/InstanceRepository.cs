﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Linq;
using Soikea.IntegrationEngine.Administration.Model;
using Soikea.IntegrationEngine.Core.Interfaces;

namespace Soikea.IntegrationEngine.Administration.Repository
{
    public class InstanceRepository : IInstanceRepository
    {
        public IEnumerable<Instance> Get(DateTimeOffset start, DateTimeOffset end)
        {
            using (var context = new AdministrationContext())
            {
                return context.Instances
                    .Where(w => w.StartTime >= start && w.StartTime <= end)
                    .OrderByDescending(x=> x.StartTime)
                    .ToList();
            }
        }
        public IEnumerable<Instance> Get(DateTimeOffset start, DateTimeOffset end, JobResultStatus status)
        {
            using (var context = new AdministrationContext())
            {
                return context.Instances
                    .Where(w => w.StartTime >= start && w.StartTime <= end && w.Result == status)
                    .OrderByDescending(x => x.StartTime)
                    .ToList();
            }
        }

        public IEnumerable<Instance> GetByStatus(IEnumerable<JobResultStatus> statuses)
        {
            using (var context = new AdministrationContext())
            {
                return context.Instances
                    .Where(w => statuses.Contains(w.Result))
                    .OrderByDescending(x => x.StartTime)
                    .ToList();
            }
        }


        public IEnumerable<Instance> GetRunningInstances()
        {
            using (var context = new AdministrationContext())
            {
                return context.Instances
                    .Where(w => w.Result == JobResultStatus.Started)
                    .ToList();
            }
        } 

        public void Add(Instance instance)
        {
            using (var context = new AdministrationContext())
            {
                var old = context.Instances
                    .FirstOrDefault(w => w.CorrelationId == instance.CorrelationId);
                if (old != null)
                {
                    throw new InvalidOperationException($"Trying to add Instance with duplicate CorrelationId of {instance.CorrelationId}");
                }
                context.Instances.Add(instance);
                context.SaveChanges();
            }
        }

        public void EndInstanceExecution(EndInstance end)
        {
            using (var context = new AdministrationContext())
            {
                var instance = context.Instances
                    .FirstOrDefault(w => w.CorrelationId == end.CorrelationId);

                if (instance == null)
                {
                    throw new ObjectNotFoundException();
                }

                instance.EndTime = end.EndTime;
                instance.Payload = end.Payload;
                instance.PayloadType = end.PayloadType;
                instance.Duration = end.EndTime - instance.StartTime;
                instance.Result = end.ResultStatus;
                context.SaveChanges();
            }
        }

        public Instance GetByCorrelationId(string correlationId)
        {
            using (var context = new AdministrationContext())
            {
                return context.Instances
                    .FirstOrDefault(w => w.CorrelationId.Equals(correlationId));
            }
        }


    }

}
