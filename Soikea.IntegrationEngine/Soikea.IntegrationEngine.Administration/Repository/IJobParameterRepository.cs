﻿namespace Soikea.IntegrationEngine.Administration.Repository
{
    public interface IJobParameterRepository
    {
        T GetParameter<T> (string key);
        void AddOrUpdateParameter<T>(string key, object value);
    }
}
