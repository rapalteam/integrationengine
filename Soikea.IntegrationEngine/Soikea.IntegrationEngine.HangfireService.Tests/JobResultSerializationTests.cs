﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soikea.IntegrationEngine.Core.Implementation;
using Soikea.IntegrationEngine.Core.Implementation.Events;
using Soikea.IntegrationEngine.Core.Interfaces;
using Soikea.IntegrationEngine.HangfireConfiguration.Configuration.Hangfire;

namespace Soikea.IntegrationEngine.HangfireService.Tests
{
    [TestClass]
    public class JobResultSerializationTests
    {

        [TestMethod]
        public void FormatJobResultReturnsAllFieldsWhenStatusIsOk()
        {
            var jobId = Guid.NewGuid();
            var jobresult = new TestJobResult(jobId)
            {
                Status = JobResultStatus.Ok,
                ExecutionDetails = new EndOfJobPayload("Tenant", jobId, "TestJobName", JobResultStatus.Ok, 99, 88)
            };

            var formatted = JobResultFormatter.Format(jobresult);

            Assert.IsNotNull(formatted);
            
            Assert.IsTrue(formatted.Contains("JobId: "+jobId));
            Assert.IsTrue(formatted.Contains("Status: Ok"));
            Assert.IsTrue(formatted.Contains("TenantId: Tenant"));
            Assert.IsTrue(formatted.Contains("JobName: TestJobName"));
            Assert.IsTrue(formatted.Contains("Input events published: 99"));
            Assert.IsTrue(formatted.Contains("Output events published: 88"));
        }

        [TestMethod]
        public void FormatJobResultReturnsJobIdWithStatusWhenStatusIsNotOk()
        {
            var jobId = Guid.NewGuid();
            var jobresult = new TestJobResult(jobId)
            {
                Status = JobResultStatus.None
            };

            var formatted = JobResultFormatter.Format(jobresult);

            Assert.IsNotNull(formatted);

            Assert.IsTrue(formatted.Contains("Status: None"));
            Assert.IsTrue(formatted.Contains("JobId: " + jobId));
        }
    }

    public class TestJobResult : JobResultBase
    {
        public TestJobResult(Guid jobId) : base(jobId)
        {
        }

        public override string Name => "TestJobResult";
    }
}
