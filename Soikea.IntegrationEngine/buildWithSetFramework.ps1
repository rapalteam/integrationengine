$path = Resolve-Path .
$engineDir = "`"$path\..\IntegrationEngine\Soikea.IntegrationEngine\`""
$enginePsake = $engineDir.Replace('\"', '\').Replace('"C', "C") + "engine.ps1"
psake "`'$enginePsake`'" -framework '4.6'
