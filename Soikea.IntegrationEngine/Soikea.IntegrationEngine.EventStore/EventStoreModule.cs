﻿using Autofac;
using Soikea.IntegrationEngine.Core.Interfaces;

namespace Soikea.IntegrationEngine.EventStore
{
    public class EventStoreModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(c=> new EventStoreRepository(c.Resolve<ISettings>())).As<IEventStoreRepository>();

            builder.RegisterType<OutputEventStoringMessageAdapter>()
                .OnActivated(b =>
                {
                    b.Instance.EventStoreRepository = b.Context.Resolve<IEventStoreRepository>();
                });

            base.Load(builder);
        }
    }
}
