﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics.Contracts;
using System.Linq;
using Newtonsoft.Json;
using Soikea.IntegrationEngine.Core.Interfaces;
using Soikea.IntegrationEngine.Core.Logging;
using Soikea.IntegrationEngine.EventStore.Model;
using SubscriptionCategory = Soikea.IntegrationEngine.Core.Interfaces.SubscriptionCategory;

namespace Soikea.IntegrationEngine.EventStore
{
    public interface IEventStoreRepository
    {
        long PersistEvent(IEvent evnt, EventStream direction);
        IEvent GetEventPayload(long eventId);
        IEnumerable<IntegrationEvent> GetEvents(DateTimeOffset start, DateTimeOffset end);
        IEnumerable<IntegrationEvent> GetByJobId(string jobId);
        IEnumerable<IntegrationEvent> GetByTenant(DateTimeOffset start, DateTimeOffset end, string tenantId);
        IEnumerable<IntegrationEvent> GetByDirection(DateTimeOffset start, DateTimeOffset end, EventStream direction);
        IEnumerable<IntegrationEvent> GetBySubscriptionCategory(DateTimeOffset start, DateTimeOffset end, string category);
        IntegrationEvent GetByEventId(long eventId);
    }

    public class EventStoreRepository : IEventStoreRepository
    {
        public const string ConnectionStringSetting = "EventStoreConnection";
        public readonly string ConnectionString;

        private static readonly object AddLock = new object();

        public EventStoreRepository(ISettings settings)
        {
            Contract.Requires(settings.ContainsKey(ConnectionStringSetting));

            ConnectionString = settings.GetValue(ConnectionStringSetting);
        }


        public long PersistEvent(IEvent evnt, EventStream direction)
        {
            using (var context = new EventStoreContext(ConnectionString))
            {
                lock (AddLock)
                {
                    var tenant = EnsureTenant(evnt.TenantId, context);
                    var category = EnsureSubscriptionCategory(evnt.Category, context);

                    var eventTypeName = evnt.GetType().AssemblyQualifiedName;

                    var eventToPersist = new IntegrationEvent()
                    {
                        Tenant = tenant,
                        SubscriptionCategory = category,
                        Direction = direction,
                        JobId = evnt.JobId,
                        PublishedAt = DateTimeOffset.Now,           
                        TypeName = eventTypeName,
                        Payload = JsonConvert.SerializeObject(evnt)
                    };

                    context.IntegrationEvents.Add(eventToPersist);

                    context.SaveChanges();        

                    Log.Info($"Persisted an event #{eventToPersist.Id} of type {eventTypeName} to category {evnt.Category.Name} into eventstore from {direction}-stream on job {evnt.JobId}");

                    return eventToPersist.Id;                                
                }

            }
        }

        public IEvent GetEventPayload(long eventId)
        {
            using (var context = new EventStoreContext(ConnectionString))
            {
                var evnt = context.IntegrationEvents.Single(x => x.Id == eventId);
                var eventType = Type.GetType(evnt.TypeName);
                
                try
                {
                    var payload = JsonConvert.DeserializeObject(evnt.Payload, eventType);

                    Log.Trace($"Getting an event #{evnt.Id} of type {evnt.TypeName}");

                    return (IEvent)payload;
                }
                catch (Exception ex)
                {
                    Log.Error(ex, $"An error deserializing an event #{evnt.Id} of type {evnt.TypeName}");
                    throw;
                }
            }
        }

        public IEnumerable<IntegrationEvent> GetEvents(DateTimeOffset start, DateTimeOffset end)
        {
            using (var context = new EventStoreContext(ConnectionString))
            {
                return IncludeTenant(IncludeSubscriptionCategory(
                        context.IntegrationEvents
                        .Where(w => w.PublishedAt > start && w.PublishedAt <= end)))
                    .ToList();
            }
        }

        public IEnumerable<IntegrationEvent> GetByJobId(string jobId)
        {
            using (var context = new EventStoreContext(ConnectionString))
            {
                return
                    IncludeTenant(IncludeSubscriptionCategory(
                        context.IntegrationEvents
                        .Where(w => w.JobId.ToString().Equals(jobId))))
                    .ToList();
            }
        }

        public IEnumerable<IntegrationEvent> GetByTenant(DateTimeOffset start, DateTimeOffset end, string tenantId)
        {
            using (var context = new EventStoreContext(ConnectionString))
            {
                return
                    IncludeSubscriptionCategory(IncludeTenant(
                        context.IntegrationEvents
                        .Where(w => w.Tenant.Identifier.Contains(tenantId) && w.PublishedAt > start && w.PublishedAt < end)))
                    .ToList();
            }
        }

        public IEnumerable<IntegrationEvent> GetByDirection(DateTimeOffset start, DateTimeOffset end, EventStream direction)
        {
            using (var context = new EventStoreContext(ConnectionString))
            {
                return
                    IncludeTenant(IncludeSubscriptionCategory(
                        context.IntegrationEvents
                        .Where(w => w.Direction == direction && w.PublishedAt > start && w.PublishedAt < end)))
                    .ToList();
            }
        }

        public IEnumerable<IntegrationEvent> GetBySubscriptionCategory(DateTimeOffset start, DateTimeOffset end, string category)
        {
            using (var context = new EventStoreContext(ConnectionString))
            {
                return
                    IncludeSubscriptionCategory(IncludeTenant(
                        context.IntegrationEvents
                        .Where(w =>
                            w.SubscriptionCategory.CategoryKey.Contains(category) && w.PublishedAt > start &&
                            w.PublishedAt < end)))
                    .ToList();
            }
        }

        public IntegrationEvent GetByEventId(long eventId)
        {
            using (var context = new EventStoreContext(ConnectionString))
            {
                return
                    IncludeSubscriptionCategory(IncludeTenant(
                        context.IntegrationEvents
                        .Where(w => w.Id == eventId)))
                    .First();
            }
        }

        private IQueryable<IntegrationEvent> IncludeTenant(IQueryable<IntegrationEvent> events)
        {
            return events.Include("Tenant");
        }

        private IQueryable<IntegrationEvent> IncludeSubscriptionCategory(IQueryable<IntegrationEvent> events)
        {
            return events.Include("SubscriptionCategory");

        } 
        private static Tenant EnsureTenant(string tenantId, EventStoreContext context)
        {
            var ensured = context.Tenants.FirstOrDefault(x => x.Identifier == tenantId);

            if (ensured != null)
            {
                return ensured;
            }

            ensured = new Tenant()
            {
                Identifier = tenantId
            };

            context.Tenants.Add(ensured);

            return ensured;
        }

        private static Model.SubscriptionCategory EnsureSubscriptionCategory(SubscriptionCategory category, EventStoreContext context)
        {
            var ensured = context.SubscriptionCategories.FirstOrDefault(x => x.CategoryKey == category.Name);

            if (ensured != null)
            {
                return ensured;
            }

            ensured = new Model.SubscriptionCategory()
            {
                CategoryKey = category.Name
            };

            context.SubscriptionCategories.Add(ensured);

            return ensured;
        }
    }
}
