﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Soikea.IntegrationEngine.Core.Interfaces;

namespace Soikea.IntegrationEngine.EventStore.Model
{
    public class IntegrationEvent
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Required]
        public virtual Tenant Tenant { get; set; }

        [Required]
        public Guid JobId { get; set; }

        [Required]
        public virtual SubscriptionCategory SubscriptionCategory { get; set; }

        [Required]
        public EventStream Direction { get; set; }

        [Required]
        public DateTimeOffset PublishedAt { get; set; }

        [Required]
        [StringLength(255)]
        public string TypeName { get; set; }

        [Required]
        public string Payload { get; set; }

    }
}
