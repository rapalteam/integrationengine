namespace Soikea.IntegrationEngine.EventStore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "eventStore.IntegrationEvents",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        JobId = c.Guid(nullable: false),
                        Direction = c.Int(nullable: false),
                        PublishedAt = c.DateTimeOffset(nullable: false, precision: 7),
                        Payload = c.String(nullable: false),
                        SubscriptionCategory_Id = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("eventStore.SubscriptionCategories", t => t.SubscriptionCategory_Id, cascadeDelete: true)
                .Index(t => t.SubscriptionCategory_Id);
            
            CreateTable(
                "eventStore.SubscriptionCategories",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        CategoryKey = c.String(nullable: false, maxLength: 255),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("eventStore.IntegrationEvents", "SubscriptionCategory_Id", "eventStore.SubscriptionCategories");
            DropIndex("eventStore.IntegrationEvents", new[] { "SubscriptionCategory_Id" });
            DropTable("eventStore.SubscriptionCategories");
            DropTable("eventStore.IntegrationEvents");
        }
    }
}
