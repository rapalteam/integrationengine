namespace Soikea.IntegrationEngine.EventStore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTenantAndTypeName : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "eventStore.Tenants",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Identifier = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("eventStore.IntegrationEvents", "TypeName", c => c.String(nullable: false, maxLength: 255));
            AddColumn("eventStore.IntegrationEvents", "Tenant_Id", c => c.Long(nullable: false));
            CreateIndex("eventStore.IntegrationEvents", "Tenant_Id");
            AddForeignKey("eventStore.IntegrationEvents", "Tenant_Id", "eventStore.Tenants", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("eventStore.IntegrationEvents", "Tenant_Id", "eventStore.Tenants");
            DropIndex("eventStore.IntegrationEvents", new[] { "Tenant_Id" });
            DropColumn("eventStore.IntegrationEvents", "Tenant_Id");
            DropColumn("eventStore.IntegrationEvents", "TypeName");
            DropTable("eventStore.Tenants");
        }
    }
}
