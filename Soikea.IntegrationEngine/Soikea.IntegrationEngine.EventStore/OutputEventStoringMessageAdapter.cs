﻿using Soikea.IntegrationEngine.Core;
using Soikea.IntegrationEngine.Core.Interfaces;
using Soikea.IntegrationEngine.Core.Logging;

namespace Soikea.IntegrationEngine.EventStore
{
    public class OutputEventStoringMessageAdapter : IEventStoringStrategy
    {
        public const string RegisterName = "OutputEventStoringMessageAdapter";
        public string Name => RegisterName;

        public IEventStoreRepository EventStoreRepository { get; set; }
        
        public void Receive(IEvent @event)
        {
            var asStorable = @event as IStorableEvent;
            if (asStorable == null)
            {
                return;
            }

            var eventId = EventStoreRepository.PersistEvent(@event, EventStream.Output);
            Log.Trace($"Stored an output-event of {@event.Category.Name} to event-store with id {eventId} from job {@event.JobId} for tenant {@event.TenantId}");
        }

        public EventStream EventStream => EventStream.Output;
    }
}
