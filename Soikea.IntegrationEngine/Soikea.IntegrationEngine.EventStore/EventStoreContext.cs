﻿using System.Data.Entity;
using Soikea.IntegrationEngine.EventStore.Model;

namespace Soikea.IntegrationEngine.EventStore
{
    public class EventStoreContext : DbContext
    {
        public IDbSet<Tenant> Tenants { get; set; }
        public IDbSet<SubscriptionCategory> SubscriptionCategories { get; set; }
        public IDbSet<IntegrationEvent> IntegrationEvents { get; set; }

        public EventStoreContext() : base("EventStore")
        {
            
        }

        public EventStoreContext(string connectionString) : base(connectionString)
        {
            
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("eventStore");

            base.OnModelCreating(modelBuilder);
        }
    }
}
