properties {
	$base_dir = Resolve-Path .
	$configuration = "Rambo"
	$sln = "Soikea.IntegrationEngine.sln"
	$outputDir = "$base_dir\..\Dist\"
	
	$mstest_assemblies = 
		"`"$outputDir\Soikea.IntegrationEngine.Console.Tests.dll`"",
		"`"$outputDir\Soikea.IntegrationEngine.Core.Tests.dll`"",
		"`"$outputDir\Soikea.IntegrationEngine.EventStore.Tests.dll`"",
		"`"$outputDir\Soikea.IntegrationEngine.HangfireService.Tests.dll`""		
}

task default -depends Build
task Build -depends RestorePackages, Compile, Test

formatTaskName {
	param($taskName)
	write-host $taskName -foregroundcolor Green
}

task Clean {
	if (Test-Path $outputDir) 
	{	
		rd $outputDir -rec -force | out-null
	}
	
	mkdir $outputDir | out-null
	
	&msbuild $sln /p:Configuration=$configuration /t:Clean /v:Quiet 
	if ($lastexitcode -ne 0) {
		throw "Clean failed"
	}
}

task Compile {
	&msbuild $sln /p:Configuration=$configuration /t:Build /v:Quiet /p:OutDir=$outputDir
	if ($lastexitcode -ne 0) {
		throw "Compilation failed"
	}
}

task Test {
	$failedTests = ""
	
	foreach($test in $mstest_assemblies) {
		&mstest /testcontainer:$test
		if ($lastexitcode -ne 0) {
			$failedTests += " - " + $test 
		}
	}
	# if tests fail, the script should fail too.
	if ($failedTests.length -gt 1) {
	#	throw `"Tests failed: $failedTests`"
	}
}

task RestorePackages {
	Exec { nuget restore }
}
