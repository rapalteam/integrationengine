﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atlas;
using Autofac;
using Autofac.Extras.Multitenant;
using Hangfire;
using Microsoft.Owin.Hosting;
using Soikea.IntegrationEngine.Administration;
using Soikea.IntegrationEngine.Core.Interfaces;
using Soikea.IntegrationEngine.Core.Logging;
using Soikea.IntegrationEngine.HangfireConfiguration.Configuration.Administration;
using Soikea.IntegrationEngine.HangfireConfiguration.Configuration.EventStore;
using Soikea.IntegrationEngine.HangfireConfiguration.Configuration.Jobs;
using Soikea.IntegrationEngine.HangfireConfiguration.Configuration.Tenants;

namespace Soikea.IntegrationEngine.HangfireService
{

    public class EngineService : IAmAHostedProcess
    {
        
        private readonly Dictionary<Tenant, BackgroundJobServer> _servers = new Dictionary<Tenant, BackgroundJobServer>();
        private ILifetimeScope _scope;
        private Dictionary<Tenant, List<IMessageAdapter>> _registeredMessageAdapters = new Dictionary<Tenant, List<IMessageAdapter>>();
        private List<IEventStoringStrategy> _eventStoringStrategies;
        private static IDisposable _hangfireDashBoard;

        public AdministrativeConfiguration AdministrativeConfiguration { get; set; }
        public ITenants Tenants { get; set; }
        

        /// <summary>
        /// Starts the Windows Service.
        /// </summary>
        public void Start()
        {
            Log.Info("Integration-Engine Windows Service starting");            
            WarmUp();
            Log.Info("Integration-Engine Windows Service started");
        }


        /// <summary>
        /// Stops the Windows Service.
        /// </summary>
        public void Stop()
        {
            
            Log.Info("Integration-Engine Windows Service stopping");                        
            CoolDown(true);
            Log.Info("Integration-Engine Windows Service stopped");
        }

        /// <summary>
        /// Resumes the Windows Service.
        /// </summary>
        public void Resume()
        {
            Log.Info("Integration-Engine Windows Service resuming");
            WarmUp();
            Log.Info("Integration-Engine Windows Service resumed");
        }

        /// <summary>
        /// Pauses the Windows Service.
        /// </summary>
        public void Pause()
        {
            Log.Info("Integration-Engine Windows Service pausing");
            CoolDown(false);
            Log.Info("Integration-Engine Windows Service paused");
        }
#if STARTDASHBOARD
        private static void StartHangfireDashboard()
        {            
            Log.Info("Starting Hangfire dashboard @ http://localhost:88/Hangfire");
            try
            {                
                _hangfireDashBoard = WebApp.Start<StartupDashboard>("http://localhost:88/");

                Log.Info("Started Hangfire-dashboard @ http://localhost:88/Hangfire");
            }
            catch (Exception e)
            {
                Log.Fatal(e, "Could not start Hangfire dashboard");
                throw;
            }

        }
#endif
        private void WarmUp()
        {
            GlobalConfiguration.Configuration.UseSqlServerStorage("Hangfire");
#if STARTDASHBOARD
            StartHangfireDashboard();
#endif
            if (_scope == null)
            {
                TenantExecutionContext.Current = null; // Make sure there will be no false tenant identification    

                _scope = ContainerProvider.Instance.ApplicationContainer.BeginLifetimeScope("service");
                // Setup for hangfire
                GlobalConfiguration.Configuration.UseAutofacMultitenantActivator(ContainerProvider.Instance.ApplicationContainer as MultitenantContainer);

                // Register event-storing strategies
                var eventStoreConfiguration = _scope.Resolve<EventStoreConfiguration>();
                _eventStoringStrategies = eventStoreConfiguration.Configure(_scope);

                var administrationConfiguration = _scope.Resolve<AdministrationConfiguration>();
                administrationConfiguration.Configure(_scope);
            }

            var configureTenants = new List<Tenant>() {AdministrativeConfiguration.AdministrativeTenant}
                .Union(Tenants.All);

            foreach (var tenant in configureTenants)
            {                
                TenantExecutionContext.Current = new TenantExecutionContext(tenant.TenantId);
                StartBackgroundJobServer(tenant);

                var tenantscope = (ContainerProvider.Instance.ApplicationContainer as MultitenantContainer)?.GetCurrentTenantScope();

                if (tenantscope == null) throw new Exception($"Could not resolve TenantScope for registering message-adapters for tenant {tenant.TenantId}");

                var tenantMessageAdapterConf = tenantscope.Resolve<TenantMessageAdapterConfiguration>();
                var tenantAdapters = tenantMessageAdapterConf.Configure(tenant);
                _registeredMessageAdapters[tenant] = tenantAdapters;

                var jobsettings = tenantscope.Resolve<JobSettings>();
                TenantJobConfiguration.Configure(tenant, jobsettings);

            }

            TenantExecutionContext.Current = null; // Make sure there will be no false tenant identification
        }

        private void StartBackgroundJobServer(Tenant tenant)
        {
            Log.Info($"Starting hangfire backgroundjob-server for tenant {tenant.TenantId}");
            _servers[tenant] = new BackgroundJobServer(new BackgroundJobServerOptions()
            {
                ServerName = tenant.TenantId,
                WorkerCount = 20,
                Queues = new[]
                {
                    $"{tenant.TenantId}_{TenantJobConfiguration.QueuePriority}".ToLower(),
                    $"{tenant.TenantId}_{TenantJobConfiguration.QueueDefault}".ToLower(),
                    $"{tenant.TenantId}_{TenantJobConfiguration.QueueSecondary}".ToLower()
                }
            }, JobStorage.Current);
        }

        private void CoolDown(bool endScope)
        {
            foreach (var tenantServer in _servers)
            {
                tenantServer.Value.Dispose();    
            }            

            if (endScope) _scope.Dispose();

            _hangfireDashBoard?.Dispose();
        }
    }
}
