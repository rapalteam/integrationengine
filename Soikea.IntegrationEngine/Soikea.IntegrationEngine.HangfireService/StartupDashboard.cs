﻿using Hangfire;
using Owin;

namespace Soikea.IntegrationEngine.HangfireService
{
    public class StartupDashboard
    {
        public void Configuration(IAppBuilder app)
        {            
            app.UseHangfireDashboard();            
        }
    }
}


