﻿using System;
using System.Configuration;
using System.Linq;
using Atlas;
using Autofac;
using Hangfire.Logging;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Soikea.IntegrationEngine.Administration;
using Soikea.IntegrationEngine.Core.Implementation.Settings;
using Soikea.IntegrationEngine.Core.Interfaces;
using Soikea.IntegrationEngine.Core.Logging;
using Soikea.IntegrationEngine.Core.Logging.Providers;
using Soikea.IntegrationEngine.EventStore;
using Soikea.IntegrationEngine.HangfireConfiguration.Configuration;
using Soikea.IntegrationEngine.HangfireConfiguration.Configuration.Administration;
using Soikea.IntegrationEngine.HangfireConfiguration.Configuration.Settings;
using Soikea.IntegrationEngine.HangfireConfiguration.Configuration.Tenants;
using Logger = Microsoft.Practices.EnterpriseLibrary.Logging.Logger;

namespace Soikea.IntegrationEngine.HangfireService
{
    public class Program
    {

        static void Main(string[] args)
        {
            // Engine setup
            DatabaseFactory.SetDatabaseProviderFactory(new DatabaseProviderFactory());
            Logger.SetLogWriter(new LogWriterFactory().Create());
            Log.SetCurrentLogProvider(new EntLibLogProvider(), "IntegrationJobs");

            // Hangfire setup
            LogProvider.SetCurrentLogProvider(new Hangfire.Logging.LogProviders.EntLibLogProvider());     
                   
            try
            {
                Log.Info("Reading app.config for IntegrationEngine Configuration-group");
                var appConfig = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                var config = (IntegrationEngineConfigurationSectionGroup)appConfig.GetSectionGroup("integrationEngine");

                if (config == null)
                {
                    Log.Warn("Could not load IntegrationEngineConfiguration from app.config");
                    throw new ConfigurationErrorsException("Could not load IntegrationEngineConfiguration from app.config");
                }

                Log.Info("Reading app.config for global settings");
                var globalSettings = config.GlobalSettings;

                Log.Info("Reading app.config for encryptedSettings");
                var encryptedSettings = config.EncryptedSettings;

                if (encryptedSettings.SectionInformation.IsProtected)
                {
                    Log.Info("Unprotecting encryptedSettings");
                    encryptedSettings.SectionInformation.UnprotectSection();
                }

                Log.Info("Reading app.config for administrative configuration");
                var administrativeConfig = AdministrativeConfiguration.Create(config.AdministrationConfiguration, AdministrationConfiguration.AdministrationTenantId);

                Log.Info("Reading app.config for default message adapters");
                var defaultMessageAdapters = config.DefaultMessageAdapters;

                Log.Info("Reading app.config for default jobs");
                var defaultJobs = config.DefaultJobs;

                EngineMultiTenantConfiguration.JobModuleFileNamePatterns = config.JobModules?.JobModulePatternItems?.ToPatternList();
                
                Log.Info("Reading app.config for tenants");
                var tenantConfiguration = config.TenantConfiguration;

                Log.Info("Reading app.config for encryptedTenantSettings");
                var encryptedTenantSettings = config.EncryptedTenantConfiguration;
                
                if (encryptedTenantSettings.SectionInformation.IsProtected)
                {
                    Log.Info("Unprotecting EncryptedTenantSettings");
                    encryptedTenantSettings.SectionInformation.UnprotectSection();
                }

                var tenants = TenantConfigurationProvider.ResolveTenants(
                    new ResolveTenantsParams(tenantConfiguration, defaultMessageAdapters, defaultJobs, 
                        globalSettings, encryptedSettings, encryptedTenantSettings));

                Log.Info("Configuring Atlas service");                
                var configuration = Host.UseAppConfig<EngineService>()
                    .WithRegistrations(b =>
                    {
                        b.RegisterModule<EngineModule>();
                        b.RegisterModule<EventStoreModule>();
                        b.RegisterModule<AdministrationModule>();
                        b.Register(c=> administrativeConfig);
                        b.Register(c=> new Tenants(){ All = tenants})
                            .As<ITenants>();
                        b.Register(c => Settings.Create(globalSettings.SettingItems))
                            .As<ISettings>();
                    })
                    .WithMultiTenantContainer(c => { return EngineMultiTenantConfiguration.Load(c, tenants, administrativeConfig); });



                if (args != null && args.Any())
                    configuration = configuration.WithArguments(args);

                Log.Info("Starting Atlas service");
                Host.Start(configuration);


            }
            catch (Exception ex)
            {
                Log.Error(ex, "Exception during Atlas service-startup.");
                Console.ReadLine();
            }           
        }
        
    }
}
