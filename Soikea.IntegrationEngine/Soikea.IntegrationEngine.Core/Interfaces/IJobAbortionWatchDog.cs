﻿using System;
using System.Runtime.Serialization;

namespace Soikea.IntegrationEngine.Core.Interfaces
{
    /// <summary>
    /// Watchdog that checks whether job is being aborted.
    /// </summary>
    public interface IJobAbortionWatchDog
    {
        /// <summary>
        /// Checks whether a signal to abort current job execution was detected.
        /// Once job should be aborted a <see cref="JobAbortedException"/> is thrown.
        /// </summary>
        /// <exception cref="JobAbortedException">When job should be aborted.</exception>
        void ThrowIfJobAbortionRequested();
    }

    [Serializable]
    public class JobAbortedException : Exception
    {

        public JobAbortedException()
        {
        }

        public JobAbortedException(string message) : base(message)
        {
        }

        public JobAbortedException(string message, Exception inner) : base(message, inner)
        {
        }

        protected JobAbortedException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }
    }
}
