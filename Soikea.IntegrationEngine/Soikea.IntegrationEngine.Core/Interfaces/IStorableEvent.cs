﻿namespace Soikea.IntegrationEngine.Core.Interfaces
{
    /// <summary>
    /// An IEvent that should be stored in to an event-store using IEventSoringStrategy.
    /// </summary>
    public interface IStorableEvent : IEvent
    {
    }
}
