﻿using System;
using System.Runtime.Serialization;

namespace Soikea.IntegrationEngine.Core.Interfaces
{
    public interface IEvent : ISerializable
    {
        string TenantId { get; }
        Guid JobId { get; }

        SubscriptionCategory Category { get; }        
    }
}
