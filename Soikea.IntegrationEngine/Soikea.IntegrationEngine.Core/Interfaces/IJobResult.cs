﻿using System;
using System.Runtime.Serialization;
using Soikea.IntegrationEngine.Core.Implementation.Events;

namespace Soikea.IntegrationEngine.Core.Interfaces
{
    public interface IJobResult : ISerializable
    {
        Guid JobId { get; }
        JobResultStatus Status { get; set; }

        IEndOfJobPayload ExecutionDetails { get; }
    }

    public interface IEndOfJobPayload
    {
        string TenantId { get; }
        Guid JobId { get; }
        string JobName { get; }
        JobResultStatus Status { get; }
        int InputCount { get; }
        int OutputCount { get; }
    }
}