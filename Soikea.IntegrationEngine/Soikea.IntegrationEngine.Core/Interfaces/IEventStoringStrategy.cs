﻿namespace Soikea.IntegrationEngine.Core.Interfaces
{
    /// <summary>
    /// Event storing strategy to be used to store IStorableEvents.
    /// </summary>
    public interface IEventStoringStrategy : IEventSubscriber
    {
        EventStream EventStream { get; }
    }
}
