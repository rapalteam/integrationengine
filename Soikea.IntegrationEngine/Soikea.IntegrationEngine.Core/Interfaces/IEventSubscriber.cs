﻿namespace Soikea.IntegrationEngine.Core.Interfaces
{
    public interface IEventSubscriber
    {
        string Name { get; }
        void Receive(IEvent @event);
    }
}
