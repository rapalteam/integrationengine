﻿using System.ComponentModel;

namespace Soikea.IntegrationEngine.Core.Interfaces
{
    public enum JobResultType
    {
        Void,
        Synchronous,
        Callback
    }

    public enum JobResultStatus
    {
        [Description("None")]
        None = 0,
        [Description("Not started")]
        NotStarted = 1,
        [Description("Started")]
        Started = 2,
        [Description("Ok")]
        Ok = 3,
        [Description("Aborted")]
        Aborted = 4,
        [Description("Error")]
        Error = 5
    }

    public enum EventStream
    {
        Input = 1,
        Output = 2
    }
}
