﻿using System.Diagnostics.Contracts;

namespace Soikea.IntegrationEngine.Core.Interfaces
{
    public interface ISettings
    {
        [Pure]
        bool ContainsKey(string key);
        string GetValue(string key);
        void SetValue(string key, string value);
    }

    public interface ITenantSettings : ISettings
    {
        
    }

    public interface IJobSettings : ISettings
    {

    }

    public interface IMessageAdapterSettings : ISettings
    {
        
    }
}
