﻿
using System;
using System.Diagnostics.Contracts;
using Soikea.IntegrationEngine.Core.Contracts;

namespace Soikea.IntegrationEngine.Core.Interfaces
{
    /// <summary>
    /// Describes an integration job that can be run on the engine.
    /// </summary>
    [ContractClass(typeof(ContractForIIntegrationJob))]
    public interface IIntegrationJob
    {        

        /// <summary>
        /// Name of the job. Declarative and unique.
        /// </summary>
        string Name { get; }

        string TenantId { get; set; }

        Guid JobId { get; set; }

        /// <summary>
        /// Type of the job, defines how the job can be run.
        /// </summary>
        JobResultType ResultType { get; }

        /// <summary>
        /// Job-scoped settings for execution of the job.
        /// </summary>
        IJobSettings JobSettings { get; set; }


        /// <summary>
        /// Factory method for correct type of job-result for this job.
        /// </summary>
        /// <param name="jobId">Unique identifier for current job-run on this type of job, populate created IJobResult with this identifier</param>
        /// <returns>A job result of correct type for given job-run</returns>
        IJobResult CreateJobResult(Guid jobId);

        /// <summary>
        /// Execute the integration job.
        /// 
        /// Job can publish results to input and output -streams when it is being run. 
        /// 
        /// Input-stream is being used to deliver events to all subscribers that are registered as subscribers of given category.
        /// Output-stream is being used to return as results of integragion-job-execution and to publish information to external subscribers.
        /// </summary>
        /// <param name="jobAbortionWatchDog">Job abortion watch dog to poll for job-abortion when doing long running jobs</param>
        /// <param name="publishInput">Delegate to use to publish to input-stream</param>
        /// <param name="publishOutput">Delegate to use to publish to output-stream</param>
        /// <returns>
        /// Current state of the execution at the end of Execution as follows:  
        /// - NotStarted: If the execution could not be started
        /// - Ok: If execution was executed as synchronous then the return value should be 
        /// - Started: If the execution is being done in asynchronous manner and the Integration engine must wait for results in the output-stream before returning response to a caller.
        /// - Error: When something went wrong with the execution.
        /// </returns>
        JobResultStatus Execute(IJobAbortionWatchDog jobAbortionWatchDog, Constants.PublishEvent publishInput, Constants.PublishEvent publishOutput);
    }
}
