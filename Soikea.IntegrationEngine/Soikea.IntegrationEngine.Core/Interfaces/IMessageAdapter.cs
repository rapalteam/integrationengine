﻿namespace Soikea.IntegrationEngine.Core.Interfaces
{
    /// <summary>
    /// Describes a message-adapter that can be registered to the engine to receive IEvents.
    /// </summary>
    public interface IMessageAdapter : IEventSubscriber
    {
        string TenantId { get; set; }
        IMessageAdapterSettings MessageAdapterSettings { get; set; }
    }
}
