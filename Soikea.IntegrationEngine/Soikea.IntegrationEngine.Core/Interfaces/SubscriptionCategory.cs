﻿using System;
using System.Diagnostics.Contracts;
using System.Runtime.Serialization;

namespace Soikea.IntegrationEngine.Core.Interfaces
{
    [Serializable]
    public class SubscriptionCategory : IEquatable<SubscriptionCategory>, IEquatable<string>, ISerializable
    {
        public readonly string Name;

        public SubscriptionCategory(string categoryName)
        {
            Contract.Requires(!string.IsNullOrEmpty(categoryName));

            Name = categoryName;
        }

        public SubscriptionCategory(SerializationInfo info, StreamingContext context)
        {
            Name = info.GetString("Name");
        }


        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Name", Name);
        }

        public bool Equals(SubscriptionCategory other)
        {
            return other.GetHashCode() == GetHashCode();
        }

        public bool Equals(string other)
        {
            return Name == other;
        }

        public override string ToString()
        {
            return Name;
        }

        public override bool Equals(object obj)
        {
            var objectAsSubscriptionCategory = obj as SubscriptionCategory;
            return objectAsSubscriptionCategory != null && Equals(objectAsSubscriptionCategory);
        }
    }
}
