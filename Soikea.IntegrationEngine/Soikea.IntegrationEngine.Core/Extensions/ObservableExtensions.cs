﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;

namespace Soikea.IntegrationEngine.Core.Extensions
{
    public static class ObservableExtensions
    {
        public static Task ExecuteInGroupsAsync<T>(this IEnumerable<T> source, Func<IList<T>, Task> func, int groupSize, int degreeOfParallelism = 0)
        {
            var dop = (degreeOfParallelism == 0) ? Environment.ProcessorCount : degreeOfParallelism;
            var tasks = source.ToObservable().Buffer(groupSize).ToEnumerable();
            return Task.WhenAll(
                from partition in Partitioner.Create(tasks).GetPartitions(dop)
                select Task.Run(async delegate
                {
                    using (partition)
                        while (partition.MoveNext())
                            await func(partition.Current);
                }));
        }

        public static void ExecuteInGroups<T>(this IEnumerable<T> source, Action<IList<T>> action, int groupSize)
        {
            var groups = source.ToObservable().Buffer(groupSize).ToEnumerable();
            Parallel.ForEach(groups, action);
        }
        public static async Task ExecuteInGroupsSync<T>(this IEnumerable<T> source, Action<IList<T>> action, int groupSize)
        {
            await Task.Run(() =>
            {
                var groups = source.ToObservable().Buffer(groupSize).ToEnumerable();
                Parallel.ForEach(groups, action);
            });
        }
    }
}
