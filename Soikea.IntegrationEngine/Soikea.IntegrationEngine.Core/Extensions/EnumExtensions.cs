﻿using System;
using System.ComponentModel;
using System.Diagnostics.Contracts;
using System.Linq;

namespace Soikea.IntegrationEngine.Core.Extensions
{
    /// <summary>
    /// Helper extension-methods for common enumerations.
    /// </summary>
    public static class EnumExtensions
    {
        /// <summary>
        /// Convert given value to the corresponding enumeration value.
        /// </summary>
        /// <exception cref="ArgumentException">When the conversion can not be done with given arguments.</exception>
        /// <typeparam name="T">Target enumeration type</typeparam>
        /// <param name="value">Raw value to convert from</param>
        /// <returns></returns>
        public static T ConvertToEnum<T>(this object value) where T : struct, IConvertible
        {
            Contract.Requires(value != null, "value != null");

            if (!value.GetType().IsEnum && !value.IsNumericType())
            {
                throw new ArgumentException("Source type is not valid enumeration conversion");
            }

            if (value.IsNumericType())
            {
                if (Math.Abs(Convert.ToDouble(value) - Convert.ToInt32(value)) > double.Epsilon)
                {
                    throw new ArgumentException("Source type is not enum, nor convertible to such.");
                }
            }

            if (!typeof(T).IsEnum)
            {
                throw new ArgumentException("Destination type is not enum");
            }

            if (value.IsNumericType())
            {
                // Numeric
                if (typeof(T).IsEnumDefined(Convert.ToInt32(value)))
                {
                    return (T)Enum.ToObject(typeof(T), Convert.ToInt32(value));
                }
            }
            else
            {
                // Enum
                if (typeof(T).IsEnumDefined((int)value))
                {
                    return (T)Enum.ToObject(typeof(T), (int)value);
                }
            }

            throw new ArgumentException(string.Format("Value {0} is not defined for enum {1}.", value, typeof(T)));
        }

        /// <summary>
        /// Get value from description annotation of enumeration value.
        /// </summary>
        /// <exception cref="ArgumentException">When given enumeration value does not have Description-value given</exception>
        /// <param name="value">enumeration value</param>
        /// <returns>Value of the Description attribute</returns>
        public static string GetDescription<T>(this T value)
        {
            Contract.Requires(value != null, "value != null");

            var fi = value.GetType().GetField(value.ToString());
            var attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (attributes.Length < 1)
                throw new ArgumentException("The description attribute for enum value is not specified.");

            return attributes[0].Description;
        }

        /// <summary>
        /// Get string representation of given enumeration value.
        /// </summary>
        /// <param name="value">The enumeration value</param>
        /// <returns>The value of Description -attribute (if given) otherwise ToString() -representation</returns>
        public static string StringValueOf<T>(this T value)
        {
            Contract.Requires(value != null, "value != null");

            var fi = value.GetType().GetField(value.ToString());
            var attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            return attributes.Length > 0
                ? attributes[0].Description
                : value.ToString();
        }

        /// <summary>
        /// Convert string value of enumeration into a given enumeration-type.
        /// </summary>
        /// <exception cref="ArgumentException">When the conversion can not be done with given arguments.</exception>
        /// <typeparam name="T">Target type to convert to</typeparam>
        /// <param name="value">String-representation to convert from</param>
        /// <returns>Parsed enumeration value of target type</returns>
        public static T EnumValueOf<T>(string value)
        {
            var enumType = typeof(T);
            var names = Enum.GetNames(enumType);

            foreach (string name in names.Where(name => StringValueOf((Enum)Enum.Parse(enumType, name)).Equals(value)))
            {
                return (T)Enum.Parse(enumType, name);
            }

            throw new ArgumentException("Matching enumeration value not found");
        }

        /// <summary>
        /// Convert string value of enumeration into a given enumeration-type or the default value of the enumeration.
        /// </summary>
        /// <typeparam name="T">Target type to convert to</typeparam>
        /// <param name="value">String-representation to convert from</param>
        /// <returns>Parsed enumeration value of target type or default</returns>
        public static T EnumValueOfOrDefault<T>(string value)
        {
            try
            {
                return EnumValueOf<T>(value);
            }
            catch (ArgumentException)
            {
                return default(T);
            }
        }
    }
}
