﻿using System.Collections.Generic;
using System.Data;

namespace Soikea.IntegrationEngine.Core.Extensions
{
    public static class DataRecordExtensions
    {
        public static Dictionary<string, QueryResultField> ToDictionary(this IDataRecord value) 
        {
            var result = new Dictionary<string, QueryResultField>();

            for (var i = 0; i < value.FieldCount; i++)
            {
                result.Add(value.GetName(i), new QueryResultField(value.GetValue(i), value.GetFieldType(i)));
            }

            return result;
        } 
    }
}
