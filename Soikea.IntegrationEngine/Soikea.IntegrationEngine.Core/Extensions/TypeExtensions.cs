﻿using System;
using System.Diagnostics.Contracts;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Soikea.IntegrationEngine.Core.Extensions
{
    /// <summary>
    /// Helper extension-methods for all objects.
    /// </summary>
    public static class TypeExtensions
    {
        /// <summary>
        /// Determines whether given object is of numeric-type.
        /// </summary>
        /// <param name="o">object to determine with</param>
        /// <returns>true if it indeed is a numeric type, false otherwise</returns>
        public static bool IsNumericType(this object o)
        {
            Contract.Requires(o != null, "a != null");

            switch (Type.GetTypeCode(o.GetType()))
            {
                case TypeCode.Byte:
                case TypeCode.SByte:
                case TypeCode.UInt16:
                case TypeCode.UInt32:
                case TypeCode.UInt64:
                case TypeCode.Int16:
                case TypeCode.Int32:
                case TypeCode.Int64:
                case TypeCode.Decimal:
                case TypeCode.Double:
                case TypeCode.Single:
                    return true;
                default:
                    return false;
            }
        }

        /// <summary>
        /// Deep clone the object using serialization -> deserialization.
        /// </summary>
        /// <typeparam name="T">Type of object to clone</typeparam>
        /// <param name="a">the object to clone</param>
        /// <returns>the cloned object</returns>
        public static T DeepClone<T>(this T a)
        {
            Contract.Requires(a != null, "a != null");

            using (MemoryStream stream = new MemoryStream())
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, a);
                stream.Position = 0;
                return (T)formatter.Deserialize(stream);
            }
        }
    }
}
