﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Soikea.IntegrationEngine.Core.Extensions
{
    public class QueryResultField
    {
        private readonly object _value;
        private readonly Type _valueType;

        public QueryResultField(object value, Type valueType)
        {
            _value = value;
            _valueType = valueType;
        }

        public T GetValue<T>()
        {
            return Cast.To<T>(_value);
        }
    }

    public static class Cast
    {
        private static readonly Dictionary<string, List<string>> ExtendedCasts = new Dictionary<string, List<string>>();

        private static void InitializeCasts()
        {
            ExtendedCasts.Add("System.Int32", new List<string> {"System.String"});
            ExtendedCasts.Add("System.Int64", new List<string> {"System.String"});
            ExtendedCasts.Add("System.Double", new List<string> {"System.String"});
        }

        private static bool IsCastableTo(Type from, Type to)
        {
            if (!ExtendedCasts.Any()) InitializeCasts();
            var fromType = from.FullName;
            return (ExtendedCasts.ContainsKey(fromType) && ExtendedCasts[fromType].Contains(to.FullName)) || @from.IsAssignableFrom(to);
        }

        public static T To<T>(object input)
        {
            if (!IsCastableTo(input.GetType(), typeof (T)))
                throw new NotSupportedException($"Cannot cast from {input.GetType()} to {typeof(T)}");

            if (typeof (T) == typeof (string)) return (T) (object) input.ToString();

            return (T) input;
        }
    }
}
