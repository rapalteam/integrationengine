﻿using Soikea.IntegrationEngine.Core.Interfaces;

namespace Soikea.IntegrationEngine.Core
{
    public static class Constants
    {
        public delegate void PublishEvent(IEvent @event);

        public static readonly SubscriptionCategories SubscriptionCategories = new SubscriptionCategories();
    }

    public class SubscriptionCategories
    {
        public static readonly SubscriptionCategory StartOfJob = new SubscriptionCategory("Soikea.IntegrationEngine.Job.StartOfJob");
        public static readonly SubscriptionCategory EndOfJob = new SubscriptionCategory("Soikea.IntegrationEngine.Job.EndOfJob");
        public static readonly SubscriptionCategory JobProgress = new SubscriptionCategory("Soikea.IntegrationEngine.Job.Progress");        
    }
}
