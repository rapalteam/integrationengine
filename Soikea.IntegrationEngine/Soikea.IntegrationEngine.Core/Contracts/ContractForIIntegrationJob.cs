﻿using System;
using System.Diagnostics.Contracts;
using Soikea.IntegrationEngine.Core.Interfaces;

namespace Soikea.IntegrationEngine.Core.Contracts
{
    [ContractClassFor(typeof(IIntegrationJob))]
    public abstract class ContractForIIntegrationJob : IIntegrationJob
    {
        public string Name
        {
            get
            {
                Contract.Ensures(!string.IsNullOrEmpty(Contract.Result<string>()));

                return "A name for contract";
            }
        }

        public string TenantId { get; set; }

        public Guid JobId { get; set; }

        public abstract JobResultType ResultType { get; }
        public IJobSettings JobSettings { get; set; }

        public IJobResult CreateJobResult(Guid jobId)
        {
            Contract.Ensures(Contract.Result<IJobResult>() != null);

            return default(IJobResult);
        }

        public JobResultStatus Execute(IJobAbortionWatchDog jobAbortionWatchDog, Constants.PublishEvent publishInput, Constants.PublishEvent publishOutput)
        {
            Contract.Ensures(Contract.Result<JobResultStatus>() != JobResultStatus.None);

            return default(JobResultStatus);
        }
    }
}
