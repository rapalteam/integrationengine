﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Net;

namespace Soikea.IntegrationEngine.Core.Implementation.Ftp
{
    public abstract class FtpTransferJobBase : SynchronousIntegrationJob
    {
        protected readonly IFtpConnectionProvider FtpConnectionProvider;
        
        public override string Name
        {
            get { throw new System.NotImplementedException(); }
        }

        protected FtpTransferJobBase(IFtpConnectionProvider ftpConnectionProvider)
        {
            FtpConnectionProvider = ftpConnectionProvider;
        }

        protected string DownloadFile(string targetUrl)
        {
            Contract.Requires(targetUrl != null, "targetUrl != null");
            
            return FtpConnectionProvider.Download(targetUrl);
        }

        protected FtpStatusCode UploadFile(string targetUrl, byte[] dataBytes)
        {
            Contract.Requires(targetUrl != null, "targetUrl != null");
            Contract.Requires(dataBytes != null, "dataBytes != null");

            return FtpConnectionProvider.Upload(targetUrl, dataBytes);
        }

        protected List<string> ListContents(string targetUrl)
        {
            Contract.Requires(targetUrl != null, "targetUrl != null");

            return FtpConnectionProvider.ListContents(targetUrl);
        }

    }
}
