﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;
using System.Net;

namespace Soikea.IntegrationEngine.Core.Implementation.Ftp
{
    public interface IFtpConnectionProvider
    {
        void Configure(string username, string password);
        string Download(string targetUrl);
        FtpStatusCode Upload(string targetUrl, byte[] dataBytes);
        List<string> ListContents(string targetUrl);
    }

    public class FtpConnectionProvider : IFtpConnectionProvider
    {
        private NetworkCredential _connectCredential;
        
        public void Configure(string username, string password)
        {
            _connectCredential = new NetworkCredential(username, password);
        }

        public string Download(string targetUrl)
        {
            var request = (FtpWebRequest)WebRequest.Create(targetUrl);
            request.Method = WebRequestMethods.Ftp.DownloadFile;
            request.Credentials = _connectCredential;
            
            var response = (FtpWebResponse)request.GetResponse();

            var responseStream = response.GetResponseStream();
            string result = null;
            if (responseStream != null)
            {
                var reader = new StreamReader(responseStream);

                result = reader.ReadToEnd();

                reader.Close();
            }
            response.Close();

            return result;
        }

        public FtpStatusCode Upload(string targetUrl, byte[] dataBytes)
        {
            Contract.Requires(targetUrl != null);
            
            var request = (FtpWebRequest)WebRequest.Create(targetUrl);
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.EnableSsl = true;
            request.Credentials = _connectCredential;
            request.ContentLength = dataBytes.Length;
            
            Logging.Log.Trace($"Uploading file to target {targetUrl} using credential {_connectCredential.UserName}");

            var requestStream = request.GetRequestStream();
            requestStream.Write(dataBytes, 0, dataBytes.Length);
            requestStream.Close();

            var response = (FtpWebResponse)request.GetResponse();

            response.Close();

            return response.StatusCode;
        }

        public List<string> ListContents(string targetUrl)
        {
            Contract.Requires(targetUrl != null);
            
            var request = (FtpWebRequest) WebRequest.Create(targetUrl);
            request.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
            request.Credentials = _connectCredential;
            
            var response = (FtpWebResponse) request.GetResponse();

            var responseStream = response.GetResponseStream();
            
            if (responseStream != null)
            {
                var reader = new StreamReader(responseStream);

                var result = reader.ReadToEnd();


                reader.Close();
                response.Close();

                return result.Split(' ').ToList(); //TODO: Verify linechanges and so on...
            }

            return new List<string>();
        }
    }
}
