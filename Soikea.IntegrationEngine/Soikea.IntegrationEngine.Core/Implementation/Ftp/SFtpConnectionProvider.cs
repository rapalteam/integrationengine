﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.IO;
using System.Net;
using Renci.SshNet;
using Soikea.IntegrationEngine.Core.Interfaces;
using Soikea.IntegrationEngine.Core.Logging;

namespace Soikea.IntegrationEngine.Core.Implementation.Ftp
{
    public class SFtpConnectionProvider : IFtpConnectionProvider
    {
        private string _user;
        private string _password;
        
        public ITenantSettings Configuration { get; set; }
        
        public void Configure(string username, string password)
        {
            _user = username;
            _password = password;
        }

        public string Download(string targetUrl)
        {
            throw new NotImplementedException();
        }

        public FtpStatusCode Upload(string targetUrl, byte[] dataBytes)
        {
            Contract.Requires(targetUrl != null);
            
            Log.Trace($"Uploading file securely to target {targetUrl} using credential {_user}");

            var splitAt = targetUrl.IndexOf('/');
            var server = targetUrl.Substring(0, splitAt);
            var path = targetUrl.Substring(splitAt);

            try
            {
                var client = new SftpClient(server, _user, _password);
                client.Connect();
                Log.Trace($"Connected to server {server} using credential {_user}");

                client.UploadFile(new MemoryStream(dataBytes), path);
                Log.Trace($"Upload successful to path {path} on server {server} using credential {_user}");

                return FtpStatusCode.ClosingData;
            }
            catch (Exception ex)
            {
                Log.Error(ex, $"Error uploading file to target {targetUrl} using credential {_user}");

                return FtpStatusCode.Undefined;
            }
        }

        public List<string> ListContents(string targetUrl)
        {
            throw new NotImplementedException();
        }
    }
}
