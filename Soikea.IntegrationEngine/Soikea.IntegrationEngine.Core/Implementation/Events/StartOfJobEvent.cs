﻿using System;
using System.Runtime.Serialization;
using HashTag;

namespace Soikea.IntegrationEngine.Core.Implementation.Events
{
    public class StartOfJobEvent : AdministrationEvent
    {
        public DateTimeOffset StartTime { get; set; }

        public StartOfJobEvent(string tenantId, Guid jobId, string jobName) 
            : base(tenantId, jobId, SubscriptionCategories.StartOfJob, jobName)
        {
            StartTime = DateTimeOffset.UtcNow;
        }

        public StartOfJobEvent(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            
        }
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValue("StartTime", StartTime);
        }
    }
}
