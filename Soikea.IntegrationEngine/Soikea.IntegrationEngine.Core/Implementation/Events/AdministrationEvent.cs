﻿using System;
using System.Runtime.Serialization;
using Soikea.IntegrationEngine.Core.Interfaces;

namespace Soikea.IntegrationEngine.Core.Implementation.Events
{
    public class AdministrationEvent : EventBase
    {
        public string JobName { get; set; }

        public AdministrationEvent(string tenantId, Guid jobId, SubscriptionCategory category, string jobName)
            : base(tenantId, jobId, category)
        {
            JobName = jobName;
        }

        public AdministrationEvent(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            JobName = info.GetString("JobName");
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValue("JobName", JobName);
        }
    }
}
