﻿using System;
using System.Runtime.Serialization;
using Soikea.IntegrationEngine.Core.Extensions;
using Soikea.IntegrationEngine.Core.Interfaces;

namespace Soikea.IntegrationEngine.Core.Implementation.Events
{

    [Serializable]
    public class EndOfJobPayload : ISerializable, IEndOfJobPayload
    {
        public string TenantId { get; private set; }
        public Guid JobId { get; private set; }
        public string JobName { get; private set; }
        public JobResultStatus Status { get; private set; }
        public int InputCount { get; private set; }
        public int OutputCount { get; private set; }

        public EndOfJobPayload(string tenantId, Guid jobId, string jobName, JobResultStatus status, int inputCount, int outputCount)
        {
            TenantId = tenantId;
            JobId = jobId;
            JobName = jobName;
            Status = status;
            InputCount = inputCount;
            OutputCount = outputCount;
        }

        public EndOfJobPayload(SerializationInfo info, StreamingContext context)
        {
            TenantId = info.GetString("TenantId");
            JobId = Guid.Parse(info.GetString("JobId"));
            JobName = info.GetString("JobName");
            Status = (JobResultStatus) Enum.Parse(typeof(JobResultStatus), info.GetString("Status"));
            InputCount = int.Parse(info.GetString("InputCount"));
            OutputCount = int.Parse(info.GetString("OutputCount"));
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("TenantId", TenantId);
            info.AddValue("JobId", JobId);
            info.AddValue("JobName", JobName);
            info.AddValue("Status", Status.StringValueOf());
            info.AddValue("InputCount", InputCount);
            info.AddValue("OutputCount", OutputCount);
        }
    }

    public class EndOfJobEvent : AdministrationEvent
    {
        public DateTimeOffset EndTime { get; set; }
       
        
        public EndOfJobPayload ExecutionDetails { get; set; }
        public EndOfJobEvent(string tenantId, Guid jobId, string jobName, JobResultStatus status, int inputCount, int outputCount) 
            : base(tenantId, jobId, SubscriptionCategories.EndOfJob, jobName)
        {
            EndTime = DateTimeOffset.UtcNow;
            ExecutionDetails = new EndOfJobPayload(tenantId, jobId, jobName, status, inputCount, outputCount);
        }

        public EndOfJobEvent(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            ExecutionDetails = (EndOfJobPayload) info.GetValue("ExecutionDetails", typeof (EndOfJobPayload));
            EndTime = info.GetDateTime("EndTime");
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValue("ExecutionDetails", ExecutionDetails);
            info.AddValue("EndTime", EndTime);
        }
    }
}
