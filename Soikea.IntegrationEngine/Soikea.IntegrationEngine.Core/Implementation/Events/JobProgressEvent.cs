﻿using System;
using System.Runtime.Serialization;

namespace Soikea.IntegrationEngine.Core.Implementation.Events
{
    public class JobProgressEvent : AdministrationEvent
    {
        public JobProgressEvent(string tenantId, Guid jobId, string jobName) 
            : base(tenantId, jobId, SubscriptionCategories.JobProgress, jobName)
        {
        }

        public JobProgressEvent(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            
        }
    }
}
