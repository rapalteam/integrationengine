﻿using System;
using System.Runtime.Serialization;
using Soikea.IntegrationEngine.Core.Interfaces;

namespace Soikea.IntegrationEngine.Core.Implementation.Events
{
    public class AnonymousEvent : EventBase
    {
        public virtual ISerializable Payload { get; private set; }

        public AnonymousEvent(string tenantId, Guid jobId, SubscriptionCategory category, ISerializable payload)
            : base(tenantId, jobId, category)
        {
            Payload = payload;
        }

        public AnonymousEvent(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            Payload = (ISerializable) info.GetValue("Payload", typeof (ISerializable));
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValue("Payload", Payload);
        }
    }
}
