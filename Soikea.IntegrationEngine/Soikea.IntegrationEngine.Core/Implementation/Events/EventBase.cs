﻿using System;
using System.Runtime.Serialization;
using Soikea.IntegrationEngine.Core.Interfaces;

namespace Soikea.IntegrationEngine.Core.Implementation.Events
{    
    public abstract class EventBase : IEvent
    {
        public string TenantId { get; private set; }

        public Guid JobId { get; private set; }

        public SubscriptionCategory Category { get; private set; }
        
        protected EventBase(string tenantId, Guid jobId, SubscriptionCategory category)
        {
            TenantId = tenantId;
            JobId = jobId;
            Category = category;
        }

        protected EventBase(SerializationInfo info, StreamingContext context)
        {
            TenantId = (string) info.GetValue("TenantId", typeof (string));
            JobId = (Guid)info.GetValue("JobId", typeof(Guid));
            Category = (SubscriptionCategory)info.GetValue("Category", typeof(SubscriptionCategory));
        }

        public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("TenantId", TenantId);
            info.AddValue("JobId", JobId);
            info.AddValue("Category", Category);
        }
    }
}
