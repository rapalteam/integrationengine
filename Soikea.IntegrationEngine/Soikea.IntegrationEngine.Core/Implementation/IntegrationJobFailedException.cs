﻿using System;
using System.Diagnostics.Contracts;
using System.Runtime.Serialization;

namespace Soikea.IntegrationEngine.Core.Implementation
{
    [Serializable]
    public class IntegrationJobFailedException : Exception
    {
        public string JobName { get; private set; }

        public IntegrationJobFailedException(string message, string jobName) : base(message)
        {
            JobName = jobName;
        }

        public IntegrationJobFailedException(string message, string jobName, Exception inner)
            : base(message, inner)
        {
            JobName = jobName;
        }

        protected IntegrationJobFailedException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
            Contract.Requires(info != null);

            JobName = info.GetString("JobName");
        }
    }
}
