﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Soikea.IntegrationEngine.Core.Extensions;
using Soikea.IntegrationEngine.Core.Interfaces;

namespace Soikea.IntegrationEngine.Core.Implementation.Sql
{
    public interface IQueryProvider
    {
        IEnumerable<T> Fetch<T>(string connectionStringName, string queryString, Action<SqlParameterCollection> parameters, Func<Dictionary<string, QueryResultField>, T> factory, IJobAbortionWatchDog jobAbortionWatchDog);

        bool Insert(string connectionStringName, SqlCommand insertCommand, IJobAbortionWatchDog jobAbortionWatchDog);
    }
}
