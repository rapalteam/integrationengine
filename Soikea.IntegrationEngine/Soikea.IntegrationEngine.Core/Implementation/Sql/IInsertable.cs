﻿using System.Data.SqlClient;

namespace Soikea.IntegrationEngine.Core.Implementation.Sql
{
    public interface IInsertable
    {
        SqlCommand GetInsertCommand(string tableName);
    }
}
