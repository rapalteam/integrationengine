﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Reactive.Linq;
using Soikea.IntegrationEngine.Core.Interfaces;
using Soikea.IntegrationEngine.Core.Logging;

namespace Soikea.IntegrationEngine.Core.Implementation.Sql
{
    public class StoredProcedureCallProvider : IStoredProcedureCallProvider
    {
        public bool ExecuteProcedureCall(string connectionStringName, string procedureName, IJobAbortionWatchDog jobAbortionWatchDog)
        {
            var connectionString = ConfigurationManager.ConnectionStrings[connectionStringName];

            if (connectionString == null)
            {
                Log.Warn($"Could not find requested connection-string '{connectionStringName}'");
                return false;
            }
            
            using (SqlConnection connection = new SqlConnection(connectionString.ConnectionString))
            using (SqlCommand clearCommand = new SqlCommand())
            {

                clearCommand.CommandText = procedureName;
                clearCommand.CommandType = CommandType.StoredProcedure;
                clearCommand.Connection = connection;
                clearCommand.CommandTimeout = 0;

                Log.Trace($"Opening connection on {connectionStringName}");
                connection.Open();
                
                var jobAbortedCheck = Observable.Interval(TimeSpan.FromSeconds(1));

                JobAbortedException abortedException = null;
                SqlException sqlException = null;
                using (jobAbortedCheck.Subscribe((i) =>
                {
                    try
                    {
                        jobAbortionWatchDog.ThrowIfJobAbortionRequested();
                    }
                    catch (JobAbortedException e)
                    {
                        abortedException = e;
                        Log.Trace($"Job abortion requested, canceling execution of '{procedureName}' on connection {connectionStringName}");
                        clearCommand.Cancel();
                    }
                    
                }))
                {
                    Log.Trace($"Starting execution of '{procedureName}' on connection {connectionStringName}");
                    try
                    {
                        clearCommand.ExecuteNonQuery();
                    }
                    catch (SqlException se)
                    {
                        // Expecting SqlException "User cancelled the operation"
                        if (!se.Message.Contains("User cancelled"))
                        {
                            sqlException = se;
                        }
                    }
                    
                }

                if (abortedException != null)
                {
                    throw abortedException;
                }

                if (sqlException != null)
                {
                    Log.Error(sqlException, $"An unexpected SQL exception occurred executing {procedureName} on connection {connectionStringName}");
                    return false;
                }

                Log.Trace($"Command '{procedureName}' on connection {connectionStringName} completed");
            }

            return true;
        }
        
    }
}
