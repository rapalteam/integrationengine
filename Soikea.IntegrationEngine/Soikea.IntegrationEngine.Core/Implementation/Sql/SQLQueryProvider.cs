﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Reactive.Linq;
using Soikea.IntegrationEngine.Core.Interfaces;
using Soikea.IntegrationEngine.Core.Logging;
using Soikea.IntegrationEngine.Core.Extensions;

namespace Soikea.IntegrationEngine.Core.Implementation.Sql
{
    public class SqlQueryProvider : IQueryProvider 
    {
        public IEnumerable<T> Fetch<T>(string connectionStringName, string queryString, Action<SqlParameterCollection> parameters, Func<Dictionary <string, QueryResultField>, T> factory, IJobAbortionWatchDog jobAbortionWatchDog)
        {

            var connectionString = ConfigurationManager.ConnectionStrings[connectionStringName];

            if (connectionString == null)
            {
                Log.Warn($"Could not find requested connection-string '{connectionStringName}'");
                return null;
            }

            if (string.IsNullOrEmpty(queryString))
            {
                Log.Warn($"Cannot execute SELECT with an empty queryString");
                return null;
            }

            
            using (var connection = new SqlConnection(connectionString.ConnectionString))
            {
                using (var command = new SqlCommand(queryString, connection))
                {
                    var result = new List<T>();
                    var jobAbortedCheck = Observable.Interval(TimeSpan.FromSeconds(1));
                    JobAbortedException abortedException = null;
                    SqlException sqlException = null;

                    using (jobAbortedCheck.Subscribe((i) =>
                    {
                        try
                        {
                            Log.Info($"Cancellation requested for query {queryString}");
                            jobAbortionWatchDog.ThrowIfJobAbortionRequested();
                        }
                        catch (JobAbortedException e)
                        {
                            abortedException = e;
                            command.Cancel();
                        }

                    }))
                    {
                        parameters?.Invoke(command.Parameters);

                        connection.Open();
                        try
                        {
                            using (var reader = command.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    result.Add(factory.Invoke(reader.ToDictionary()));
                                }
                            }
                        }
                        catch (SqlException e)
                        {
                            if (!e.Message.Contains("cancelled by user"))
                            {
                                sqlException = e;
                            }
                        }

                        if (abortedException != null)
                            throw abortedException;

                        if (sqlException != null)
                        {
                            Log.Warn($"Unexpected exception when executing query {queryString}");
                            throw sqlException;
                        }
                            

                        Log.Info($"Successfully fininshed query {queryString}");
                        connection.Close();

                        return result;
                    }
                }

            }
        }

        public bool Insert(string connectionStringName, SqlCommand insertCommand, IJobAbortionWatchDog jobAbortionWatchDog)
        {
            var connectionString = ConfigurationManager.ConnectionStrings[connectionStringName];

            if (connectionString == null)
            {
                Log.Warn($"Could not find connectionString with '{connectionStringName}' in INSERT command.");
                return false;
            }

            if (insertCommand == null)
            {
                Log.Warn($"Cannot execute INSERT with null insertCommand");
                return false;
            }

            using (var connection = new SqlConnection(connectionString.ConnectionString))
            {
                
                    var jobAbortedCheck = Observable.Interval(TimeSpan.FromSeconds(1));
                    JobAbortedException abortedException = null;
                    SqlException sqlException = null;

                    using (jobAbortedCheck.Subscribe((i) =>
                    {
                        try
                        {
                            Log.Info($"Cancellation requested for INSERT query");
                            jobAbortionWatchDog.ThrowIfJobAbortionRequested();
                        }
                        catch (JobAbortedException e)
                        {
                            abortedException = e;
                            insertCommand.Cancel();
                        }

                    }))
                    {
                        insertCommand.Connection = connection;
                        Log.Trace($"Starting to execute INSERT command {insertCommand.CommandText}");
                        connection.Open();
                        try
                        {
                            insertCommand.ExecuteNonQuery();
                        }
                        catch (SqlException e)
                        {
                            if (!e.Message.Contains("cancelled by user"))
                            {
                                sqlException = e;
                            }
                        }

                        if (abortedException != null)
                            throw abortedException;

                        if (sqlException != null)
                        {
                            Log.Warn($"Unexpected exception when executing INSERT query {insertCommand.CommandText}");
                            throw sqlException;
                        }


                        Log.Trace($"Successfully fininshed INSERT query {insertCommand.CommandText}");
                        connection.Close();

                        return true;
                    }
                

            }
        }
    }
}
