﻿using System.Threading.Tasks;
using Soikea.IntegrationEngine.Core.Interfaces;

namespace Soikea.IntegrationEngine.Core.Implementation.Sql
{
    public interface IStoredProcedureCallProvider
    {
        bool ExecuteProcedureCall(string connectionStringName, string procedureName, IJobAbortionWatchDog jobAbortionWatchDog);
    }
}