﻿using System;
using System.Runtime.Serialization;
using Soikea.IntegrationEngine.Core.Extensions;
using Soikea.IntegrationEngine.Core.Implementation.Events;
using Soikea.IntegrationEngine.Core.Interfaces;

namespace Soikea.IntegrationEngine.Core.Implementation
{
    public abstract class JobResultBase : IJobResult, IEventSubscriber
    {
        public Guid JobId { get; private set; }
        public JobResultStatus Status { get; set; }

        public IEndOfJobPayload ExecutionDetails { get; set; }

        public abstract string Name { get; }

        private readonly object _readResult = new object();

        protected static bool IsEndOfJobResult(IEvent @event)
        {
            return @event is EndOfJobEvent;
        }

        protected JobResultBase(Guid jobId)
        {
            JobId = jobId;
        }


        public virtual void Receive(IEvent @event)
        {
            
            lock (_readResult)
            {
                if (IsEndOfJobResult(@event))
                {
                    ExecutionDetails = ((EndOfJobEvent) @event).ExecutionDetails;                    
                }                
            }
        }


        public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {            
            info.AddValue("Status", Status.StringValueOf());
            info.AddValue("ExecutionDetails", ExecutionDetails);
        }

    }
}
