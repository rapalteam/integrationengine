﻿using System;
using Soikea.IntegrationEngine.Core.Interfaces;

namespace Soikea.IntegrationEngine.Core.Implementation
{
    public abstract class SynchronousIntegrationJob : IIntegrationJob
    {        
        public abstract string Name { get; }

        public virtual JobResultType ResultType
        {
            get
            {
                return JobResultType.Synchronous;
            }
        }

        public IJobSettings JobSettings { get; set; }

        public string TenantId { get; set; }
        public Guid JobId { get; set; }
        
        public abstract IJobResult CreateJobResult(Guid jobId);
        
        public abstract JobResultStatus Execute(IJobAbortionWatchDog jobAbortionWatchDog, Constants.PublishEvent publishInput, Constants.PublishEvent publishOutput);

    }
}
