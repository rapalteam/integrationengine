﻿using Soikea.IntegrationEngine.Core.Interfaces;

namespace Soikea.IntegrationEngine.Core.Implementation.Settings
{
    /// <summary>
    /// Provide access to global settings via static implementation.
    /// </summary>
    public class GlobalSettings
    {
        public static ISettings Settings = null;
    }
}
