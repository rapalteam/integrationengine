﻿using Soikea.IntegrationEngine.Core.Implementation.Events;
using Soikea.IntegrationEngine.Core.Interfaces;

namespace Soikea.IntegrationEngine.Core.Implementation
{
    public class JobResultCounter : IEventSubscriber
    {
        public virtual int ItemsPassedThrough { get; private set; }
        public string Name => "JobResultCounter";

        protected static bool IsAdministrationEvent(IEvent @event)
        {
            return @event is AdministrationEvent;
        }

        public void Receive(IEvent @event)
        {
            if (!IsAdministrationEvent(@event))
                ItemsPassedThrough++;
        }
    }
}
