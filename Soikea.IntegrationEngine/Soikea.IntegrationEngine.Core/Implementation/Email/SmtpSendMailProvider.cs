﻿using System.Diagnostics.Contracts;
using System.Net.Mail;
using Soikea.IntegrationEngine.Core.Interfaces;
using Soikea.IntegrationEngine.Core.Logging;

namespace Soikea.IntegrationEngine.Core.Implementation.Email
{
    public class SmtpSendMailProvider : ISendMailProvider
    {
        private SmtpClient Server { get; set; }
        public ITenantSettings Configuration { get; set; }

        public void Send(string sender, string receiver, string subject, string body)
        {
            Contract.Requires(Configuration != null);
            Contract.Requires(Configuration.ContainsKey("DefaultSmtpServer"));
            Contract.Requires(Configuration.ContainsKey("DefaultSmtpServerPort"));

            Server = new SmtpClient(Configuration.GetValue("DefaultSmtpServer"));
            int port;
            Server.Port = int.TryParse(Configuration.GetValue("DefaultSmtpServerPort"), out port) ? port : 25;
            Server.DeliveryMethod = SmtpDeliveryMethod.Network;

            MailMessage mail;

            if (receiver.Contains(";"))
            {
                var receivers = receiver.Split(';');
                mail = new MailMessage(sender, receivers[0]) { Body = body, Subject = subject};

                for (int i = 1; i < receivers.Length; i++)
                {
                    var rcv = receivers[i];
                    mail.To.Add(rcv);
                }
            }
            else
            {
                mail = new MailMessage(sender, receiver) { Body = body, Subject = subject };
            }

            

            try
            {
                Server.Send(mail);
            }
            catch (SmtpFailedRecipientsException e)
            {
                Log.Error(e, "Could not deliver email to one or more recipients");
            }
            catch (SmtpException e)
            {
                throw new SendMailFailedException(e.Message);
            }
        }
    }
}
