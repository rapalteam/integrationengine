﻿using System;
namespace Soikea.IntegrationEngine.Core.Implementation.Email
{
    public interface ISendMailProvider
    {
        void Send(string sender, string receiver, string subject, string content);
    }
}
