﻿using System.Diagnostics.Contracts;
using System.Net.Mail;
using Soikea.IntegrationEngine.Core.Interfaces;
using Soikea.IntegrationEngine.Core.Logging;

namespace Soikea.IntegrationEngine.Core.Implementation.Email
{
    public class SmtpEmailProvider : IEmailConnectionProvider
    {
        private SmtpClient Server { get; set; }
        public ITenantSettings Configuration { get; set; }

        public void Send(string sender, string receiver, string subject, string body)
        {
            Contract.Requires(Configuration != null);
            Contract.Requires(Configuration.ContainsKey("DefaultStmpServer"));
            Contract.Requires(Configuration.ContainsKey("DefaultStmpServerPort"));

            Server = new SmtpClient(Configuration.GetValue("DefaultStmpServer"));
            int port;
            Server.Port = int.TryParse(Configuration.GetValue("DefaultStmpServerPort"), out port) ? port : 25;
            Server.DeliveryMethod = SmtpDeliveryMethod.Network;
            Server.EnableSsl = true;

            var mail = new MailMessage(sender, receiver)
            {
                Body = body,
                Subject = subject
            };

            try
            {
                Log.Trace($"Sending email to {receiver} from {sender} using server {Server.Host}:{Server.Port}");
                Server.Send(mail);
            }
            catch (SmtpFailedRecipientsException e)
            {
                Log.Error(e, "Could not deliver email to one or more recipients");
            }
            catch (SmtpException e)
            {                
                throw new SendMailFailedException($"Failed on email-delivery with status-code {e.StatusCode}", e);
            }
        }
    }
}
