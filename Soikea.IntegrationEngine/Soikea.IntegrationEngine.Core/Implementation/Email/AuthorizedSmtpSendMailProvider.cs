﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Net;
using System.Net.Mail;
using Soikea.IntegrationEngine.Core.Interfaces;
using Soikea.IntegrationEngine.Core.Logging;

namespace Soikea.IntegrationEngine.Core.Implementation.Email
{
    public class AuthorizedSmtpSendMailProvider : ISendMailProvider
    {
        public ISettings Configuration { get; set; }

        /// <summary>
        /// Default sender for the MailMessage.
        /// </summary>
        public string DefaultSender { get; set; }

        /// <summary>
        /// Default receivers for the MailMessage.
        /// </summary>
        public List<string> DefaultReceivers { get; set; }

        public const string RegisterName = "AuthorizedSmtpSendMailProvider";

        /// <summary>
        /// Send an email-message using authorized SmtpClient.
        /// Uses following ISettings values:
        ///     - AuthorizedSmtpServer
        ///     - AuthorizedSmtpUser
        ///     - AuthorizedSmtpPassword
        ///     - AuthorizedSmtpDomain
        ///     - AuthorizedSmtpServerPort (optional, if not specified 587 is used)
        /// </summary>
        /// <param name="sender">Sender for the email, if empty the DefaultSender-property value is used</param>
        /// <param name="receiver">Receiver for the email, optional. The DefaultReceivers-property value is used always</param>
        /// <param name="subject">Subject field for the MailMessage</param>
        /// <param name="content">Content for the body of the MailMessage</param>
        public void Send(string sender, string receiver, string subject, string content)
        {
            Contract.Requires(Configuration != null);
            Contract.Requires(Configuration.ContainsKey("AuthorizedSmtpServer"));

            Contract.Requires(Configuration.ContainsKey("AuthorizedSmtpUser"));
            Contract.Requires(Configuration.ContainsKey("AuthorizedSmtpPassword"));
            Contract.Requires(Configuration.ContainsKey("AuthorizedSmtpDomain"));

            var credentials = new NetworkCredential(Configuration.GetValue("AuthorizedSmtpUser"),
                Configuration.GetValue("AuthorizedSmtpPassword"), Configuration.GetValue("AuthorizedSmtpDomain"));

            var port = 0;
            int.TryParse(Configuration.GetValue("AuthorizedSmtpServerPort"), out port);

            var client = new SmtpClient
            {
                Host = Configuration.GetValue("AuthorizedSmtpServer"),
                UseDefaultCredentials = false,
                Credentials = credentials,
                Port = port > 0 ? port : 587
            };

            var message = new MailMessage();

            var mailAddress = new MailAddress(string.IsNullOrEmpty(sender) ? DefaultSender : sender);
            message.From = mailAddress;
            message.Subject = subject;
            message.Body = content;
            
            if (!string.IsNullOrEmpty(receiver))
            {
                if (receiver.Contains(";"))
                {
                    var receivers = receiver.Split(';');
                    foreach (var rcv in receivers)
                    {
                        message.To.Add(rcv);
                    }
                }
                else
                {
                    message.To.Add(receiver);
                }
            }

            if (DefaultReceivers != null)
            {
                foreach (var defaultReceiver in DefaultReceivers)
                {
                    message.To.Add(defaultReceiver);
                }
            }

            try
            {
                client.Send(message);
            }
            catch (SmtpFailedRecipientsException e)
            {
                Log.Error(e, "Could not deliver email to one or more recipients");
            }
            catch (SmtpException e)
            {
                throw new SendMailFailedException(e.Message);
            }
        }
    }
}
