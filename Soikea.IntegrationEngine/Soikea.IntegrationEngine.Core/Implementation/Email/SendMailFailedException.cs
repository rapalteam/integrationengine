﻿using System;
using System.Runtime.Serialization;

namespace Soikea.IntegrationEngine.Core.Implementation.Email
{
    [Serializable]
    public class SendMailFailedException : Exception
    {
        //
        // For guidelines regarding the creation of new exception types, see
        //    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/cpgenref/html/cpconerrorraisinghandlingguidelines.asp
        // and
        //    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/dncscol/html/csharp07192001.asp
        //

        public SendMailFailedException()
        {
        }

        public SendMailFailedException(string message) : base(message)
        {
        }

        public SendMailFailedException(string message, Exception inner) : base(message, inner)
        {
        }

        protected SendMailFailedException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }
    }
}
