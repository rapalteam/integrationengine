﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using Soikea.IntegrationEngine.Core.Logging.Providers;

namespace Soikea.IntegrationEngine.Core.Logging
{
    public interface ILogProvider
    {
        ILog GetLogger(string name, string module);
    }


    public enum LogLevel
    {
        Trace,
        Debug,
        Info,
        Warn,
        Error,
        Fatal
    }

    /// <summary>
    /// Interface that represent a logger.
    /// </summary>
    public interface ILog
    {
        /// <summary>
        /// Log a message the specified log level.
        /// </summary>
        /// <param name="logLevel">The log level.</param>
        /// <param name="writeLogFunction">The write function.</param>
        /// <param name="exception">An optional exception.</param>
        /// <returns>true if the message was logged. Otherwise false.</returns>
        /// <remarks>
        /// Note: The message func should not be called if the loglevel is not enabled.
        /// To check if LogLevel is enabled call Log with only LogLevel and check the return value, no event will be written.
        /// </remarks>
        bool Log(LogLevel logLevel, Func<string> writeLogFunction, Exception exception = null);
    }


    /// <summary>
    /// Static class to use for logging purposes.
    /// 
    /// Usage:
    /// 
    /// First a log-provider must be set
    /// Log.SetCurrentLogProvider(new EntLibLogProvider());
    /// 
    /// Then static helper methods can be used directly
    /// Log.Info("Message to log");
    /// 
    /// Or you can grap the logger and use ILog -extension methods to do the logging
    /// var logger = Log.GetLogger("typename");
    /// logger.InfoFormat("Message {0}", "Valuetopush");
    /// 
    /// </summary>
    public static class Log
    {
        private static ILogProvider _currentLogProvider;

        private static void WriteLog(LogLevel logLevel, string message, string caller, string callerMember, Exception exception = null)
        {
            var callerFileName = caller.Substring(caller.LastIndexOf("\\", StringComparison.InvariantCulture)+1);
            var callerPath = string.Format("{0}:{1}",
                callerFileName,
                callerMember
                );

            var logger = GetLogger(callerPath);

            logger.Log(logLevel, () => message, exception);
        }

        /// <summary>
        /// Write a log entry of level "Trace" using current logprovider.
        /// </summary>
        /// <param name="message">Message to write</param>
        /// <param name="caller">implicitly populated caller</param>
        /// <param name="callerMember">implicitly populated caller membername</param>
        public static void Trace(string message, [CallerFilePath] string caller = "Unknown", [CallerMemberName] string callerMember = "Unknown")
        {
            WriteLog(LogLevel.Trace, message, caller, callerMember);
        }

        /// <summary>
        /// Write a log entry of level "Debug" using current logprovider.
        /// </summary>
        /// <param name="message">Message to write</param>
        /// <param name="caller">implicitly populated caller</param>
        /// <param name="callerMember">implicitly populated caller membername</param>
        public static void Debug(string message, [CallerFilePath] string caller = "Unknown", [CallerMemberName] string callerMember = "Unknown")
        {
            WriteLog(LogLevel.Debug, message, caller, callerMember);
        }

        /// <summary>
        /// Write a log entry of level "Info" using current logprovider.
        /// </summary>
        /// <param name="message">Message to write</param>
        /// <param name="caller">implicitly populated caller</param>
        /// <param name="callerMember">implicitly populated caller membername</param>
        public static void Info(string message, [CallerFilePath] string caller = "Unknown",  [CallerMemberName] string callerMember = "Unknown")
        {
            WriteLog(LogLevel.Info, message, caller, callerMember);   
        }

        /// <summary>
        /// Write a log entry of level "Warn" using current logprovider.
        /// </summary>
        /// <param name="message">Message to write</param>
        /// <param name="caller">implicitly populated caller</param>
        /// <param name="callerMember">implicitly populated caller membername</param>
        public static void Warn(string message, [CallerFilePath] string caller = "Unknown", [CallerMemberName] string callerMember = "Unknown")
        {
            WriteLog(LogLevel.Warn, message, caller, callerMember);
        }

        /// <summary>
        /// Write a log entry of level "Error" using current logprovider.
        /// </summary>
        /// <param name="exception">Exception to unwrap as log message</param>
        /// <param name="message">Message to write</param>
        /// <param name="caller">implicitly populated caller</param>
        /// <param name="callerMember">implicitly populated caller membername</param>
        public static void Error(Exception exception, string message, [CallerFilePath] string caller = "Unknown", [CallerMemberName] string callerMember = "Unknown")
        {
            WriteLog(LogLevel.Error, message, caller, callerMember, exception);
        }

        /// <summary>
        /// Write a log entry of level "Fatal" using current logprovider.
        /// </summary>
        /// <param name="exception">Exception to unwrap as log message</param>
        /// <param name="message">Message to write</param>
        /// <param name="caller">implicitly populated caller</param>
        /// <param name="callerMember">implicitly populated caller membername</param>
        public static void Fatal(Exception exception, string message, [CallerFilePath] string caller = "Unknown", [CallerMemberName] string callerMember = "Unknown")
        {
            WriteLog(LogLevel.Fatal, message, caller, callerMember, exception);
        }


        /// <summary>
        /// Gets a logger with the specified type-name so you can juse ILog -extension methods directly.
        /// </summary>
        /// <param name="name">The type-name.</param>
        /// <returns>An instance of <see cref="ILog"/></returns>
        public static ILog GetLogger(string name)
        {
            ILogProvider logProvider = _currentLogProvider ?? ResolveLogProvider();
            return logProvider == null 
                ? new NoOpLogger() 
                : (ILog) new LoggerExecutionWrapper(logProvider.GetLogger(name, _module));
        }

        /// <summary>
        /// Sets the current log provider.
        /// </summary>
        /// <param name="logProvider">The log provider.</param>
        public static void SetCurrentLogProvider(ILogProvider logProvider, string module)
        {
            _currentLogProvider = logProvider;
            _module = module;
        }

        public delegate bool IsLoggerAvailable();

        public delegate ILogProvider CreateLogProvider();

        public static readonly List<Tuple<IsLoggerAvailable, CreateLogProvider>> LogProviderResolvers =
            new List<Tuple<IsLoggerAvailable, CreateLogProvider>>
        {
            new Tuple<IsLoggerAvailable, CreateLogProvider>(EntLibLogProvider.IsLoggerAvailable, () => new EntLibLogProvider())
        };

        private static string _module;

        private static ILogProvider ResolveLogProvider()
        {
            try
            {
                foreach (var providerResolver in LogProviderResolvers.Where(providerResolver => providerResolver.Item1())) // IsLoggerAvailable()
                {
                    return providerResolver.Item2(); // CreateLogProvider()
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(
                    "Exception occured resolving a log provider. Logging for this assembly {0} is disabled. {1}",
                    typeof(Log).Assembly.FullName,
                    ex);
            }
            return null;
        }

        public class NoOpLogger : ILog
        {
            public bool Log(LogLevel logLevel, Func<string> messageFunc, Exception exception)
            {
                return false;
            }
        }
    }
}
