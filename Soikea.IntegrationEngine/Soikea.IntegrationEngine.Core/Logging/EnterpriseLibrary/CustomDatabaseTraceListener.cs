﻿using System;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.EnterpriseLibrary.Logging.Formatters;
using Microsoft.Practices.EnterpriseLibrary.Logging.TraceListeners;

namespace Soikea.IntegrationEngine.Core.Logging.EnterpriseLibrary
{
    [ConfigurationElementType(typeof(CustomDatabaseTraceListener))]
    public class CustomDatabaseTraceListener : FormattedTraceListenerBase
    {
        private readonly string _writeLogStoredProcName;
        private readonly string _addCategoryStoredProcName;
        private readonly Database _database;

        public CustomDatabaseTraceListener(Database database, string writeLogStoredProcName,
            string addCategoryStoredProcName, ILogFormatter formatter) : base(formatter)
        {
            _writeLogStoredProcName = writeLogStoredProcName;
            _addCategoryStoredProcName = addCategoryStoredProcName;
            _database = database;
        }

        public override void Write(string message)
        {
                ExecuteWriteLogStoredProcedure(0, 5, TraceEventType.Information, string.Empty, DateTime.Now, string.Empty,
                string.Empty, message, _database, null);
        }
        
        public override void WriteLine(string message)
        {
            Write(message);
        }

        public override void TraceData(TraceEventCache eventCache, string source, TraceEventType eventType, int id, object data)
        {
            if ((Filter == null) || Filter.ShouldTrace(eventCache, source, eventType, id, null, null, data, null))
            {
                if (data is LogEntry)
                {
                    CustomLogEntry entry;

                    if (data is CustomLogEntry)
                    {
                        entry = (CustomLogEntry) data;
                    }
                    else
                    {
                        entry = new CustomLogEntry((LogEntry)data);
                    }
                    
                    if (ValidateParameters())
                    {
                        ExecuteStoredProcedure(entry);
                    }                 
                }
                else if (data is string)
                {
                    Write((string) data);
                }
                else
                {
                    base.TraceData(eventCache, source, eventType, id, data);
                }
            }
        }

        private void ExecuteWriteLogStoredProcedure(int eventId, int priority, TraceEventType severity, string title,
            DateTime timeStamp, string machineName, string appDomainName, string message, Database db, string jobId)
        {
            var logEntry = new CustomLogEntry
            {
                EventId = eventId,
                Priority = priority,
                Severity = severity,
                Title = title,
                TimeStamp = timeStamp,
                MachineName = machineName,
                AppDomainName = appDomainName,
                Message = message,
                JobId = jobId
            };

            ExecuteWriteLogStoredProcedure(logEntry, db);
        }

        private long ExecuteWriteLogStoredProcedure(CustomLogEntry logEntry, Database db)
        {
            var cmd = db.GetStoredProcCommand(_writeLogStoredProcName);

            db.AddInParameter(cmd, "eventId", DbType.Int32, logEntry.EventId);
            db.AddInParameter(cmd, "priority", DbType.Int32, logEntry.Priority);
            db.AddParameter(cmd, "severity", DbType.String, 32, ParameterDirection.Input, false, 0, 0, null,
                DataRowVersion.Default, logEntry.Severity.ToString());
            db.AddParameter(cmd, "title", DbType.String, 256, ParameterDirection.Input, false, 0, 0, null,
                DataRowVersion.Default, logEntry.Title);
            db.AddInParameter(cmd, "timestamp", DbType.DateTime2, logEntry.TimeStamp.ToUniversalTime());
            db.AddParameter(cmd, "machineName", DbType.String, 32, ParameterDirection.Input, false, 0, 0, null,
                DataRowVersion.Default, logEntry.MachineName);
            db.AddParameter(cmd, "message", DbType.String, 1500, ParameterDirection.Input, false, 0, 0, null,
                DataRowVersion.Default, logEntry.Message);
        
            db.AddParameter(cmd, "JobId", DbType.String, 512, ParameterDirection.Input, false, 0, 0, null, DataRowVersion.Default, logEntry.ActivityIdString);
            db.AddParameter(cmd, "module", DbType.String, 512, ParameterDirection.Input, false, 0, 0, null, DataRowVersion.Default, logEntry.Module);
            db.AddOutParameter(cmd, "LogId", DbType.Int64, 4);
            db.ExecuteNonQuery(cmd);
            var logId = Convert.ToInt64(cmd.Parameters[cmd.Parameters.Count - 1].Value, CultureInfo.InvariantCulture);
            return logId;
        }

        private void ExecuteStoredProcedure(CustomLogEntry logEntry)
        {
            using (var connection = _database.CreateConnection())
            {
                connection.Open();
                try
                {
                    var logId = Convert.ToInt32(ExecuteWriteLogStoredProcedure(logEntry, _database));
                    ExecuteAddCategoryStoredProcedure(logEntry, logId, _database);
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        private void ExecuteAddCategoryStoredProcedure(CustomLogEntry logEntry, long logId, Database db)
        {
            foreach (var category in logEntry.Categories)
            {
                var cmd = db.GetStoredProcCommand(_addCategoryStoredProcName);
                db.AddInParameter(cmd, "categoryName", DbType.String, category);
                db.AddInParameter(cmd, "logID", DbType.Int64, logId);
                db.ExecuteNonQuery(cmd);
            }
        }


        protected override string[] GetSupportedAttributes()
        {
            return new []{ "formatter", "_writeLogStoredProcName", "_addCategoryStoredProcName", "databaseInstanceName" };
        }

        private bool ValidateParameters()
        {
            if (string.IsNullOrEmpty(_writeLogStoredProcName))
            {
                return false;
            }

            if (string.IsNullOrEmpty(_addCategoryStoredProcName))
            {
                return false;
            }

            return true;
        }
    }
}
