﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.Design;
using Microsoft.Practices.EnterpriseLibrary.Data.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging.Configuration;
using Soikea.IntegrationEngine.Core.Implementation.Email;
using Soikea.IntegrationEngine.Core.Interfaces;

namespace Soikea.IntegrationEngine.Core.Logging.EnterpriseLibrary
{
    [AddSateliteProviderCommand("connectionStrings", typeof(DatabaseSettings), "DefaultDatabase", "DatabaseInstanceName")]
    public class CustomSendMailTraceListenerData : TraceListenerData
    {
        private const string SendMailProviderNameProperty = "sendMailProvider";
        private const string FormatterNameProperty = "formatter";
        private const string TresholdNameProperty = "messageTreshold";
        

        /// <summary>
        /// Initializes a <see cref="CustomSendMailTraceListenerData"/>.
        /// </summary>
        public CustomSendMailTraceListenerData() : base(typeof(CustomSendMailTraceListener))
        {
            ListenerDataType = typeof(CustomSendMailTraceListener);
        }

        /// <summary>
        /// Initializes a named instance of <see cref="CustomDatabaseTraceListenerData"/> with 
        /// name, stored procedure name, databse instance name, and formatter name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="tresholdNameProprety"></param>
        /// <param name="formatterName">The formatter name.</param>
        /// <param name="sendMailProviderNameProperty"></param>        
        public CustomSendMailTraceListenerData(string name,
                                                  string sendMailProviderNameProperty,
                                                  string tresholdNameProprety,
                                                  string formatterName)
            : this(
                name,
                sendMailProviderNameProperty,
                tresholdNameProprety,
                formatterName,
                TraceOptions.None,
                SourceLevels.All)
        {
        }

        /// <summary>
        /// Initializes a named instance of <see cref="CustomDatabaseTraceListenerData"/> with 
        /// name, stored procedure name, databse instance name, and formatter name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="tresholdNameProprety"></param>
        /// <param name="formatterName">The formatter name.</param>
        /// <param name="traceOutputOptions">The trace options.</param>
        /// <param name="filter">The filter to be applied</param>
        /// <param name="sendMailProviderName"></param>
        public CustomSendMailTraceListenerData(string name, string sendMailProviderName, string tresholdNameProprety, string formatterName, TraceOptions traceOutputOptions, SourceLevels filter)
            : base(name, typeof(CustomSendMailTraceListener), traceOutputOptions, filter)
        {
            Formatter = formatterName;
            SendMailProvider = sendMailProviderName;
            Treshold = tresholdNameProprety;
        }
       
        [ConfigurationProperty(SendMailProviderNameProperty, IsRequired = true, DefaultValue = "AuthorizedSmtpSendMailProvider")]
      
        public string SendMailProvider
        {
            get { return (string)base[SendMailProviderNameProperty]; }
            set { base[SendMailProviderNameProperty] = value; }
        }

        [ConfigurationProperty(TresholdNameProperty, IsRequired = true, DefaultValue = "100")]
        public string Treshold
        {
            get { return (string)base[TresholdNameProperty]; }
            set { base[TresholdNameProperty] = value; }
        }

        /// <summary>
        /// Gets and sets the formatter name.
        /// </summary>
        [ConfigurationProperty(FormatterNameProperty, IsRequired = false)]
        [Reference(typeof(NameTypeConfigurationElementCollection<FormatterData, CustomFormatterData>), typeof(FormatterData))]
        public string Formatter
        {
            get { return (string)base[FormatterNameProperty]; }
            set { base[FormatterNameProperty] = value; }
        }

        
        /// <summary>
        /// Returns a lambda expression that represents the creation of the trace listener described by this
        /// configuration object.
        /// </summary>
        /// <returns>A lambda expression to create a trace listener.</returns>
        protected override TraceListener CoreBuildTraceListener(LoggingSettings settings)
        {
            var type = Type.GetType(SendMailProvider);
            if (type == null) return null;

            var mailProvider = Activator.CreateInstance(type) as AuthorizedSmtpSendMailProvider; //TODO: change for generic type
            if (mailProvider == null) return null;
            var treshold = int.Parse(Treshold);

            return new CustomSendMailTraceListener(mailProvider, treshold);
        }
    }
}


 