﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.EnterpriseLibrary.Logging.TraceListeners;
using Newtonsoft.Json;
using Soikea.IntegrationEngine.Core.Implementation.Email;
using Soikea.IntegrationEngine.Core.Implementation.Settings;

namespace Soikea.IntegrationEngine.Core.Logging.EnterpriseLibrary
{
    public class CustomSendMailTraceListener : FormattedTraceListenerBase
    {
        public AuthorizedSmtpSendMailProvider MailProvider { get; set; }

        private object _cacheLock = new object();
        private List<CustomLogEntry> Cache { get; set; }

        private object _sentErrorsLock = new object();
        private List<CustomLogEntry> SentErrors { get; set; }
         
        private static int _treshold;
        private DateTime _warningEmailLastSent = DateTime.MinValue;

        public CustomSendMailTraceListener(AuthorizedSmtpSendMailProvider provider, int treshold)
        {
            Filter = new EventTypeFilter(SourceLevels.Critical | SourceLevels.Error | SourceLevels.Warning);
            MailProvider = provider;
            _treshold = treshold;
            SentErrors = new List<CustomLogEntry>();
        }

        public override void Write(string message)
        {
            throw new System.NotImplementedException();
        }

        public override void WriteLine(string message)
        {
            Write(message);
        }

        public override void TraceData(TraceEventCache eventCache, string source, TraceEventType eventType, int id, object data)
        {
            if ((Filter != null) && !Filter.ShouldTrace(eventCache, source, eventType, id, null, null, data, null))
                return;

            var message = data as LogEntry;
            if (message != null)
            {
                var logEntry = data as CustomLogEntry;
                var entry = logEntry ?? new CustomLogEntry(message);

                if (!VerifyMailProviderConfiguration())
                {
                    return;
                }

                if (entry.Severity <= TraceEventType.Error)
                {
                    var sendMail = false;
                    lock (_sentErrorsLock)
                    {
                        var sentError = SentErrors.FirstOrDefault(w => w.Message.Equals(entry.Message));

                        if (sentError == null)
                        {
                            SentErrors.Add(entry);
                            sendMail = true;
                        }
                        else
                        {
                            if (sentError.TimeStamp > entry.TimeStamp.AddMinutes(30))
                            {
                                SentErrors.Remove(sentError);
                                SentErrors.Add(entry);
                                sendMail = true;
                            }
                        }

                        if (sendMail)
                        {
                            try
                            {
                                MailProvider.Send(string.Empty, string.Empty, $"IntegrationEngine: Error logged in {entry.Module}", CreateContent(entry));
                            }
                            catch (Exception)
                            {
                                // This is the end of the line
                            }
                        }
                    }
                }
                else if (entry.Severity == TraceEventType.Warning )
                {
                    string content;

                    lock (_cacheLock)
                    {
                        if (Cache == null) Cache = new List<CustomLogEntry>();
                        Cache.Add(entry);

                        if (Cache.Count <= _treshold || _warningEmailLastSent > DateTime.Now.AddMinutes(-30))
                        {
                            return;
                        }

                        var cache = Cache.ToList();
                        Cache = new List<CustomLogEntry>();

                        content = CreateContentFromList(cache);
                        
                        _warningEmailLastSent = DateTime.Now;                        
                    }

                    try
                    {
                        MailProvider.Send(string.Empty, string.Empty, $"Warnings have reached the treshold in system", content);
                    }
                    catch (Exception)
                    {
                        // This is the end of the line
                    }                    
                }
            }
            else
            {
                base.TraceData(eventCache, source, eventType, id, data);
            }
        }

        private bool VerifyMailProviderConfiguration()
        {
            if (MailProvider.Configuration == null && GlobalSettings.Settings != null)
            {
                MailProvider.Configuration = GlobalSettings.Settings;

                if (MailProvider.Configuration.ContainsKey("AlertEmail_Sender"))
                {
                    MailProvider.DefaultSender = MailProvider.Configuration.GetValue("AlertEmail_Sender");
                }

                if (MailProvider.Configuration.ContainsKey("AlertEmail_Receivers"))
                {
                    MailProvider.DefaultReceivers = MailProvider.Configuration.GetValue("AlertEmail_Receivers")
                        .Split(';').ToList();
                }
            }

            return MailProvider.Configuration != null;
        }

        private string CreateContentFromList(List<CustomLogEntry> cache)
        {
            var instances = string.Join(Environment.NewLine, cache.Select(CreateContent));
            return instances;
        }

        private string CreateContent(CustomLogEntry entry)
        {
            var s = "";

            s += "Severity: " + entry.Severity + Environment.NewLine;
            s += "Module: " + entry.Module + Environment.NewLine;
            s += "CorrelationId: " + entry.ActivityId + Environment.NewLine;
            s += "JobId: " + entry.JobId + Environment.NewLine;
            s += "Timestamp: " + entry.TimeStamp + Environment.NewLine;
            s += "Categories: " + entry.Categories + Environment.NewLine;
            s += "Message: " + entry.Message;

            return s;
        }
    }
}
