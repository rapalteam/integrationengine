﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Xml.Serialization;
using Microsoft.Practices.EnterpriseLibrary.Logging;

namespace Soikea.IntegrationEngine.Core.Logging.EnterpriseLibrary
{
        [XmlRoot("customLogEntry")]
        [Serializable]
        public class CustomLogEntry : LogEntry
        {
            private string _jobId;
            
            /// <summary>
            /// Create an empty <see cref="CustomLogEntry"/>.
            /// </summary>
            public CustomLogEntry()
            {
            }

            /// <summary>
            /// Create a new instance of <see cref="LogEntry"/> with a full set of constructor parameters
            /// </summary>
            /// <param name="message">Message body to log.  Value from ToString() method from message object.</param>
            /// <param name="category">Category name used to route the log entry to a one or more trace listeners.</param>
            /// <param name="priority">Only messages must be above the minimum priority are processed.</param>
            /// <param name="eventId">Event number or identifier.</param>
            /// <param name="severity">Log entry severity as a <see cref="LogEntry.Severity"/> enumeration. (Unspecified, Information, Warning or Error).</param>
            /// <param name="title">Additional description of the log entry message.</param>
            /// <param name="properties">Dictionary of key/value pairs to record.</param>
            /// <param name="jobId">Custom data not included in base LogEntry</param>
            /// <param name="module">Name of the module that calls logger</param>        
            public CustomLogEntry(object message, string category, int priority, int eventId,
                            TraceEventType severity, string title, IDictionary<string, object> properties, string jobId, string module)
                : base(message, category, priority, eventId, severity, title, properties)
            {
                JobId = jobId;
                Module = module;
            }

            /// <summary>
            /// Create a new instance of <see cref="LogEntry"/> with a full set of constructor parameters
            /// </summary>
            /// <param name="message">Message body to log.  Value from ToString() method from message object.</param>
            /// <param name="categories">Collection of category names used to route the log entry to a one or more sinks.</param>
            /// <param name="priority">Only messages must be above the minimum priority are processed.</param>
            /// <param name="eventId">Event number or identifier.</param>
            /// <param name="severity">Log entry severity as a <see cref="LogEntry.Severity"/> enumeration. (Unspecified, Information, Warning or Error).</param>
            /// <param name="title">Additional description of the log entry message.</param>
            /// <param name="properties">Dictionary of key/value pairs to record.</param>
            /// <param name="jobId">Custom data not included in base LogEntry</param>
            /// <param name="module">Name of the module that calls logger</param>        
            public CustomLogEntry(object message, ICollection<string> categories, int priority, int eventId,
                            TraceEventType severity, string title, IDictionary<string, object> properties, string jobId, string module)
                : base(message, categories, priority, eventId, severity, title, properties)
            {
                JobId = jobId;
                Module = module;
            }

            public string JobId
            {
                get { return _jobId; }
                set { _jobId = value; }
            }

            public string Module { get; set; }
        
            public CustomLogEntry(LogEntry entry)
            {
                ActivityId = entry.ActivityId;
                Message = entry.Message;
                Categories = entry.Categories;
                Priority = entry.Priority;
                EventId = entry.EventId;
                Severity = entry.Severity;
                Title = entry.Title;
                ExtendedProperties = entry.ExtendedProperties;
            }
        }
    }

