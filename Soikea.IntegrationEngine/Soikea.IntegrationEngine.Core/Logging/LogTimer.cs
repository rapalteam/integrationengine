﻿using System;
using System.Diagnostics;
using System.Diagnostics.Contracts;

namespace Soikea.IntegrationEngine.Core.Logging
{
    /// <summary>
    /// A helper class for situation that you want to log execution time of something.
    /// </summary>
    public class LogTimer
    {
        public delegate void WriteLog(string message);

        private readonly WriteLog _writeLog;
        private readonly Stopwatch _stopwatch;

        public bool IsRunning => _stopwatch.IsRunning;

        public LogTimer(WriteLog writeLog) : this(writeLog, false)
        {            
        }

        public LogTimer(WriteLog writeLog, bool startAutomatically)
        {
            if (writeLog == null)
            {
                throw new ArgumentException("Writelog-delegate not provided");
            }

            _writeLog = writeLog;
            _stopwatch = new Stopwatch();

            if (startAutomatically)
            {
                Start();
            }
        }

        public void Start()
        {
            if (!_stopwatch.IsRunning)
                _stopwatch.Start();
        }

        public TimeSpan Stop()
        {
            _stopwatch.Stop();
            return _stopwatch.Elapsed;
        }
        
        public void LogElapsedTime(string message)
        {
            var ts = _stopwatch.Elapsed;
            var msg2 = String.Format("{4}: {0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds,
                ts.Milliseconds / 10, message);
            _writeLog(msg2);
        }
        public TimeSpan StopAndLog(string message)
        {
            var ts = Stop();
            var msg2 = String.Format("{4}: {0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds,
                ts.Milliseconds / 10, message);
            _writeLog(msg2);
            return ts;
        }

        public void StopAndLog(string format, params object[] args)
        {
            Contract.Requires(format != null, "format != null");
            Contract.Requires(args != null, "args != null");

            var msg1 = String.Format(format, args);
            StopAndLog(msg1);
        }

        public void LogActionTime(Action action, string message)
        {
            if (action == null)
            {
                throw new ArgumentException("Action-delegate not provided");
            }

            Start();
            action();
            StopAndLog(message);
        }

        public T LogActionTime<T>(Func<T> action, string message)
        {
            if (action == null)
            {
                throw new ArgumentException("Action-delegate not provided");
            }

            Start();
            T result = action();
            StopAndLog(message);
            return result;
        }
    }

}
