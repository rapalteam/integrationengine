using System;

namespace Soikea.IntegrationEngine.Core.Logging
{
    public class LoggerExecutionWrapper : ILog
    {
        private readonly ILog _log;
        public const string FailedToGenerateLogMessage = "Failed to generate logger with given configuration";

        public ILog WrappedLogger
        {
            get { return _log; }
        }

        public LoggerExecutionWrapper(ILog log)
        {
            _log = log;
        }

        public bool Log(LogLevel logLevel, Func<string> writeLogFunc, Exception exception = null)
        {
            if (writeLogFunc == null)
            {
                return _log.Log(logLevel, null);
            }

            Func<string> wrappedWriteLogFunc = () =>
            {
                try
                {
                    return writeLogFunc();
                }
                catch (Exception ex)
                {
                    Log(LogLevel.Error, () => FailedToGenerateLogMessage, ex);
                }
                return null;
            };
            return _log.Log(logLevel, wrappedWriteLogFunc, exception);
        }
    }
}