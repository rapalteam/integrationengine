﻿using System;
using System.Diagnostics.Contracts;

namespace Soikea.IntegrationEngine.Core.Logging
{

    public class Logger
    {
        public delegate void WriteLog(string message);

        private readonly WriteLog _writeLog;

        public Logger(WriteLog writeLog)
        {
            _writeLog = writeLog;
        }

        public void Write(string format, params object[] args)
        {
            Contract.Requires(format != null, "format != null");
            Contract.Requires(args != null, "args != null");

            var msg1 = String.Format(format, args);
            _writeLog(msg1);
        }
    }
}
