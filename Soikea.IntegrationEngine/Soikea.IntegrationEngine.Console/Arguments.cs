﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Soikea.IntegrationEngine.Console
{


    public interface IArguments
    {
        Commands AvailableCommands { get; }
        Command CommandGiven { get; }
        Dictionary<string, string> NamedParameters { get; }
        List<string> CommandLineArgs { get; set; }
        Arguments.ParametersType ParametersGiven { get; set; }
        string CommandLineArgsString { get; }
        string GetAvailableCommands();
    }

    public class Arguments : IArguments
    {
        public const string HelpArg = "help";

        public Commands AvailableCommands { get; private set; }

        public Command CommandGiven { get; private set; }

        public enum ParametersType { NotGiven, FreeForm, Named }

        public Dictionary<string, string> NamedParameters { get; private set; }

        public List<string> CommandLineArgs { get; set; }

        public ParametersType ParametersGiven { get; set; }

        public string CommandLineArgsString
        {
            get
            {
                if (CommandLineArgs == null || CommandLineArgs.Count < 1)
                    return string.Empty;

                return string.Join(" ", CommandLineArgs);
            }
        }


        public Arguments(string[] args, Commands commands)
        {
            AvailableCommands = commands;

            CommandGiven = args.Length < 1 || args.Any(x => x == HelpArg) // Default to help-command
                ? AvailableCommands.Get(HelpArg) 
                : AvailableCommands.Get(args[0]);
            
            NamedParameters = new Dictionary<string, string>();
            CommandLineArgs = new List<string>();
            if (args.Length > 1)
            {
                var argsExcludingCommand = new string[args.Length-1];
                Array.Copy(args, 1, argsExcludingCommand, 0, args.Length -1);

                InitializeParameters(argsExcludingCommand);
            }
        }

        private void InitializeParameters(string[] args)
        {
            if (HasNamedParameters(args))
            {
                ParametersGiven = ParametersType.Named;

                NamedParameters = ParseNamedParameters(args);
            }
            else
            {
                ParametersGiven = ParametersType.FreeForm;
                foreach (string arg in args)
                {
                    CommandLineArgs.Add(arg);
                }
            }
        }

        private static Dictionary<string, string> ParseNamedParameters(string[] args)
        {
            var ret = new Dictionary<string, string>();

            string activeParam = null;
            string activeValue = null;

            foreach (string arg in args)
            {
                if (arg.StartsWith("--")) // New parameter value initialization
                {
                    if (!string.IsNullOrEmpty(activeParam)) // End of previous parameter value initialization
                    {
                        // Flush initialized values to result-dictionary
                        ret[activeParam] = activeValue;
                    }

                    activeParam = arg.TrimStart('-');
                    activeValue = null;
                }
                else if (!string.IsNullOrEmpty(activeParam))
                {
                    if (string.IsNullOrEmpty(activeValue))
                        activeValue = arg;
                    else activeValue += " " + arg;
                }
            }

            if (!string.IsNullOrEmpty(activeParam)) // Flush latest parameter values to result-dictionary
            {                
                ret[activeParam] = activeValue;
            }

            return ret;
        }

        private static bool HasNamedParameters(string[] args)
        {
            return args.Any(x => x.StartsWith("--"));
        }


        public string GetAvailableCommands()
        {
            return string.Join(Environment.NewLine, AvailableCommands);
        }
    }
}
