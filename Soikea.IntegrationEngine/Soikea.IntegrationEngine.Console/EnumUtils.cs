﻿using System;
using System.ComponentModel;
using System.Linq;

namespace Soikea.IntegrationEngine.Console
{
    public static class EnumUtils
    {
        public static string StringValueOf(Enum value)
        {
            var fi = value.GetType().GetField(value.ToString());
            var attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            return attributes.Length > 0
                ? attributes[0].Description
                : value.ToString();
        }

        public static T EnumValueOf<T>(string value)
        {
            var enumType = typeof(T);
            var names = Enum.GetNames(enumType);

            foreach (string name in names.Where(name => StringValueOf((Enum)Enum.Parse(enumType, name)).Equals(value)))
            {
                return (T)Enum.Parse(enumType, name);
            }

            throw new ArgumentException("Matching enumeration value not found");
        }

        public static T EnumValueOfOrDefault<T>(string value)
        {
            try
            {
                return EnumValueOf<T>(value);
            }
            catch (ArgumentException)
            {
                return default(T);
            }
        }
    }
}
