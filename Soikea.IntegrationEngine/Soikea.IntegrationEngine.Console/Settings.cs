﻿using System.Collections.Generic;
using Soikea.IntegrationEngine.Core.Interfaces;

namespace Soikea.IntegrationEngine.Console
{
    public class Settings : Dictionary<string, string>, ISettings, ITenantSettings
    {
        public string GetValue(string key)
        {
            return this[key];
        }

        public void SetValue(string key, string value)
        {
            this[key] = value;
        }
        
        public static Settings Create(Dictionary<string, string> values)
        {
            var settings = new Settings();

            foreach (var keyValuePair in values)
            {
                settings[keyValuePair.Key] = keyValuePair.Value;
            }

            return settings;
        }
    }
}
