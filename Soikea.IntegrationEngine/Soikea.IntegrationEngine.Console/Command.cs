﻿using System;
using Soikea.IntegrationEngine.Console.Tasks;

namespace Soikea.IntegrationEngine.Console
{
    public class Command
    {
        protected bool Equals(Command other)
        {
            return string.Equals(CommandString, other.CommandString);
        }

        public override int GetHashCode()
        {
            return (CommandString != null ? CommandString.GetHashCode() : 0);
        }

        public readonly Func<TaskBase> Task;
        public readonly string CommandString;


        public Command(string commandString, Func<TaskBase> task)
        {
            Task = task;
            CommandString = commandString;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            
            return obj.GetType() == this.GetType() && Equals((Command) obj);
        }       
    }
}