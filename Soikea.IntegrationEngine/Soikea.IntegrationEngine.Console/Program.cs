﻿using System;
using System.Net;

namespace Soikea.IntegrationEngine.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("--- Soikea Integration Engine console ---");
            ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, errors) =>
            {
                System.Console.WriteLine("Validating server certificate");
                return true;
            };

            try
            {
                TaskConfiguration.Initialize();

                var arguments = new Arguments(args ?? new []{ Arguments.HelpArg }, TaskConfiguration.AvailableCommands);

                var task = arguments.CommandGiven.Task();
                try
                {
                    task.Execute(System.Console.WriteLine, arguments);
                }
                catch (Exception e)
                {
                    System.Console.WriteLine("An error occurred: {0}", e.Message);
                }   
            }
            catch (Exception)
            {
                System.Console.WriteLine("Invalid argument for command, use \"--help\" to get available commands.");
            }

#if DEBUG
            System.Console.ReadKey();
#endif
        }
    }
}
