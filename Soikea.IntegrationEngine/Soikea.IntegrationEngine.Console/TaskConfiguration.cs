﻿using System.Linq.Expressions;
using Soikea.IntegrationEngine.Console.Tasks;

namespace Soikea.IntegrationEngine.Console
{
    public static class TaskConfiguration
    {
        public static readonly Commands AvailableCommands = new Commands();

        public static void Initialize()
        {
            AvailableCommands.Add(new Command("help", () => new HelpTask()));
            AvailableCommands.Add(new Command("sendmailtest", () => new SendMailTest()));
            AvailableCommands.Add(new Command("encrypt", () => new EncryptionTask()));
        }
    }
}
