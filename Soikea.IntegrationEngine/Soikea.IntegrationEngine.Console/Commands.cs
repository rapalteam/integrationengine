using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Soikea.IntegrationEngine.Console
{
    /// <summary>
    /// Allowed commands for application arguments.
    /// Command-line value is defined as description for enum value.
    /// </summary>
    public class Commands : IEnumerable<string>
    {
        private readonly List<Command> _commands = new List<Command>();

        public void Add(Command command)
        {
            _commands.Add(command);
        }

        public bool IsAvailable(string commandString)
        {
            return _commands.Any(x => x.CommandString == commandString);
        }

        public Command Get(string commandString)
        {
            return _commands.Single(x => x.CommandString == commandString);
        }

        public string GetHelp(string commandString)
        {
            return _commands.Single(x => x.CommandString == commandString).Task().GetHelp();
        }        

        public IEnumerator<string> GetEnumerator()
        {
            return _commands.Select(x => x.CommandString)
                .GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}