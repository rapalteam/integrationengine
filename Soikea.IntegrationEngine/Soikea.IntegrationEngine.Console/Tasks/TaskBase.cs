﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Soikea.IntegrationEngine.Console.Tasks
{
    public abstract class TaskBase
    {

        public abstract string GetHelp();

        public delegate void WriteOutput(string output);

        public abstract void Execute(WriteOutput writeOutputDelegate, IArguments arguments);

        protected static List<string> ParseFreeFormParameters(Regex pattern, string source)
        {
            if (!pattern.IsMatch(source))
                throw new ArgumentException("Invalid pattern on given arguments");

            var match = pattern.Match(source);

            var res = new List<string>();

            for (int i = 1; i < match.Groups.Count; i++) //Drop the first match since it is the whole source that matches
            {
                Group group = match.Groups[i];
                res.Add(group.Value);
            }

            return res;
        }
    }
}
