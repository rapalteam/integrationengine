﻿using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;
using System.Web.Configuration;

namespace Soikea.IntegrationEngine.Console.Tasks
{
    public class EncryptionTask : TaskBase
    {
        private static readonly Regex FreeFormParametersPattern = new Regex(@"^([a-zA-Z]*) (.*)\\([a-zA-Z]*)/(.*)@([\.a-zA-Z]*)\:([0-9]) from (.*) to (.*)?$");

        public override string GetHelp()
        {
            return @"
Encrypt settings with EncryptTask. 


Parameters:
    --direction : encrypt or decrypt
    --path      : path to web.config (related to this location)
    --section   : sectiongroup  where section is (ie. encryptedSettings) (not used currently)
    --section   : section to be encrypted
    
Usage:
    {task} --section {section}
";
        }

        public override void Execute(WriteOutput writeOutputDelegate, IArguments arguments)
        {
            if (arguments.ParametersGiven == Arguments.ParametersType.NotGiven)
                return;

            var parameters = arguments.ParametersGiven == Arguments.ParametersType.FreeForm
                ? MapFreeFormParameters(ParseFreeFormParameters(FreeFormParametersPattern, arguments.CommandLineArgsString))
                : arguments.NamedParameters;

            writeOutputDelegate.Invoke($"Parsed parameters, opening configuration from {parameters["path"]}");

            var webConfLocation = MapLocation(parameters["path"]);

            var manager = WebConfigurationManager.OpenMappedWebConfiguration(webConfLocation, "/");

            writeOutputDelegate.Invoke($"Opened web.config");

            var section = manager.GetSectionGroup(parameters["sectiongroup"]);

            if (section == null)
            {
                writeOutputDelegate.Invoke($"Could not find sectionGroup");
                return;
            }

            var encrypted = section.Sections.Get(parameters["section"]);

            if (encrypted == null)
            {
                writeOutputDelegate.Invoke($"Could not find section to encrypt");
                return;
            }

            if (parameters["direction"].Equals("encrypt"))
            {
                if (encrypted.SectionInformation.IsProtected)
                {
                    writeOutputDelegate.Invoke($"Cannot encrypt already crypted setting");
                    return;
                }

                writeOutputDelegate.Invoke($"EncryptedSettings IsProtected:  {encrypted.SectionInformation.IsProtected}");
                encrypted.SectionInformation.ProtectSection("DataProtectionConfigurationProvider");

                writeOutputDelegate.Invoke($"Finished encrypting settings");
            }
            else if (parameters["direction"].Equals("decrypt"))
            {
                if (!encrypted.SectionInformation.IsProtected)
                {
                    writeOutputDelegate.Invoke($"Cannot decrypt unprotected setting");
                    return;
                }

                writeOutputDelegate.Invoke($"EncryptedSettings IsProtected:  {encrypted.SectionInformation.IsProtected}");
                encrypted.SectionInformation.UnprotectSection();

                writeOutputDelegate.Invoke($"Finished decrypting settings");
            }

            encrypted.SectionInformation.ForceSave = true;

            manager.Save(ConfigurationSaveMode.Full);
        }

        private WebConfigurationFileMap MapLocation(string s)
        {
            var configFile = new FileInfo(s);
            var vdm = new VirtualDirectoryMapping(configFile.DirectoryName, true, configFile.Name);
            var wcfm = new WebConfigurationFileMap();
            wcfm.VirtualDirectories.Add("/", vdm);
            return wcfm;
        }

        private static Dictionary<string, string> MapFreeFormParameters(List<string> parseFreeFormParametersResult)
        {
            var ret = new Dictionary<string, string>
            {
                ["direction"] = parseFreeFormParametersResult[0] ?? string.Empty,
                ["path"] = parseFreeFormParametersResult[1] ?? string.Empty,
                ["sectiongroup"] = parseFreeFormParametersResult[2] ?? string.Empty,
                ["section"] = parseFreeFormParametersResult[3] ?? string.Empty,
            };
            
            return ret;
        }
    }
}
