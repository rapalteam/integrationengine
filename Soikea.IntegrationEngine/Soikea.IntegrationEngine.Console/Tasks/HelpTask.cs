﻿namespace Soikea.IntegrationEngine.Console.Tasks
{
    public class HelpTask : TaskBase
    {
        public override string GetHelp()
        {
            return @"
Command-line usage of the tool: {task} {parameters}
Get help for a task: help {task}
";
        }

        public override void Execute(WriteOutput writeOutputDelegate, IArguments arguments)
        {
            if (arguments.ParametersGiven == Arguments.ParametersType.NotGiven)
            {
                writeOutputDelegate(GetHelp());
                writeOutputDelegate("Available tasks:");
                writeOutputDelegate(arguments.GetAvailableCommands());
                return;
            }

            if (arguments.ParametersGiven == Arguments.ParametersType.FreeForm)
            {
                foreach (var commandLineArg in arguments.CommandLineArgs)
                {
                    writeOutputDelegate(arguments.AvailableCommands.IsAvailable(commandLineArg)
                        ? arguments.AvailableCommands.GetHelp(commandLineArg)
                        : string.Format("Invalid argument: {0}", commandLineArg));
                }

                return;
            }

            writeOutputDelegate("Invalid arguments");
        }

    }
}
