﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text.RegularExpressions;
using Soikea.IntegrationEngine.Core.Implementation.Email;

namespace Soikea.IntegrationEngine.Console.Tasks
{
    public class SendMailTest : TaskBase
    {
        private static readonly Regex FreeFormParametersPattern = new Regex(@"^([a-zA-Z]*) (.*)\\([a-zA-Z]*)/(.*)@([\.a-zA-Z]*)\:([0-9]) from (.*) to (.*)?$");

        public enum SendMailProviders
        {
            [Description("authorized")]
            Authorized,
            [Description("anonymous")]
            Anonymous
        }

        public override string GetHelp()
        {
            return @"
Test ISendMailProvider by sending a mail with it.

Parameters:
    --provider  : SendMailProvider to use, available values: authorized, anonymous
    --server    : DNS name or an IP-address for SMTP-server (required)
    --port      : Port to use (required)
    --user      : UserName to use (required)
    --password  : Password (required)
    --domain    : Domain to authenticate against (required)
    --sender    : Sender email (required)
    --receiver  : Receiver for the email (required)

Usage: 
    {task} --provider {provider} --server {server} --port {port} --user {user} --password {password} --domain {domain} --sender {sender} --receiver {receiver}     
    or  (Not currently available)
    {task} {provider} {domain}\{user}/{password}@{server}:{port} from {sender} to {receiver}
";
        }

        public override void Execute(WriteOutput writeOutputDelegate, IArguments arguments)
        {
            if (arguments.ParametersGiven == Arguments.ParametersType.NotGiven)
                return;

            var parameters = arguments.ParametersGiven == Arguments.ParametersType.FreeForm
                ? MapFreeFormParameters(ParseFreeFormParameters(FreeFormParametersPattern, arguments.CommandLineArgsString))
                : arguments.NamedParameters;

            var provider = EnumUtils.EnumValueOf<SendMailProviders>(parameters["provider"]);
            writeOutputDelegate($"Using provider: {provider}");

            var settings = MapToSmtpSettings(parameters);
            ISendMailProvider sendmail;

            switch (provider)
            {
                case SendMailProviders.Authorized:
                    sendmail = new AuthorizedSmtpSendMailProvider()
                    {
                        Configuration = settings
                    };
                    break;
                case SendMailProviders.Anonymous:
                default:
                    sendmail = new SmtpSendMailProvider()
                    {
                        Configuration = settings
                    };
                break;
            }

            writeOutputDelegate($"Sending test-email using settings: {string.Join(", ", settings.Select(x => $"{x.Key}: {x.Value}"))}");
            try
            {
                sendmail.Send(parameters["sender"], parameters["receiver"], "Test email provider", "Testing email provider");
                writeOutputDelegate("Great success");
            }
            catch (Exception e)
            {
                writeOutputDelegate($"Caught an error: {e.Message}");
            }            
        }

        private static Dictionary<string, string> MapFreeFormParameters(List<string> parseFreeFormParametersResult)
        {
            var ret = new Dictionary<string, string>();

            ret["provider"] = parseFreeFormParametersResult[0] ?? string.Empty;
            ret["domain"] = parseFreeFormParametersResult[1] ?? string.Empty;
            ret["user"] = parseFreeFormParametersResult[2] ?? string.Empty;
            ret["password"] = parseFreeFormParametersResult[3] ?? string.Empty;
            ret["server"] = parseFreeFormParametersResult[4] ?? string.Empty;
            ret["port"] = parseFreeFormParametersResult[5] ?? string.Empty;
            ret["sender"] = parseFreeFormParametersResult[6] ?? string.Empty;
            ret["receiver"] = parseFreeFormParametersResult[7] ?? string.Empty;

            return ret;
        }

        private static Settings MapToSmtpSettings(Dictionary<string, string> parameters)
        {
            var settingParams = new Dictionary<string, string>();

            settingParams["provider"] = parameters["provider"];
            settingParams["AuthorizedSmtpDomain"] = parameters["domain"];
            settingParams["AuthorizedSmtpUser"] = parameters["user"];
            settingParams["AuthorizedSmtpPassword"] = parameters["password"];
            settingParams["AuthorizedSmtpServer"] = parameters["server"];
            settingParams["DefaultSmtpServer"] = parameters["server"];
            settingParams["AuthorizedSmtpServerPort"] = parameters["port"];
            settingParams["DefaultSmtpServerPort"] = parameters["port"];
            settingParams["Sender"] = parameters["sender"];
            settingParams["Receiver"] = parameters["receiver"];

            return Settings.Create(settingParams);
        } 
    }
}
