﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Soikea.IntegrationEngine.Administration.EventSubscribers;
using Soikea.IntegrationEngine.Administration.Model;
using Soikea.IntegrationEngine.Administration.Repository;
using Soikea.IntegrationEngine.Core.Implementation.Events;
using Soikea.IntegrationEngine.Core.Interfaces;
using Soikea.IntegrationEngine.Core.Logging;

namespace Soikea.IntegrationEngine.Administration.Tests
{
    [TestClass]
    public class EndOfJobEventSubscriberTests
    {
        [ThreadStatic]
        private ILogProvider _logProvider;

        [TestMethod]
        public void InstanceExecutionEndsWhenEndOfJobEventReceived()
        {
            #region Setup
            var mockLogProvider = new Mock<ILogProvider>();
            var mockLogger = new Mock<ILog>();

            mockLogger.Setup(x => x.Log(LogLevel.Trace, It.IsAny<Func<string>>(), It.IsAny<Exception>()));
            mockLogProvider.Setup(x => x.GetLogger(It.IsAny<string>(), It.IsAny<string>()))
              .Returns(mockLogger.Object);
            _logProvider = mockLogProvider.Object;
            Log.SetCurrentLogProvider(_logProvider, "Test");

            var guid = Guid.NewGuid();
            var mockRepo = new Mock<IInstanceRepository>();

            mockRepo.Setup(x => x.EndInstanceExecution(It.Is<EndInstance>(w =>
                 w.CorrelationId.Equals(guid.ToString()) &&
                 w.ResultStatus == JobResultStatus.Ok)))
            .Verifiable();

            mockRepo.Setup(w => w.GetByCorrelationId(guid.ToString())).Returns(new Instance());
            #endregion

            var repo = mockRepo.Object;

            var endJob = new EndOfJobEventSubscriber(repo);


            var endEvent = new EndOfJobEvent("default-tenant", guid, "default-job", JobResultStatus.Ok, 10, 10);

            endJob.Receive(endEvent);


            mockRepo.VerifyAll();
        }

        [TestMethod]
        public void AnErrorMessageIsLoggedWhenEndOfJobEventReceivedBeforeStartOfJobEvent()
        {
            #region Setup
            var mockLogProvider = new Mock<ILogProvider>();
            var mockLogger = new Mock<ILog>();
            var mockRepo = new Mock<IInstanceRepository>();
            var logMessage = string.Empty;

            mockLogger.Setup(x => x.Log(LogLevel.Warn, It.IsAny<Func<string>>(), It.IsAny<Exception>())).
                Callback((LogLevel l, Func<string> func, Exception ex) => logMessage = func.Invoke());
            mockLogProvider.Setup(x => x.GetLogger(It.IsAny<string>(), It.IsAny<string>()))
              .Returns(mockLogger.Object);
            _logProvider = mockLogProvider.Object;
            Log.SetCurrentLogProvider(_logProvider, "Test");

            mockRepo.Setup(x => x.EndInstanceExecution(It.IsAny<EndInstance>()));
            mockRepo.Setup(x => x.GetByCorrelationId(It.IsAny<string>()))
                .Returns((string s) => null);
            #endregion
            var guid = Guid.NewGuid();
            var end = new EndOfJobEvent("default-tenant", guid, "default-job", JobResultStatus.Ok, 10, 10);

            var repo = mockRepo.Object;

            var endJob = new EndOfJobEventSubscriber(repo);

            endJob.Receive(end);

            Assert.AreEqual($"Could not find instance with id {end.JobId}", logMessage);
        }
    }
}
