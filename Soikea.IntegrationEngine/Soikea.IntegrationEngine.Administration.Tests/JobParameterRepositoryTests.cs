﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soikea.IntegrationEngine.Administration.Repository;

namespace Soikea.IntegrationEngine.Administration.Tests
{
    [TestClass]
    public class JobParameterRepositoryTests
    {
       
        [TestMethod]
        public void SuccessfullyAddAndGetStringValueFromRepostiory()
        {
            var repo = new JobParameterRepository("defaultTenant", "defaultJob");
            var guid = Guid.NewGuid().ToString();
            repo.AddOrUpdateParameter<string>(guid, "testValue");

            var value = repo.GetParameter<string>(guid);
            Assert.AreEqual("testValue", value);
        }

        [TestMethod]
        public void SuccessfullyAddAndGetIntValueFromRepository()
        {
            var repo = new JobParameterRepository("defaultTenant", "defaultJob");
            var guid = Guid.NewGuid().ToString();
            repo.AddOrUpdateParameter<int>(guid, 16);

            var value = repo.GetParameter<int>(guid);

            Assert.AreEqual(16, value);
        }

        [TestMethod]
        public void SuccessfullyAddAndGetDateTimeOffsetFromRepository()
        {
            var repo = new JobParameterRepository("defaultTenant", "defaultJob");
            var guid = Guid.NewGuid().ToString();
            var now = DateTimeOffset.UtcNow;
            repo.AddOrUpdateParameter<DateTimeOffset>(guid, now);

            var value = repo.GetParameter<DateTimeOffset>(guid);
            Assert.AreEqual(now, value);

        }

        [TestMethod]
        public void TryToGetValueThatDoesNotExistFromRepoAndGetDefaultValue()
        {
            var repo = new JobParameterRepository("defaultTenant", "defaultJob");
            var value = repo.GetParameter<int>("nonExistingKey");

            Assert.AreEqual(default(int), value);
        }

        [ExpectedException(typeof (NotSupportedException))]
        [TestMethod]
        public void TryToGetNotSupportedValueTypeFromRepoAndReceiveException()
        {
            var repo = new JobParameterRepository("defaultTenant", "defaultJob");
            repo.GetParameter<DateTime>("nonExistingKey");
        }

        [ExpectedException(typeof (NotSupportedException))]
        [TestMethod]
        public void TryToSaveNotSupportedTypeToRepoAndReceiveException()
        {
            var repo = new JobParameterRepository("defaultTenant", "defaultJob");
            repo.AddOrUpdateParameter<DateTime>("failingKey", DateTime.UtcNow);
        }

        [TestMethod]
        public void AddDuplicateKeyToRepository()
        {
            var repo = new JobParameterRepository("defaultTenant", "defaultJob");
            repo.AddOrUpdateParameter<string>("example", "example");
            repo.AddOrUpdateParameter<string>("example", "example2");

            var param = repo.GetParameter<string>("example");
            Assert.AreEqual("example2", param);
        }

        [TestMethod]
        public void SaveEntryToDbAndTryToGetAnotherTypeWithSameKey()
        {
            var repo = new JobParameterRepository("defaultTenant", "defaultJob");
            repo.AddOrUpdateParameter<string>("example", "example");
            var value = repo.GetParameter<DateTimeOffset>("example");

            Assert.AreEqual(default(DateTimeOffset), value);
        }
    }
}
