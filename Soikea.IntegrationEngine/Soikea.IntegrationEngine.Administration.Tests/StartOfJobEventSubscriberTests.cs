﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Soikea.IntegrationEngine.Administration.EventSubscribers;
using Soikea.IntegrationEngine.Administration.Model;
using Soikea.IntegrationEngine.Administration.Repository;
using Soikea.IntegrationEngine.Core.Implementation.Events;
using Soikea.IntegrationEngine.Core.Logging;

namespace Soikea.IntegrationEngine.Administration.Tests
{
    [TestClass]
    public class StartOfJobEventSubscriberTests
    {
        [TestMethod]
        public void InstanceGetsAddedWhenReceivedStartOfJobEvent()
        {
            #region Setup

            var mockLogProvider = new Mock<ILogProvider>();
            var mockLogger = new Mock<ILog>();

            mockLogger.Setup(x => x.Log(LogLevel.Trace, It.IsAny<Func<string>>(), It.IsAny<Exception>()));
            mockLogProvider.Setup(x => x.GetLogger(It.IsAny<string>(), It.IsAny<string>()))
              .Returns(mockLogger.Object);
            Log.SetCurrentLogProvider(mockLogProvider.Object, "Test");

            var guid = Guid.NewGuid();
            var @event = new StartOfJobEvent("default-tenant", guid, "default-job");
            var mockRepo = new Mock<IInstanceRepository>();

            mockRepo.Setup(w => w.Add(It.Is<Instance>( x => x.CorrelationId.Equals(guid.ToString()) && x.JobName.Equals("default-job")))).Verifiable();
            #endregion

            var repo = mockRepo.Object;

            var startJob = new StartOfJobEventSubscriber(repo);

            startJob.Receive(@event);
            mockRepo.VerifyAll();
        }
    }
}
