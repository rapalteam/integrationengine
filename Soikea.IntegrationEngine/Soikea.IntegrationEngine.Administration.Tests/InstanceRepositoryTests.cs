﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soikea.IntegrationEngine.Administration.Model;
using Soikea.IntegrationEngine.Administration.Repository;
using Soikea.IntegrationEngine.Administration.Migrations;
using Soikea.IntegrationEngine.Core.Interfaces;

namespace Soikea.IntegrationEngine.Administration.Tests
{
    [TestClass]
    public class InstanceRepositoryTests
    {
        private static readonly DateTimeOffset DefaultStart = new DateTimeOffset(2016, 2, 2, 12, 37, 00,
            new TimeSpan(0, 0, 0, 0));
        private static readonly DateTimeOffset DefaultEnd = new DateTimeOffset(2016, 2, 2, 13, 37, 00, new TimeSpan(0, 0, 0, 0));

        [ClassInitialize]
        public static void Init(TestContext context)
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<AdministrationContext, Configuration>());
        }

        [TestMethod]
        public void GetReturnsInitializedSetOfInstances()
        {
            var repo = new InstanceRepository();
            var instances = repo.Get(DefaultStart, DefaultEnd);
            Assert.IsNotNull(instances);
            Assert.AreEqual(4, instances.Count());
        }

        [TestMethod]
        public void GetReturnsCorrectResultsWhenFilteredWithStatus()
        {
            var repo = new InstanceRepository();
            var instances = repo.Get(DefaultStart, DefaultEnd, JobResultStatus.Ok);
            Assert.IsNotNull(instances);
            Assert.AreEqual(2, instances.Count());
        }

        [TestMethod]
        public void AddInstanceSucceeds()
        {
            try
            {
                var repo = new InstanceRepository();
                var instances = repo.Get(DefaultStart, DateTimeOffset.UtcNow);

                var before = instances.ToList();
                var instance = new Instance
                {
                    TenantId = "Test",
                    CorrelationId = Guid.NewGuid().ToString(),
                    StartTime = DateTimeOffset.UtcNow.AddMinutes(-20),
                    Id = before.Max(w => w.Id) + 1,
                    JobName = Guid.NewGuid().ToString(),
                    EndTime = DateTimeOffset.UtcNow.AddMinutes(-10),
                    Duration = TimeSpan.FromMinutes(10),
                    Result = JobResultStatus.Ok
                };
                repo.Add(instance);

                var after = repo.Get(DefaultStart, DateTimeOffset.UtcNow);

                Assert.AreEqual(after.Count(), before.Count() + 1);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }

        [ExpectedException(typeof (InvalidOperationException))]
        [TestMethod]
        public void AddInstanceFailsWhenDuplicateCorrelationIdIsProvided()
        {

            var repo = new InstanceRepository();

            var instance = new Instance
            {
                TenantId = "Test",
                CorrelationId = Guid.NewGuid().ToString(),
                StartTime = DateTimeOffset.UtcNow.AddMinutes(-20),
                JobName = Guid.NewGuid().ToString(),
                EndTime = DateTimeOffset.UtcNow.AddMinutes(-10),
                Duration = TimeSpan.FromMinutes(10),
                Result = JobResultStatus.Ok
            };
            repo.Add(instance);

            repo.Add(instance);
        }

        [TestMethod]
        public void EndInstanceExecutionSucceeds()
        {
            try
            {
                var repo = new InstanceRepository();
                
                var before = repo.GetByCorrelationId("correlation-id-for-update-test");
                Assert.AreEqual(JobResultStatus.Started, before.Result,"Can't test because setup is faulted");

                var end = new EndInstance
                {
                    CorrelationId = "correlation-id-for-update-test",
                    EndTime = DateTimeOffset.UtcNow,
                    Payload = "payload",
                    ResultStatus = JobResultStatus.Ok
                };

                repo.EndInstanceExecution(end);

                var after = repo.GetByCorrelationId("correlation-id-for-update-test");

                Assert.AreEqual(JobResultStatus.Ok, after.Result);
                Assert.AreEqual(end.EndTime, after.EndTime);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }

        [ExpectedException(typeof (ObjectNotFoundException))]
        [TestMethod]
        public void EndInstanceExecutionFailsWhenNonExistingCorrelationIdIsGiven()
        {
            var repo = new InstanceRepository();
            var end = new EndInstance
            {
                CorrelationId = Guid.NewGuid().ToString(),
                EndTime = DateTimeOffset.UtcNow,
                Payload = "payload",
                ResultStatus = JobResultStatus.Ok
            };

            repo.EndInstanceExecution(end);
        }

        [TestMethod]
        public void GetByResultStatus()
        {
            try
            {
                var repo = new InstanceRepository();
                var states = new List<JobResultStatus>
                {
                    JobResultStatus.Error,
                    JobResultStatus.Aborted,
                };

                var instances = repo.GetByStatus(states);

                Assert.IsNotNull(instances);
                var enumerable = instances.ToList();

                Assert.AreEqual(enumerable.Count(), 2);
                Assert.AreEqual(enumerable.Count(w => w.Result == JobResultStatus.Aborted), 1);
                Assert.AreEqual(enumerable.Count(w => w.Result == JobResultStatus.Error) , 1);
                Assert.IsTrue(!enumerable.Any(w => w.Result != JobResultStatus.Error && w.Result != JobResultStatus.Aborted));
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }


    }
}
