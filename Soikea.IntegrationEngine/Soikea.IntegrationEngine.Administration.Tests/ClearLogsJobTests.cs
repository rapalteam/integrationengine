﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Soikea.IntegrationEngine.Administration.Jobs;
using Soikea.IntegrationEngine.Core.Implementation.Sql;
using Soikea.IntegrationEngine.Core.Interfaces;
using Soikea.IntegrationEngine.Core.Logging;

namespace Soikea.IntegrationEngine.Administration.Tests
{
    [TestClass]
    public class ClearLogsJobTests
    {
        [TestInitialize]
        public void Initialize()
        {
            Log.SetCurrentLogProvider(new NoOpLogProvider(), "Unit-test");
        }

        private ClearLogsJob GetTestJob(Mock<IStoredProcedureCallProvider> mockProcCallProvider)
        {
            var mockSettings = new Mock<ISettings>();

            var testJob = new ClearLogsJob()
            {
                Settings = mockSettings.Object,
                StoredProcedureCallProvider = mockProcCallProvider.Object
            };

            return testJob;
        }

        [TestMethod]
        public void ExecuteReturnsErrorWhenAnExceptionOccurs()
        {
            var mockWatchDog = new Mock<IJobAbortionWatchDog>();
            var mockProcCallProvider = new Mock<IStoredProcedureCallProvider>();
            mockProcCallProvider.Setup(x => x.ExecuteProcedureCall(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<IJobAbortionWatchDog>()))
                .Throws<InvalidOperationException>();

            var testJob = GetTestJob(mockProcCallProvider);
            
            var result = testJob.Execute(mockWatchDog.Object, (e) => { }, (e) => { });

            Assert.AreEqual(JobResultStatus.Error, result);
        }

        [TestMethod]
        public void ExecuteReturnsAbortedWhenJobAbortionThrown()
        {
            var mockWatchDog = new Mock<IJobAbortionWatchDog>();
            var mockProcCallProvider = new Mock<IStoredProcedureCallProvider>();
            mockProcCallProvider.Setup(x => x.ExecuteProcedureCall(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<IJobAbortionWatchDog>()))
                .Throws<JobAbortedException>();

            var testJob = GetTestJob(mockProcCallProvider);

            var result = testJob.Execute(mockWatchDog.Object, (e) => { }, (e) => { });

            Assert.AreEqual(JobResultStatus.Aborted, result);
        }

        [TestMethod]
        public void ExecuteReturnsOkWhenStoredProcedureExecutionIsSuccessful()
        {
            var mockWatchDog = new Mock<IJobAbortionWatchDog>();
            var mockProcCallProvider = new Mock<IStoredProcedureCallProvider>();
            mockProcCallProvider.Setup(x => x.ExecuteProcedureCall(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<IJobAbortionWatchDog>()))
                .Returns(true);

            var testJob = GetTestJob(mockProcCallProvider);

            var result = testJob.Execute(mockWatchDog.Object, (e) => { }, (e) => { });

            Assert.AreEqual(JobResultStatus.Ok, result);
        }

        [TestMethod]
        public void ExecuteReturnsErrorWhenStoredProcedureExecutionIsNotSuccessful()
        {
            var mockWatchDog = new Mock<IJobAbortionWatchDog>();
            var mockProcCallProvider = new Mock<IStoredProcedureCallProvider>();
            mockProcCallProvider.Setup(x => x.ExecuteProcedureCall(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<IJobAbortionWatchDog>()))
                .Returns(false);

            var testJob = GetTestJob(mockProcCallProvider);

            var result = testJob.Execute(mockWatchDog.Object, (e) => { }, (e) => { });

            Assert.AreEqual(JobResultStatus.Error, result);
        }

        [TestMethod]
        public void ExecuteFallsBackToDefaultConnectionStringNameWhenOneNotGivenInSettings()
        {
            var mockWatchDog = new Mock<IJobAbortionWatchDog>();
            var mockSettings = new Mock<ISettings>();
            var mockProcCallProvider = new Mock<IStoredProcedureCallProvider>();
            mockProcCallProvider.Setup(x => x.ExecuteProcedureCall("Logging", It.IsAny<string>(), It.IsAny<IJobAbortionWatchDog>()))
                .Verifiable("Expected to call with connectionstring 'Logging'");

            var testJob = new ClearLogsJob()
            {
                Settings = mockSettings.Object,
                StoredProcedureCallProvider = mockProcCallProvider.Object
            };

            testJob.Execute(mockWatchDog.Object, (e) => { }, (e) => { });

            mockProcCallProvider.VerifyAll();
        }

        [TestMethod]
        public void ExecuteCallsStoredProcedureWithCorrectConnectionString()
        {
            var mockWatchDog = new Mock<IJobAbortionWatchDog>();
            var mockSettings = new Mock<ISettings>();
            mockSettings.Setup(x => x.ContainsKey(ClearLogsJob.LogDatabaseConnectionStringName))
                .Returns(true);
            mockSettings.Setup(x => x.GetValue(ClearLogsJob.LogDatabaseConnectionStringName))
                .Returns("ExpectedConnection");

            var mockProcCallProvider = new Mock<IStoredProcedureCallProvider>();
            mockProcCallProvider.Setup(x => x.ExecuteProcedureCall("ExpectedConnection", It.IsAny<string>(), It.IsAny<IJobAbortionWatchDog>()))
                .Verifiable("Expected to call with connectionstring 'ExpectedConnection'");

            var testJob = new ClearLogsJob()
            {
                Settings = mockSettings.Object,
                StoredProcedureCallProvider = mockProcCallProvider.Object
            };

            testJob.Execute(mockWatchDog.Object, (e) => { }, (e) => { });

            mockProcCallProvider.VerifyAll();
        }
    }

}
