﻿using System;
using System.Diagnostics.Contracts;
using System.Threading.Tasks;
using Quartz;
using Soikea.IntegrationEngine.Core;
using Soikea.IntegrationEngine.Core.Interfaces;
using Soikea.IntegrationEngine.Core.Logging;

namespace Soikea.IntegrationEngine.Service.Configuration
{
    public class ExecuteIntegrationJob : IJob
    {
        private readonly ExecutionContext _executionContext;
        private readonly IIntegrationJob _jobToExecute;

        public ExecuteIntegrationJob(ExecutionContext executionContext, IIntegrationJob jobToExecute)
        {
            Contract.Requires(executionContext != null, "executionContext != null");
            Contract.Requires(jobToExecute != null, "jobToExecute != null");

            _executionContext = executionContext;
            _jobToExecute = jobToExecute;
        }

        public void Execute(IJobExecutionContext context)
        {
            Log.Write(string.Format("Starting a run of IIntegrationJob {0}:{1} invoked by {2}", 
                _jobToExecute.Name, context.JobDetail.Description, context.Trigger.JobKey));

            Task.Run(() =>
            {
                try
                {
                    switch (_jobToExecute.ResultType)
                    {
                        case JobResultType.Void:
                            _executionContext.RunJob(_jobToExecute);
                            break;
                        case JobResultType.Synchronous:
                            _executionContext.RunJobSynchronous(_jobToExecute);
                            break;
                        case JobResultType.Callback:
                            _executionContext.RunJobAsynchronousCallback(_jobToExecute);
                            break;
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex, string.Format("An error happened while executing job {0}:{1} invoked by {2}",
                        _jobToExecute.Name, context.JobDetail.Description, context.Trigger.JobKey));
                }

            });
        }
    }
}
