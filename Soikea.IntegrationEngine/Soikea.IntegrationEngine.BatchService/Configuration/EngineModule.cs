﻿using System;
using System.Reflection;
using Atlas;
using Autofac;
using Quartz;
using Quartz.Impl;
using Quartz.Spi;
using Soikea.IntegrationEngine.Core;
using Soikea.IntegrationEngine.Core.Implementation;
using Soikea.IntegrationEngine.Core.Interfaces;
using Module = Autofac.Module;

namespace Soikea.IntegrationEngine.Service.Configuration
{
    public class EngineModule : Module
    {
        public bool MockProviders { get; set; }
        public bool MockServices { get; set; }

        /// <summary>
        /// Override to add registrations to the container.
        /// </summary>
        /// <param name="builder">The builder through which components can be registered.</param>
        protected override void Load(ContainerBuilder builder)
        {
            this.LoadEngine(builder);
            this.LoadQuartz(builder);
            this.LoadServices(builder);
        }

        /// <summary>
        /// Loads the quartz scheduler instance.
        /// </summary>
        /// <param name="builder">The builder through which components can be registered.</param>
        private void LoadQuartz(ContainerBuilder builder)
        {
            builder.Register(c => new StdSchedulerFactory().GetScheduler())
                   .As<IScheduler>()
                   .InstancePerLifetimeScope();
            builder.Register(c => new EngineJobFactory(ContainerProvider.Instance.ApplicationContainer))
                   .As<IJobFactory>();
            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
                   .Where(p => typeof(IJob).IsAssignableFrom(p))
                   .PropertiesAutowired();
            builder.Register(c => new EngineJobListener(ContainerProvider.Instance))
                   .As<IJobListener>();
        }

        /// <summary>
        /// Loads the service instance.
        /// </summary>
        /// <param name="builder">The builder through which components can be registered.</param>
        private void LoadServices(ContainerBuilder builder)
        {
            builder.RegisterType<EngineService>()
                   .As<IAmAHostedProcess>()
                   .PropertiesAutowired();
        }

        /// <summary>
        /// Loads Integration-Engine specific interfaces and services.
        /// </summary>
        /// <param name="builder">The builder through which components can be registered.</param>
        private void LoadEngine(ContainerBuilder builder)
        {
            if (!MockProviders)
            {
                //builder.RegisterType<MeteringwareProviderImpl>()
                //    .As<IMeteringwareProvider>();
            }
            else
            {
                //builder.RegisterType<MockMeteringwareProvider>()
                //    .As<IMeteringwareProvider>();
            }

            builder.RegisterAssemblyTypes(AppDomain.CurrentDomain.GetAssemblies())
                    .Where(t => t.BaseType == typeof(SynchronousIntegrationJob))
                    .As<IIntegrationJob>();

            builder.RegisterType<SubscriptionContext>()
                .SingleInstance();

            builder.RegisterType<ExecutionContext>();
        }
    }
}
