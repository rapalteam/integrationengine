﻿using System;
using System.Security.AccessControl;
using Atlas;
using Quartz;
using Quartz.Spi;
using Soikea.IntegrationEngine.Core;
using Soikea.IntegrationEngine.Core.Logging;
using Soikea.IntegrationEngine.Samples;
using Soikea.IntegrationEngine.Service.Configuration;

namespace Soikea.IntegrationEngine.Service
{

    public class EngineService : IAmAHostedProcess
    {
        /// <summary>
        /// Gets or sets the scheduler instance.
        /// </summary>
        public IScheduler Scheduler { get; set; }

        /// <summary>
        /// Gets or sets the job factory instance.
        /// </summary>
        public IJobFactory JobFactory { get; set; }

        /// <summary>
        /// Gets or sets the job listener instance.
        /// </summary>
        public IJobListener JobListener { get; set; }

        /// <summary>
        /// Gets or sets the integration job execution context.
        /// </summary>
        public ExecutionContext ExecutionContext { get; set; }

        /// <summary>
        /// Gets or sets the integrationjob result subscription context.
        /// </summary>
        public SubscriptionContext SubscriptionContext { get; set; }

        /// <summary>
        /// Starts the Windows Service.
        /// </summary>
        public void Start()
        {
            Log.Write("Integration-Engine Windows Service starting");

            //TODO: Schedule jobs according to their configured schedules with JobsModule

            var job = JobBuilder.Create<ExecuteIntegrationJob>()
                                .WithIdentity("ExecuteIntegrationJob", "Integration-EngineService")
                                .Build();

            var trigger = TriggerBuilder.Create()
                                        .WithIdentity("EngineTrigger", "Integration-EngineService")
                                        .WithSimpleSchedule(builder => builder
                                            .WithIntervalInSeconds(300)
                                            .RepeatForever())
                                        .ForJob("ExecuteIntegrationJob", "Integration-EngineService")
                                        .Build();

            this.Scheduler.ScheduleJob(job, trigger);

            this.Scheduler.JobFactory = this.JobFactory;
            this.Scheduler.ListenerManager.AddJobListener(this.JobListener);
            this.Scheduler.Start();

            Log.Write("Integration-Engine Windows Service started");
        }

        /// <summary>
        /// Stops the Windows Service.
        /// </summary>
        public void Stop()
        {
            Log.Write("Integration-Engine Windows Service stopping");

            this.Scheduler.Shutdown();

            Log.Write("Integration-Engine Windows Service stopped");
        }

        /// <summary>
        /// Resumes the Windows Service.
        /// </summary>
        public void Resume()
        {
            Log.Write("Integration-Engine Windows Service resuming");

            this.Scheduler.ResumeAll();

            Log.Write("Integration-Engine Windows Service resumed");
        }

        /// <summary>
        /// Pauses the Windows Service.
        /// </summary>
        public void Pause()
        {
            Log.Write("Integration-Engine Windows Service pausing");

            this.Scheduler.PauseAll();

            Log.Write("Integration-Engine Windows Service paused");
        }
    }

}
