﻿using System;
using System.Linq;
using Atlas;
using Autofac;
using Soikea.IntegrationEngine.Core.Logging;
using Soikea.IntegrationEngine.Service.Configuration;

namespace Soikea.IntegrationEngine.Service
{
    static class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var configuration = Host.UseAppConfig<EngineService>()
                                        .WithRegistrations(p => p.RegisterModule(new EngineModule()));
                if (args != null && args.Any())
                    configuration = configuration.WithArguments(args);

                Host.Start(configuration);

            }
            catch (Exception ex)
            {
                Log.Error(ex, "Exception during startup.");
                Console.ReadLine();
            }
        }
    }
 
}
