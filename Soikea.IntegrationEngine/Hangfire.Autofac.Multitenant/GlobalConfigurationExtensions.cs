﻿using System;
using Autofac.Extras.Multitenant;
using Hangfire.Annotations;

namespace Hangfire
{
    public static class GlobalConfigurationExtensions
    {
        public static IGlobalConfiguration<AutofacMultitenantJobActivator> UseAutofacMultitenantActivator(
            [NotNull] this IGlobalConfiguration configuration,
            [NotNull] MultitenantContainer lifetimeScope)
        {
            if (configuration == null) throw new ArgumentNullException("configuration");
            if (lifetimeScope == null) throw new ArgumentNullException("lifetimeScope");

            return configuration.UseActivator(new AutofacMultitenantJobActivator(lifetimeScope));
        }
    }
}
