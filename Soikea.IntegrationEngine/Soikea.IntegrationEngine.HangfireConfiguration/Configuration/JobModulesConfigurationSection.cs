﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace Soikea.IntegrationEngine.HangfireConfiguration.Configuration
{
    public class JobModulesConfigurationSection : ConfigurationSection
    {
        [ConfigurationProperty("modulePatterns", IsDefaultCollection = true)]
        [ConfigurationCollection(typeof(JobModulePatternItem), AddItemName = "add")]
        public JobModulePatternItems JobModulePatternItems
        {
            get { return ((JobModulePatternItems) (base["modulePatterns"])); }
            set { base["modulePatterns"] = value; }
        }

    }


    public class JobModulePatternItem : ConfigurationElement
    {
        [ConfigurationProperty("key", IsRequired = false)]
        public string Key
        {
            get { return (string) base["key"]; }
            set { base["key"] = value; }
        }

        [ConfigurationProperty("pattern", IsRequired = true)]
        public string Pattern
        {
            get { return (string)base["pattern"]; }
            set { base["pattern"] = value; }
        }
    }

    [ConfigurationCollection(typeof(JobModulePatternItem))]
    public class JobModulePatternItems : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new JobModulePatternItem();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((JobModulePatternItem)element).Key;
        }

        protected override string ElementName => "add";

        public JobModulePatternItem this[int idx] => (JobModulePatternItem)BaseGet(idx);

        public List<JobModulePatternItem> ToList()
        {
            var list = new List<JobModulePatternItem>();

            for (int i = 0; i < this.Count; i++)
            {
                list.Add(this[i]);
            }

            return list;
        }

        public List<string> ToPatternList()
        {
            var list = new List<JobModulePatternItem>();

            for (int i = 0; i < this.Count; i++)
            {
                list.Add(this[i]);
            }

            return list.Select(x => x.Pattern).ToList();
        }
    }
}
