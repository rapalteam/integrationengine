﻿using System.Configuration;

namespace Soikea.IntegrationEngine.HangfireConfiguration.Configuration.Tenants
{
    public class EncryptedTenantConfigurationSection : ConfigurationSection
    {
        [ConfigurationProperty("tenants", IsDefaultCollection = true)]
        [ConfigurationCollection(typeof(TenantItems), AddItemName = "tenant")]
        public TenantItems TenantItems
        {
            get { return ((TenantItems)(base["tenants"])); }
            set { base["tenants"] = value; }
        }
    }
}
