﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Soikea.IntegrationEngine.Core.Interfaces;
using Soikea.IntegrationEngine.HangfireConfiguration.Configuration.Jobs;
using Soikea.IntegrationEngine.HangfireConfiguration.Configuration.MessageAdapters;
using Soikea.IntegrationEngine.HangfireConfiguration.Configuration.Settings;

namespace Soikea.IntegrationEngine.HangfireConfiguration.Configuration.Tenants
{
    public class Tenant
    {
        public string TenantId { get; set; }
        public List<JobSchedule> Jobs { get; set; }
        public List<MessageAdapter> MessageAdapters { get; set; }
        public ISettings TenantSettings { get; set; }

        public static Tenant Create(TenantItem item)
        {
            return new Tenant()
            {
                TenantId = item.Id,
                MessageAdapters = item.MessageAdapterItems.ToList().Select(MessageAdapter.Create).ToList(),
                Jobs = item.JobScheduleItems.ToList().Select(JobSchedule.Create).ToList(),
                TenantSettings = Settings.Settings.Create(item.SettingItems)
            };
        }
    }

    public class TenantItem : ConfigurationElement
    {
        [ConfigurationProperty("id", IsRequired = true)]
        public string Id 
        {
            get { return (string) this["id"]; }
            set { this["id"] = value; }
        }

        [ConfigurationProperty("messageAdapters", IsDefaultCollection = true)]
        [ConfigurationCollection(typeof(MessageAdapterItem), AddItemName = "messageAdapter")]
        public MessageAdapterItems MessageAdapterItems
        {
            get { return ((MessageAdapterItems)(base["messageAdapters"])); }
            set { base["messageAdapters"] = value; }
        }

        [ConfigurationProperty("jobs")]
        [ConfigurationCollection(typeof(JobScheduleItems), AddItemName = "jobSchedule")]
        public JobScheduleItems JobScheduleItems
        {
            get { return ((JobScheduleItems)(base["jobs"])); }
            set { base["jobs"] = value; }
        }

        [ConfigurationProperty("settings")]
        [ConfigurationCollection(typeof(SettingItems), AddItemName = "add")]
        public SettingItems SettingItems
        {
            get { return ((SettingItems)(base["settings"])); }
            set { base["settings"] = value; }
        }
    }

    public interface ITenants
    {
        List<Tenant> All { get; set; }
    }

    public class Tenants : ITenants
    {
        public List<Tenant> All { get; set; }
    }
}
