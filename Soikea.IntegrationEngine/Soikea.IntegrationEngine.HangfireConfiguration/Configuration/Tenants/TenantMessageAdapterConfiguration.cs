﻿using System.Collections.Generic;
using Soikea.IntegrationEngine.Core.Interfaces;
using Soikea.IntegrationEngine.Core.Logging;
using Soikea.IntegrationEngine.HangfireConfiguration.Configuration.MessageAdapters;
using Soikea.IntegrationEngine.Runtime;

namespace Soikea.IntegrationEngine.HangfireConfiguration.Configuration.Tenants
{
    public class TenantMessageAdapterConfiguration
    {
        private readonly SubscriptionContext _subscriptionContext;
        private readonly MessageAdapterResolver _resolver;
        
        public TenantMessageAdapterConfiguration(SubscriptionContext subscriptionContext, MessageAdapterResolver resolver)
        {
            _subscriptionContext = subscriptionContext;
            _resolver = resolver;            
        }

        public List<IMessageAdapter> Configure(Tenant tenant)
        {
            Log.Info($"Registering message-adapters for tenant {tenant.TenantId}");

            var adapters = new List<IMessageAdapter>();

            foreach (var messageAdapter in tenant.MessageAdapters)
            {
                var adapter = _resolver.Resolve(messageAdapter.Name);

                if (adapter == null)
                {
                    Log.Warn($"Could not resolve message-adapter with given name: {messageAdapter.Name} for tenant: {tenant.TenantId}");

                    continue;
                }


                adapter.TenantId = tenant.TenantId;
                adapter.MessageAdapterSettings = messageAdapter.MessageAdapterSettings;

                if (messageAdapter.Stream == MessageAdapter.EventStream.In)
                {
                    _subscriptionContext.SubscribeToInput(adapter, messageAdapter.SubscriptionCategory);
                } 
                else if (messageAdapter.Stream == MessageAdapter.EventStream.Out)
                {
                    _subscriptionContext.SubscribeToOutput(adapter, messageAdapter.SubscriptionCategory);
                }
                else
                {
                    Log.Warn($"Could not resolve subscription-stream for message-adapter: {messageAdapter.Name} for tenant: {tenant.TenantId}");

                    continue;
                }

                adapters.Add(adapter);
                Log.Info($"Registered message-adapter {messageAdapter.Name} for tenant {tenant.TenantId}");
            }

            Log.Info($"Registered total of {adapters.Count} message-adapters for tenant {tenant.TenantId}");
            return adapters;
        }
    }
}
