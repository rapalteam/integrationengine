﻿using System;
using System.Collections.Generic;
using Hangfire;
using Soikea.IntegrationEngine.Core.Logging;
using Soikea.IntegrationEngine.HangfireConfiguration.Configuration.Jobs;

namespace Soikea.IntegrationEngine.HangfireConfiguration.Configuration.Tenants
{
    public static class TenantJobConfiguration
    {
        public const string QueuePriority = "priority";
        public const string QueueDefault = "default";
        public const string QueueSecondary = "secondary";
        public static readonly List<string> AllowedQueues = new List<string>() {QueuePriority, QueueDefault, QueueSecondary}; 

        public static void Configure(Tenant tenant, JobSettings jobsettings)
        {
            if (tenant == null) return;
            var tenantId = tenant.TenantId;

            Log.Info($"Registering tenant jobs for tenant \"{tenantId}\"");

            if (tenant.Jobs == null) return;

            foreach (var jobScheduleItem in tenant.Jobs)
            {
                var item = jobScheduleItem;
                var jobQueue = (string.IsNullOrEmpty(item.Queue) || !AllowedQueues.Contains(item.Queue)
                    ? QueueDefault
                    : item.Queue);
                var queue = $"{tenant.TenantId}_{jobQueue}".ToLower();

                if (item.RetriesEnabled)
                {
                    RecurringJob.AddOrUpdate<ExecuteIntegrationJobWithRetries>(
                        item.ScheduleIdentifier,
                        x => x.Execute(JobCancellationToken.Null, tenantId, item.ScheduleIdentifier, item.JobName),
                        item.CronSchedule,
                        TimeZoneInfo.Local,
                        queue
                        );
                }
                else
                {
                    RecurringJob.AddOrUpdate<ExecuteIntegrationJobWithoutRetries>(
                        item.ScheduleIdentifier,
                        x => x.Execute(JobCancellationToken.Null, tenantId, item.ScheduleIdentifier, item.JobName),
                        item.CronSchedule,
                        TimeZoneInfo.Local,
                        queue
                        );
                }

                jobsettings.AddSettings(item.ScheduleIdentifier, item.JobSettings);

                var retriesString = item.RetriesEnabled ? "retryable" : "not-retryable";
                Log.Info($"Registered {item.JobName} job as {retriesString} for tenant \"{tenant.TenantId}\" with schedule \"{item.CronSchedule}\"");
            }

            Log.Info($"Registered {tenant.Jobs.Count} jobs for tenant \"{tenant.TenantId}\"");

        }
    }
}
