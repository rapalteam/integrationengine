﻿using System.Collections.Generic;
using System.Configuration;

namespace Soikea.IntegrationEngine.HangfireConfiguration.Configuration.Tenants
{
    public class TenantsConfigurationSection : ConfigurationSection
    {
        [ConfigurationProperty("tenants", IsDefaultCollection = true)]
        [ConfigurationCollection(typeof (TenantItems), AddItemName = "tenant")]
        public TenantItems TenantItems
        {
            get { return ((TenantItems)(base["tenants"])); }
            set { base["tenants"] = value; }
        }
    }

    [ConfigurationCollection(typeof(TenantItem))]
    public class TenantItems : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new TenantItem();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((TenantItem)element).Id;
        }

        protected override string ElementName => "tenant";

        public TenantItem this[int idx] => (TenantItem)BaseGet(idx);

        public List<TenantItem> ToList()
        {
            var list = new List<TenantItem>();

            for (int i = 0; i < this.Count; i++)
            {
                list.Add(this[i]);
            }

            return list;
        }
    }
}

