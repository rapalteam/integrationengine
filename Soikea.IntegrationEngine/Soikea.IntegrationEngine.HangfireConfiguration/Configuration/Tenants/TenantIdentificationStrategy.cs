﻿using Autofac.Extras.Multitenant;
using Hangfire.Server;

namespace Soikea.IntegrationEngine.HangfireConfiguration.Configuration.Tenants
{
    public class JobExecutionServerNameStrategy : ITenantIdentificationStrategy
    {
        public bool TryIdentifyTenant(out object tenantId)
        {
            tenantId = (BackgroundProcessContext.Current == null
                ? null
                : BackgroundProcessContext.Current.Properties["ServerName"]);

            if (tenantId == null && TenantExecutionContext.Current != null)
            {
                tenantId = TenantExecutionContext.Current.TenantId;
            }

            return tenantId != null;
        }
    }
}
