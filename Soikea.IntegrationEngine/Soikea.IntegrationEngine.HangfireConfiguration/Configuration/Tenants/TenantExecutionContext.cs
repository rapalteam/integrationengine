﻿using System;

namespace Soikea.IntegrationEngine.HangfireConfiguration.Configuration.Tenants
{
    public class TenantExecutionContext
    {
        public string TenantId { get; set; }

        public TenantExecutionContext(string tenantId)
        {
            TenantId = tenantId;
        }

        [ThreadStatic]
        private static TenantExecutionContext _current;

        public static TenantExecutionContext Current
        {
            get { return _current; }
            set { _current = value; }
        }
    }
}
