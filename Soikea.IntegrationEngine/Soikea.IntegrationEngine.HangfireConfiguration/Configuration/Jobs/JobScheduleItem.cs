﻿using System.Collections.Generic;
using System.Configuration;
using Soikea.IntegrationEngine.HangfireConfiguration.Configuration.Settings;

namespace Soikea.IntegrationEngine.HangfireConfiguration.Configuration.Jobs
{
    public class JobSchedule
    {
        public string CronSchedule { get; set; }
        public string JobName { get; set; }
        public string ScheduleIdentifier { get; set; }
        public string Queue { get; set; }
        public bool RetriesEnabled { get; set; }
        public Settings.Settings JobSettings { get; set; }

        public static JobSchedule Create(JobScheduleItem item)
        {
            return new JobSchedule()
            {
                CronSchedule = item.CronSchedule,
                JobName = item.JobName,
                ScheduleIdentifier = item.ScheduleIdentifier,
                Queue = item.Queue,
                RetriesEnabled = item.RetriesEnabled,
                JobSettings = Settings.Settings.Create(item.Settings)
            };
        }
        
    }

    public class JobScheduleItem : ConfigurationElement
    {
        [ConfigurationProperty("cronSchedule", DefaultValue = "* * * * *", IsRequired = true)] //Defaults to Minutely
        public string CronSchedule
        {
            get { return (string) this["cronSchedule"]; }
            set { this["cronSchedule"] = value; }
        }

        [ConfigurationProperty("jobName", IsRequired = true)]
        public string JobName
        {
            get { return (string)this["jobName"]; }
            set { this["jobName"] = value; }
        }

        [ConfigurationProperty("scheduleIdentifier", IsRequired = true)]
        public string ScheduleIdentifier
        {
            get { return (string)this["scheduleIdentifier"]; }
            set { this["scheduleIdentifier"] = value; }
        }

        [ConfigurationProperty("retriesEnabled", IsRequired = false)]
        public bool RetriesEnabled
        {
            get { return (bool)this["retriesEnabled"]; }
            set { this["retriesEnabled"] = value; }
        }

        [ConfigurationProperty("queue", IsRequired = false)]
        public string Queue
        {
            get { return (string)this["queue"]; }
            set { this["queue"] = value; }
        }

        [ConfigurationProperty("settings", IsDefaultCollection = false)]
        public SettingItems Settings
        {
            get { return (SettingItems)this["settings"]; }
            set { this["settings"] = value; }
        }
    }

    [ConfigurationCollection(typeof(JobScheduleItem))]
    public class JobScheduleItems : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new JobScheduleItem();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((JobScheduleItem) element).ScheduleIdentifier;
        }

        protected override string ElementName
        {
            get { return "jobSchedule"; }
        }

        public void Add(JobScheduleItem item)
        {
            BaseAdd(item);
        }

        public JobScheduleItem this[int idx]
        {
            get { return (JobScheduleItem) BaseGet(idx); }
        }

        public List<JobScheduleItem> ToList()
        {
            var list = new List<JobScheduleItem>();

            for (int i = 0; i < this.Count; i++)
            {
                list.Add(this[i]);
            }

            return list;
        } 
    }
}
