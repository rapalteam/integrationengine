﻿using Autofac;
using Soikea.IntegrationEngine.Core.Interfaces;

namespace Soikea.IntegrationEngine.HangfireConfiguration.Configuration.Jobs
{
    public class JobResolver
    {
        private readonly ILifetimeScope _scope;

        public JobResolver(ILifetimeScope scope)
        {
            _scope = scope;
        }

        public IIntegrationJob Resolve(string resolveName)
        {
            var isRegistered = _scope.IsRegisteredWithName<IIntegrationJob>(resolveName);  
            
            return isRegistered ? _scope.ResolveNamed<IIntegrationJob>(resolveName) : null;
        }
    }
}
