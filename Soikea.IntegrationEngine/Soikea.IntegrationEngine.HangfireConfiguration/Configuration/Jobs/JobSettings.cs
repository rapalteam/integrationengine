﻿using System.Collections.Generic;
using Soikea.IntegrationEngine.Core.Interfaces;

namespace Soikea.IntegrationEngine.HangfireConfiguration.Configuration.Jobs
{
    public class JobSettings
    {
        private Dictionary<string, IJobSettings> _settings;

        public JobSettings()
        {
            _settings = new Dictionary<string, IJobSettings>();
        }

        public IJobSettings ResolveSettings(string jobscheduleIdentifier)
        {
            return _settings.ContainsKey(jobscheduleIdentifier)
                ? _settings[jobscheduleIdentifier]
                : new Settings.Settings();
        }

        public void AddSettings(string jobscheduleIdentifier, IJobSettings settings)
        {
            _settings.Add(jobscheduleIdentifier, settings);
        }
    }
}
