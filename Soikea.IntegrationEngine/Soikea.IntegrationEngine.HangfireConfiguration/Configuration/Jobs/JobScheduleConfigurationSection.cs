﻿using System.Configuration;

namespace Soikea.IntegrationEngine.HangfireConfiguration.Configuration.Jobs
{
    /// <summary>
    /// Schedule of jobs to execute for all tenants.
    /// </summary>
    public class JobScheduleConfigurationSection : ConfigurationSection
    {
        [ConfigurationProperty("jobs", IsDefaultCollection = true)]
        [ConfigurationCollection(typeof(JobScheduleItem), AddItemName = "jobSchedule")]
        public JobScheduleItems JobScheduleItems 
        {
            get { return ((JobScheduleItems)(base["jobs"])); }
            set { base["jobs"] = value; }
        }

    }
}
