﻿using System;
using Hangfire;
using Soikea.IntegrationEngine.Core.Interfaces;

namespace Soikea.IntegrationEngine.HangfireConfiguration.Configuration
{
    public class HangfireJobAbortionWatchDog : IJobAbortionWatchDog
    {
        private readonly IJobCancellationToken _jobCancellationToken;

        public HangfireJobAbortionWatchDog(IJobCancellationToken jobCancellationToken)
        {
            _jobCancellationToken = jobCancellationToken;
        }

        public void ThrowIfJobAbortionRequested()
        {
            try
            {
                _jobCancellationToken.ThrowIfCancellationRequested();
            }
            catch (JobAbortedException e)
            {
                throw new JobAbortedException("Job abortion requested by JobAbortedException", e);
            }
            catch (OperationCanceledException e)
            {
                throw new JobAbortedException("Job abortion requested by OperationCanceledException", e);
            }
        }
    }
}
