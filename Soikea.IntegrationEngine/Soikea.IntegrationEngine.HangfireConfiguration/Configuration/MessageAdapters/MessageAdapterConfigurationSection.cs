﻿using System.Configuration;

namespace Soikea.IntegrationEngine.HangfireConfiguration.Configuration.MessageAdapters
{

    public class MessageAdapterConfigurationSection : ConfigurationSection
    {
        [ConfigurationProperty("messageAdapters", IsDefaultCollection = true)]
        [ConfigurationCollection(typeof (MessageAdapterItem), AddItemName = "messageAdapter")]
        public MessageAdapterItems MessageAdapterItems
        {
            get { return ((MessageAdapterItems) (base["messageAdapters"])); }
            set { base["messageAdapters"] = value; }
        }
    }
}
