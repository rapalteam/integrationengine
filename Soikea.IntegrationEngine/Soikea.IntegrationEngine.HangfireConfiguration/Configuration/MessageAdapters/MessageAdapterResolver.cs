﻿using Autofac;
using Soikea.IntegrationEngine.Core.Interfaces;

namespace Soikea.IntegrationEngine.HangfireConfiguration.Configuration.MessageAdapters
{
    public class MessageAdapterResolver
    {
        private readonly ILifetimeScope _scope;

        public MessageAdapterResolver(ILifetimeScope scope)
        {
            _scope = scope;
        }

        public IMessageAdapter Resolve(string resolveName)
        {
            var isRegistered = _scope.IsRegisteredWithName<IMessageAdapter>(resolveName);

            return isRegistered ? _scope.ResolveNamed<IMessageAdapter>(resolveName) : null;
        }
    }
}
