﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Soikea.IntegrationEngine.HangfireConfiguration.Configuration.Settings;

namespace Soikea.IntegrationEngine.HangfireConfiguration.Configuration.MessageAdapters
{

    public class MessageAdapter
    {
        public enum EventStream {  In, Out }

        public string Name { get; set; }

        public string SubscriptionIdentifier { get; set; }

        public string SubscriptionCategory { get; set; }

        public EventStream? Stream { get; set; }

        public Settings.Settings MessageAdapterSettings { get; set; }

        public static MessageAdapter Create(MessageAdapterItem item)
        {
            return new MessageAdapter()
            {
                SubscriptionIdentifier = item.SubscriptionIdentifier,
                Name = item.Name,
                SubscriptionCategory = item.SubscriptionCategory,
                Stream = (EventStream?) Enum.Parse(typeof(EventStream), item.Stream),
                MessageAdapterSettings = Settings.Settings.Create(item.Settings)
            };
        }
    }

    public class MessageAdapterItem : ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true)]
        public string Name
        {
            get { return (string)this["name"]; }
            set { this["name"] = value; }
        }

        [ConfigurationProperty("subscriptionCategory", IsRequired = true)]
        public string SubscriptionCategory
        {
            get { return (string)this["subscriptionCategory"]; }
            set { this["subscriptionCategory"] = value; }
        } 

        [ConfigurationProperty("subscriptionIdentifier", IsRequired = true)]
        public string SubscriptionIdentifier
        {
            get { return (string)this["subscriptionIdentifier"]; }
            set { this["subscriptionIdentifier"] = value; }
        }

        [ConfigurationProperty("stream", IsRequired = true)]
        public string Stream
        {
            get { return (string)this["stream"]; }
            set { this["stream"] = value; }
        }

        [ConfigurationProperty("settings", IsDefaultCollection = false)]
        public SettingItems Settings
        {
            get { return (SettingItems)this["settings"]; }
            set { this["settings"] = value; }
        }
    }


    [ConfigurationCollection(typeof(MessageAdapterItem))]
    public class MessageAdapterItems : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new MessageAdapterItem();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((MessageAdapterItem) element).SubscriptionIdentifier;
        }

        protected override string ElementName => "messageAdapter";

        public void Add(MessageAdapterItem item)
        {
            BaseAdd(item);
        }

        public MessageAdapterItem this[int idx]
        {
            get { return (MessageAdapterItem)BaseGet(idx); }
        }

        public List<MessageAdapterItem> ToList()
        {
            var list = new List<MessageAdapterItem>();

            for (int i = 0; i < this.Count; i++)
            {
                list.Add(this[i]);
            }

            return list;
        } 
    }
}
