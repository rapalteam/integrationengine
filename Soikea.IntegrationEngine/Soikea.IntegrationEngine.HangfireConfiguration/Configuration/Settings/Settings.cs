﻿using System.Collections.Generic;
using Soikea.IntegrationEngine.Core.Interfaces;
using Soikea.IntegrationEngine.HangfireConfiguration.Configuration.Jobs;

namespace Soikea.IntegrationEngine.HangfireConfiguration.Configuration.Settings
{


    public class Settings : Dictionary<string, string>, ISettings, ITenantSettings, IJobSettings, IMessageAdapterSettings
    {
        public string GetValue(string key)
        {
            return this[key];
        }

        public void SetValue(string key, string value)
        {
            this[key] = value;
        }

        public static Settings Create(SettingItems settingItems)
        {
            var settings = new Settings();

            if (settingItems == null)
                return settings;

            foreach (var settingItem in settingItems.ToList())
            {
                settings[settingItem.Key] = settingItem.Value;
            }

            return settings;
        }

        public static Settings Create(Dictionary<string, string> values)
        {
            var settings = new Settings();

            foreach (var keyValuePair in values)
            {
                settings[keyValuePair.Key] = keyValuePair.Value;
            }

            return settings;
        }
    }

}
