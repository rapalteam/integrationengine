﻿using System.Collections.Generic;
using System.Configuration;

namespace Soikea.IntegrationEngine.HangfireConfiguration.Configuration.Settings
{
    public class SettingsConfigurationSection : ConfigurationSection
    {
        [ConfigurationProperty("settings", IsDefaultCollection = true)]
        [ConfigurationCollection(typeof(SettingItems), AddItemName = "add")]
        public SettingItems SettingItems
        {
            get { return (SettingItems) base["settings"]; }
            set { base["settings"] = value; }
        }
    }

    public class SettingItems : ConfigurationElementCollection
    {
        protected override string ElementName => "add";

        protected override ConfigurationElement CreateNewElement()
        {
            return new SettingItem();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((SettingItem)element).Key;
        }

        public SettingItem this[int idx] => (SettingItem)BaseGet(idx);

        public List<SettingItem> ToList()
        {
            var list = new List<SettingItem>();

            for (int i = 0; i < this.Count; i++)
            {
                list.Add(this[i]);
            }

            return list;
        }
    }

    public class SettingItem : ConfigurationElement
    {
        [ConfigurationProperty("key", IsRequired = true)]
        public string Key
        {
            get { return (string) this["key"]; }
            set { this["key"] = value; }
        }

        [ConfigurationProperty("value", IsRequired = true)]
        public string Value
        {
            get { return (string)this["value"]; }
            set { this["value"] = value; }
        }
    }
}
