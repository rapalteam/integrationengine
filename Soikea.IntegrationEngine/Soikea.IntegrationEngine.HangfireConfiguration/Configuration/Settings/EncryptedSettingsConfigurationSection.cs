﻿using System.Configuration;

namespace Soikea.IntegrationEngine.HangfireConfiguration.Configuration.Settings
{
    public class EncryptedSettingsConfigurationSection : ConfigurationSection
    {
        [ConfigurationProperty("settings", IsDefaultCollection = true)]
        [ConfigurationCollection(typeof (SettingItem), AddItemName = "add")]
        public SettingItems EncryptedSettingItems
        {
            get { return (SettingItems) base["settings"]; }
            set { base["settings"] = value; }
        }
    }
}