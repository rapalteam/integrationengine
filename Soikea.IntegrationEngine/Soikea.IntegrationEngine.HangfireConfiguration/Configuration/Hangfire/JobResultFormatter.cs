﻿using Soikea.IntegrationEngine.Core.Interfaces;

namespace Soikea.IntegrationEngine.HangfireConfiguration.Configuration.Hangfire
{
    public static class JobResultFormatter
    {
        public static string Format(IJobResult jobResult)
        {
            return jobResult.Status != JobResultStatus.Ok 
                ? $"Status: {jobResult.Status}, JobId: {jobResult.JobId}" 
                : $"Status: {jobResult.Status}, JobId: {jobResult.JobId}, TenantId: {jobResult.ExecutionDetails.TenantId}, JobName: {jobResult.ExecutionDetails.JobName}, Input events published: {jobResult.ExecutionDetails.InputCount}, Output events published: {jobResult.ExecutionDetails.OutputCount}";
        }
    }
}
