﻿using System.Linq;
using Soikea.IntegrationEngine.HangfireConfiguration.Configuration.Jobs;
using Soikea.IntegrationEngine.HangfireConfiguration.Configuration.MessageAdapters;
using Soikea.IntegrationEngine.HangfireConfiguration.Configuration.Tenants;

namespace Soikea.IntegrationEngine.HangfireConfiguration.Configuration.Administration
{

    public class AdministrativeConfiguration
    {
        public Tenant AdministrativeTenant { get; set; }

        public static AdministrativeConfiguration Create(AdministrationConfigurationSection item, string adminTenantId)
        {
            return new AdministrativeConfiguration()
            {
                
                AdministrativeTenant = new Tenant()
                {
                    TenantId = adminTenantId,
                    MessageAdapters = item.MessageAdapterItems.ToList().Select(MessageAdapter.Create).ToList(),
                    Jobs = item.JobScheduleItems.ToList().Select(JobSchedule.Create).ToList(),
                    TenantSettings = Settings.Settings.Create(item.SettingItems)
                }
            };
        }
    }
    
}
