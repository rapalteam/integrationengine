﻿using System.Configuration;
using Soikea.IntegrationEngine.HangfireConfiguration.Configuration.Jobs;
using Soikea.IntegrationEngine.HangfireConfiguration.Configuration.MessageAdapters;
using Soikea.IntegrationEngine.HangfireConfiguration.Configuration.Settings;

namespace Soikea.IntegrationEngine.HangfireConfiguration.Configuration.Administration
{
    public class AdministrationConfigurationSection : ConfigurationSection
    {
        [ConfigurationProperty("messageAdapters", IsDefaultCollection = true)]
        [ConfigurationCollection(typeof(MessageAdapterItem), AddItemName = "messageAdapter")]
        public MessageAdapterItems MessageAdapterItems
        {
            get { return ((MessageAdapterItems)(base["messageAdapters"])); }
            set { base["messageAdapters"] = value; }
        }

        [ConfigurationProperty("jobs")]
        [ConfigurationCollection(typeof(JobScheduleItems), AddItemName = "jobSchedule")]
        public JobScheduleItems JobScheduleItems
        {
            get { return ((JobScheduleItems)(base["jobs"])); }
            set { base["jobs"] = value; }
        }

        [ConfigurationProperty("settings")]
        [ConfigurationCollection(typeof(SettingItems), AddItemName = "add")]
        public SettingItems SettingItems
        {
            get { return ((SettingItems)(base["settings"])); }
            set { base["settings"] = value; }
        }
    }
}
