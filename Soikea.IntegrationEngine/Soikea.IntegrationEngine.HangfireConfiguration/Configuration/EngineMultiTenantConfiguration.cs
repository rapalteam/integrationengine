﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Autofac;
using Autofac.Core;
using Autofac.Extras.Multitenant;
using Soikea.IntegrationEngine.Core.Interfaces;
using Soikea.IntegrationEngine.Core.Logging;
using Soikea.IntegrationEngine.HangfireConfiguration.Configuration.Administration;
using Soikea.IntegrationEngine.HangfireConfiguration.Configuration.Jobs;
using Soikea.IntegrationEngine.HangfireConfiguration.Configuration.Tenants;
using Soikea.IntegrationEngine.Runtime;

namespace Soikea.IntegrationEngine.HangfireConfiguration.Configuration
{
    public static class EngineMultiTenantConfiguration
    {
        public static List<string> JobModuleFileNamePatterns = new List<string>();

        public static MultitenantContainer Load(IContainer container, List<Tenant> tenants, AdministrativeConfiguration administrativeConfiguration)
        {
            var tenantIdentifierStategy = new JobExecutionServerNameStrategy();
            var mtc = new MultitenantContainer(tenantIdentifierStategy , container);

            var configureTenants = new List<Tenant>() {administrativeConfiguration.AdministrativeTenant}
                .Union(tenants);

            foreach (var tenant in configureTenants)
            {
                var tenantId = tenant.TenantId;
                var tenantSettings = tenant.TenantSettings;
                mtc.ConfigureTenant(tenantId, c =>
                {
                    c.Register(b => tenant).As<Tenant>();
                    c.Register(b => new TenantExecutionContext(tenantId)).InstancePerTenant();
                    c.Register(b => new ExecutionContext(b.Resolve<SubscriptionContext>())).SingleInstance();
                    c.Register(b => new JobSettings()).SingleInstance();
                    c.RegisterType<ExecuteIntegrationJobWithoutRetries>();
                    c.RegisterType<ExecuteIntegrationJobWithRetries>();
                    
                    c.Register(b => Settings.Settings.Create(tenantSettings as Dictionary<string, string>))
                        .As<ITenantSettings>();                    

                    RegisterIntegrationJobModules(c);
                });
            }

            return mtc;
        }

        private static void RegisterIntegrationJobModules(ContainerBuilder builder)
        {
            var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            if (string.IsNullOrWhiteSpace(path)  || JobModuleFileNamePatterns == null)
            {
                return;
            }

            foreach (var jobModuleFileNamePattern in JobModuleFileNamePatterns)
            {
                Log.Trace($"Scanning for Autofac modules with pattern {jobModuleFileNamePattern}");
                var assemblies =
                    Directory.GetFiles(path, jobModuleFileNamePattern, SearchOption.TopDirectoryOnly)
                        .Select(Assembly.LoadFrom)
                        .ToArray();

                foreach (var assembly in assemblies)
                {
                    var scanningBuilder = new ContainerBuilder();

                    scanningBuilder.RegisterAssemblyTypes(assembly)
                        .Where(t => t.BaseType.Name == "Module") // typeof (IModule).IsAssignableFrom(t)
                        .As<IModule>();

                    using (var scanningContainer = scanningBuilder.Build())
                    {
                        foreach (var m in scanningContainer.Resolve<IEnumerable<IModule>>())
                            builder.RegisterModule(m);
                    }
                }
            }
        }
    }
}
