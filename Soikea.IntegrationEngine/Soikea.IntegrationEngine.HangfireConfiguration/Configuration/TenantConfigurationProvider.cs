﻿using System.Collections.Generic;
using System.Linq;
using Soikea.IntegrationEngine.HangfireConfiguration.Configuration.Jobs;
using Soikea.IntegrationEngine.HangfireConfiguration.Configuration.MessageAdapters;
using Soikea.IntegrationEngine.HangfireConfiguration.Configuration.Settings;
using Soikea.IntegrationEngine.HangfireConfiguration.Configuration.Tenants;

namespace Soikea.IntegrationEngine.HangfireConfiguration.Configuration
{
    public class ResolveTenantsParams
    {
        private TenantsConfigurationSection _tenantConfiguration;
        private MessageAdapterConfigurationSection _defaultMessageAdapters;
        private JobScheduleConfigurationSection _defaultJobs;
        private SettingsConfigurationSection _globalSettings;
        private EncryptedSettingsConfigurationSection _encryptedSettings;
        private EncryptedTenantConfigurationSection _encryptedTenantConfiguration;

        public ResolveTenantsParams(TenantsConfigurationSection tenantConfiguration, MessageAdapterConfigurationSection defaultMessageAdapters, JobScheduleConfigurationSection defaultJobs, SettingsConfigurationSection globalSettings, EncryptedSettingsConfigurationSection encryptedSettings, EncryptedTenantConfigurationSection encryptedTenantConfiguration)
        {
            _tenantConfiguration = tenantConfiguration;
            _defaultMessageAdapters = defaultMessageAdapters;
            _defaultJobs = defaultJobs;
            _globalSettings = globalSettings;
            _encryptedSettings = encryptedSettings;
            _encryptedTenantConfiguration = encryptedTenantConfiguration;
        }

        public TenantsConfigurationSection TenantConfiguration
        {
            get { return _tenantConfiguration; }
        }

        public MessageAdapterConfigurationSection DefaultMessageAdapters
        {
            get { return _defaultMessageAdapters; }
        }

        public JobScheduleConfigurationSection DefaultJobs
        {
            get { return _defaultJobs; }
        }

        public SettingsConfigurationSection GlobalSettings
        {
            get { return _globalSettings; }
        }

        public EncryptedSettingsConfigurationSection EncryptedSettings
        {
            get { return _encryptedSettings; }
        }

        public EncryptedTenantConfigurationSection EncryptedTenantConfiguration
        {
            get { return _encryptedTenantConfiguration; }
        }
    }

    public static class TenantConfigurationProvider
    {
        public static List<Tenant> ResolveTenants(ResolveTenantsParams resolveTenantsParams)
        {
            var tenants = new List<Tenant>();
            
            foreach (var tenant in resolveTenantsParams.TenantConfiguration.TenantItems.ToList().Select(Tenant.Create))
            {
                MergeMessageAdapters(tenant, resolveTenantsParams.DefaultMessageAdapters);
                MergeJobSchedules(tenant, resolveTenantsParams.DefaultJobs);
                MergeSettings(tenant, resolveTenantsParams.GlobalSettings);
                MergeEncryptedSettings(tenant, resolveTenantsParams.EncryptedSettings);
                MergeEncryptedTenantSettings(tenant, resolveTenantsParams.EncryptedTenantConfiguration);
                tenants.Add(tenant);
            }

            return tenants;
        }

        private static void MergeEncryptedSettings(Tenant tenant, EncryptedSettingsConfigurationSection encryptedSettings)
        {
            foreach (var settingItem in encryptedSettings.EncryptedSettingItems.ToList())
            {
                tenant.TenantSettings.SetValue(settingItem.Key, settingItem.Value);
            }
        }

        private static void MergeEncryptedTenantSettings(Tenant tenant, EncryptedTenantConfigurationSection encryptedTenantConfiguration)
        {
            var foundTenant = encryptedTenantConfiguration.TenantItems.ToList()
                .FirstOrDefault(w => w.Id == tenant.TenantId);

            if (foundTenant == null) return;

            foreach (var setting in foundTenant.SettingItems.ToList())
            {
                tenant.TenantSettings.SetValue(setting.Key, setting.Value);
            }
        }

        private static void MergeSettings(Tenant tenant, SettingsConfigurationSection globalSettings)
        {
            // Merge tenant-settings with global settings. Tenants settings override global settings.
            foreach (var globalSetting in globalSettings.SettingItems.ToList())
            {
                if (!tenant.TenantSettings.ContainsKey(globalSetting.Key))
                {
                    tenant.TenantSettings.SetValue(globalSetting.Key, globalSetting.Value);
                }
            }
        }

        private static void MergeJobSchedules(Tenant tenant, JobScheduleConfigurationSection defaultJobs)
        {
            // Register default jobs for tenant
            foreach (JobScheduleItem jobScheduleItem in defaultJobs.JobScheduleItems)
            {
                tenant.Jobs.Add(JobSchedule.Create(jobScheduleItem));
            }

            // Point jobschedule-identifiers correctly at tenant
            foreach (var jobSchedule in tenant.Jobs)
            {
                jobSchedule.ScheduleIdentifier = $"{tenant.TenantId}_{jobSchedule.ScheduleIdentifier}";
            }
        }

        private static void MergeMessageAdapters(Tenant tenant, MessageAdapterConfigurationSection defaultMessageAdapters)
        {
            // Register default message adapters for tenant
            foreach (MessageAdapterItem messageAdapterItem in defaultMessageAdapters.MessageAdapterItems)
            {
                tenant.MessageAdapters.Add(MessageAdapter.Create(messageAdapterItem));
            }

            // Point messageadapters subscription-identifier correctly at tenant
            foreach (var messageAdapter in tenant.MessageAdapters)
            {
                messageAdapter.SubscriptionIdentifier = $"{tenant.TenantId}_{messageAdapter.SubscriptionIdentifier}";
            }
        }
    }
}
