﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using Soikea.IntegrationEngine.Core.Interfaces;
using Soikea.IntegrationEngine.Core.Logging;
using Soikea.IntegrationEngine.Runtime;

namespace Soikea.IntegrationEngine.HangfireConfiguration.Configuration.EventStore
{
    public class EventStoreConfiguration
    {
        private readonly SubscriptionContext _subscriptionContext;

        public EventStoreConfiguration(SubscriptionContext subscriptionContext)
        {
            _subscriptionContext = subscriptionContext;
        }

        public List<IEventStoringStrategy> Configure(ILifetimeScope scope)
        {
            var types = scope.ComponentRegistry.Registrations
                .Where(r => typeof(IEventStoringStrategy).IsAssignableFrom(r.Activator.LimitType))
                .Select(r => r.Activator.LimitType);

            var eventStoreStrategies = types
                .Select(t => scope.Resolve(t) as IEventStoringStrategy)
                .ToList();

            foreach (var eventStoringStrategy in eventStoreStrategies)
            {
                switch (eventStoringStrategy.EventStream)
                {
                    case EventStream.Input:
                        _subscriptionContext.InputStream.Subscribe(eventStoringStrategy.Receive);
                        Log.Info($"Attached Event-storing strategy {eventStoringStrategy.Name} to InputStream");
                        break;
                    case EventStream.Output:
                        _subscriptionContext.OutputStream.Subscribe(eventStoringStrategy.Receive);
                        Log.Info($"Attached Event-storing strategy {eventStoringStrategy.Name} to OutputStream");
                        break;
                }
                //TODO: Code smell when subscribing directly and having to expose the streams ?
            }

            return eventStoreStrategies;
        }
         
    }
}
