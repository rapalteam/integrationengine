﻿using Autofac;
using Soikea.IntegrationEngine.HangfireConfiguration.Configuration.EventStore;
using Soikea.IntegrationEngine.HangfireConfiguration.Configuration.Jobs;
using Soikea.IntegrationEngine.HangfireConfiguration.Configuration.MessageAdapters;
using Soikea.IntegrationEngine.HangfireConfiguration.Configuration.Tenants;
using Soikea.IntegrationEngine.Runtime;

namespace Soikea.IntegrationEngine.HangfireConfiguration.Configuration
{
    public class EngineModule : Module
    {
        
        /// <summary>
        /// Override to add registrations to the container.
        /// </summary>
        /// <param name="builder">The builder through which components can be registered.</param>
        protected override void Load(ContainerBuilder builder)
        {
            LoadEngine(builder);
            base.Load(builder);
        }


        /// <summary>
        /// Loads Integration-Engine specific interfaces and services.
        /// </summary>
        /// <param name="builder">The builder through which components can be registered.</param>
        private void LoadEngine(ContainerBuilder builder)
        {
            builder.Register(b => new SubscriptionContext()).SingleInstance();

            builder.RegisterType<EventStoreConfiguration>();

            builder.RegisterType<JobResolver>();
            builder.RegisterType<MessageAdapterResolver>();
            builder.RegisterType<TenantMessageAdapterConfiguration>();
        }
        
    }
}
