﻿using System.Configuration;
using Soikea.IntegrationEngine.HangfireConfiguration.Configuration.Administration;
using Soikea.IntegrationEngine.HangfireConfiguration.Configuration.Jobs;
using Soikea.IntegrationEngine.HangfireConfiguration.Configuration.MessageAdapters;
using Soikea.IntegrationEngine.HangfireConfiguration.Configuration.Settings;
using Soikea.IntegrationEngine.HangfireConfiguration.Configuration.Tenants;

namespace Soikea.IntegrationEngine.HangfireConfiguration.Configuration
{
    public class IntegrationEngineConfigurationSectionGroup : ConfigurationSectionGroup
    {
        [ConfigurationProperty("jobModules")]
        public JobModulesConfigurationSection JobModules
            => (JobModulesConfigurationSection) base.Sections["jobModules"];

        [ConfigurationProperty("globalSettings")]
        public SettingsConfigurationSection GlobalSettings
            => (SettingsConfigurationSection) base.Sections["globalSettings"];

        [ConfigurationProperty("encryptedSettings")]
        public EncryptedSettingsConfigurationSection EncryptedSettings
            => (EncryptedSettingsConfigurationSection) base.Sections["encryptedSettings"];

        [ConfigurationProperty("administrationConfiguration")]
        public AdministrationConfigurationSection AdministrationConfiguration
            => (AdministrationConfigurationSection)base.Sections["administrationConfiguration"];

        [ConfigurationProperty("defaultMessageAdapters")]
        public MessageAdapterConfigurationSection DefaultMessageAdapters 
            => (MessageAdapterConfigurationSection) base.Sections["defaultMessageAdapters"];

        [ConfigurationProperty("defaultJobs")]
        public JobScheduleConfigurationSection DefaultJobs
            => (JobScheduleConfigurationSection) base.Sections["defaultJobs"];

        [ConfigurationProperty("tenantConfiguration")]
        public TenantsConfigurationSection TenantConfiguration 
            => (TenantsConfigurationSection) base.Sections["tenantConfiguration"];

        [ConfigurationProperty("encryptedTenantSettings")]
        public EncryptedTenantConfigurationSection EncryptedTenantConfiguration
            => (EncryptedTenantConfigurationSection) base.Sections["encryptedTenantSettings"];
    }

}
