﻿using System;
using System.ComponentModel;
using System.Diagnostics.Contracts;
using Hangfire;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Soikea.IntegrationEngine.Core.Interfaces;
using Soikea.IntegrationEngine.Core.Logging;
using Soikea.IntegrationEngine.HangfireConfiguration.Configuration.Hangfire;
using Soikea.IntegrationEngine.HangfireConfiguration.Configuration.Jobs;
using Soikea.IntegrationEngine.Runtime;

namespace Soikea.IntegrationEngine.HangfireConfiguration.Configuration
{
    public class ExecuteIntegrationJobWithRetries : ExecuteIntegrationJob
    {
        public ExecuteIntegrationJobWithRetries(ExecutionContext executionContext, JobResolver jobResolver, JobSettings jobSettings)
            : base(executionContext, jobResolver, jobSettings)
        {
            
        }

        [AutomaticRetry(Attempts = 10)]
        [DisplayName("Integration engine job execution with retry attempts")]
        public string Execute(IJobCancellationToken cancellationToken, string tenantId, string scheduleIdentifier, string jobNameToExecute)
        {
            return ExecuteJob(cancellationToken, tenantId, scheduleIdentifier, jobNameToExecute);
        }
    }

    public class ExecuteIntegrationJobWithoutRetries : ExecuteIntegrationJob
    {
        public ExecuteIntegrationJobWithoutRetries(ExecutionContext executionContext, JobResolver jobResolver, JobSettings jobSettings)
            : base(executionContext, jobResolver, jobSettings)
        {

        }

        [AutomaticRetry(Attempts = 0)]
        [DisplayName("Integration engine job execution with no retries")]
        public string Execute(IJobCancellationToken cancellationToken, string tenantId, string scheduleIdentifier, string jobNameToExecute)
        {
            return ExecuteJob(cancellationToken, tenantId, scheduleIdentifier, jobNameToExecute);
        }
    }

    public abstract class ExecuteIntegrationJob 
    {
        private readonly ExecutionContext _executionContext;
        private readonly JobResolver _jobResolver;
        private readonly JobSettings _jobSettings;

        protected ExecuteIntegrationJob(ExecutionContext executionContext, JobResolver jobResolver, JobSettings jobSettings)
        {
            Contract.Requires(executionContext != null, "executionContext != null");
            Contract.Requires(jobResolver != null, "jobResolver != null");
            
            _executionContext = executionContext;
            _jobResolver = jobResolver;
            _jobSettings = jobSettings;
        }
                
        protected string ExecuteJob(IJobCancellationToken cancellationToken, string tenantId, string scheduleIdentifier, string jobNameToExecute)
        {
            Contract.Requires(!string.IsNullOrEmpty(tenantId), "tenantId not empty");
            Contract.Requires(jobNameToExecute != null, "jobToExecute != null");            

            Log.Info($"Starting a run of IIntegrationJob {jobNameToExecute}:{scheduleIdentifier} invoked with tenant {tenantId}");

            IIntegrationJob jobToExecute = _jobResolver.Resolve(jobNameToExecute);
            
            if (jobToExecute == null)
            {
                Log.Warn($"Could not resolve IIntegrationJob to execute with given parameter \"{jobNameToExecute}\" of schedule {scheduleIdentifier} invoked with tenant {tenantId}");

                throw new ArgumentException($"The integration job could not be resolved in current scope with given jobname \"{jobNameToExecute}\"");
            }

            jobToExecute.TenantId = tenantId;
            jobToExecute.JobSettings = _jobSettings.ResolveSettings(scheduleIdentifier);

            var jobId = Guid.NewGuid();
            
            using (new Tracer("ExecuteIntegrationJob", jobId)) // Used so log-events can be tracked down to job-execution
            {

                try
                {
                    var watchDog = new HangfireJobAbortionWatchDog(cancellationToken);
                    
                    switch (jobToExecute.ResultType)
                    {
                        case JobResultType.Void:
                            _executionContext.RunJob(watchDog, jobId, jobToExecute);
                            return $"Background job of type {jobNameToExecute}, no result data expected.";

                        case JobResultType.Synchronous:
                            var jobResult = _executionContext.RunJobSynchronous(watchDog, jobId, jobToExecute);
                            return JobResultFormatter.Format(jobResult);

                        case JobResultType.Callback:
                            _executionContext.RunJobAsynchronousCallback(watchDog, jobId, jobToExecute);
                            break;
                    }
                }
                catch (JobAbortedException) { }
                catch (Exception ex)
                {
                    Log.Error(ex, $"An error happened while executing job {jobNameToExecute}:{scheduleIdentifier} invoked with tenant {tenantId}");
                    throw; //Thrown for Hangfire to catch and populate execution log accordingly
                }
            }

            throw new NotSupportedException("An unsupported Integration job ResultType was scheduled.");
        }

    }

 
}
