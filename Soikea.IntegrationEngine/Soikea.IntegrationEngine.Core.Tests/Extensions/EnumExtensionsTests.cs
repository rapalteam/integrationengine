﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soikea.IntegrationEngine.Core.Extensions;

namespace Soikea.IntegrationEngine.Core.Tests.Extensions
{
    [TestClass]
    public class EnumExtensionsTests
    {
        public enum TestEnumForStrings
        {
            [System.ComponentModel.Description("firstDesc")]
            First,
            [System.ComponentModel.Description("secondDesc")]
            Second,
            [System.ComponentModel.Description("thirdDesc")]
            Third,
            Fourth
        }

        public enum TestEnumForIntegers
        {            
            First = 1,
            Second = 2,
            Third = 3,
            Fourth = 4
        }

        public enum TestEnumForIntegersToMapTo
        {
            TFirst = 1,
            TThird = 3,
            TFourth = 4
        }

        [TestMethod]
        public void StringValueOfReturnsDescriptionWhenProvided()
        {
            Assert.AreEqual("firstDesc", TestEnumForStrings.First.StringValueOf());
        }

        [TestMethod]
        public void StringValueOfReturnsToStringWhenDescriptionNotProvided()
        {
            Assert.AreEqual("Fourth", TestEnumForStrings.Fourth.StringValueOf());
        }

        [TestMethod]
        public void EnumValueOfReturnsCorrectResultByDescription()
        {
            Assert.AreEqual(TestEnumForStrings.Second, EnumExtensions.EnumValueOf<TestEnumForStrings>("secondDesc"));
        }

        [TestMethod]
        public void EnumValueOfReturnsCorrectResultByToString()
        {
            Assert.AreEqual(TestEnumForStrings.Fourth, EnumExtensions.EnumValueOf<TestEnumForStrings>("Fourth"));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void EnumValueOfThrowsArgumentExceptionWhenEnumNotFound()
        {
            EnumExtensions.EnumValueOf<TestEnumForStrings>("third");

            Assert.Fail("Should have thrown an argument-exception");
        }

        [TestMethod]
        public void EnumValueOfOrDefaultReturnsValueWhenFound()
        {
            Assert.AreEqual(TestEnumForStrings.First, EnumExtensions.EnumValueOfOrDefault<TestEnumForStrings>("First"));
        }

        [TestMethod]
        public void EnumValueOfOrDefaultReturnsDefaultValueWhenNotFound()
        {
            Assert.AreEqual(TestEnumForStrings.First, EnumExtensions.EnumValueOfOrDefault<TestEnumForStrings>("third"));
        }

        [TestMethod()]
        public void GetDescriptionReturnsCorrectResult()
        {
            var enumValue = TestEnumForStrings.First;

            Assert.AreEqual("firstDesc", enumValue.GetDescription());
        }

        [ExpectedException(typeof(ArgumentException))]
        [TestMethod()]
        public void GetDescriptionThrowsArgumentExceptionWhenNoDescriptionValueExists()
        {
            var enumValue = TestEnumForStrings.Fourth;

            enumValue.GetDescription();

            Assert.Fail("Should have thrown an ArgumentException");
        }


        [TestMethod()]
        public void ConvertToEnumSucceedsWhenValuesAreEqual()
        {
            var originalEnumValue = TestEnumForIntegers.First;
            var convertedEnumValue = originalEnumValue.ConvertToEnum<TestEnumForIntegersToMapTo>();

            Assert.AreEqual(TestEnumForIntegersToMapTo.TFirst, convertedEnumValue);
        }

        [ExpectedException(typeof(ArgumentException))]
        [TestMethod()]
        public void ConvertToEnumFailsWheNoCorresPondingValueExists()
        {
            var originalEnumValue = TestEnumForIntegers.Second;

            originalEnumValue.ConvertToEnum<TestEnumForIntegersToMapTo>();

            Assert.Fail("Should have thrown an ArgumentException");
        }


        [ExpectedException(typeof(ArgumentException))]
        [TestMethod()]
        public void ConvertToEnumFailsWhenSourceIsNotAnEnum()
        {
            var originalValue = "";

            originalValue.ConvertToEnum<TestEnumForIntegersToMapTo>();

            Assert.Fail("Should have thrown an ArgumentException");
        }
    }
}
