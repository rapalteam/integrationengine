﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soikea.IntegrationEngine.Core.Extensions;

namespace Soikea.IntegrationEngine.Core.Tests.Extensions
{
    [TestClass()]
    public class TypeExtensionsTests
    {
        [TestMethod()]
        public void IsNumericTypeReturnsTrueOnAllSupportedTypes()
        {
            Assert.IsTrue(new Byte().IsNumericType());
            Assert.IsTrue(new SByte().IsNumericType());
            Assert.IsTrue(new UInt16().IsNumericType());
            Assert.IsTrue(new UInt32().IsNumericType());
            Assert.IsTrue(new UInt64().IsNumericType());
            Assert.IsTrue(new Int16().IsNumericType());
            Assert.IsTrue(new Int32().IsNumericType());
            Assert.IsTrue(new Int64().IsNumericType());
            Assert.IsTrue(new Decimal().IsNumericType());
            Assert.IsTrue(new Double().IsNumericType());
            Assert.IsTrue(new Single().IsNumericType());
        }

        [TestMethod()]
        public void IsNumericTypeReturnsFalseOnNotSupportedType()
        {
            Assert.IsFalse(new {}.IsNumericType());
        }

        [TestMethod()]
        public void DeepCloneResultsInEqualObjects()
        {
            var testObject = new TestClass(){ Field1 = "Test", Field2 = 99 };

            var copy = testObject.DeepClone();

            Assert.AreEqual(testObject.Field1, copy.Field1);
            Assert.AreEqual(testObject.Field2, copy.Field2);            
        }
    }

    [Serializable]
    public class TestClass
    {
        public string Field1 { get; set; }
        public int Field2 { get; set; }
    }
}
