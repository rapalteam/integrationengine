﻿using System;
using System.Collections.Generic;
using Soikea.IntegrationEngine.Core.Implementation;
using Soikea.IntegrationEngine.Core.Interfaces;

namespace Soikea.IntegrationEngine.Core.Tests.TestAssets
{
    public class TwoWayPublishResult : JobResultBase
    {
        public List<DummyEvent> DummyEvents { get; private set; }
        public TwoWayPublishResult(Guid jobId) : base(jobId)
        {
            DummyEvents = new List<DummyEvent>();
        }

        public override string Name => "TwoWayPublishResult";

        public override void Receive(IEvent @event)
        {
            base.Receive(@event);

            if (IsEndOfJobResult(@event)) return;

            var dummyEvent = @event as DummyEvent;

            if (dummyEvent != null)
                DummyEvents.Add(dummyEvent);
        }
    }
}
