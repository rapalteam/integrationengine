﻿using System;
using Soikea.IntegrationEngine.Core.Implementation;
using Soikea.IntegrationEngine.Core.Interfaces;

namespace Soikea.IntegrationEngine.Core.Tests.TestAssets
{
    public class TestJobTwoWayPublishJob : SynchronousIntegrationJob
    {
        private readonly int _testItemCount;

        public TestJobTwoWayPublishJob(int testItemCount)
        {
            _testItemCount = testItemCount;
        }

        public override string Name => "TestJobTwoWayPublishJob";

        public override IJobResult CreateJobResult(Guid jobId)
        {
            return new TwoWayPublishResult(jobId);
        }

        public override JobResultStatus Execute(IJobAbortionWatchDog jobAbortionWatchDog, Constants.PublishEvent publishInput, Constants.PublishEvent publishOutput)
        {
            var dummyCategory = new SubscriptionCategory("Dummy");

            for (int i = 0; i < _testItemCount; i++)
            {
                publishInput(new DummyEvent()
                {
                    JobId = JobId,
                    Category = dummyCategory,
                    Payload = new ExamplePayload() { Value = i }
                });

                publishOutput(new DummyEvent()
                {
                    JobId = JobId,
                    Category = dummyCategory,
                    Payload = new ExamplePayload() { Value = i }
                });
            }

            return JobResultStatus.Started;
        }
    }
}
