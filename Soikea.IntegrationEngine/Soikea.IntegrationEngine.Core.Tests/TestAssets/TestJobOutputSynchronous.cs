using System;
using Soikea.IntegrationEngine.Core.Implementation;
using Soikea.IntegrationEngine.Core.Interfaces;

namespace Soikea.IntegrationEngine.Core.Tests.TestAssets
{
    public class TestJobOutputSynchronous : SynchronousIntegrationJob
    {
        private readonly int _publishDummiesCount;
        public override string Name => "TestJobOutputSynchronous";

        public override IJobResult CreateJobResult(Guid jobId)
        {
            return new TestJobResult(jobId);
        }

        public TestJobOutputSynchronous(int publishDummiesCount = 0)
        {
            _publishDummiesCount = publishDummiesCount;
        }

        public override JobResultStatus Execute(IJobAbortionWatchDog jobAbortionWatchDog, Constants.PublishEvent publishInput, Constants.PublishEvent publishOutput)
        {            
            var dummyCategory = new SubscriptionCategory("Dummy");

            for (int i = 0; i < _publishDummiesCount; i++)
            {
                publishOutput(new DummyEvent()
                {
                    JobId = JobId,
                    Category = dummyCategory,
                    Payload = new ExamplePayload() {  Value = i }
                });
            }

            return JobResultStatus.Ok;
        }
    }
}