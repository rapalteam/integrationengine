using System;
using Soikea.IntegrationEngine.Core.Implementation;

namespace Soikea.IntegrationEngine.Core.Tests.TestAssets
{
    public class TestJobResult : JobResultBase
    {
        public override string Name
        {
            get { return "TestJobResult"; }
        }

        public TestJobResult(Guid jobId) : base(jobId)
        {
        }
    }
}