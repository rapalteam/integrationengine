using System;
using System.Runtime.Serialization;
using Soikea.IntegrationEngine.Core.Interfaces;

namespace Soikea.IntegrationEngine.Core.Tests.TestAssets
{
    public class DummyEvent : IEvent
    {
        public string TenantId { get; set; }
        public Guid JobId { get; set; }
        public SubscriptionCategory Category { get; set; }
        public ISerializable Payload { get; set; }
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            throw new NotImplementedException();
        }
    }

    [Serializable]
    public class ExamplePayload : ISerializable
    {
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Value", Value);
        }

        public int Value { get; set; }
    }
}