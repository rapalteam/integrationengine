﻿using Soikea.IntegrationEngine.Core.Logging;

namespace Soikea.IntegrationEngine.Core.Tests
{
    public class NoOpLogProvider : ILogProvider
    {
        public ILog GetLogger(string name, string module)
        {
            return new Log.NoOpLogger();
        }
    }
}
