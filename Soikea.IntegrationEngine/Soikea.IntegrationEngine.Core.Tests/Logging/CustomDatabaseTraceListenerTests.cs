﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.EnterpriseLibrary.Logging.Formatters;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soikea.IntegrationEngine.Core.Logging.EnterpriseLibrary;

namespace Soikea.IntegrationEngine.Core.Tests.Logging
{
    [TestClass]
    public class CustomDatabaseTraceListenerTests
    {
        private static string _connectionString;

        #region Setup

        [TestInitialize]
        public void Setup()
        {
            _connectionString = ConfigurationManager.ConnectionStrings["Logging"].ConnectionString;
        }

        #endregion

        #region Tests

        [TestMethod]
        public void FormatterListenerWithStoredProcsAndDbInstance()
        {
            var listener = new CustomDatabaseTraceListener(new SqlDatabase(_connectionString), "WriteLog", "AddCategory", new TextFormatter("TEST{newline}TEST"));
            var before = GetNumberOfLogMessages();
            var logEntry = new CustomLogEntry
            {
                Message = "message",
                Categories = new List<string> { "cat1" },
                Priority = 0,
                EventId = 0,
                Severity = TraceEventType.Error,
                Title = "title",
                ExtendedProperties = null,
                Module = "test",
            };

            listener.TraceData(new TraceEventCache(), "source", TraceEventType.Error, 0, logEntry);

            var after = GetNumberOfLogMessages();
            string messageContents = GetLastLogMessage();
            Assert.AreEqual("message", messageContents);
            Assert.AreEqual(before, after-1);
        }

        [TestMethod]
        public void FormattedListenerWithStoredProcsAndRegularLogEntry()
        {
            var listener = new CustomDatabaseTraceListener(new SqlDatabase(_connectionString), "WriteLog", "AddCategory", null);

            var logEntry = new CustomLogEntry {
                Message = "message",
                Categories = new List<string> { "cat1" } ,
                Priority = 0,
                EventId = 0,
                Severity = TraceEventType.Error,
                Title = "title",
                ExtendedProperties = null,
                Module = "test",
            };

            listener.TraceData(new TraceEventCache(), "source", TraceEventType.Error, 0, logEntry);

            string messageContents = GetLastLogMessage();
            Assert.AreEqual(logEntry.Message, messageContents);
        }

        [TestMethod]
        public void FormatterListenerWithStoredProcsAndCustomLogEntry()
        {
            var listener = new CustomDatabaseTraceListener(new SqlDatabase(_connectionString), "WriteLog", "AddCategory", new TextFormatter("TEST{newline}TEST"));

            var customEntry = new CustomLogEntry
            {
                Message = "message",
                Categories = new List<string> {"cat1"},
                Priority = 0,
                EventId = 0,
                Severity = TraceEventType.Error,
                Title = "title",
                ExtendedProperties = null,
                ActivityId = Guid.NewGuid()
            };

            listener.TraceData(new TraceEventCache(), "source", TraceEventType.Error, 0, customEntry);

            var jobId = GetLastLogJobId();
            var message = GetLastLogMessage();

            Assert.AreEqual(customEntry.Message, message);
            Assert.AreEqual(customEntry.ActivityIdString, jobId);
        }

        [TestMethod]
        public void ListenerWithoutFormattingWithStoredProcsAndCustomLogEntry()
        {
            var listener = new CustomDatabaseTraceListener(new SqlDatabase(_connectionString), "WriteLog", "AddCategory", null);

            var customEntry = new CustomLogEntry
            {
                Message = "message",
                Categories = new List<string> {"cat1"},
                Priority = 0,
                EventId = 0,
                Severity = TraceEventType.Error,
                Title = "title",
                ExtendedProperties = null,
                ActivityId = Guid.NewGuid()
            };

            listener.TraceData(new TraceEventCache(), "source", TraceEventType.Error, 0, customEntry);

            var jobId = GetLastLogJobId();
            var message = GetLastLogMessage();

            Assert.AreEqual(customEntry.Message, message);
            Assert.AreEqual(customEntry.ActivityIdString, jobId);
        }

        [TestMethod]
        public void LogEntryWithFormatter()
        {
            var listener = new CustomDatabaseTraceListener(new SqlDatabase(_connectionString), "WriteLog", "AddCategory", null);

            var customEntry = new LogEntry
            {
                Message = "message",
                Categories = new List<string> { "cat1" },
                Priority = 0,
                EventId = 0,
                Severity = TraceEventType.Error,
                Title = "title",
                ExtendedProperties = null,
                ActivityId = Guid.NewGuid()
            };

            listener.TraceData(new TraceEventCache(), "source", TraceEventType.Error, 0, customEntry);

            var jobId = GetLastLogJobId();
            var message = GetLastLogMessage();

            Assert.AreEqual(customEntry.Message, message);
            Assert.AreEqual(customEntry.ActivityIdString, jobId);
        }
        #endregion

        #region Helpers

        string GetLastLogMessage()
        {
            var db = new SqlDatabase(_connectionString);

            var command = db.GetSqlStringCommand("SELECT TOP 1 Message FROM Log ORDER BY LogID DESC");
            var content = Convert.ToString(db.ExecuteScalar(command));
            return content;
        }

        static int GetNumberOfLogMessages()
        {
            var db = new SqlDatabase(_connectionString);

            var cmd = db.GetSqlStringCommand("SELECT COUNT(*) FROM Log");
            return Convert.ToInt32(db.ExecuteScalar(cmd));
        }

        string GetLastLogJobId()
        {
            var db = new SqlDatabase(_connectionString);

            var command = db.GetSqlStringCommand("SELECT TOP 1 JobId FROM Log ORDER BY LogID DESC");
            return Convert.ToString(db.ExecuteScalar(command));
        }

        #endregion
    }
}
