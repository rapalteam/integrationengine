﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soikea.IntegrationEngine.Core.Logging;

namespace Soikea.IntegrationEngine.Core.Tests.Logging
{
    [TestClass()]
    public class LoggerTests
    {

        [TestMethod()]
        public void WriteOutputsCorrectString()
        {
            var testString = string.Empty;

            var log = new Logger((string s)=> { testString = s; });

            log.Write("{0} {1}", "foo", "bar");

            Assert.AreEqual("foo bar", testString);
        }
    }
}
