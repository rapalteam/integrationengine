﻿using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soikea.IntegrationEngine.Core.Logging;

namespace Soikea.IntegrationEngine.Core.Tests.Logging
{
    [TestClass()]
    public class LogTimerTests
    {

        [ExpectedException(typeof(ArgumentException))]
        [TestMethod]
        public void ConstructorThrowsArgumentExceptionWhenOutputDelegateNotProvided()
        {
            var logTimer = new LogTimer(null);

            Assert.Fail("Should've thrown an argumentexception");
        }


        [TestMethod]
        public void TimerIsStartedAutomatically()
        {
            var logTimer = new LogTimer((string s) => {}, true);
            
            Assert.IsTrue(logTimer.IsRunning);
        }

        [TestMethod]
        public void TimerIsNotStartedAutomatically()
        {
            var logTimer = new LogTimer((string s) => { });

            Assert.IsFalse(logTimer.IsRunning);
        }

        [TestMethod()]
        public void StartDoesActuallyStartTheTimer()
        {
            var logTimer = new LogTimer((string s) => { });

            Assert.IsFalse(logTimer.IsRunning);

            logTimer.Start();
            
            Assert.IsTrue(logTimer.IsRunning);
        }

        [TestMethod()]
        public void StopActuallyStopsTheTimer()
        {
            var logTimer = new LogTimer((string s) => { }, true);
            
            Assert.IsTrue(logTimer.IsRunning);
            
            logTimer.Stop();

            Assert.IsFalse(logTimer.IsRunning);
        }

        [TestMethod()]
        public void StopAndLogStopsTheTimerAndOutputsASeeminglyCorrectMessage()
        {
            var testString = string.Empty;
            var logTimer = new LogTimer((string s) => { testString = s; }, true);

            Assert.IsTrue(logTimer.IsRunning);

            logTimer.StopAndLog("Testmessage");

            Assert.IsFalse(logTimer.IsRunning);
            Assert.IsTrue(testString.StartsWith("Testmessage:"));
        }

        [TestMethod()]
        public void StopAndLogStopsTheTimerAndOutputsAFormattedMessage()
        {
            var testString = string.Empty;
            var logTimer = new LogTimer((string s) => { testString = s; }, true);

            Assert.IsTrue(logTimer.IsRunning);

            logTimer.StopAndLog("Testmessage {0} {1}", "foo", "bar");

            Assert.IsFalse(logTimer.IsRunning);
            Assert.IsTrue(testString.StartsWith("Testmessage foo bar:"));            
        }


        [TestMethod()]
        public void LogElapsedTimeOutputsElapsedTime()
        {
            var testString = string.Empty;
            var logTimer = new LogTimer((string s) => { testString = s; }, true);

            Thread.Sleep(100);

            logTimer.Stop();

            logTimer.LogElapsedTime("TestMessage");

            Assert.AreEqual("TestMessage: 00:00:00.10",testString);

            var msgAtFirst = testString;
            testString = string.Empty;

            logTimer.Start();

            Thread.Sleep(100);

            logTimer.Stop();

            logTimer.LogElapsedTime("TestMessage");
            var msgAfterAll = testString;

            Assert.AreEqual("TestMessage: 00:00:00.20", testString);

            Assert.AreNotEqual(msgAtFirst, msgAfterAll);
        }

        [ExpectedException(typeof(ArgumentException))]
        [TestMethod()]
        public void LogActionTimeThrowsArgumentExceptionWhenActionNotProvided()
        {
            var logTimer = new LogTimer((string s) => { });

            logTimer.LogActionTime(null, "Should throw an exception");

            Assert.Fail();
        }

        [TestMethod()]
        public void LogActionTimeOutputsCorrectResult()
        {
            var testString = string.Empty;
            var logTimer = new LogTimer((string s) => { testString = s; });

            logTimer.LogActionTime(()=> Thread.Sleep(100), "TestMessage");

            Assert.IsFalse(logTimer.IsRunning);
            Assert.AreEqual("TestMessage: 00:00:00.10", testString);
        }

        [ExpectedException(typeof(ArgumentException))]
        [TestMethod()]
        public void LogActionTimeWithDelegateThrowsArgumentExceptionWhenActionNotProvided()
        {
            var logTimer = new LogTimer((string s) => { });

            logTimer.LogActionTime<int>(null, "Should throw an exception");

            Assert.Fail();
        }

        [TestMethod()]
        public void LogActionTimeWithDelegateOutputsCorrectResult()
        {
            var testString = string.Empty;
            var logTimer = new LogTimer((string s) => { testString = s; });

            var result = logTimer.LogActionTime<int>(() => { 
                Thread.Sleep(100);
                return 1;
            }, "TestMessage");

            Assert.IsFalse(logTimer.IsRunning);
            Assert.AreEqual("TestMessage: 00:00:00.10", testString);
            Assert.AreEqual(1, result);
        }
    }
}
