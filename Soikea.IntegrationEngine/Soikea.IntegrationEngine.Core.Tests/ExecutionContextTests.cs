﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Soikea.IntegrationEngine.Core.Implementation.Events;
using Soikea.IntegrationEngine.Core.Interfaces;
using Soikea.IntegrationEngine.Core.Logging;
using Soikea.IntegrationEngine.Core.Tests.TestAssets;
using Soikea.IntegrationEngine.Runtime;

namespace Soikea.IntegrationEngine.Core.Tests
{
    [DeploymentItem("Moq.dll")]
    [TestClass]
    public class ExecutionContextTests
    {

        private string _tenantId = "tenant";

        [ClassInitialize]
        public static void Init(TestContext context)
        {
            var mockLogProvider = new Mock<ILogProvider>();
            var mockLogger = new Mock<ILog>();

            mockLogger.Setup(x => x.Log(LogLevel.Trace, It.IsAny<Func<string>>(), It.IsAny<Exception>()));
            mockLogger.Setup(x => x.Log(LogLevel.Info, It.IsAny<Func<string>>(), It.IsAny<Exception>()));
            mockLogProvider.Setup(x => x.GetLogger(It.IsAny<string>(), It.IsAny<string>()))
              .Returns(mockLogger.Object);            
            Log.SetCurrentLogProvider(mockLogProvider.Object, "Test");
        }

        [TestMethod()]
        public async Task RunJobStartsJobExecution()
        {
            var mockSubscriptionContext = new Mock<SubscriptionContext>();
            mockSubscriptionContext.Setup(x=> x.PublishOutputEvent(It.IsAny<StartOfJobEvent>()))
                .Verifiable();
            mockSubscriptionContext.Setup(x => x.PublishOutputEvent(It.IsAny<EndOfJobEvent>()))
                .Verifiable();
            var mockWatchDog = new Mock<IJobAbortionWatchDog>();
            
            var executionContext = new ExecutionContext(mockSubscriptionContext.Object);

            var mockJob = new Mock<IIntegrationJob>();
            
            mockJob.Setup(x => x.Name).Returns("MockJob");
            mockJob.Setup(x => x.TenantId).Returns(_tenantId);
            mockJob.Setup(x => x.Execute(It.IsAny<IJobAbortionWatchDog>(), It.IsAny<Constants.PublishEvent>(), It.IsAny<Constants.PublishEvent>()))
                .Verifiable("Should've called Execute");


            await executionContext.RunJob(mockWatchDog.Object, Guid.NewGuid(), mockJob.Object);

            mockJob.VerifyAll();
            mockSubscriptionContext.VerifyAll();
        }

        [TestMethod()]
        public void RunJobSynchronousExecutesOk()
        {
            var mockSubscriptionContext = new Mock<SubscriptionContext>();
            mockSubscriptionContext.Setup(x => x.PublishOutputEvent(It.IsAny<StartOfJobEvent>()))
                .Verifiable();
            mockSubscriptionContext.Setup(x => x.PublishOutputEvent(It.IsAny<EndOfJobEvent>()))
                .Verifiable();
            var mockWatchDog = new Mock<IJobAbortionWatchDog>();

            var executionContext = new ExecutionContext(mockSubscriptionContext.Object);

            var job = new TestJobInputSynchronous()
            {
                TenantId = _tenantId
            };

            var result = executionContext.RunJobSynchronous(mockWatchDog.Object, Guid.NewGuid(), job);

            Assert.AreEqual(JobResultStatus.Ok, result.Status);
            mockSubscriptionContext.VerifyAll();
        }

        [TestMethod()]
        public void RunJobSynchronousPublishesAllInputResults()
        {
            var mockSubscriptionContext = new Mock<SubscriptionContext>();
            mockSubscriptionContext.Setup(x => x.PublishOutputEvent(It.IsAny<StartOfJobEvent>()))
                .Verifiable();
            mockSubscriptionContext.Setup(x => x.PublishOutputEvent(It.IsAny<EndOfJobEvent>()))
                .Verifiable();
            var mockWatchDog = new Mock<IJobAbortionWatchDog>();

            var publishedResults = new List<IEvent>();

            mockSubscriptionContext.Setup(x=> x.PublishInputEvent(It.IsAny<IEvent>()))
                .Callback((IEvent r) => publishedResults.Add(r));

            var executionContext = new ExecutionContext(mockSubscriptionContext.Object);

            var job = new TestJobInputSynchronous(5)
            {
                TenantId = _tenantId
            };

            var result = executionContext.RunJobSynchronous(mockWatchDog.Object, Guid.NewGuid(), job);
            
            Assert.AreEqual(JobResultStatus.Ok, result.Status);
            Assert.AreEqual(5, publishedResults.Count);
            
            mockSubscriptionContext.VerifyAll();
        }

        [TestMethod()]
        public void RunJobSynchronousPublishesAllOutputResults()
        {
            var mockSubscriptionContext = new Mock<SubscriptionContext>();
            var mockWatchDog = new Mock<IJobAbortionWatchDog>();

            var publishedResults = new List<IEvent>();
            var publishedAdministrationEvents = new List<IEvent>();

            mockSubscriptionContext.Setup(x => x.PublishOutputEvent(It.IsAny<IEvent>()))
                .Callback((IEvent r) =>
                {
                    if (!(r is EndOfJobEvent) && !(r is StartOfJobEvent)) publishedResults.Add(r);
                    else publishedAdministrationEvents.Add(r);
                });

            var executionContext = new ExecutionContext(mockSubscriptionContext.Object);

            var job = new TestJobOutputSynchronous(5)
            {
                TenantId = _tenantId
            };

            var result = executionContext.RunJobSynchronous(mockWatchDog.Object, Guid.NewGuid(), job);

            Assert.AreEqual(JobResultStatus.Ok, result.Status);
            Assert.AreEqual(5, publishedResults.Count);

            Assert.AreEqual(2, publishedAdministrationEvents.Count);
        }

        [TestMethod]
        public void RunJobSynchronousPublishesResultsBothWays()
        {
            var subscriptionContext = new SubscriptionContext();
            var mockWatchDog = new Mock<IJobAbortionWatchDog>();

            var executionContext = new ExecutionContext(subscriptionContext);

            var job = new TestJobTwoWayPublishJob(5)
            {
                TenantId = _tenantId
            };
            var runId = Guid.NewGuid();

            var result = executionContext.RunJobSynchronous(mockWatchDog.Object, runId, job);

            Assert.AreEqual(JobResultStatus.Ok, result.Status);

            var twowayResult = result as TwoWayPublishResult;

            Assert.IsNotNull(twowayResult);

            Assert.AreEqual(runId, twowayResult.JobId);
            Assert.IsFalse(string.IsNullOrEmpty(twowayResult.ExecutionDetails.JobName));
            
            Assert.AreEqual(5, twowayResult.ExecutionDetails.InputCount);
            Assert.AreEqual(5, twowayResult.ExecutionDetails.OutputCount);
            Assert.AreEqual(5, twowayResult.DummyEvents.Count);
            
        }

        [TestMethod()]
        public void RunJobAsynchronousCallbackDoesWhat()
        {
            Assert.Fail();
        }
    }
}
