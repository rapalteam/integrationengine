﻿using System;
using System.Runtime.Serialization;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soikea.IntegrationEngine.Core.Implementation;
using Soikea.IntegrationEngine.Core.Implementation.Events;
using Soikea.IntegrationEngine.Core.Interfaces;

namespace Soikea.IntegrationEngine.Core.Tests.Implementation
{
    [TestClass()]
    public class EndOfJobPayloadTests
    {

        [TestMethod()]
        public void EndOfJobPayloadConstructorPopulatesProperties()
        {
            var tenant = "tenant";
            var guid = Guid.NewGuid();

            const string jobName = "Test";
            const JobResultStatus status = JobResultStatus.Ok;
            const int inputCount = 1;
            const int outputCount = 2;

            var payload = new EndOfJobPayload(tenant, guid, jobName, status, inputCount, outputCount);

            Assert.AreEqual(tenant, payload.TenantId);
            Assert.AreEqual(guid, payload.JobId);
            Assert.AreEqual(jobName, payload.JobName);
            Assert.AreEqual(status, payload.Status);
            Assert.AreEqual(inputCount, payload.InputCount);
            Assert.AreEqual(outputCount, payload.OutputCount);
        }

        [TestMethod()]
        public void GetObjectDataTest()
        {
            var tenant = "tenant";
            var guid = Guid.NewGuid();

            const string jobName = "Test";
            const JobResultStatus status = JobResultStatus.Ok;
            const int inputCount = 1;
            const int outputCount = 2;

            var payload = new EndOfJobPayload(tenant, guid, jobName, status, inputCount, outputCount);

            var serializationInfo = new SerializationInfo(typeof(SubscriptionCategory), new FormatterConverter());

            payload.GetObjectData(serializationInfo, new StreamingContext());

            Assert.AreEqual(tenant, serializationInfo.GetValue("TenantId", typeof(string)));
            Assert.AreEqual(guid, serializationInfo.GetValue("JobId", typeof(Guid)));
            Assert.AreEqual(jobName, serializationInfo.GetValue("JobName", typeof(string)));
            Assert.AreEqual("Ok", serializationInfo.GetValue("Status", typeof(string)));
            Assert.AreEqual(inputCount, serializationInfo.GetValue("InputCount", typeof(int)));
            Assert.AreEqual(outputCount, serializationInfo.GetValue("OutputCount", typeof(int)));
        }
    }
}
