GO
CREATE TABLE [dbo].[InsertTestTable] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [StringValue] NVARCHAR (MAX) NULL,
    [IntValue]    INT            NULL,
    [BoolValue]   BIT            NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

GO
CREATE TABLE [dbo].[SelectTestTable] (
    [Id]          INT            NOT NULL,
    [StringValue] NVARCHAR (MAX) NULL,
    [IntValue]    INT            NULL,
    [BoolValue]   BIT            NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE TABLE [dbo].[TestTable] (
    [Id]        INT NOT NULL,
    [BoolValue] BIT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

INSERT INTO [dbo].[SelectTestTable] (Id, StringValue, IntValue, BoolValue) VALUES (1, 'default-string', 10, True)
GO

INSERT INTO [dbo].[TestTable] (Id, BoolValue) VALUES (1, False)
GO
