﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Soikea.IntegrationEngine.Core.Extensions;
using Soikea.IntegrationEngine.Core.Implementation.Sql;
using Soikea.IntegrationEngine.Core.Interfaces;
using Soikea.IntegrationEngine.Core.Logging;

namespace Soikea.IntegrationEngine.Core.Tests.Implementation.Sql
{
    [TestClass]
    public class SqlQueryProviderTests
    {

        [TestMethod]
        public void FetchReturnsFalseWhenConnectionStringIsNotFound()
        {
            var log = new Mock<ILog>();
            var logMessage = "";
            log.Setup(x => x.Log(LogLevel.Warn, It.IsAny<Func<string>>(), It.IsAny<Exception>())).Callback((LogLevel l, Func<string> sFunc, Exception e) => logMessage = sFunc.Invoke());

            var logProvider = new Mock<ILogProvider>();
            logProvider.Setup(w => w.GetLogger(It.IsAny<string>(), It.IsAny<string>())).Returns(log.Object);
            Log.SetCurrentLogProvider(logProvider.Object, "asdf");

            var abortDog = new Mock<IJobAbortionWatchDog>();
            var provider = new SqlQueryProvider();

            var result = provider.Fetch(string.Empty, string.Empty, null, TestClass.Create, abortDog.Object);

            Assert.IsNull(result);
            Assert.AreEqual(logMessage, "Could not find requested connection-string ''");
        }

        [TestMethod]
        public void FetchReturnsFalseWhenQueryStringIsNullOrEmpty()
        {
            var log = new Mock<ILog>();
            var logMessage = "";
            log.Setup(x => x.Log(LogLevel.Warn, It.IsAny<Func<string>>(), It.IsAny<Exception>())).Callback((LogLevel l, Func<string> sFunc, Exception e) => logMessage = sFunc.Invoke());

            var logProvider = new Mock<ILogProvider>();
            logProvider.Setup(w => w.GetLogger(It.IsAny<string>(), It.IsAny<string>())).Returns(log.Object);
            Log.SetCurrentLogProvider(logProvider.Object, "asdf");
            var abort = new Mock<IJobAbortionWatchDog>();
            var provider = new SqlQueryProvider();

            var result = provider.Fetch("Test", string.Empty, null, TestClass.Create, abort.Object);

            Assert.IsNull(result);
            Assert.AreEqual(logMessage, "Cannot execute SELECT with an empty queryString");
        }

        [TestMethod]
        public void FetchReturnsCorrectResultWhenQueryStringIsOk()
        {
            var log = new Mock<ILog>();

            var logProvider = new Mock<ILogProvider>();
            logProvider.Setup(w => w.GetLogger(It.IsAny<string>(), It.IsAny<string>())).Returns(log.Object);
            Log.SetCurrentLogProvider(logProvider.Object, "asdf");
            var abort = new Mock<IJobAbortionWatchDog>();
            var provider = new SqlQueryProvider();

            var queryString = "SELECT * FROM SelectTestTable";

            var result = provider.Fetch("TestDb", queryString, null, TestClass.Create, abort.Object);
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Count(), 1);
        }

        [TestMethod]
        public void FetchReturnsCorrectResultWhenRequestingFromManyTables()
        {
            var log = new Mock<ILog>();
         
            var logProvider = new Mock<ILogProvider>();
            logProvider.Setup(w => w.GetLogger(It.IsAny<string>(), It.IsAny<string>())).Returns(log.Object);
            Log.SetCurrentLogProvider(logProvider.Object, "asdf");
            var abort = new Mock<IJobAbortionWatchDog>();
            var provider = new SqlQueryProvider();

            var queryString = "SELECT Id, (Select stringvalue from SelectTestTable where selecttesttable.id = testtable.id) as StringValue, BoolValue FROM TestTable";

            var result = provider.Fetch("TestDb", queryString, null, AnotherTestClass.Create, abort.Object).ToList();
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Count, 1);
            Assert.AreEqual(result[0].StringValue, "default-string");
        }

        [TestMethod]
        public void FetchReturnsCorrectResultWhenRequestingWithIdThatIsNotFound()
        {
            var log = new Mock<ILog>();
          
            var logProvider = new Mock<ILogProvider>();
            logProvider.Setup(w => w.GetLogger(It.IsAny<string>(), It.IsAny<string>())).Returns(log.Object);
            Log.SetCurrentLogProvider(logProvider.Object, "asdf");
            var abort = new Mock<IJobAbortionWatchDog>();
            var provider = new SqlQueryProvider();

            var queryString = "SELECT * FROM SelectTestTable where id = 3";

            var result = provider.Fetch("TestDb", queryString, null, AnotherTestClass.Create, abort.Object).ToList();
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Count, 0);
        }

        [TestMethod]
        public void FetchReturnsCorrectResultWhenAbortIsCalled()
        {
            var log = new Mock<ILog>();
           
            var logProvider = new Mock<ILogProvider>();
            logProvider.Setup(w => w.GetLogger(It.IsAny<string>(), It.IsAny<string>())).Returns(log.Object);
            Log.SetCurrentLogProvider(logProvider.Object, "asdf");

            var checkCount = 0;
            var abort = new Mock<IJobAbortionWatchDog>();
            abort.Setup(w => w.ThrowIfJobAbortionRequested()).Callback(() =>
            {
                if (checkCount++ == 1)
                {
                    throw new JobAbortedException("Aborting job execution from unit-test");
                }
            });

            var provider = new SqlQueryProvider();

            const string queryString = "BEGIN TRANSACTION\nSELECT * FROM TestTable WITH (TABLOCKX, HOLDLOCK) WHERE 0 = 1\nWAITFOR DELAY '00:05'\nROLLBACK TRANSACTION";

            try
            {
                provider.Fetch("TestDb", queryString, null, AnotherTestClass.Create, abort.Object);
                Assert.Fail("Should have gotten exception");
            }
            catch (JobAbortedException e)
            {
                Assert.AreEqual("Aborting job execution from unit-test", e.Message);
            }            
        }

        [TestMethod]
        public void FetchReturnsCorrectEntitiesWhenUsingAddedQueryParameters()
        {
            var log = new Mock<ILog>();
           
            var logProvider = new Mock<ILogProvider>();
            logProvider.Setup(w => w.GetLogger(It.IsAny<string>(), It.IsAny<string>())).Returns(log.Object);
            Log.SetCurrentLogProvider(logProvider.Object, "asdf");
            var abort = new Mock<IJobAbortionWatchDog>();
            var provider = new SqlQueryProvider();

            var queryString = "SELECT * FROM SelectTestTable where Id = @Id";

            var result = provider.Fetch("TestDb", queryString, delegate(SqlParameterCollection p)
            {
                p.AddWithValue("@Id", "2");
            }, AnotherTestClass.Create, abort.Object).ToList();
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Count, 0);
        }

        [TestMethod]
        public void InsertReturnsCorrectResultWhenConnectionStringIsNotFound()
        {
            var log = new Mock<ILog>();
            var logMessage = "";
            log.Setup(x => x.Log(LogLevel.Warn, It.IsAny<Func<string>>(), It.IsAny<Exception>())).Callback((LogLevel l, Func<string> sFunc, Exception e) => logMessage = sFunc.Invoke());

            var logProvider = new Mock<ILogProvider>();
            logProvider.Setup(w => w.GetLogger(It.IsAny<string>(), It.IsAny<string>())).Returns(log.Object);
            Log.SetCurrentLogProvider(logProvider.Object, "asdf");

            var abortDog = new Mock<IJobAbortionWatchDog>();
            var provider = new SqlQueryProvider();

            var result = provider.Insert(string.Empty, null, abortDog.Object);

            Assert.IsFalse(result);
            Assert.AreEqual(logMessage, "Could not find connectionString with '' in INSERT command.");
        }

        [TestMethod]
        public void InsertReturnsCorrectResultWhenSqlCommandIsNotFound()
        {
            var log = new Mock<ILog>();
            var logMessage = "";
            log.Setup(x => x.Log(LogLevel.Warn, It.IsAny<Func<string>>(), It.IsAny<Exception>())).Callback((LogLevel l, Func<string> sFunc, Exception e) => logMessage = sFunc.Invoke());

            var logProvider = new Mock<ILogProvider>();
            logProvider.Setup(w => w.GetLogger(It.IsAny<string>(), It.IsAny<string>())).Returns(log.Object);
            Log.SetCurrentLogProvider(logProvider.Object, "asdf");

            var abortDog = new Mock<IJobAbortionWatchDog>();
            var provider = new SqlQueryProvider();

            var result = provider.Insert("Test", null, abortDog.Object);

            Assert.IsFalse(result);
            Assert.AreEqual(logMessage, "Cannot execute INSERT with null insertCommand");
        }

        [TestMethod]
        public void InsertReturnsCorrectResultWhenInsertingSingleValue()
        {
            var log = new Mock<ILog>();
            var logMessage = "";
            log.Setup(x => x.Log(LogLevel.Trace, It.IsAny<Func<string>>(), It.IsAny<Exception>())).Callback((LogLevel l, Func<string> sFunc, Exception e) => logMessage = sFunc.Invoke());

            var logProvider = new Mock<ILogProvider>();
            logProvider.Setup(w => w.GetLogger(It.IsAny<string>(), It.IsAny<string>())).Returns(log.Object);
            Log.SetCurrentLogProvider(logProvider.Object, "asdf");

            var abortDog = new Mock<IJobAbortionWatchDog>();
            var provider = new SqlQueryProvider();

            var testValue = new TestClass
            {
                BoolValue = false,
                IntValue = 10,
                StringValue = "defaults"
            };

            const string testDbName = "TestDb";

            var countBefore = provider.Fetch(testDbName, "SELECT * FROM InsertTestTable", null, TestClass.Create,
                abortDog.Object).ToList().Count;

            var result = provider.Insert(testDbName, testValue.GetInsertCommand("InsertTestTable"), abortDog.Object);

            Assert.AreEqual(logMessage, "Successfully fininshed INSERT query INSERT INTO InsertTestTable(StringValue, IntValue, BoolValue) VALUES (@stringValue, @intValue, @boolValue)");

            var countAfter = provider.Fetch(testDbName, "SELECT * FROM InsertTestTable", null, TestClass.Create,
                abortDog.Object).ToList().Count;

            Assert.IsTrue(result);
            Assert.IsTrue(--countAfter == countBefore);
        }

        [TestMethod]
        [ExpectedException(typeof (NotSupportedException))]
        public void FetchReturnsCorrectExceptionWhenDoingNotSupportedCast()
        {
            var log = new Mock<ILog>();
            
            var logProvider = new Mock<ILogProvider>();
            logProvider.Setup(w => w.GetLogger(It.IsAny<string>(), It.IsAny<string>())).Returns(log.Object);
            Log.SetCurrentLogProvider(logProvider.Object, "asdf");
            var abort = new Mock<IJobAbortionWatchDog>();
            var provider = new SqlQueryProvider();

            var queryString = "SELECT * FROM SelectTestTable";

            var result = provider.Fetch("TestDb", queryString, null, InvalidTestClass.Create, abort.Object);
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Count(), 1);
        }
        
    }

    public class TestClass : IInsertable
    {
        public int Id { get; set; }
        public string StringValue { get; set; }

        public int IntValue { get; set; }

        public bool BoolValue { get; set; }

        public static TestClass Create(Dictionary<string, QueryResultField> record)
        {
            var result = new TestClass
            {
                BoolValue = record["BoolValue"].GetValue<bool>(),
                IntValue = record["IntValue"].GetValue<int>(),
                StringValue = record["StringValue"].GetValue<string>(),
                Id = record["Id"].GetValue<int>()
            };
            return result;
        }

        public SqlCommand GetInsertCommand(string tableName)
        {
            var command = new SqlCommand
            {
                CommandType = CommandType.Text
            };

            var queryString = @"INSERT INTO " +tableName+ "(StringValue, IntValue, BoolValue) VALUES (@stringValue, @intValue, @boolValue)";

            command.CommandText = queryString;

            command.Parameters.AddWithValue("@stringValue", StringValue);
            command.Parameters.AddWithValue("@intValue", IntValue);
            command.Parameters.AddWithValue("@boolValue", BoolValue);

            return command;
        }
    }

    public class AnotherTestClass : IInsertable
    {
        public int Id { get; set; }
        public string StringValue { get; set; }
        public bool BoolValue { get; set; }

        public static AnotherTestClass Create(Dictionary<string, QueryResultField> record)
        {
            var result = new AnotherTestClass
            {
                Id = record["Id"].GetValue<int>(),
                StringValue = record["StringValue"].GetValue<string>(),
                BoolValue = record["BoolValue"].GetValue<bool>(),
            };
            return result;
        }

        public SqlCommand GetInsertCommand(string tableName)
        {
            var queryString = $"INSERT INTO {tableName} (BoolValue) VALUES (@b)";
            var command = new SqlCommand(queryString)
            {
                CommandType = CommandType.Text
            };

            command.Parameters.AddWithValue("@b", BoolValue);
            return command;
        }
    }

    public class InvalidTestClass : IInsertable
    {
        public string StringValue { get; set; }
        public int IntValue { get; set; }
        public static InvalidTestClass Create(Dictionary<string, QueryResultField> record)
        {
            return new InvalidTestClass
            {
                IntValue = record["StringValue"].GetValue<int>()
            };
        }
        public SqlCommand GetInsertCommand(string tableName)
        {
            return new SqlCommand();
        }
    }
}
