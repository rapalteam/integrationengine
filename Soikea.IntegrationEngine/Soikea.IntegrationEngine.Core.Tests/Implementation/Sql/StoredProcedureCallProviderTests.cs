﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Soikea.IntegrationEngine.Core.Implementation.Sql;
using Soikea.IntegrationEngine.Core.Interfaces;
using Soikea.IntegrationEngine.Core.Logging;

namespace Soikea.IntegrationEngine.Core.Tests.Implementation.Sql
{
    [TestClass]
    public class StoredProcedureCallProviderTests
    {
        [TestInitialize]
        public void Initialize()
        {
            Log.SetCurrentLogProvider(new NoOpLogProvider(), "Unit-test");
        }

        [TestMethod]
        public void ExecuteReturnsFalseWhenConnectionStringNotFound()
        {
            var mockAbortionDog = new Mock<IJobAbortionWatchDog>();
            var callProvider = new StoredProcedureCallProvider();

            var result = callProvider.ExecuteProcedureCall("NonExistent", "ShouldNotBeCalled", mockAbortionDog.Object);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void ExecuteReturnsTrueWhenCallFinishesAsExpected()
        {
            var mockAbortionDog = new Mock<IJobAbortionWatchDog>();
            var callProvider = new StoredProcedureCallProvider();

            var result = callProvider.ExecuteProcedureCall("TestDb", "ExecuteOneSecond", mockAbortionDog.Object);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void ExecuteCancelsOnJobAbortionAndRethrowsJobAbortedException()
        {
            var mockAbortionDog = new Mock<IJobAbortionWatchDog>();
            var abortionCheckCount = 0;
            mockAbortionDog.Setup(x => x.ThrowIfJobAbortionRequested())
                .Callback(() =>
                {
                    if (++abortionCheckCount == 3)
                    {
                        throw new JobAbortedException("Aborting job execution from unit-test");
                    }
                });

            var callProvider = new StoredProcedureCallProvider();

            try
            {
                callProvider.ExecuteProcedureCall("TestDb", "ExecuteFiveSeconds", mockAbortionDog.Object);
                Assert.Fail("Expecting to throw an exception");
            }
            catch (JobAbortedException e)
            {
                Assert.AreEqual("Aborting job execution from unit-test", e.Message);
            }
        }

        [TestMethod]
        public void ExecuteReturnsFalseWhenUnknowSqlExceptionOccurs()
        {
            var mockAbortionDog = new Mock<IJobAbortionWatchDog>();
            var callProvider = new StoredProcedureCallProvider();

            var result = callProvider.ExecuteProcedureCall("TestDb", "NonExistent", mockAbortionDog.Object);

            Assert.IsFalse(result);
        }
    }
}
