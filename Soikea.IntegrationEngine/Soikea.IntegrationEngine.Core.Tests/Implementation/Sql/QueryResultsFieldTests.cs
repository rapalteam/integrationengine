﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soikea.IntegrationEngine.Core.Extensions;

namespace Soikea.IntegrationEngine.Core.Tests.Implementation.Sql
{
    [TestClass]
    public class QueryResultsFieldTests
    {
        [TestMethod]
        public void QueryResultsFieldWorksCorrectlyWhenRequestingFieldWithSameType()
        {
            var field = new QueryResultField("StringValue", typeof(string));
            var result = field.GetValue<string>();
            Assert.AreEqual("StringValue", result);
        }

        [TestMethod]
        [ExpectedException(typeof(NotSupportedException))]
        public void QueryResultsFieldWorksCorrectlyWhenRequestingFieldWithNotCastableType()
        {
            var field = new QueryResultField(16, typeof(int));
            var result = field.GetValue<long>();
            Assert.AreEqual(16L, result);
        }

        [TestMethod]
        public void QueryResultsFieldWorksCorrectlyWhenCastingIntToString()
        {
            var field = new QueryResultField(16, typeof(int));
            var result = field.GetValue<string>();
            Assert.AreEqual(result, "16");
        }

        [TestMethod]
        public void QueryResultsFieldWorksCorrectlyWhenCastingLongToString()
        {
            var field = new QueryResultField(16L, typeof(long));
            var result = field.GetValue<string>();
            Assert.AreEqual(result, "16");
        }

        [TestMethod]
        public void QueryResultsFieldWorksCorrectlyWhenCastingDoubleToString()
        {
            var field = new QueryResultField(16.2, typeof(double));
            var result = field.GetValue<string>();
            Assert.AreEqual(result, "16.2");
        }
    }
}
