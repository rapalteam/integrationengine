﻿using System;
using System.Runtime.Serialization;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soikea.IntegrationEngine.Core.Implementation;
using Soikea.IntegrationEngine.Core.Implementation.Events;
using Soikea.IntegrationEngine.Core.Interfaces;
using Soikea.IntegrationEngine.Core.Tests.TestAssets;

namespace Soikea.IntegrationEngine.Core.Tests.Implementation
{
    [TestClass]
    public class JobResultBaseTests
    {
        [TestMethod]
        public void GetObjectDataAddsPropertiesToSerializationInfo()
        {
            var tenantId = "tenant";
            var jobId = Guid.NewGuid();
            var jobResult = new TestJobResult(jobId)
            {
                Status = JobResultStatus.Ok,
                ExecutionDetails = new EndOfJobPayload(tenantId, jobId, "TestJob", JobResultStatus.Ok, 1, 1)
            };


            var serializationInfo = new SerializationInfo(typeof(SubscriptionCategory), new FormatterConverter());

            jobResult.GetObjectData(serializationInfo, new StreamingContext());

            Assert.AreEqual("Ok", serializationInfo.GetValue("Status", typeof(string)));
            Assert.AreEqual(jobResult.ExecutionDetails, serializationInfo.GetValue("ExecutionDetails", typeof(EndOfJobPayload)));
        }

        [TestMethod]
        public void ReceiveDoesNotDoAnythingOnEvent()
        {
            var jobId = Guid.NewGuid();
            var jobResult = new TestJobResult(jobId);

            jobResult.Receive(new DummyEvent());

            Assert.IsNull(jobResult.ExecutionDetails);
        }

        [TestMethod]
        public void ReceivePopulatesExecutionDetails()
        {
            var tenantId = "tenant";
            var jobId = Guid.NewGuid();
            var jobResult = new TestJobResult(jobId);

            jobResult.Receive(new EndOfJobEvent(tenantId, jobId, "TestJob", JobResultStatus.Ok, 1, 1));

            Assert.IsNotNull(jobResult.ExecutionDetails);
            Assert.AreEqual(tenantId, jobResult.ExecutionDetails.TenantId);
            Assert.AreEqual(jobId, jobResult.ExecutionDetails.JobId);
            Assert.AreEqual("TestJob", jobResult.ExecutionDetails.JobName);
            Assert.AreEqual(JobResultStatus.Ok, jobResult.ExecutionDetails.Status);
            Assert.AreEqual(1, jobResult.ExecutionDetails.InputCount);
            Assert.AreEqual(1, jobResult.ExecutionDetails.OutputCount);
        }
    }
}
