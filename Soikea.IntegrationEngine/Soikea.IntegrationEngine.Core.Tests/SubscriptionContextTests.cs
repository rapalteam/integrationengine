﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Soikea.IntegrationEngine.Core.Implementation;
using Soikea.IntegrationEngine.Core.Interfaces;
using Soikea.IntegrationEngine.Core.Logging;
using Soikea.IntegrationEngine.Core.Tests.TestAssets;
using Soikea.IntegrationEngine.Runtime;

namespace Soikea.IntegrationEngine.Core.Tests
{
    [DeploymentItem("Moq.dll")]
    [TestClass()]
    public class SubscriptionContextTests
    {

        [ClassInitialize]
        public static void Init(TestContext context)
        {
            var mockLogProvider = new Mock<ILogProvider>();
            var mockLogger = new Mock<ILog>();

            mockLogger.Setup(x => x.Log(LogLevel.Trace, It.IsAny<Func<string>>(), It.IsAny<Exception>()));
            mockLogger.Setup(x => x.Log(LogLevel.Info, It.IsAny<Func<string>>(), It.IsAny<Exception>()));
            mockLogProvider.Setup(x => x.GetLogger(It.IsAny<string>(), It.IsAny<string>()))
              .Returns(mockLogger.Object);
            Log.SetCurrentLogProvider(mockLogProvider.Object, "Test");
        }

        [TestMethod()]
        public void SubscribeToInputResultsInDeliveringEvents()
        {
            var mockSubscriber = new Mock<IMessageAdapter>();
            mockSubscriber.Setup(x => x.Name).Returns("Test");
            mockSubscriber.Setup(x => x.TenantId).Returns("Tenant");

            var eventReceived = false;
            mockSubscriber.Setup(x => x.Receive(It.IsAny<IEvent>()))
                .Callback(() => { eventReceived = true; });

            var subscriptionToCategory = new SubscriptionCategory("Test");

            var subscriptionContext = new SubscriptionContext();

            subscriptionContext.SubscribeToInput(mockSubscriber.Object, subscriptionToCategory);

            var dummyEvent = new DummyEvent()
            {
                TenantId = "Tenant",
                Category = subscriptionToCategory
            };

            subscriptionContext.PublishInputEvent(dummyEvent);
            
            Assert.IsTrue(eventReceived);
        }

        [TestMethod()]
        public void SubscribeToInputWithCategoryNameResultsInDeliveringEvents()
        {
            var mockSubscriber = new Mock<IMessageAdapter>();
            mockSubscriber.Setup(x => x.Name).Returns("Test");
            mockSubscriber.Setup(x => x.TenantId).Returns("Tenant");

            var eventReceived = false;
            mockSubscriber.Setup(x => x.Receive(It.IsAny<IEvent>()))
                .Callback(() => { eventReceived = true; });

            var subscriptionToCategory = new SubscriptionCategory("Test");

            var subscriptionContext = new SubscriptionContext();

            subscriptionContext.SubscribeToInput(mockSubscriber.Object, "Test");

            var dummyEvent = new DummyEvent()
            {
                TenantId = "Tenant",
                Category = subscriptionToCategory
            };

            subscriptionContext.PublishInputEvent(dummyEvent);
            
            Assert.IsTrue(eventReceived);
        }

        [TestMethod()]
        public void PublishInputEventWorksAsExpected()
        {
            var mockSubscriber = new Mock<IMessageAdapter>();
            mockSubscriber.Setup(x => x.Name).Returns("Test");
            mockSubscriber.Setup(x => x.TenantId).Returns("Tenant");
        

            var eventReceived = false;
            mockSubscriber.Setup(x => x.Receive(It.IsAny<IEvent>()))
                .Callback(() => { eventReceived = true; });

            var subscriptionToCategory = new SubscriptionCategory("Test");

            var subscriptionContext = new SubscriptionContext();

            subscriptionContext.SubscribeToInput(mockSubscriber.Object, subscriptionToCategory);

            var dummyEvent = new DummyEvent()
            {
                TenantId = "Tenant",
                Category = subscriptionToCategory
            };

            subscriptionContext.PublishInputEvent(dummyEvent);

            Assert.IsTrue(eventReceived);
        }

        [TestMethod()]
        public void PublishInputWorksAsExpectedWithParameterizedOverload()
        {
            var mockSubscriber = new Mock<IMessageAdapter>();
            mockSubscriber.Setup(x => x.Name).Returns("Test");
            mockSubscriber.Setup(x => x.TenantId).Returns("Tenant");

            var eventReceived = false;
            mockSubscriber.Setup(x => x.Receive(It.IsAny<IEvent>()))
                .Callback(() => { eventReceived = true; });

            var subscriptionToCategory = new SubscriptionCategory("Test");

            var subscriptionContext = new SubscriptionContext();

            subscriptionContext.SubscribeToInput(mockSubscriber.Object, subscriptionToCategory);

            subscriptionContext.PublishInputEvent("Tenant", Guid.NewGuid(), new ExamplePayload(), subscriptionToCategory);

            Assert.IsTrue(eventReceived);
        }

        [TestMethod()]
        public void SubscribeToOutputDeliversEvents()
        {
            var mockSubscriber = new Mock<IMessageAdapter>();
            mockSubscriber.Setup(x => x.Name).Returns("Test");
            mockSubscriber.Setup(x => x.TenantId).Returns("Tenant");

            var eventReceived = false;
            mockSubscriber.Setup(x => x.Receive(It.IsAny<IEvent>()))
                .Callback(() => { eventReceived = true; });

            var subscriptionToCategory = new SubscriptionCategory("Test");

            var subscriptionContext = new SubscriptionContext();

            subscriptionContext.SubscribeToOutput(mockSubscriber.Object, subscriptionToCategory);

            var dummyEvent = new DummyEvent()
            {
                TenantId = "Tenant",
                Category = subscriptionToCategory
            };

            subscriptionContext.PublishOutputEvent(dummyEvent);

            Assert.IsTrue(eventReceived);
        }

        [TestMethod()]
        public void SubscribeToOutputWithCategoryNameResultsInDeliveringEvents()
        {
            var mockSubscriber = new Mock<IMessageAdapter>();
            mockSubscriber.Setup(x => x.Name).Returns("Test");
            mockSubscriber.Setup(x => x.TenantId).Returns("Tenant");

            var eventReceived = false;
            mockSubscriber.Setup(x => x.Receive(It.IsAny<IEvent>()))
                .Callback(() => { eventReceived = true; });

            var subscriptionToCategory = new SubscriptionCategory("Test");

            var subscriptionContext = new SubscriptionContext();

            subscriptionContext.SubscribeToOutput(mockSubscriber.Object, "Test");

            var dummyEvent = new DummyEvent()
            {
                TenantId = "Tenant",
                Category = subscriptionToCategory
            };

            subscriptionContext.PublishOutputEvent(dummyEvent);

            Assert.IsTrue(eventReceived);
        }
        
        [TestMethod()]
        public void PublishOutputWorksAsExpected()
        {
            var mockSubscriber = new Mock<IMessageAdapter>();
            mockSubscriber.Setup(x => x.Name).Returns("Test");
            mockSubscriber.Setup(x => x.TenantId).Returns("Tenant");

            var eventReceived = false;
            mockSubscriber.Setup(x => x.Receive(It.IsAny<IEvent>()))
                .Callback(() => { eventReceived = true; });

            var subscriptionToCategory = new SubscriptionCategory("Test");

            var subscriptionContext = new SubscriptionContext();

            subscriptionContext.SubscribeToOutput(mockSubscriber.Object, subscriptionToCategory);

            var dummyEvent = new DummyEvent()
            {
                TenantId = "Tenant",
                Category = subscriptionToCategory
            };

            subscriptionContext.PublishOutputEvent(dummyEvent);

            Assert.IsTrue(eventReceived);
        }

        [TestMethod()]
        public void PublishOutputWorksAsExpectedWithParameterizedOverload()
        {
            var mockSubscriber = new Mock<IMessageAdapter>();
            mockSubscriber.Setup(x => x.Name).Returns("Test");
            mockSubscriber.Setup(x => x.TenantId).Returns("Tenant");

            var eventReceived = false;
            mockSubscriber.Setup(x => x.Receive(It.IsAny<IEvent>()))
                .Callback(() => { eventReceived = true; });

            var subscriptionToCategory = new SubscriptionCategory("Test");

            var subscriptionContext = new SubscriptionContext();

            subscriptionContext.SubscribeToOutput(mockSubscriber.Object, subscriptionToCategory);

            subscriptionContext.PublishOutputEvent("Tenant", Guid.NewGuid(), new ExamplePayload(), subscriptionToCategory);

            Assert.IsTrue(eventReceived);
        }

        [TestMethod()]
        public void VerifyEndOfJobReturnsTrueWhenItemCountsMatch()
        {
            var subscriptionContext = new SubscriptionContext();

            var inputCounter = new Mock<JobResultCounter>();
            inputCounter.Setup(x => x.ItemsPassedThrough).Returns(2);

            var outputCounter = new Mock<JobResultCounter>();
            outputCounter.Setup(x => x.ItemsPassedThrough).Returns(2);

            Assert.IsTrue(subscriptionContext.VerifyEndOfJob(inputCounter.Object, outputCounter.Object));
        }

        [TestMethod()]
        public void VerifyEndOfJobReturnsFalseWhenItemCountsDoNotMatch()
        {
            var subscriptionContext = new SubscriptionContext();

            var inputCounter = new Mock<JobResultCounter>();
            inputCounter.Setup(x => x.ItemsPassedThrough).Returns(1);
            
            var outputCounter = new Mock<JobResultCounter>();
            outputCounter.Setup(x => x.ItemsPassedThrough).Returns(2);
            
            
            Assert.IsFalse(subscriptionContext.VerifyEndOfJob(inputCounter.Object, outputCounter.Object));
        }
    }
}
