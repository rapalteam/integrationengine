﻿using System.Runtime.Serialization;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Soikea.IntegrationEngine.Core.Interfaces.Tests
{
    [TestClass()]
    public class SubscriptionCategoryTests
    {
        [TestMethod()]
        public void SubscriptionCategoryConstructorSetsNameProperty()
        {
            var subCategory = new SubscriptionCategory("Test");
            Assert.AreEqual("Test", subCategory.Name);
        }

        [TestMethod()]
        public void GetHashCodeEqualsWithNameHashCode()
        {
            var subCategory = new SubscriptionCategory("Test");

            Assert.AreEqual(subCategory.Name.GetHashCode(), subCategory.GetHashCode());
        }

        [TestMethod()]
        public void GetObjectDataAddsNamePropertySerializationInfo()
        {
            var subCategory = new SubscriptionCategory("Test");

            var serializationInfo = new SerializationInfo(typeof (SubscriptionCategory), new FormatterConverter());

            subCategory.GetObjectData(serializationInfo, new StreamingContext());

            Assert.AreEqual("Test", serializationInfo.GetValue("Name", typeof(string)));
        }

        [TestMethod()]
        public void EqualsWithAnotherString()
        {
            var subCategory = new SubscriptionCategory("Test");
            
            Assert.IsTrue(subCategory.Equals("Test"));
        }

        [TestMethod()]
        public void DoesNotEqualWithAnotherString()
        {
            var subCategory = new SubscriptionCategory("Test");
            
            Assert.IsFalse(subCategory.Equals("hest"));
        }

        [TestMethod()]
        public void EqualsWithAnotherObject()
        {
            var subCategory = new SubscriptionCategory("Test");

            var anotherTestCategoryObject = new SubscriptionCategory("Test");

            Assert.IsTrue(subCategory.Equals(anotherTestCategoryObject as object));
        }

        [TestMethod()]
        public void DoesNotEqualWithAnotherObject()
        {
            var subCategory = new SubscriptionCategory("Test");

            var anotherTestCategoryObject = new SubscriptionCategory("hest");

            Assert.IsFalse(subCategory.Equals(anotherTestCategoryObject as object));
        }


        [TestMethod()]
        public void DoesNotEqualWithObject()
        {
            var subCategory = new SubscriptionCategory("Test");

            var anotherTestCategoryObject = new object();

            Assert.IsFalse(subCategory.Equals(anotherTestCategoryObject));
        }

        [TestMethod()]
        public void ToStringReturnsNameProperty()
        {
            var subCategory = new SubscriptionCategory("Test");
            Assert.AreEqual("Test", subCategory.ToString());
        }

        [TestMethod()]
        public void EqualsWithAnotherSubscriptionCategory()
        {
            var subCategory = new SubscriptionCategory("Test");

            var anotherTestCategoryObject = new SubscriptionCategory("Test");

            Assert.IsTrue(subCategory.Equals(anotherTestCategoryObject));
        }

        [TestMethod()]
        public void DoesNotEqualWithAnotherSubscriptionCategory()
        {
            var subCategory = new SubscriptionCategory("Test");

            var anotherTestCategoryObject = new SubscriptionCategory("hest");

            Assert.IsFalse(subCategory.Equals(anotherTestCategoryObject));
        }
    }
}
