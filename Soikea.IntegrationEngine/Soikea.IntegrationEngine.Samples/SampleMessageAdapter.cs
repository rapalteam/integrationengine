﻿using System;
using System.Diagnostics.Contracts;
using Soikea.IntegrationEngine.Core.Interfaces;
using Soikea.IntegrationEngine.Core.Logging;
using Soikea.IntegrationEngine.SampleJobs.Model;

namespace Soikea.IntegrationEngine.SampleJobs
{
    public class SampleMessageAdapter : IMessageAdapter
    {
        public string TenantId { get; set; }
        public IMessageAdapterSettings MessageAdapterSettings { get; set; }

        public ITenantSettings Configuration { get; set; }
        
        public string Name => "SampleMessageAdapter";
        
        public void Receive(IEvent @event)
        {
            var sampleEvent = @event as SampleEvent;

            Contract.Requires(sampleEvent != null);
            Contract.Requires(Configuration.ContainsKey("SampleContextConnectionStringKey"));
            
            Log.Info(string.Format("Received an event at SampleMessageAdapter of tenant {3}: {0} @ {1} for tenant {2}", 
                @event.Category, @event.JobId, @event.TenantId, TenantId));

            if (MessageAdapterSettings.ContainsKey("Setting"))
            {
                Log.Info($"{Name} for tenant {TenantId} contains a Setting value: {MessageAdapterSettings.GetValue("Setting")}");
            }

            var targetConnection = Configuration.GetValue("SampleContextConnectionStringKey");
            using (var context = new SampleContext(targetConnection))
            {
                context.Samples.Add(new Sample()
                {
                    Number = sampleEvent.Number,
                    String = sampleEvent.String,
                    LoggedAt = DateTime.Now
                });

                context.SaveChanges();

                Log.Info($"Persisted sample-event for tenant {TenantId} to target-context {targetConnection}");
            }
        }

    }
}
