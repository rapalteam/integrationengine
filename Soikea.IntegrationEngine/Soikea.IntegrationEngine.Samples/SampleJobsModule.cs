﻿using Autofac;
using Soikea.IntegrationEngine.Administration.Repository;
using Soikea.IntegrationEngine.Core.Interfaces;
using Soikea.IntegrationEngine.HangfireConfiguration.Configuration.Tenants;


namespace Soikea.IntegrationEngine.SampleJobs
{
    public class SampleJobsModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<SamplePublishJob>()
                .Named<IIntegrationJob>("SamplePublishJob")
                .OnActivated(m =>
                {
                    m.Instance.Configuration = m.Context.Resolve<ITenantSettings>();
                    var factory = m.Context.Resolve<JobParameterRepository.Factory>();
                    var tenant = m.Context.Resolve<Tenant>();
                    m.Instance.Repository = factory.Invoke(tenant.TenantId, m.Instance.Name);
                });

            builder.RegisterType<SampleFailingJob>()
                .Named<IIntegrationJob>("SampleFailingJob")
                .OnActivated(m =>
                {
                    m.Instance.Configuration = m.Context.Resolve<ITenantSettings>();
                });

            builder.RegisterType<SampleMessageAdapter>()
                .Named<IMessageAdapter>("SampleMessageAdapter")
                .OnActivated(m =>
                {
                    m.Instance.Configuration = m.Context.Resolve<ITenantSettings>();
                });
        }
    }
}
