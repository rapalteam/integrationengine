﻿using System.Data.Entity;

namespace Soikea.IntegrationEngine.SampleJobs.Model
{
    public class SampleContext : DbContext
    {
        public IDbSet<Sample> Samples { get; set; }

        public SampleContext() : base("SampleContext")
        {
            
        }

        public SampleContext(string connection) : base(connection)
        {
            
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("sample");

            base.OnModelCreating(modelBuilder);
        }
    }
}
