﻿using System;

namespace Soikea.IntegrationEngine.SampleJobs.Model
{
    public class Sample
    {
        public long Id { get; set; }
        public int Number { get; set; }
        public string String { get; set; }
        public DateTime LoggedAt { get; set; }
    }
}
