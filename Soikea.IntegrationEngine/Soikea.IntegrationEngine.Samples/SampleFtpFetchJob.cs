﻿using System;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Threading.Tasks;
using Soikea.IntegrationEngine.Core;
using Soikea.IntegrationEngine.Core.Implementation.Ftp;
using Soikea.IntegrationEngine.Core.Interfaces;

namespace Soikea.IntegrationEngine.SampleJobs
{
    /// <summary>
    /// Fetch files from an FTP-server that follows this file-structure:
    /// 
    /// {TargetUrl}/{BatchNo}.complete
    /// {TargetUrl}/{BatchNo}/{filename1}
    /// {TargetUrl}/{BatchNo}/{filename2}
    /// {TargetUrl}/{BatchNo}/{filename3}
    /// 
    /// Where TargetUrl is configured as the base-directory to search for the batches complete-files.
    /// 
    /// Publishes DownloadedFtpFileEvent input-events for each downloaded file.
    /// </summary>
    public class SampleFtpFetchJob : FtpTransferJobBase, IIntegrationJob
    {
        public SampleFtpFetchJob(IFtpConnectionProvider ftpConnectionProvider) : base(ftpConnectionProvider)
        {
        }

        public override string Name => "SampleFtpFetchJob";

        public string TargetUrl { get; set; }

        public override IJobResult CreateJobResult(Guid jobId)
        { 
            return new SampleFtpFetchJobResult(jobId);
        }

        public override JobResultStatus Execute(IJobAbortionWatchDog jobAbortionWatchDog, Constants.PublishEvent publishInput, Constants.PublishEvent publishOutput)
        {
            Contract.Requires(!string.IsNullOrEmpty(TargetUrl), "TargetUrl empty");

            var targetFiles = ListContents(TargetUrl);

            var completeBatches = targetFiles.Where(x => x.EndsWith(".complete")).ToList();

            if (!completeBatches.Any())
            {
                return JobResultStatus.Aborted;
            }

            foreach (var completeBatch in completeBatches)
            {
                var batchNo = completeBatch.Substring(0, completeBatch.IndexOf('.') + 1);

                foreach (var targetFile in targetFiles)
                {
                    string file = targetFile;
                    Task.Run(() =>
                    {
                        var payload = FtpConnectionProvider.Download($"{TargetUrl}/{file}");
                        if (!string.IsNullOrEmpty(payload))
                        {
                            publishInput(new DownloadedFtpFileEvent(TenantId, JobId, SampleSubscriptionCategories.DownloadedFtpFile, payload, batchNo));
                        }
                    });                
                }                
            }



            return JobResultStatus.Ok;
        }
    }
}
