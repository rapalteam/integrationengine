﻿using System;
using Soikea.IntegrationEngine.Core;
using Soikea.IntegrationEngine.Core.Implementation;
using Soikea.IntegrationEngine.Core.Interfaces;
using Soikea.IntegrationEngine.Core.Logging;

namespace Soikea.IntegrationEngine.SampleJobs
{
    public class SampleFailingJob : SynchronousIntegrationJob
    {

        public override string Name => "SampleFailingJob";
        public ITenantSettings Configuration { get; set; }



        public override IJobResult CreateJobResult(Guid jobId)
        {
            return new SamplePublishJobResult(jobId);
        }

        public override JobResultStatus Execute(IJobAbortionWatchDog jobAbortionWatchDog, Constants.PublishEvent publishInput, Constants.PublishEvent publishOutput)
        {
            Log.Info("Starting a failing job");

            try
            {
                for (int i = 0; i < 20; i++)
                {
                    Log.Warn("Logging a warning");
                    jobAbortionWatchDog.ThrowIfJobAbortionRequested();
                }
            }
            catch (JobAbortedException)
            {
                return JobResultStatus.Aborted;
            }

            

            Log.Error(new NotImplementedException("Just a dummy exception"), "Logging an error");
            Log.Error(new NotImplementedException("Just a dummy exception"), "Logging an error");

            return JobResultStatus.Error;
        }
    }
}
