﻿using System;
using System.Runtime.Serialization;
using Soikea.IntegrationEngine.Core.Implementation.Events;
using Soikea.IntegrationEngine.Core.Interfaces;

namespace Soikea.IntegrationEngine.SampleJobs
{
    [Serializable]
    public class SampleEvent : EventBase, IStorableEvent
    {
        public int Number { get; set; }
        public string String { get; set; }

        public SampleEvent(string tenantId, Guid jobId, SubscriptionCategory category) : base(tenantId, jobId, category)
        {
        }

        public SampleEvent(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            Number = (int) info.GetValue("Number", typeof (int));
            String = info.GetString("String");
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValue("Number", Number);
            info.AddValue("String", String);
        }
    }
}
