﻿using System;
using Soikea.IntegrationEngine.Core.Implementation;
using Soikea.IntegrationEngine.Core.Implementation.Events;
using Soikea.IntegrationEngine.Core.Interfaces;

namespace Soikea.IntegrationEngine.SampleJobs
{
    public class DownloadedFtpFileEvent : EventBase
    {
        public string BatchNo { get; set; }
        public string FilePayload { get; set; }

        public DownloadedFtpFileEvent(string tenantId, Guid jobId, SubscriptionCategory category, string filePayload, string batchNo) 
            : base(tenantId, jobId, category)
        {
            FilePayload = filePayload;
            BatchNo = batchNo;
        }
    }
}
