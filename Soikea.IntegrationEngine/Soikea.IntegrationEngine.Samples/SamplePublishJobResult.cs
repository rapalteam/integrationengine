﻿using System;
using Soikea.IntegrationEngine.Core.Implementation;

namespace Soikea.IntegrationEngine.SampleJobs
{
    public class SamplePublishJobResult : JobResultBase
    {
        public SamplePublishJobResult(Guid jobId) : base(jobId)
        {

        }

        public override string Name
        {
            get { return "SamplePublishJobResult"; }
        }
    }
}
