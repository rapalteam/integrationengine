﻿using System;
using Soikea.IntegrationEngine.Core.Implementation;

namespace Soikea.IntegrationEngine.SampleJobs
{
    public class SampleFtpFetchJobResult : JobResultBase
    {
        public SampleFtpFetchJobResult(Guid jobId) : base(jobId)
        {
        }

        public override string Name
        {
            get { return "SampleFtpFetchJobResult"; }
        }
    }
}
