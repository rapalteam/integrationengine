﻿using Soikea.IntegrationEngine.Core.Interfaces;

namespace Soikea.IntegrationEngine.SampleJobs
{
    public static class SampleConstants
    {
        public static readonly SampleSubscriptionCategories SampleSubscriptionCategories = new SampleSubscriptionCategories();
    }

    public class SampleSubscriptionCategories
    {
        public static readonly SubscriptionCategory DownloadedFtpFile = new SubscriptionCategory("Samples.DownloadedFtpFile");
        public static readonly SubscriptionCategory SampleEvents = new SubscriptionCategory("Samples.SampleEvent");
    }
}
