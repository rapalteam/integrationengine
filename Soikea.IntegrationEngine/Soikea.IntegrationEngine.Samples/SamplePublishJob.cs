﻿using System;
using System.Diagnostics.Contracts;
using System.Security.Cryptography.X509Certificates;
using Soikea.IntegrationEngine.Administration.Repository;
using Soikea.IntegrationEngine.Core;
using Soikea.IntegrationEngine.Core.Implementation;
using Soikea.IntegrationEngine.Core.Interfaces;
using Soikea.IntegrationEngine.Core.Logging;


namespace Soikea.IntegrationEngine.SampleJobs
{
    public class SamplePublishJob : SynchronousIntegrationJob
    {
        public int OutputPublishCount { get; set; }
        public int InputPublishCount { get; set; }

        public override string Name => "SamplePublishJob";
        public ITenantSettings Configuration { get; set; }
        public IJobParameterRepository Repository { get; set; }
        
        public SamplePublishJob()
        {
            OutputPublishCount = 5;
            InputPublishCount = 5;
        }

        public override IJobResult CreateJobResult(Guid jobId)
        {
            return new SamplePublishJobResult(jobId);
        }

        public override JobResultStatus Execute(IJobAbortionWatchDog jobAbortionWatchDog, Constants.PublishEvent publishInput, Constants.PublishEvent publishOutput)
        {
            Contract.Requires(JobSettings != null);

            Log.Info($"PublishOutputCountfromRepo {Repository.GetParameter<int>("outputCount")}");
            try
            {
                Log.Info($"Job settings value of Dummy: {JobSettings.GetValue("Dummy")}");   
                Log.Info("Publish to Input job starting");
                for (int i = 0; i < InputPublishCount; i++)
                {
                    publishInput(new SampleEvent(TenantId, JobId, SampleSubscriptionCategories.SampleEvents) { Number = i + 1, String = "In" });
                    jobAbortionWatchDog.ThrowIfJobAbortionRequested();
                }
                Log.Info("Publish to Input job ended");

                Log.Info("Publish to Output job starting");
                for (int i = 0; i < OutputPublishCount; i++)
                {
                    publishOutput(new SampleEvent(TenantId, JobId, SampleSubscriptionCategories.SampleEvents) { Number = i + 1, String = "Out" });
                    jobAbortionWatchDog.ThrowIfJobAbortionRequested();
                }
                Log.Info("Publish to Output job ended");                                
            }
            catch (JobAbortedException)
            {
                return JobResultStatus.Aborted;
            }

            Repository.AddOrUpdateParameter<int>("outputCount", 5);
          
            return JobResultStatus.Ok;
        }
    }
}
