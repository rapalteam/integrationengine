﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soikea.IntegrationEngine.Management.UI.Migrations;
using Soikea.IntegrationEngine.Management.UI.Models.Log;

namespace Soikea.IntegrationEngine.Management.UI.Tests
{
    [TestClass]
    public class LogRepositoryTests
    {
        private static string _connectionString = "LogContext";
        private const string _defaultJobId = "default-job-id";
        private const int _defaultLogId = 1;
        private const int _pageSize = 10;
        private static LogRepository _logRepository ;


#region init/cleanup
        [ClassInitialize]
        public static void Init(TestContext context)
        {            
            _logRepository = new LogRepository();
        }

        [ClassCleanup]
        public static void Cleanup()
        {
            
        }

#endregion

        [TestMethod]
        public void GetByCategoryReturnsExpectedResult()
        {
            var from = TestLogDataInitializer<LogContext>.TestReferenceTimeStamp;
            int pageCount;
            var result = _logRepository.GetByCategoryId(1, from, from, 0, _pageSize, out pageCount);

            Assert.IsNotNull(result);
            var enumerable = result as IList<Log> ?? result.ToList();
            Assert.AreEqual(1, enumerable.Count);
            var match = enumerable.First();
            Assert.AreEqual("test log entry1", match.Message);
            Assert.IsNotNull(match.CategoryLogs.First().Category);
        }

        [TestMethod]
        public void GetByCategoryReturnsNothingWithTimestampBeforeReferenceTime()
        {
            var from = TestLogDataInitializer<LogContext>.TestReferenceTimeStamp.AddMinutes(-1);
            var to = TestLogDataInitializer<LogContext>.TestReferenceTimeStamp.AddSeconds(-1);
            int pageCount;

            var result = _logRepository.GetByCategoryId(1, from, to, 0, _pageSize, out pageCount);

            Assert.AreEqual(0, result.Count());
        }

        [TestMethod]
        public void GetByCategoryReturnsNothingWithTimestampAfterReferenceTime()
        {
            var from = TestLogDataInitializer<LogContext>.TestReferenceTimeStamp.AddSeconds(1);
            var to = TestLogDataInitializer<LogContext>.TestReferenceTimeStamp.AddMinutes(1);
            int pageCount;

            var result = _logRepository.GetByCategoryId(1, from, to, 0, _pageSize, out pageCount);

            Assert.AreEqual(0, result.Count());
        }

        [TestMethod]
        public void GetAllCategories()
        {

            var result = _logRepository.GetAllCategories();
            Assert.IsNotNull(result);

            var categories = result as IList<Category> ?? result.ToList();
            Assert.AreEqual(categories.Count(), 3);

            var s = string.Join(", ", categories.Select(x=> x.CategoryName));
            Assert.AreEqual("category 1, category 2, category 3", s);
        }

        [TestMethod]
        public void GetByJobIdReturnsCorrectAmountOfResults()
        {
            int pageCount;
            var result = _logRepository.GetByJobId(_defaultJobId, 0, _pageSize, out pageCount);
            
            Assert.AreEqual(result.Count(), 2);
            Assert.AreEqual(pageCount, 1);
        }

        [TestMethod]
        public void GetByJobIdReturnsCorrectAmountOfPages()
        {
            int pageCount;
            var result = _logRepository.GetByJobId(_defaultJobId, 0, 1, out pageCount);

            Assert.AreEqual(result.Count(), 1);
            Assert.AreEqual(pageCount, 2);            
        }

        [TestMethod]
        public void GetByLogId()
        {
            var result = _logRepository.GetByLogId(_defaultLogId);
            
            Assert.AreEqual(result.Count(), 1);
        }

        [TestMethod]
        public void GetBySeverity()
        {
            var from = TestLogDataInitializer<LogContext>.TestReferenceTimeStamp.AddMinutes(-1);
            var to = TestLogDataInitializer<LogContext>.TestReferenceTimeStamp.AddMinutes(1);
            int pageCount;
            var result = _logRepository.GetBySeverity(Constants.LogSeverity.Error, from, to, 0, _pageSize, out pageCount);
            
            Assert.AreEqual(result.Count(), 1);
        }
    }
}
