﻿CREATE PROCEDURE [dbo].[WriteLog]
(
	@EventID INT, 
	@Priority INT, 
	@Severity NVARCHAR(32), 
	@Title NVARCHAR(256), 
	@Timestamp DATETIME,
	@MachineName NVARCHAR(32), 
	@Message NVARCHAR(MAX),
	@JobId NVARCHAR(128),	
	@Module NVARCHAR(256),
	@LogId BIGINT OUTPUT
	)
AS 
BEGIN
	INSERT INTO [Log] (
		EventID,
		[Priority],
		Severity,
		Title,
		[Timestamp],
		MachineName,
		[Message],
		JobId,
		Module
	)
	VALUES (
		@EventID, 
		@Priority, 
		@Severity, 
		@Title, 
		@Timestamp,
		@MachineName,
		@Message,
		@JobId,
		@Module)

	SET @LogID = @@IDENTITY
	
	RETURN 0

END