﻿CREATE PROCEDURE InsertCategoryLog
	@CategoryID INT,
	@LogID BIGINT,
	@CatLogID BIGINT OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT @CatLogID FROM CategoryLog WHERE CategoryID=@CategoryID and LogID = @LogID
	IF @CatLogID IS NULL
	BEGIN
		INSERT INTO CategoryLog (CategoryID, LogID) VALUES(@CategoryID, @LogID)
		SET @CatLogID = @@IDENTITY
	END
	RETURN 0
END