﻿CREATE PROCEDURE ClearLogs
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @CurrentDate DATETIME = GETDATE();
	DECLARE @ClearOlderThan DATETIME = DATEADD(month, -1, @CurrentDate);
	DECLARE @MaxLogId BIGINT;

	SELECT @MaxLogId = MAX(LogID) FROM [Log] 
		WHERE [Timestamp] < @ClearOlderThan;

	DELETE CategoryLog 
		FROM CategoryLog cl 
		INNER JOIN [Log] l ON cl.LogID = l.LogID
		WHERE l.LogId <= @MaxLogId;

	DELETE FROM [Log] 
		WHERE LogId <= @MaxLogId;

	DELETE FROM [Category] 
		FROM [Category] c
		LEFT OUTER JOIN [CategoryLog] cl ON c.CategoryID = cl.CategoryID
		WHERE cl.CategoryID IS NULL;

	RETURN 0;
END