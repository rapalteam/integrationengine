﻿
CREATE PROCEDURE [dbo].[AddCategory]
	@CategoryName NVARCHAR(64),
	@LogID BIGINT
AS
BEGIN
	SET NOCOUNT ON;
    DECLARE @CatID INT
	SELECT @CatID = CategoryID FROM Category WHERE CategoryName = @CategoryName
	IF @CatID IS NULL
	BEGIN
		INSERT INTO Category (CategoryName) VALUES(@CategoryName)
		SET @CatID = @@IDENTITY
	END

	DECLARE @CatLogId BIGINT

	EXEC InsertCategoryLog @CatID, @LogID, @CatLogId

	IF @CatLogId <> NULL AND @CatLogId > 0
		RETURN 0
	ELSE
		RETURN 1 
END