﻿CREATE TABLE [dbo].[Log](
	[LogID] BIGINT IDENTITY(1,1) NOT NULL,
	[EventID] [int] NULL,
	[Priority] [int] NOT NULL,
	[Severity] [nvarchar](32) NOT NULL,
	[Title] [nvarchar](256) NOT NULL,
	[Timestamp] [datetime] NOT NULL,
	[MachineName] [nvarchar](32) NOT NULL,
	[Message] [nvarchar](MAX) NULL,
	[JobId] [nvarchar](128) NULL,
	[Module] [nvarchar](256) NULL
 CONSTRAINT [PK_Log] PRIMARY KEY CLUSTERED 
(
	[LogID] ASC
) ON [PRIMARY] 
   
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE INDEX [IDX_Log_All_By_JobId] ON [dbo].[Log] ([JobId])
	INCLUDE ([LogID],[EventID],[Priority],[Severity],[Title],[Timestamp],[MachineName],[Message],[Module])
GO

CREATE NONCLUSTERED INDEX [IDX_Log_SeverityTimeStamp] ON [dbo].[Log] ([Severity],[Timestamp])