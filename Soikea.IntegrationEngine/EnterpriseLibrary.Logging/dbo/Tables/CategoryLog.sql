﻿CREATE TABLE [dbo].[CategoryLog](
	[CategoryLogID] BIGINT IDENTITY(1,1) NOT NULL,
	[CategoryID] [int] NOT NULL,
	[LogID] BIGINT NOT NULL,
 CONSTRAINT [PK_CategoryLog] PRIMARY KEY CLUSTERED 
(
	[CategoryLogID] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CategoryLog]  WITH CHECK ADD  CONSTRAINT [FK_CategoryLog_Category] FOREIGN KEY(	[CategoryID])
REFERENCES [dbo].[Category] (	[CategoryID])
GO
ALTER TABLE [dbo].[CategoryLog]  WITH CHECK ADD  CONSTRAINT [FK_CategoryLog_Log] FOREIGN KEY(	[LogID])
REFERENCES [dbo].[Log] (	[LogID])
GO

CREATE INDEX [IX_CategoryLog] ON [dbo].[CategoryLog] ([LogId] ASC, [CategoryID] ASC)
GO

CREATE NONCLUSTERED INDEX [IDX_CategoryLog_LogId] ON [dbo].[CategoryLog] ([CategoryID])
INCLUDE ([CategoryLogID],[LogID])
GO