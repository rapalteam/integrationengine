﻿/*
Deployment script for Logging

This code was generated by a tool.
Changes to this file may cause incorrect behavior and will be lost if
the code is regenerated.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "INTG_LOG"

GO
:on error exit
GO
/*
Detect SQLCMD mode and disable script execution if SQLCMD mode is not supported.
To re-enable the script after enabling SQLCMD mode, execute the following:
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'SQLCMD mode must be enabled to successfully execute this script.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO
PRINT N'Creating [dbo].[Category]...';


GO
CREATE TABLE [dbo].[Category] (
    [CategoryID]   INT           IDENTITY (1, 1) NOT NULL,
    [CategoryName] NVARCHAR (64) NOT NULL,
    CONSTRAINT [PK_Categories] PRIMARY KEY CLUSTERED ([CategoryID] ASC) ON [PRIMARY]
) ON [PRIMARY];


GO
PRINT N'Creating [dbo].[CategoryLog]...';


GO
CREATE TABLE [dbo].[CategoryLog] (
    [CategoryLogID] BIGINT IDENTITY (1, 1) NOT NULL,
    [CategoryID]    INT    NOT NULL,
    [LogID]         BIGINT NOT NULL,
    CONSTRAINT [PK_CategoryLog] PRIMARY KEY CLUSTERED ([CategoryLogID] ASC) ON [PRIMARY]
) ON [PRIMARY];


GO
PRINT N'Creating [dbo].[CategoryLog].[IX_CategoryLog]...';


GO
CREATE NONCLUSTERED INDEX [IX_CategoryLog]
    ON [dbo].[CategoryLog]([LogID] ASC, [CategoryID] ASC);


GO
PRINT N'Creating [dbo].[Log]...';


GO
CREATE TABLE [dbo].[Log] (
    [LogID]       BIGINT         IDENTITY (1, 1) NOT NULL,
    [EventID]     INT            NULL,
    [Priority]    INT            NOT NULL,
    [Severity]    NVARCHAR (32)  NOT NULL,
    [Title]       NVARCHAR (256) NOT NULL,
    [Timestamp]   DATETIME       NOT NULL,
    [MachineName] NVARCHAR (32)  NOT NULL,
    [Message]     NVARCHAR (MAX) NULL,
    [JobId]       NVARCHAR (128) NULL,
    [Module]      NVARCHAR (256) NULL,
    CONSTRAINT [PK_Log] PRIMARY KEY CLUSTERED ([LogID] ASC) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY];


GO
PRINT N'Creating [dbo].[FK_CategoryLog_Category]...';


GO
ALTER TABLE [dbo].[CategoryLog] WITH NOCHECK
    ADD CONSTRAINT [FK_CategoryLog_Category] FOREIGN KEY ([CategoryID]) REFERENCES [dbo].[Category] ([CategoryID]);


GO
PRINT N'Creating [dbo].[FK_CategoryLog_Log]...';


GO
ALTER TABLE [dbo].[CategoryLog] WITH NOCHECK
    ADD CONSTRAINT [FK_CategoryLog_Log] FOREIGN KEY ([LogID]) REFERENCES [dbo].[Log] ([LogID]);


GO
PRINT N'Creating [dbo].[ClearLogs]...';


GO
CREATE PROCEDURE ClearLogs
AS
BEGIN
    SET NOCOUNT ON;

    DELETE FROM CategoryLog
    DELETE FROM [Log]
    DELETE FROM Category
END
GO
PRINT N'Creating [dbo].[InsertCategoryLog]...';


GO
CREATE PROCEDURE InsertCategoryLog
    @CategoryID INT,
    @LogID BIGINT,
    @CatLogID BIGINT OUTPUT
AS
BEGIN
    SET NOCOUNT ON;
    
    SELECT @CatLogID FROM CategoryLog WHERE CategoryID=@CategoryID and LogID = @LogID
    IF @CatLogID IS NULL
    BEGIN
        INSERT INTO CategoryLog (CategoryID, LogID) VALUES(@CategoryID, @LogID)
        SET @CatLogID = @@IDENTITY
    END
    RETURN 0
END
GO
PRINT N'Creating [dbo].[WriteLog]...';


GO
CREATE PROCEDURE [dbo].[WriteLog]
(
    @EventID INT, 
    @Priority INT, 
    @Severity NVARCHAR(32), 
    @Title NVARCHAR(256), 
    @Timestamp DATETIME,
    @MachineName NVARCHAR(32), 
    @Message NVARCHAR(MAX),
    @JobId NVARCHAR(128),	
    @Module NVARCHAR(256),
    @LogId BIGINT OUTPUT
    )
AS 
BEGIN
    INSERT INTO [Log] (
        EventID,
        [Priority],
        Severity,
        Title,
        [Timestamp],
        MachineName,
        [Message],
        JobId,
        Module
    )
    VALUES (
        @EventID, 
        @Priority, 
        @Severity, 
        @Title, 
        @Timestamp,
        @MachineName,
        @Message,
        @JobId,
        @Module)

    SET @LogID = @@IDENTITY
    
    RETURN 0

END
GO
PRINT N'Creating [dbo].[AddCategory]...';


GO

CREATE PROCEDURE [dbo].[AddCategory]
    @CategoryName NVARCHAR(64),
    @LogID BIGINT
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @CatID INT
    SELECT @CatID = CategoryID FROM Category WHERE CategoryName = @CategoryName
    IF @CatID IS NULL
    BEGIN
        INSERT INTO Category (CategoryName) VALUES(@CategoryName)
        SET @CatID = @@IDENTITY
    END

    DECLARE @CatLogId BIGINT

    EXEC InsertCategoryLog @CatID, @LogID, @CatLogId

    IF @CatLogId <> NULL AND @CatLogId > 0
        RETURN 0
    ELSE
        RETURN 1 
END
GO
PRINT N'Checking existing data against newly created constraints';


GO
USE [$(DatabaseName)];


GO
ALTER TABLE [dbo].[CategoryLog] WITH CHECK CHECK CONSTRAINT [FK_CategoryLog_Category];

ALTER TABLE [dbo].[CategoryLog] WITH CHECK CHECK CONSTRAINT [FK_CategoryLog_Log];


GO
PRINT N'Update complete.';


GO
